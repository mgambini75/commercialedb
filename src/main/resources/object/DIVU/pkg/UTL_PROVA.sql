--liquibase formatted sql
--changeset ccarretti:creazione_iniziale logicalFilePath:UTL_PROVA.sql runOnChange:true failOnError:true

create or replace PACKAGE UTL_PROVA IS
/******************************************************************************
   NAME:       UTL_PROVA
   PURPOSE:
   DEPENDENCY: DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0.2    20161222    A. Acinapura     Added CHECK_ABIL in RICERCA_FASE: 
                                           skip working if client is not enabled.
   1.0.0.1    20161222    A. Acinapura     Added IS_CLIENT_ABIL function.
   1.0.0.0    -			  -			 	   Porting this package.
******************************************************************************/

  C_VERSION VARCHAR2(100):='V:1.0.0.0 LASTUPDATE:22/12/2016';

  FUNCTION GET_VERSION RETURN VARCHAR2;
 END UTL_PROVA;
/

create or replace PACKAGE BODY UTL_PROVA IS

  FUNCTION GET_VERSION RETURN VARCHAR2 IS
  BEGIN
    RETURN C_VERSION;
  END;

 
END UTL_PROVA;
/