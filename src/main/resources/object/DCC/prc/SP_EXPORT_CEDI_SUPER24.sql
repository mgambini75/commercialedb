create or replace PROCEDURE sp_export_cedi_super24(p_load_id IN NUMBER) AS
    
    C_DIRECTORY_PATH               CONSTANT VARCHAR2(50) := FN_GET_DEFVAL('BRC_DCC','SUPER24','DIRECTORY_NAME');
    C_NOME_MODULO                  CONSTANT VARCHAR2(30) := 'SP_EXPORT_CEDI_SUPER24';
  
    G_LOGGER                       TYP_LOGGER_NEW        := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);
  
    l_tmp_file_name                VARCHAR2(30);
    l_csv_file_name                VARCHAR2(30);
    l_file 			                   UTL_FILE.FILE_TYPE;
    
    l_rec_intest                   VARCHAR2(1024)        := '"ean";"gdo_id";"brand";"name";"type";"selling_type";"selling_type_display";"selling_value";"price";"price_promo";"url_thumbnail";"url_image";"url_ingredients";"url_nutritional_values";"variable_weight";"stock";"area";"area_desc";"rep";"rep_desc";"gru";"gru_desc";"sett";"sett_desc";"fam";"fam_desc";"ricvend";"ricvend_desc";' || CHR(13);
    l_rec                          VARCHAR2(4096);
    l_rec_cont                     NUMBER                := 0;
    
    l_dati_quantita_netta          VARCHAR2(20);
    l_dati_prezzo_vendita          VARCHAR2(20);
    l_dati_prezzo_vendita_off      VARCHAR2(20);
    l_dati_variable_weight         VARCHAR2(20);

BEGIN

 G_LOGGER.log_info('SP_EXPORT_CEDI_SUPER24 iniziata', 'SP_EXPORT_CEDI_SUPER24', p_load_id);
      
  FOR cedi IN (SELECT DISTINCT codice_pdd FROM SUPER24_ASSORTIMENTO)
  LOOP
    l_tmp_file_name := 'supermercato24_' || cedi.codice_pdd || '.tmp';
    l_rec_cont      := 0;
      
      BEGIN
        l_file  := UTL_FILE.FOPEN(C_DIRECTORY_PATH, l_tmp_file_name, 'A'); 
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_rec_intest, 'AL32UTF8'));
          
          FOR dati IN (SELECT * FROM SUPER24_ASSORTIMENTO WHERE cedi.codice_pdd = codice_pdd AND load_id = p_load_id)
          LOOP
           SELECT DECODE(dati.peso_variabile,1,'TRUE','FALSE') INTO l_dati_variable_weight FROM DUAL;
           SELECT REPLACE ((CASE WHEN SUBSTR(dati.prezzo_vendita, 1,1) = ',' THEN '0' || dati.prezzo_vendita ELSE TO_CHAR(dati.prezzo_vendita) END), ',' , '.') INTO l_dati_prezzo_vendita FROM DUAL;
           SELECT REPLACE ((CASE WHEN SUBSTR(dati.prezzo_vendita_off, 1,1) = ',' THEN '0' || dati.prezzo_vendita_off ELSE TO_CHAR(dati.prezzo_vendita_off) END), ',' , '.') INTO l_dati_prezzo_vendita_off FROM DUAL;
           SELECT REPLACE ((CASE WHEN SUBSTR(dati.quantita_netta, 1,1) = ',' THEN '0' || dati.quantita_netta ELSE TO_CHAR(dati.quantita_netta) END), ',' , '.') INTO l_dati_quantita_netta FROM DUAL;
           l_rec       := '"' || dati.barcode || '";"' || dati.cod_art_interno || '";"' || dati.marca || '";"' 
                              || dati.descr_articolo || '";"' || dati.unita_mis_gestionale || '";"' 
                              || dati.unita_mis_fiscale || '";"' || NULL  || '";"' || l_dati_quantita_netta || '";"' 
                              || l_dati_prezzo_vendita || '";"' || l_dati_prezzo_vendita_off || '";"' || NULL || '";"' || NULL || '";"' || NULL || '";"' 
                              || NULL || '";"' || l_dati_variable_weight || '";"'
                              || dati.giacenza || '";"' || NULL || '";"' || NULL || '";"' || dati.val_merc_1 || '";"' ||  dati.descr_merc_1 || '";"' 
                              || dati.val_merc_2 || '";"' || dati.descr_merc_2 || '";"' || dati.val_merc_3 || '";"' || dati.descr_merc_3 || '";"' || dati.val_merc_4 || '";"' 
                              || dati.descr_merc_4 || '";"' || NULL || '";"' || NULL || '";' || CHR(13);
           UTL_FILE.PUT_LINE(l_file, CONVERT(l_rec, 'AL32UTF8'));
           l_rec_cont  := l_rec_cont + 1;
          END LOOP;
        
        l_csv_file_name := 'supermercato24_' || cedi.codice_pdd || '.csv';
        
        UTL_FILE.FCLOSE(l_file);
        -- se file gi� esistente, va in eccezione..ridenominazione err
        UTL_FILE.FRENAME(src_location => C_DIRECTORY_PATH, src_filename => l_tmp_file_name, dest_location => C_DIRECTORY_PATH, dest_filename => l_csv_file_name, overwrite => FALSE);
      
        G_LOGGER.log_info('rinominato da TMP in CSV', 'SP_EXPORT_CEDI_SUPER24', p_load_id, 'l_tmp_file_name:=' || l_tmp_file_name, 'l_csv_file_name:=' || l_csv_file_name, 'l_rec_cont:=' || l_rec_cont);
      
      EXCEPTION
      WHEN OTHERS THEN
          G_LOGGER.log_error('eccezione, rimosso file tmp:=' || l_tmp_file_name, 'SP_EXPORT_CEDI_SUPER24', p_load_id, 'l_tmp_file_name:=' || l_tmp_file_name, '-20080');
          UTL_FILE.FCLOSE(l_file);
          UTL_FILE.FREMOVE(location => C_DIRECTORY_PATH, filename => l_tmp_file_name);
          DITECH_MONITOR.SEND_MAIL(
            v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', 
            v_receiver => 'luca.pancaldi@ditechspa.it,diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it',
            v_subject => '[ERR] FLUSSO ESTRAZIONE SUPERMERCATO24 ',
            v_message => 'Eccezione file tmp:=' || l_tmp_file_name || ' -- LOAD_ID:' || p_load_id);
      END;
       
   END LOOP;
   
 G_LOGGER.log_info('SP_EXPORT_CEDI_SUPER24 finita', 'SP_EXPORT_CEDI_SUPER24', p_load_id);
 
END sp_export_cedi_super24;
/