create or replace PROCEDURE SP_SET_MODXLOAXTAB(P_MODULO IN VARCHAR2, P_TABLE_NAME IN VARCHAR2, O_LOAD_ID OUT NUMBER)
AS
  PRAGMA AUTONOMOUS_TRANSACTION;
  l_load_id               NUMBER;
  l_count                 NUMBER;
  C_NOME_MODULO           CONSTANT VARCHAR2(30) := 'SP_SET_MODXLOAXTAB';
  G_LOGGER                TYP_LOGGER_NEW        := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);
BEGIN
    
    G_LOGGER.log_info('SP_SET_MODXLOAXTAB iniziata','SP_SET_MODXLOAXTAB');
  
    SELECT COUNT(*) INTO l_count
    FROM   DB_MODXLOAXTAB
    WHERE  MODULO_ID        = P_MODULO
    AND    TABLE_NAME       = P_TABLE_NAME
    AND    MODULO_TYPE      = 'P'
    AND    WORK_IN_PROGRESS = 'N';
    
    IF (l_count = 1)
    THEN
        SELECT LOADID INTO l_load_id
        FROM   DB_MODXLOAXTAB
        WHERE  MODULO_ID        = P_MODULO
        AND    TABLE_NAME       = P_TABLE_NAME
        AND    MODULO_TYPE      = 'P'
        AND    WORK_IN_PROGRESS = 'N';
      
        l_load_id := l_load_id + 1;
      
        UPDATE DB_MODXLOAXTAB
        SET    LOADID = l_load_id,
               WORK_IN_PROGRESS = 'S'
        WHERE  MODULO_ID        = P_MODULO
        AND    TABLE_NAME       = P_TABLE_NAME
        AND    MODULO_TYPE      = 'P'
        AND    WORK_IN_PROGRESS = 'N';
        
        O_LOAD_ID := l_load_id;
        
        COMMIT;
        
        G_LOGGER.log_info('Eseguita COMMIT','SP_SET_MODXLOAXTAB');
        
    ELSE
        G_LOGGER.log_error('UPDATE nuovo load_id:='  || O_LOAD_ID || ' DB_MODXLOAXTAB non effettuato ','SP_SET_MODXLOAXTAB', NULL, 'p_modulo:=' || P_MODULO, '-20015', 'p_table_name:=' || P_TABLE_NAME);
        O_LOAD_ID := NULL;
        raise_application_error(-20015, 'p_modulo:=' || P_MODULO || ' - p_table_name:=' || P_TABLE_NAME);
    END IF;
    
        G_LOGGER.log_info('SP_SET_MODXLOAXTAB finita','SP_SET_MODXLOAXTAB');

END SP_SET_MODXLOAXTAB;
/