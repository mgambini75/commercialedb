create or replace PROCEDURE SP_RELEASE_MODXLOAXTAB(P_MODULO IN VARCHAR2, P_TABLE_NAME IN VARCHAR2)
AS
  PRAGMA AUTONOMOUS_TRANSACTION;
  l_load_id               NUMBER;
  l_count                 NUMBER;
  C_NOME_MODULO           CONSTANT VARCHAR2(30) := 'SP_RELEASE_MODXLOAXTAB';
  G_LOGGER                TYP_LOGGER_NEW        := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);
BEGIN
    
    G_LOGGER.log_info('SP_RELEASE_MODXLOAXTAB iniziata','SP_RELEASE_MODXLOAXTAB');
  
    SELECT COUNT(*) INTO l_count
    FROM   DB_MODXLOAXTAB
    WHERE  MODULO_ID        = P_MODULO
    AND    TABLE_NAME       = P_TABLE_NAME
    AND    MODULO_TYPE      = 'P'
    AND    WORK_IN_PROGRESS = 'S';
    
    IF (l_count = 1)
    THEN
        UPDATE DB_MODXLOAXTAB
        SET    WORK_IN_PROGRESS = 'N'
        WHERE  MODULO_ID        = P_MODULO
        AND    TABLE_NAME       = P_TABLE_NAME
        AND    MODULO_TYPE      = 'P'
        AND    WORK_IN_PROGRESS = 'S';
        
        COMMIT;
        G_LOGGER.log_info('Eseguita COMMIT','SP_RELEASE_MODXLOAXTAB');
        
    ELSE
        G_LOGGER.log_error('UPDATE DB_MODXLOAXTAB a ''N'' non effettuato ','SP_RELEASE_MODXLOAXTAB', NULL, 'p_modulo:=' || P_MODULO, '-20015', 'p_table_name:=' || P_TABLE_NAME);
        raise_application_error(-20015, 'p_modulo:=' || P_MODULO || ' - p_table_name:=' || P_TABLE_NAME);
    END IF;
    
        G_LOGGER.log_info('SP_RELEASE_MODXLOAXTAB finita','SP_RELEASE_MODXLOAXTAB');

END SP_RELEASE_MODXLOAXTAB;
/