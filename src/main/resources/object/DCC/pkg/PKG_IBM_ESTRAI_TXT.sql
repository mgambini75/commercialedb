create or replace PACKAGE PKG_IBM_ESTRAI_TXT AS
  FUNCTION  FN_GET_ABILITATO_PRICING_IBM(p_codice IN VARCHAR2, p_nome_tabella IN VARCHAR2, p_frequenza IN CHAR) RETURN CHAR;
  PROCEDURE SP_ART_X_PDD;
  PROCEDURE SP_ALBERO_MERC;
  PROCEDURE SP_MERCEOLOGIA;
  PROCEDURE SP_PIAZZE_PREZZI;
  PROCEDURE SP_RIL_CONC;
  PROCEDURE SP_LOCATION_DATA;
  PROCEDURE SP_PREZZI_PUBBLICO;
  PROCEDURE SP_ARTICOLI;
  PROCEDURE SP_VAT_DIVISION;
  PROCEDURE SP_COSTI;
  PROCEDURE SP_MAIN(p_frequenza IN CHAR);
END PKG_IBM_ESTRAI_TXT;
/