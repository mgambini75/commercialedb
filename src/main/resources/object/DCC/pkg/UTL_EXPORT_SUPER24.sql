create or replace package         UTL_EXPORT_SUPER24 IS

  /******************************************************************************
    NAME:       UTL_EXPORT_SUPER24
    PURPOSE:
  
    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.0.0.0    09/03/2017  Luca Pancaldi      Created this package.
  ******************************************************************************/


  PROCEDURE SP_GET_NEXT_ID(i_modulo  IN VARCHAR2,
                           i_tabella IN VARCHAR2,
                           o_loadId  OUT NUMBER);

  FUNCTION FN_GET_LAST_ID(i_modulo IN VARCHAR2, i_tabella IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE EXPORT_SUPER24_MAIN;

END;
/