create or replace PACKAGE BODY PKG_SEND_LISTA_PREPARAZIONI
AS
  /******************************************************************************
  NAME:       PKG_SEND_LISTA_PREPARAZIONI
  PURPOSE:
  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0.0.0    24/01/2017  Franco Pitzolu   Created this package.
  ******************************************************************************/
  
  C_NOME_MODULO CONSTANT VARCHAR2(30) := 'PKG_SEND_LISTA_PREPARAZIONE';
  G_LOGGER TYP_LOGGER_NEW  := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

PROCEDURE SP_CARICAMENTO(
    LoaId IN NUMBER)
IS
  d NUMBER:=0;
  t NUMBER:=0;
  v_riga_d LISTA_PREPARAZIONE_D%ROWTYPE;
  v_riga_t LISTA_PREPARAZIONE_T%ROWTYPE;
  
  v_anno_ordine   NUMBER(4); -- lISTA_PREPARAZIONE_T.D_DATA_ORDINE
  v_mese_ordine   NUMBER(2); -- lISTA_PREPARAZIONE_T.D_DATA_ORDINE
  v_giorno_ordine NUMBER(2); -- lISTA_PREPARAZIONE_T.D_DATA_ORDINE
  
  -- contengono sysdate in anno, mese, giorno e ora per LISTA_D e LISTA_T
  anno_pubblicazione_DCC   NUMBER(4):= to_number(TO_CHAR(sysdate, 'YYYY')); -- sysdate
  mese_pubblicazione_DCC   NUMBER(2):= to_number(TO_CHAR(sysdate, 'MM'));
  giorno_pubblicazione_DCC NUMBER(2):= to_number(TO_CHAR(sysdate, 'DD'));
  ora_pubblicazione_DCC    NUMBER(4):= to_number(TO_CHAR(sysdate, 'HH24MI'));
  
  -- VARIABILI DA LISTA_PREPARAZIONE_T PER CURSORE LISTA_D --
  v_magazzino_codice            VARCHAR2(50);
  v_codice                      VARCHAR2(50);
  v_prefisso                    VARCHAR2(50);
  v_cliente_codice              VARCHAR2(50);
  v_destinazione_codice         VARCHAR2(50);
  v_progressivo                 VARCHAR2(50);
  v_documento_gestionale_codice VARCHAR2(50);  
  v_data_ordine                 VARCHAR2(50);
  
  -- VARIABIILI PER TESTATA
  anno_ricezione_sistema   NUMBER(4); -- data_ricezione nel sistema di sede dell�ordine trasmesso dal cliente: anno 
  mese_ricezione_sistema   NUMBER(2); -- data ricezione nel sistema di sede dell�ordine trasmesso dal cliente: mese
  giorno_ricezione_sistema NUMBER(2); -- data ricezione nel sistema di sede dell�ordine trasmesso dal cliente: giorno
  ora_ricezione_sistema    NUMBER(4); -- data ricezione nel sistema di sede dell�ordine trasmesso dal cliente: ora
  
  CURSOR LISTA_D IS
    SELECT LD.* 
    FROM LISTA_PREPARAZIONE_D LD 
    WHERE LD.LOAD_ID = LoaId;
  
  CURSOR LISTA_T IS
    SELECT LT.* 
    FROM LISTA_PREPARAZIONE_T LT 
    WHERE LT.LOAD_ID = LoaId;
    
BEGIN
  
  G_LOGGER.log_info('Esecuzione procedura SP_CARICAMENTO...');
  
  BEGIN -- POPOLAMENTO DELLA TABELLA FDR7RROL ( RIGHE )
    G_LOGGER.log_info('Caricamento dati da LISTA_PREPARAZIONE_D a FDR7RROL ( RIGHE )');
    OPEN LISTA_D;
    LOOP
      FETCH LISTA_D INTO v_riga_d;
      EXIT
    WHEN LISTA_D%NOTFOUND;
      SELECT PT.D_MAGAZZINO_CODICE,
        PT.D_CODICE,
        PT.D_DATA_ORDINE,
        TRIM(PT.D_PREFISSO),
        PT.D_CLIENTE_CODICE,
        PT.D_DESTINAZIONE_CODICE,
        PT.D_PROGRESSIVO,
        PT.D_DOCUMENTO_GESTIONALE_CODICE
      INTO v_magazzino_codice,
        v_codice,
        v_data_ordine,
        v_prefisso,
        v_cliente_codice,
        v_destinazione_codice,
        v_progressivo,
        v_documento_gestionale_codice
      FROM LISTA_PREPARAZIONE_T PT
      WHERE PT.D_CODICE    = v_riga_d.D_CODICE
      AND PT.LOAD_ID       = v_riga_d.LOAD_ID
      AND PT.D_PREFISSO    = v_riga_d.D_PREFISSO
      AND PT.D_PROGRESSIVO = v_riga_d.D_PROGRESSIVO;
      
      --   data_ordine := to_date(LT.D_DATA_ORDINE,'YYYY');
      v_anno_ordine   := to_number(extract(YEAR FROM to_date(v_data_ordine, 'DD-MM-YY HH24:mi:ss')));  
      v_mese_ordine   := to_number(extract(MONTH FROM to_date(v_data_ordine, 'DD-MM-YY HH24:mi:ss')));  
      v_giorno_ordine := to_number(extract(DAY FROM to_date(v_data_ordine, 'DD-MM-YY HH24:mi:ss')));
      
     INSERT
INTO FDR7RROL
  (
    DR7CDE,      -- D_MAGAZZINO_CODICE
    DR7NRA,      -- D_CODICE
    DR7ASP,      -- D_DATA_ORDINE (ANNO)
    DR7MSP,      -- D_DATA_ORDINE (MESE)
    DR7GSP,      -- D_DATA_ORDINE (GIORNO)
    DR7CAU,      -- D_PREFISSO
    DR7CD1,      -- D_CLIENTE_CODICE
    DR7CLI,      -- D_DESTINAZIONE_CODICE
    DR7NRO,      -- D_PROGRESSIVO
    DR7RIG,      -- D_NUMERO_RIGA
    DR7CAR,      -- 'RINE'
    DR7CDA,      -- D_ARTICOLO_CODICE
    DR7CSP,      -- D_QUANTITA_IC
    DR7PSP,      -- decode(d_tipo_pesatura_codice) ...
    -- DR7CES,   fix me
    -- DR7PRZ,   fix me
    -- DR7SFR,   fix me
    DR7PRF,      -- if D_TIPO_PESATURA_CODICE = 1, 1, else 2
    -- DR7IPR,   fix me
    -- DR7LIS,   fix me
    -- DR7OMG,   fix me
    DR7TIP,      -- if D_DOCUMENTO_GESTIONALE_CODICE = 'FAC' , 1, else 0
    -- DR7CDC,   fix me
    -- DR7MD1,   fix me
    -- DR7VL1,   fix me
    DR7AGE,      -- D_AGENTE
    -- DR7PAG,   fix me
    -- DR7VOU,   fix me
    DR7UMU,      -- D_UNITA_MISURA_UTIF
    -- DR7GRU,   fix me
    -- DR7OFIM,  fix me
    -- DR7OFDM,  fix me
    -- DR7CSL,   fix me
    DR7TPA,      -- if D_TIPO_PESATURA_CODICE = 1, else 2
    DR7PEZ,      -- D_IMBALLO_CESSIONE
    DR7STA,      -- D_STATO_EVADIBILITA_CODICE
    DR7IDM,      -- LOAD_ID
    DR7MAA,      -- anno_pubblicazione_DCC
    DR7MAM,      -- mese_pubblicazione_DCC
    DR7MAG,      -- giorno_pubblicazione_DCC
    DR7MAO       -- ora_pubblicazione_DCC
  )
        VALUES
        (
          v_magazzino_codice,
          v_codice,
          v_anno_ordine,
          v_mese_ordine,
          v_giorno_ordine,
          v_prefisso,
          'B130',
          v_destinazione_codice,
          v_progressivo,
          v_riga_d.D_NUMERO_RIGA,
          'RINE',
          v_riga_d.D_ARTICOLO_CODICE,
          v_riga_d.D_QUANTITA_IC,
          DECODE(v_riga_d.d_tipo_pesatura_codice, 1, v_riga_d.D_QUANTITA_PZ, v_riga_d.D_QUANTITA_PZ * v_riga_d.d_peso_medio_pezzo),
          --  v_riga_d.FIXME, --> DR7CES
          --  v_riga_d.FIXME, --> DR7PRZ
          --  v_riga_d.FIXME, --> DR7SFR
          DECODE(v_riga_d.D_TIPO_PESATURA_CODICE,1,1, 2), -- DR7PRF
          --  v_riga_d.FIXME, --> DR7IPR
          --  v_riga_d.FIXME, --> DR7LIS
          --  v_riga_d.FIXME, --> DR7OMG
          DECODE(v_documento_gestionale_codice, 'FAC', 1,0), -- 0 = DDT , 1 = Fattura immediata
          --  v_riga_d.FIXME, --> DR7CDC
          --  v_riga_d.FIXME, --> DR7MD1
          --  v_riga_d.FIXME, --> DR7VL1
          v_riga_d.D_AGENTE,  --> DR7AGE
          --  v_riga_d.FIXME, --> DR7PAG
          --  v_riga_d.FIXME, --> DR7VOU
          v_riga_d.D_UNITA_MISURA_UTIF,  -- DR7UMU
          --  v_riga_d.FIXME, --> DR7GRU
          --  v_riga_d.FIXME, --> DR7OFIM
          --  v_riga_d.FIXME, --> DR7OFDM
          --  v_riga_d.FIXME, --> DR7CSL
          DECODE(v_riga_d.D_TIPO_PESATURA_CODICE,1,1, 2),
          v_riga_d.D_IMBALLO_CESSIONE,
          DECODE(v_riga_d.d_stato_evadibilita_codice,'EP',1, 'EC',1,  0),
          v_riga_d.LOAD_ID,
          anno_pubblicazione_DCC,
          mese_pubblicazione_DCC,
          giorno_pubblicazione_DCC,
          ora_pubblicazione_DCC
        );
      --   d:=d+1;
    END LOOP;
    CLOSE LISTA_D;
    G_LOGGER.log_info('Eseguito INSERT FDR7RROL per: '|| SQL%ROWCOUNT ||' record');
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('Errore cursore LISTA_D    '||sqlerrm);
    RAISE;
  END;
  
  BEGIN -- POPOLAMENTO DELLA TABELLA FDR6TROL ( TESTATA )
    G_LOGGER.log_info('Caricamento dati da LISTA_PREPARAZIONE_T a FDR6TROL ( TESTATA )');
    OPEN LISTA_T;
    LOOP
      FETCH LISTA_T INTO v_riga_t;
      EXIT
    WHEN LISTA_T%NOTFOUND;
      v_anno_ordine            := to_number(extract(YEAR FROM to_date(v_riga_t.D_DATA_ORDINE, 'DD-MM-YY HH24:mi:ss')));
      v_mese_ordine            := to_number(extract(MONTH FROM to_date(v_riga_t.D_DATA_ORDINE, 'DD-MM-YY HH24:mi:ss')));
      v_giorno_ordine          := to_number(extract(DAY FROM to_date(v_riga_t.D_DATA_ORDINE, 'DD-MM-YY HH24:mi:ss')));
  
      anno_ricezione_sistema   := to_number(extract(YEAR FROM to_date(v_riga_t.D_DATA_ACQUISIZIONE,'YYYY/MM/DD HH24:MI')));      -- data_ricezione nel sistema di sede dell�ordine trasmesso dal cliente: anno
      mese_ricezione_sistema   := to_number(extract(MONTH FROM to_date(v_riga_t.D_DATA_ACQUISIZIONE,'YYYY/MM/DD HH24:MI')));     -- data ricezione nel sistema di sede dell�ordine trasmesso dal cliente: mese
      giorno_ricezione_sistema := to_number(extract(DAY FROM to_date(v_riga_t.D_DATA_ACQUISIZIONE,'YYYY/MM/DD HH24:MI')));       -- data ricezione nel sistema di sede dell�ordine trasmesso dal cliente: giorno
      ora_ricezione_sistema    := to_number(extract(HOUR FROM TO_TIMESTAMP(v_riga_t.D_DATA_ACQUISIZIONE,'YYYY/MM/DD HH24:MI'))); --||' '||
      
      INSERT
      INTO FDR6TROL
        (
          DR6CDE,   -- D_MAGAZZINO_CODICE
          DR6NRA,   -- D_CODICE
          DR6ASP,   -- D_DATA_ORDINE (anno)
          DR6MSP,   -- D_DATA_ORDINE (mese)
          DR6GSP,   -- D_DATA_ORDINE (giorno)
          DR6CAU,   -- D_PREFISSO
          DR6CD1,   -- 'B130'
          DR6CLI,   -- D_DESTINAZIONE_CODICE
          DR6TRO,   -- if D_TIPO_PREPARAZIONE_CODICE = 'PK', 2, else 5
          DR6NRO,   -- D_PROGRESSIVO
          --   DR6TOC,  �
          --   DR6PES,  �
          --   DR6VUT,  �
          DR6STA,   -- 'P' 
          DR6TIP,   -- if D_DOCUMENTO_GESTIONALE_CODICE = 'FAC', 1, else 0
          DR6ARC,   -- anno_ricezione_sistema
          DR6MRC,   -- mese_ricezione_sistema         
          DR6GRC,   -- giorno_ricezione_sistema
          DR6HRC,   -- ora_ricezione_sistema
          DR6IDM,   -- LOAD_ID
          DR6MAA,   -- anno_pubblicazione_DCC
          DR6MAM,   -- mese_pubblicazione_DCC
          DR6MAG,   -- giorno_pubblicazione_DCC
          DR6MAO    -- ora_pubblicazione_DCC
        )
        VALUES
        (
          v_riga_t.D_MAGAZZINO_CODICE,
          v_riga_t.D_CODICE,
          v_anno_ordine,   -- v_riga_t.D_DATA_ORDINE  �
          v_mese_ordine,   -- v_riga_t.D_DATA_ORDINE  �
          v_giorno_ordine, -- v_riga_t.D_DATA_ORDINE  �
          trim(v_riga_t.D_PREFISSO),
          'B130',
          v_riga_t.D_DESTINAZIONE_CODICE,
          DECODE (v_riga_t.D_TIPO_PREPARAZIONE_CODICE, 'PK', 2, 5),
          v_riga_t.D_PROGRESSIVO,
          -- FIXME, --> DR6TOC   �
          -- FIXME, --> DR6PES   �
          -- FIXME, --> DR6VUT   �
          'P',   -- P = valorizzo in fase di scittura su AS400
          DECODE(v_documento_gestionale_codice, 'FAC', 1,0), -- 0 = DDT , 1 = Fattura immediata
          anno_ricezione_sistema,
          mese_ricezione_sistema,
          giorno_ricezione_sistema,
          ora_ricezione_sistema,
          v_riga_t.LOAD_ID,
          anno_pubblicazione_DCC,   
          mese_pubblicazione_DCC,   
          giorno_pubblicazione_DCC, 
          ora_pubblicazione_DCC    
        );
      -- t:=t+1;
    END LOOP;
    CLOSE LISTA_T;
    G_LOGGER.log_info('Eseguito INSERT FDR6TROL per: '|| SQL%ROWCOUNT ||' record');
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('Eccezione cursore LISTA_T'||sqlerrm);
    RAISE;
  END;
  G_LOGGER.log_info('Fine esecuzione procedura SP_CARICAMENTO.');
END SP_CARICAMENTO;

PROCEDURE SP_SEND_MAIL
  (
    Oggetto   IN VARCHAR2,
    Messaggio IN VARCHAR2,
    LoaId     IN NUMBER
  )
IS
  mailToAddr VARCHAR2(128);
  v_email_mittente DB_FIELDDEFVAL.FIELD_DEFVAL%TYPE := FN_GET_DEFVAL ('BRC_DCC', 'SEND_MAIL', 'MITTENTE');
  CURSOR mailToCur
  IS
    SELECT FIELD_DEFVAL
    FROM db_fielddefval
    WHERE field_name LIKE 'ORDINI_CLIENTI_DEST_%'
    AND DB_OWNER   = 'BRC_DCC'
    AND TABLE_NAME = 'SEND_MAIL';
BEGIN
  IF FN_GET_DEFVAL('BRC_DCC','SEND_MAIL','FLAG_ORACLE_MAIL') = 'S' THEN
    OPEN mailToCur;
    LOOP
      FETCH mailToCur INTO mailToAddr;
      EXIT
    WHEN mailToCur%NOTFOUND;
      DITECH_MONITOR.SEND_MAIL(v_sender => v_email_mittente, v_receiver => mailToAddr , v_subject => Oggetto ||' LoadId--> '||LoaId, v_message => Messaggio);
    END LOOP;
    CLOSE mailToCur;
  END IF;
END;

PROCEDURE SP_MAIN(
    LoaId IN NUMBER )
IS
BEGIN
  IF LoaId IS NULL THEN
    G_LOGGER.log_error('Warning: LoadId is null');
  ELSE
    SP_CARICAMENTO(LoaId);
    COMMIT;
    --  RAISE;
  END IF;
EXCEPTION
WHEN OTHERS THEN -- SP_SEND_MAIL(Oggetto,Messaggio, Loaid);
  G_LOGGER.log_error('Eccezione SP_MAIN: ' || SQLERRM );
  ROLLBACK;
END;
END PKG_SEND_LISTA_PREPARAZIONI;