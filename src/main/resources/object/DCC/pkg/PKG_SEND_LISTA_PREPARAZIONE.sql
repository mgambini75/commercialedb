create or replace PACKAGE PKG_SEND_LISTA_PREPARAZIONI AS
------------------------------------------------------------------
 
 
  PROCEDURE SP_CARICAMENTO ( LoaId   IN  NUMBER);
  PROCEDURE SP_SEND_MAIL (Oggetto   IN VARCHAR2, Messaggio  IN VARCHAR2, LoaId    IN NUMBER);
  PROCEDURE SP_MAIN (LoaId    IN NUMBER);

  
 
------------------------------------------------------------------
END PKG_SEND_LISTA_PREPARAZIONI;