create or replace PACKAGE BODY PKG_LOAD_LISTA_PREPARAZIONE AS

C_NOME_MODULO           CONSTANT VARCHAR2(30)  := 'PKG_LOAD_LISTA_PREPARAZIONE';

G_LOGGER                TYP_LOGGER_NEW := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);
G_LOAD_ID				        NUMBER;

------------------------------------------------------------------------------------------------------------------

PROCEDURE send_mail(Subject in varchar2, Msg in varchar2)
AS
BEGIN
	UTILITY.SPEDISCI_MAIL(Oggetto => 'ORDINI CLIENTI: ' || C_NOME_MODULO || ' - ' || Subject,
						  Messaggio => Msg,
						  GruppoDestinatario => 'ORDINI_CLIENTI_DEST',
						  LoaId => G_LOAD_ID);
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE SP_CARICAMENTO
IS
BEGIN
  
  UPDATE ncl_lista_preparazione_t core_lpt SET core_lpt.flag_trasmesso = 'E' WHERE core_lpt.flag_trasmesso = 'N';
  
  G_LOGGER.log_info('Eseguito UPDATE FLAG_TRASMESSO in stato temporaneo per: '|| SQL%ROWCOUNT ||' record','SP_CARICAMENTO');
  
  INSERT INTO LISTA_PREPARAZIONE_D
    (
      d_codice,
      d_numero_riga,
      d_articolo_codice,
      d_flag_stralcio,
      d_imballo_cessione,
      d_pallet_imballi,
      d_qta_ordine,
      --d_um_ordine_codice,
      d_qta_preparata,
      --d_um_preparazione_codice,
      d_peso_prodotto,
      d_ean,
      d_dettaglio_sostitutivo_codice,
      d_barcode_tipo_codice,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      version,
      load_id,
      d_prefisso,
      d_progressivo,
      d_suffisso,
      d_tipo_pesatura_codice,
      d_peso_medio_pezzo,
      d_quantita_pz,
      d_quantita_kg,
      d_quantita_ic,
      d_quantita_if,
      d_agente,
      d_stato_evadibilita_codice,
      d_unita_misura_utif
    )
   SELECT
      core_lpt.codice      					                                AS d_codice,
      core_lpd.numero_riga                                          AS d_numriga,
      core_articolo.codice                                          AS d_articolo_codice,
      core_lpd.flag_stralcio                                        AS d_flag_stralcio,
      core_art_x_pdd.imballo_cessione                               AS d_imballo_cessione,
      core_art_x_pdd.pallet_imballi                                 AS d_pallet_imballi,
      core_lpd.qta_ordine                                           AS d_qta_ordine,
      --core_unita_misura.codice                                    AS d_um_ordine_codice,
      core_lpd.qta_preparata                                        AS d_qta_preparata,
      --core_unita_misura.codice                                    AS d_um_preparazione_codice, --FIX ME
      core_lpd.peso_prodotto                                        AS d_peso_prodotto,
      core_lpd.ean                                                  AS d_ean,
      NULL                                                          AS d_dettaglio_sostitutivo_codice,
      core_barc_tipo.codice                                         AS d_barcode_tipo_codice,
      SYSDATE                                                       AS ins_data,
      NULL                                                          AS ins_utente,
      NULL                                                          AS ins_modulo,
      SYSDATE                                                       AS upd_data,
      NULL                                                          AS upd_utente,
      NULL                                                          AS upd_modulo,
      SYSDATE                                                       AS version,
      G_LOAD_ID                                                     AS load_id,
      core_lpd.prefisso                                             AS d_prefisso,
      core_lpd.progressivo                                          AS d_progressivo,
      core_lpd.suffisso                                             AS d_suffisso,
      core_tipo_pst_ces.codice                                      AS d_tipo_pesatura_codice,
      core_art_x_pdd.peso_medio_pezzo                               AS d_peso_medio_pezzo,
      core_d_x_umi_pz.quantita_da_preparare                         AS d_quantita_pz,
      core_d_x_umi_kg.quantita_da_preparare                         AS d_quantita_kg,
      core_d_x_umi_ic.quantita_da_preparare                         AS d_quantita_ic,
      core_d_x_umi_if.quantita_da_preparare                         AS d_quantita_if,
      core_interloc.codice                                          AS d_agente,
      core_stato_evad.codice                                        AS d_stato_evadibilita_codice,
      CDC.GET_ATTR_ANAG_A('UMUTI', core_lpd.articolo_id, 'CODICE')  AS d_unita_misura_utif
  FROM ncl_lista_preparazione_d core_lpd
  JOIN ncl_lista_preparazione_t core_lpt ON (core_lpt.lista_preparazione_t_id = core_lpd.lista_preparazione_t_id)
  JOIN ncl_articolo core_articolo ON (core_lpd.articolo_id = core_articolo.articolo_id)
  JOIN ncl_articolo_x_divisione core_art_x_div ON (core_art_x_div.articolo_id = core_lpd.articolo_id)
  JOIN ncl_articolo_x_pdd core_art_x_pdd ON (core_art_x_pdd.articolo_x_divisione_id = core_art_x_div.articolo_x_divisione_id AND core_art_x_pdd.pdd_id = core_lpt.magazzino_id)
  JOIN ncl_interlocutore core_interloc ON (core_interloc.interlocutore_id = core_lpt.cliente_id)
  JOIN ncl_lista_preparazione_d_x_umi core_d_x_umi_pz ON (core_d_x_umi_pz.lista_preparazione_d_id = core_lpd.lista_preparazione_d_id AND core_d_x_umi_pz.unita_misura_id = 1)
  JOIN ncl_lista_preparazione_d_x_umi core_d_x_umi_kg ON (core_d_x_umi_kg.lista_preparazione_d_id = core_lpd.lista_preparazione_d_id AND core_d_x_umi_kg.unita_misura_id = 2)
  JOIN ncl_lista_preparazione_d_x_umi core_d_x_umi_ic ON (core_d_x_umi_ic.lista_preparazione_d_id = core_lpd.lista_preparazione_d_id AND core_d_x_umi_ic.unita_misura_id = 3)
  JOIN ncl_lista_preparazione_d_x_umi core_d_x_umi_if ON (core_d_x_umi_if.lista_preparazione_d_id = core_lpd.lista_preparazione_d_id AND core_d_x_umi_if.unita_misura_id = 4)
  JOIN ncl_unita_misura core_unita_misura_pz ON (core_unita_misura_pz.unita_misura_id = core_d_x_umi_pz.unita_misura_id)
  JOIN ncl_unita_misura core_unita_misura_kg ON (core_unita_misura_kg.unita_misura_id = core_d_x_umi_kg.unita_misura_id)
  JOIN ncl_unita_misura core_unita_misura_ic ON (core_unita_misura_ic.unita_misura_id = core_d_x_umi_ic.unita_misura_id)
  JOIN ncl_unita_misura core_unita_misura_if ON (core_unita_misura_if.unita_misura_id = core_d_x_umi_if.unita_misura_id)
  LEFT JOIN ncl_barcode_tipo core_barc_tipo ON (core_barc_tipo.barcode_tipo_id = core_lpd.barcode_tipo_id) 
  LEFT JOIN ncl_tipo_pesatura_cessione core_tipo_pst_ces ON (core_tipo_pst_ces.tipo_pesatura_cessione_id = core_art_x_pdd.tipo_pesatura_cessione_id)
  LEFT JOIN ncl_stato_evadibilita core_stato_evad ON (core_stato_evad.stato_evadibilita_id = core_lpd.stato_evadibilita_id)
  WHERE core_lpt.flag_trasmesso = 'E';
  
  G_LOGGER.log_info('Eseguito INSERT LISTA_PREPARAZIONE_D per: '|| SQL%ROWCOUNT ||' record','SP_CARICAMENTO');

  INSERT INTO LISTA_PREPARAZIONE_T
    (
      d_codice,
      d_cliente_codice,
      d_destinazione_codice,
      d_documento_gestionale_codice,
      d_prefisso,
      d_progressivo,
      d_suffisso,
      d_data_ordine,
      d_data_acquisizione,
      d_data_inizio_preparazione,
      d_magazzino_codice,
      d_tipo_consegna_codice,
      d_preparatore_codice,
      d_stato_preparazione_codice,
      d_annullato,
      d_priorita,
      d_tipo_preparazione_codice,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      version,
      load_id
    )
  SELECT
      core_lpt.codice      					        AS d_codice,
      core_interloc.codice                  AS d_cliente_codice,
      core_dest.codice                      AS d_destinazione_codice,
      core_gest.codice                      AS d_documento_gestionale_codice,
      core_lpt.prefisso                     AS d_prefisso,
      core_lpt.progressivo                  AS d_progressivo,
      core_lpt.suffisso                     AS d_suffisso,
      core_lpt.data_ordine                  AS d_data_ordine,
      core_lpt.data_acquisizione            AS d_data_acquisizione,
      core_lpt.data_inizio_preparazione     AS d_data_inizio_preparazione,
      core_pdd.codice                       AS d_magazzino_codice,
      core_consegna.codice                  AS d_tipo_consegna_codice,
      NULL                                  AS d_preparatore_codice, -- FIXME
      core_stato_prep.codice                AS d_stato_preparazione_codice,
      core_lpt.annullato                    AS d_annullato,
      core_lpt.priorita                     AS d_priorita,
      logi_tipo_prep.codice                 AS d_tipo_preparazione_codice,   
      SYSDATE                               AS ins_data,
      NULL                                  AS ins_utente,
      NULL                                  AS ins_modulo,
      SYSDATE                               AS upd_data,
      NULL                                  AS upd_utente,
      NULL                                  AS upd_modulo,
      SYSDATE                               AS version,
      G_LOAD_ID						                  AS load_id
  FROM ncl_lista_preparazione_t core_lpt
  JOIN ncl_cliente core_cliente ON (core_lpt.cliente_id = core_cliente.interlocutore_id)
  JOIN ncl_interlocutore core_interloc ON (core_cliente.interlocutore_id = core_interloc.interlocutore_id)
  JOIN ncl_destinazione core_dest ON (core_dest.destinazione_id = core_lpt.destinazione_id)
  JOIN ncl_documento_gestionale core_gest ON (core_gest.documento_gestionale_id = core_lpt.documento_gestionale_id)
  JOIN ncl_pdd core_pdd ON (core_pdd.pdd_id = core_lpt.magazzino_id)
  JOIN ncl_tipo_consegna core_consegna ON (core_consegna.tipo_consegna_id = core_lpt.tipo_consegna_id)
  --JOIN ncl_figura_aziendale core_figura_az ON (core_figura_az.figura_aziendale_id = core_lpt.preparatore_id)
  JOIN ncl_stato_preparazione core_stato_prep ON (core_stato_prep.stato_preparazione_id = core_lpt.stato_preparazione_id)
  JOIN ncl_tipo_preparazione logi_tipo_prep ON (logi_tipo_prep.tipo_preparazione_id = core_lpt.tipo_preparazione_id)
  WHERE core_lpt.flag_trasmesso = 'E';
  
  G_LOGGER.log_info('Eseguito INSERT LISTA_PREPARAZIONE_T per: '|| SQL%ROWCOUNT ||' record');

  UPDATE ncl_LISTA_PREPARAZIONE_T core_lpt SET core_lpt.flag_trasmesso = 'S' WHERE core_lpt.flag_trasmesso = 'E';
  
  G_LOGGER.log_info('Eseguito UPDATE FLAG_TRASMESSO in stato elaborato per: '|| SQL%ROWCOUNT ||' record','SP_CARICAMENTO');
  
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('Eccezione SP_CARICAMENTO: ' || SQLERRM);
  RAISE;
  
END SP_CARICAMENTO;

------------------------------------------------------------------------------------------------------------------

PROCEDURE SP_MAIN
IS
l_msg 			VARCHAR2(255);
BEGIN
  G_LOAD_ID := sq_load_id_lista_prep.nextval;
  SP_CARICAMENTO();
  COMMIT;
  G_LOGGER.log_info('Eseguita COMMIT');
  EXCEPTION
  WHEN OTHERS THEN
    l_msg := 'LOAD LISTA PREPARAZIONE ORDINI BLOCCATO ' || DBMS_UTILITY.FORMAT_ERROR_STACK || '  ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
    --send_mail('GENERAZIONE LISTA PREPARAZIONE ORDINI BLOCCATO',l_msg,0);
    G_LOGGER.log_error('Eccezione SP_MAIN: ' || SQLERRM);    
  RAISE;
  ROLLBACK;
  G_LOGGER.log_info('Eseguita ROLLBACK');
END;

END PKG_LOAD_LISTA_PREPARAZIONE;
/