create or replace PACKAGE BODY PKG_IBM_ESTRAI_TXT AS

  C_DIRECTORY_PATH        CONSTANT VARCHAR2(50) := FN_GET_DEFVAL('BRC_DCC','PRICING_IBM','DIRECTORY_NAME');
  C_NOME_MODULO           CONSTANT VARCHAR2(30) := 'PKG_IBM_ESTRAI_TXT';
  
  G_LOGGER                TYP_LOGGER_NEW        := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);
  
  G_LOAD_ID_PUB           NUMBER                := 0;
  G_LOAD_ID_SUB           NUMBER                := 0;

  G_TAB_NVAR              VARCHAR2(1024)        := 'TABELLE NON VARIATE:= ' || CHR(10) || CHR(13);   
  
  C_ART_X_PDD             VARCHAR2(50)          := 'SPS_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_ALBERO_MERC           VARCHAR2(50)          := 'PHM_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp'; -- non in uso
  C_MERCEOLOGIA           VARCHAR2(50)          := 'PHT_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_PIAZZE_PREZZI         VARCHAR2(50)          := 'ZGF_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp'; -- non in uso
  C_RIL_CONC              VARCHAR2(50)          := 'CPL_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_LOCATION_DATA         VARCHAR2(50)          := 'LOC_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp'; -- non in uso
  C_PREZZI_PUBBLICO       VARCHAR2(50)          := 'PCF_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_ARTICOLI              VARCHAR2(50)          := 'SIF_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_VAT_DIVISION          VARCHAR2(50)          := 'VDP_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  C_COSTI                 VARCHAR2(50)          := 'CCF_MAXIDI_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp';
  
  G_ART_X_PDD_ABIL        CHAR(1);
  G_ALBERO_MERC_ABIL      CHAR(1);
  G_MERCEOLOGIA_ABIL      CHAR(1);
  G_PIAZZE_PREZZI_ABIL    CHAR(1);
  G_RIL_CONC_ABIL         CHAR(1);
  G_LOCATION_DATA_ABIL    CHAR(1);
  G_PREZZI_PUBBLICO_ABIL  CHAR(1);
  G_ARTICOLI_ABIL         CHAR(1);
  G_VAT_DIVISION_ABIL     CHAR(1);
  G_COSTI_ABIL            CHAR(1);
  
  TYPE type_filename IS TABLE OF VARCHAR2(50);
  arr_filename type_filename;
  /*
  TYPE ARRAY_T IS TABLE OF VARCHAR2(50);
  arr_filename ARRAY_T := ARRAY_T(
    C_ART_X_PDD,
    --C_ALBERO_MERC,
    C_MERCEOLOGIA,
    --C_PIAZZE_PREZZI,
    C_RIL_CONC,
    --C_LOCATION_DATA,
    C_PREZZI_PUBBLICO,
    C_ARTICOLI,
    C_VAT_DIVISION,
    C_COSTI
  );
 */
 
------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE send_mail(Oggetto IN VARCHAR2, Messaggio IN VARCHAR2)
  AS
    mailToAddr VARCHAR2(128);
    v_email_mittente DB_FIELDDEFVAL.FIELD_DEFVAL%TYPE := FN_GET_DEFVAL ('BRC_DCC', 'SEND_MAIL', 'MITTENTE');
    CURSOR mailToCur
    IS
      SELECT FIELD_DEFVAL
      FROM db_fielddefval
      WHERE field_name LIKE 'PRICING_IBM_%'
      AND DB_OWNER   = 'BRC_DCC'
      AND TABLE_NAME = 'SEND_MAIL';
  BEGIN
    IF FN_GET_DEFVAL('BRC_DCC','SEND_MAIL','FLAG_ORACLE_MAIL') = 'S' THEN
      OPEN mailToCur;
      LOOP
        FETCH mailToCur INTO mailToAddr;
        EXIT
      WHEN mailToCur%NOTFOUND;
        DITECH_MONITOR.SEND_MAIL(v_sender => v_email_mittente, v_receiver => mailToAddr , v_subject => Oggetto, v_message => Messaggio);
      END LOOP;
      CLOSE mailToCur;
    END IF;
  END; 
  
------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_ABILITATO_PRICING_IBM(p_codice IN VARCHAR2, p_nome_tabella IN VARCHAR2, p_frequenza IN CHAR) RETURN CHAR
  IS
    v_ret CHAR:='N';
  BEGIN
    SELECT (CASE WHEN ABILITATO = 'S' AND (FREQUENZA = 'E' OR FREQUENZA = p_frequenza) THEN 'S' ELSE 'N' END) INTO v_ret FROM CFG_PRICING_IBM WHERE CODICE = SUBSTR(p_codice,0,10) AND NOME_TABELLA = p_nome_tabella;
    RETURN v_ret;
  END FN_GET_ABILITATO_PRICING_IBM;

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_COUNT_PRICING_IBM(p_frequenza IN CHAR) RETURN NUMBER
  IS
    v_ret          NUMBER:=0;
    l_c_1          NUMBER:=0;
    l_c_2          NUMBER:=0;
    l_c_3          NUMBER:=0;
    l_c_4          NUMBER:=0;
    l_c_5          NUMBER:=0;
    l_c_6          NUMBER:=0;
    l_c_7          NUMBER:=0;
    l_c_8          NUMBER:=0;
    l_c_9          NUMBER:=0;
    l_c_10         NUMBER:=0;
    
    l_tot_abil     NUMBER:=0;
    l_tot_count    NUMBER:=0;
  BEGIN
  
  IF (p_frequenza = 'S')
  THEN
     SELECT COUNT(*) INTO l_tot_abil FROM CFG_PRICING_IBM WHERE ABILITATO = 'S' AND (FREQUENZA = 'E' OR FREQUENZA = 'S');
  ELSE 
     SELECT COUNT(*) INTO l_tot_abil FROM CFG_PRICING_IBM WHERE ABILITATO = 'S' AND (FREQUENZA = 'E' OR FREQUENZA = 'G');
  END IF;
 
  IF(FN_GET_ABILITATO_PRICING_IBM(C_ART_X_PDD,'PRICING_IBM_ART_X_PDD',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_1 FROM PRICING_IBM_ART_X_PDD WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_1 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_ART_X_PDD' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_ALBERO_MERC,'PRICING_IBM_ALBERO_MERC',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_2 FROM PRICING_IBM_ALBERO_MERC WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_2 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_ALBERO_MERC' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_MERCEOLOGIA,'PRICING_IBM_MERCEOLOGIA',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_3 FROM PRICING_IBM_MERCEOLOGIA WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_3 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_MERCEOLOGIA' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_PIAZZE_PREZZI,'PRICING_IBM_PIAZZE_PREZZI',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_4 FROM PRICING_IBM_PIAZZE_PREZZI WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_4 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_PIAZZE_PREZZI' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_RIL_CONC,'PRICING_IBM_RIL_CONC',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_5 FROM PRICING_IBM_RIL_CONC WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_5 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_RIL_CONC' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_LOCATION_DATA,'PRICING_IBM_LOCATION_DATA',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_6 FROM PRICING_IBM_LOCATION_DATA WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_6 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_LOCATION_DATA' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_PREZZI_PUBBLICO,'PRICING_IBM_PREZZI_PUBBLICO',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_7 FROM PRICING_IBM_PREZZI_PUBBLICO WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_7 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_PREZZI_PUBBLICO' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_ARTICOLI,'PRICING_IBM_ARTICOLI',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_8 FROM PRICING_IBM_ARTICOLI WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_8 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_ARTICOLI' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_VAT_DIVISION,'PRICING_IBM_ARTICOLO_X_IVA',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_9 FROM PRICING_IBM_ARTICOLO_X_IVA WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_9 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_ARTICOLO_X_IVA' || CHR(10) || CHR(13);
      END IF;
  END IF;
  
  IF(FN_GET_ABILITATO_PRICING_IBM(C_COSTI,'PRICING_IBM_COSTI',p_frequenza) = 'S')
  THEN
      SELECT COUNT(*) INTO l_c_10 FROM PRICING_IBM_COSTI WHERE LOAD_ID > G_LOAD_ID_SUB;
      IF(l_c_10 > 0)
      THEN
        l_tot_count := l_tot_count + 1;
      ELSE
        G_TAB_NVAR  := G_TAB_NVAR || 'PRICING_IBM_COSTI' || CHR(10) || CHR(13);
      END IF;
  END IF;
   
  IF (l_tot_abil = l_tot_count)
  THEN
    v_ret := 1;
  ELSE 
    v_ret := 0;
  END IF;
  
  G_LOGGER.log_info('FN_COUNT_PRICING_IBM - v_ret:=' || v_ret || ' l_tot_abil:=' || l_tot_abil || ' l_tot_count:=' || l_tot_count, 'FN_COUNT_PRICING_IBM', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'P_FREQUENZA:=' || p_frequenza);
      
  RETURN v_ret;
  
  END FN_COUNT_PRICING_IBM;

------------------------------------------------------------------------------------------------------------------
    
  PROCEDURE SP_INIT(p_frequenza IN CHAR)
  AS
  BEGIN
      G_LOGGER.log_info('SP_INIT iniziata', 'SP_INIT', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'P_FREQUENZA:=' || p_frequenza);
    
      IF (p_frequenza = 'S')
      THEN
         SELECT CODICE || '_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp'
         BULK COLLECT INTO arr_filename
         FROM CFG_PRICING_IBM WHERE ABILITATO = 'S' AND (FREQUENZA = 'E' OR FREQUENZA = 'S');
      ELSE
         SELECT CODICE || '_' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.txt.tmp'
         BULK COLLECT INTO arr_filename
         FROM CFG_PRICING_IBM WHERE ABILITATO = 'S' AND (FREQUENZA = 'E' OR FREQUENZA = 'G');
      END IF;
      
      G_ART_X_PDD_ABIL        := FN_GET_ABILITATO_PRICING_IBM(C_ART_X_PDD,'PRICING_IBM_ART_X_PDD',p_frequenza);
      G_ALBERO_MERC_ABIL      := FN_GET_ABILITATO_PRICING_IBM(C_ALBERO_MERC,'PRICING_IBM_ALBERO_MERC',p_frequenza);
      G_MERCEOLOGIA_ABIL      := FN_GET_ABILITATO_PRICING_IBM(C_MERCEOLOGIA,'PRICING_IBM_MERCEOLOGIA',p_frequenza);
      G_PIAZZE_PREZZI_ABIL    := FN_GET_ABILITATO_PRICING_IBM(C_PIAZZE_PREZZI,'PRICING_IBM_PIAZZE_PREZZI',p_frequenza);
      G_RIL_CONC_ABIL         := FN_GET_ABILITATO_PRICING_IBM(C_RIL_CONC,'PRICING_IBM_RIL_CONC',p_frequenza);
      G_LOCATION_DATA_ABIL    := FN_GET_ABILITATO_PRICING_IBM(C_LOCATION_DATA,'PRICING_IBM_LOCATION_DATA',p_frequenza);
      G_PREZZI_PUBBLICO_ABIL  := FN_GET_ABILITATO_PRICING_IBM(C_PREZZI_PUBBLICO,'PRICING_IBM_PREZZI_PUBBLICO',p_frequenza);
      G_ARTICOLI_ABIL         := FN_GET_ABILITATO_PRICING_IBM(C_ARTICOLI,'PRICING_IBM_ARTICOLI',p_frequenza);
      G_VAT_DIVISION_ABIL     := FN_GET_ABILITATO_PRICING_IBM(C_VAT_DIVISION,'PRICING_IBM_ARTICOLO_X_IVA',p_frequenza);
      G_COSTI_ABIL            := FN_GET_ABILITATO_PRICING_IBM(C_COSTI,'PRICING_IBM_COSTI',p_frequenza);
      
      G_LOGGER.log_info('SP_INIT finita', 'SP_INIT', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'P_FREQUENZA:=' || p_frequenza);
  
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('SP_INIT eccezione', 'SP_INIT', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    RAISE;
  END SP_INIT;

------------------------------------------------------------------------------------------------------------------   

  FUNCTION FILE_EXISTS (p_dirname IN VARCHAR2, p_filename IN VARCHAR2) RETURN BOOLEAN
  IS
    l_fexists BOOLEAN;
    l_flen    NUMBER;
    l_bsize   NUMBER;
  BEGIN  
    UTL_FILE.FGETATTR(UPPER(p_dirname), p_filename, l_fexists, l_flen, l_bsize);
    RETURN l_fexists;
  END;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_ART_X_PDD
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_ART_X_PDD iniziata', 'SP_ART_X_PDD', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_ART_X_PDD_ABIL = 'S')
    THEN
      l_file:= UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_ART_X_PDD, 'A');

      FOR j IN
      (SELECT * FROM PRICING_IBM_ART_X_PDD A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*(SELECT * FROM PRICING_IBM_ART_X_PDD A WHERE ( NOT EXISTS (SELECT NULL FROM PRICING_IBM_ART_X_PDD A_IN
                     WHERE A_IN.COD_ART = A.COD_ART
                     AND   A_IN.COD_PDD = A.COD_PDD
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.cod_art || '|' || j.cod_pdd || '|' || j.stato_art || '|' || j.data_inizio_mark || '|' || j.data_fine_mark || '|' || j.cod_mark || '|' || j.week || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_ART_X_PDD elaborati: ' || cont || ' record', 'SP_ART_X_PDD', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
    
    END IF;
    
    G_LOGGER.log_info('SP_ART_X_PDD finita', 'SP_ART_X_PDD', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ART_X_PDD_ABIL:=' || G_ART_X_PDD_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_ART_X_PDD eccezione', 'SP_ART_X_PDD', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ART_X_PDD_ABIL:=' || G_ART_X_PDD_ABIL);
    RAISE;
  END SP_ART_X_PDD;
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_ALBERO_MERC
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_ALBERO_MERC iniziata', 'SP_ALBERO_MERC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_ALBERO_MERC_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_ALBERO_MERC, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_ALBERO_MERC A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*(SELECT * FROM PRICING_IBM_ALBERO_MERC A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_ALBERO_MERC A_IN
                     WHERE A_IN.NODO = A.NODO
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.tipo_record || '|' || j.tipo_nodo || '|' || j.descrizione_nodo || '|' || j.descrizione_alternativa || '|' || j.nodo || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_ALBERO_MERC elaborati: ' || cont || ' record', 'SP_ALBERO_MERC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
       
    END IF;
    
    G_LOGGER.log_info('SP_ALBERO_MERC finita', 'SP_ALBERO_MERC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ALBERO_MERC_ABIL:=' || G_ALBERO_MERC_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_ALBERO_MERC eccezione', 'SP_ALBERO_MERC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ALBERO_MERC_ABIL:=' || G_ALBERO_MERC_ABIL);
    RAISE;
  END SP_ALBERO_MERC;
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_MERCEOLOGIA
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_MERCEOLOGIA iniziata', 'SP_MERCEOLOGIA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_MERCEOLOGIA_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_MERCEOLOGIA, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_MERCEOLOGIA A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_MERCEOLOGIA A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_MERCEOLOGIA A_IN
                     WHERE A_IN.NODO = A.NODO
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
       l_record := j.tipo_record || '|' || j.tipo_merceologia || '|' || j.padre || '|' || j.nodo || CHR(13);
       UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
       cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE (l_file);
      
      G_LOGGER.log_info('SP_MERCEOLOGIA elaborati: ' || cont || ' record', 'SP_ALBERO_MERC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
      
    END IF;
    
    G_LOGGER.log_info('SP_MERCEOLOGIA finita', 'SP_MERCEOLOGIA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_MERCEOLOGIA_ABIL:=' || G_MERCEOLOGIA_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_MERCEOLOGIA eccezione', 'SP_MERCEOLOGIA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_MERCEOLOGIA_ABIL:=' || G_MERCEOLOGIA_ABIL);
    RAISE;
  END SP_MERCEOLOGIA;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_PIAZZE_PREZZI
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_PIAZZE_PREZZI iniziata', 'SP_PIAZZE_PREZZI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_PIAZZE_PREZZI_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_PIAZZE_PREZZI, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_PIAZZE_PREZZI A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_PIAZZE_PREZZI A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_PIAZZE_PREZZI A_IN
                     WHERE A_IN.CODICE_PDD = A.CODICE_PDD
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.cod_societa || '|' || j.tipo_piazza || '|' || j.zona_prezzi || '|' || j.codice_zona || '|' || j.codice_pdd || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_PIAZZE_PREZZI elaborati: ' || cont || ' record', 'SP_PIAZZE_PREZZI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
      
    END IF;
    
    G_LOGGER.log_info('SP_PIAZZE_PREZZI finita', 'SP_PIAZZE_PREZZI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_PIAZZE_PREZZI_ABIL:=' || G_PIAZZE_PREZZI_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_PIAZZE_PREZZI eccezione', 'SP_PIAZZE_PREZZI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_PIAZZE_PREZZI_ABIL:=' || G_PIAZZE_PREZZI_ABIL);
    RAISE;
  END SP_PIAZZE_PREZZI;
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_RIL_CONC
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_RIL_CONC iniziata', 'SP_RIL_CONC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_RIL_CONC_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_RIL_CONC, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_RIL_CONC A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_RIL_CONC A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_RIL_CONC A_IN
                     WHERE A_IN.COD_ART = A.COD_ART
                     AND   A_IN.COD_PDV = A.COD_PDV
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.cod_art || '|' || j.cod_pdv || '|' || j.cod_concorrente || '|' || (CASE WHEN SUBSTR(j.prezzo_rilevato, 1,1) = '.' THEN '0' || j.prezzo_rilevato ELSE TO_CHAR(j.prezzo_rilevato) END) || '|' || j.qta_riferimento_prezzo || '|' || j.tipo_rilevazione || '|' || j.data_rilevazione || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_RIL_CONC elaborati: ' || cont || ' record', 'SP_RIL_CONC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
      
    END IF;
    
    G_LOGGER.log_info('SP_RIL_CONC finita', 'SP_RIL_CONC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_RIL_CONC_ABIL:=' || G_RIL_CONC_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_RIL_CONC eccezione', 'SP_RIL_CONC', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_RIL_CONC_ABIL:=' || G_RIL_CONC_ABIL);
    RAISE;
  END SP_RIL_CONC;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_LOCATION_DATA
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_LOCATION_DATA iniziata', 'SP_LOCATION_DATA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_LOCATION_DATA_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_LOCATION_DATA, 'A');
    
      FOR j IN
      (SELECT * FROM PRICING_IBM_LOCATION_DATA A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_LOCATION_DATA A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_LOCATION_DATA A_IN
                     WHERE A_IN.PDD_CODICE = A.PDD_CODICE
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                               
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.pdd_codice || '|' || j.org_codice || '|' || j.stato || '|' || j.pdd_descrizione || '|' || j.codice_zona || '|' || j.codice_descrizione || '|' || j.riservato_1 || '|' || j.data_inizio|| '|' || j.data_fine || '|' || j.centro_distribuzione
                                 || '|' || j.indirizzo_1 || '|' || j.indirizzo_2 || '|' || j.citta || '|' || j.regione || '|' || j.provincia || '|' || j.cap || '|' || j.nazione|| '|' || j.pdd_formato || '|' || j.pdd_metratura || '|' || j.eta_media
                                 || '|' || j.ricavato_medio || '|' || j.perc_etnia || '|' || j.data_ultima_loc || '|' || j.cod_liv_1|| '|' || j.desc_liv_1 || '|' || j.cod_liv_2 || '|' || j.desc_liv_2 || '|' || j.cod_liv_3 || '|' || j.desc_liv_3 || '|' || j.cod_liv_4
                                 || '|' || j.desc_liv_4 || '|' || j.cod_liv_5 || '|' || j.desc_liv_5 || '|' || j.cod_liv_6|| '|' || j.desc_liv_6|| '|' || j.cod_liv_7 || '|' || j.desc_liv_7 || '|' || j.cod_liv_8 || '|' || j.desc_liv_8 || '|' || j.cod_liv_9
                                 || '|' || j.desc_liv_9 || '|' || j.cod_liv_10 || '|' || j.desc_liv_10 || '|' || j.cod_liv_11 || '|' || j.desc_liv_11 || '|' || j.cod_liv_12 || '|' || j.desc_liv_12 || '|' || j.market || '|' || j.divisione || '|' || j.attr_1
                                 || '|' || j.attr_2 || '|' || j.attr_3 || '|' || j.attr_4 || '|' || j.riservato_2 || '|' || j.tipo_locazione || '|' || j.timezone || '|' || j.sorgente_pdd_chiave || '|' || j.riservato_3 || '|' || j.riservato_4
                                 || '|' || j.flag_franchise || '|' || j.latitudine || '|' || j.longitudine || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_LOCATION_DATA elaborati: ' || cont || ' record', 'SP_LOCATION_DATA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
      
    END IF;
    
    G_LOGGER.log_info('SP_LOCATION_DATA finita', 'SP_LOCATION_DATA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_LOCATION_DATA_ABIL:=' || G_LOCATION_DATA_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_LOCATION_DATA eccezione', 'SP_LOCATION_DATA', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_LOCATION_DATA_ABIL:=' || G_LOCATION_DATA_ABIL);
    RAISE;
  END SP_LOCATION_DATA;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_PREZZI_PUBBLICO
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_PREZZI_PUBBLICO iniziata', 'SP_PREZZI_PUBBLICO', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_PREZZI_PUBBLICO_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_PREZZI_PUBBLICO, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_PREZZI_PUBBLICO A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /* 
      (SELECT * FROM PRICING_IBM_PREZZI_PUBBLICO A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_PREZZI_PUBBLICO A_IN
                     WHERE A_IN.COD_ART = A.COD_ART
                     AND   A_IN.PDD_CODICE = A.PDD_CODICE
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                             
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.cod_art || '|' || j.pdd_codice || '|' || j.data_inizio || '|' || j.unita_prezzo
                              || '|' || j.rapp_conv || '|' || j.locale || '|' || j.flag_1 || '|' || j.flag_2
                              || '|' || j.flag_3 || '|' || j.flag_4 || '|' ||  j.flag_5 || '|' || j.user_lock || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_PREZZI_PUBBLICO elaborati: ' || cont || ' record', 'SP_PREZZI_PUBBLICO', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
     
    END IF;
    
    G_LOGGER.log_info('SP_PREZZI_PUBBLICO finita', 'SP_PREZZI_PUBBLICO', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_PREZZI_PUBBLICO_ABIL:=' || G_PREZZI_PUBBLICO_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_PREZZI_PUBBLICO eccezione', 'SP_PREZZI_PUBBLICO', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_PREZZI_PUBBLICO_ABIL:=' || G_PREZZI_PUBBLICO_ABIL);
    RAISE;
  END SP_PREZZI_PUBBLICO;

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_ARTICOLI
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_ARTICOLI iniziata', 'SP_ARTICOLI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_ARTICOLI_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_ARTICOLI, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_ARTICOLI A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_ARTICOLI A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_ARTICOLI A_IN
                     WHERE A_IN.ARTICOLO = A.ARTICOLO
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                             
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.articolo || '|' || j.tipo_codice || '|' || j.desc_art || '|' || j.campo_no_obb_1 
                               || '|' || j.campo_no_obb_2 || '|' || j.padre || '|' || j.campo_no_obb_3 || '|' || j.campo_no_obb_4 || '|' || j.campo_no_obb_5
                               || '|' || j.forn_prev || '|' || j.um_grammatura || '|' || j.quantita_netta || '|' || j.campo_no_obb_6 || '|' || j.pack || '|' || j.prezzo_pre_stampato || '|' || j.stagionale || '|' || j.marca 
                               || '|' || j.prodotto_a_marchio || '|' || j.campo_no_obb_7 || '|' || j.campo_no_obb_8 || '|' || j.tipo_attributo_1 || '|' || j.attributo_1 || '|' || j.tipo_attributo_2 || '|' || j.attributo_2 || '|' || j.campo_no_obb_9 || '|' || j.campo_no_obb_10 
                               || '|' || j.biologico || '|' || j.biol || '|' || j.no_latt || '|' || j.nolat || '|' || j.no_glu || '|' || j.noglu || '|' || j.paniere || '|' || j.campo_no_obb_11 || '|' || j.campo_no_obb_12 || '|' || j.campo_no_obb_13 || '|' || j.campo_no_obb_14
                               || '|' || j.campo_no_obb_15 || '|' || j.campo_no_obb_16 || '|' || j.campo_no_obb_17 || '|' || j.nodo_1 || '|' || j.descrizione_nodo_1 || '|' || j.nodo_2 || '|' || j.descrizione_nodo_2 || '|' || j.nodo_3 || '|' || j.descrizione_nodo_3 || '|' || j.nodo_4 || '|' || j.descrizione_nodo_4 || '|' || j.campo_no_obb_18 || '|' || j.campo_no_obb_19 || '|' || j.campo_no_obb_20 || '|' || j.campo_no_obb_21 || '|' || j.campo_no_obb_22 || '|' || j.campo_no_obb_23 || '|' || j.campo_no_obb_24 || '|' || j.campo_no_obb_25
                               || '|' || j.campo_no_obb_26 || '|' || j.campo_no_obb_27 || '|' || j.campo_no_obb_28 || '|' || j.campo_no_obb_29 || '|' || j.campo_no_obb_30 || '|' || j.campo_no_obb_31 || '|' || j.campo_no_obb_32 || '|' || j.campo_no_obb_33 || '|' || j.campo_no_obb_34 || '|' || j.campo_no_obb_35 || '|' || j.campo_no_obb_36 || '|' || j.campo_no_obb_37 || '|' || j.campo_no_obb_38 || '|' || j.cod_sotfam || '|' || j.campo_no_obb_39 || '|' || j.campo_no_obb_40
                               || '|' || j.campo_no_obb_41 || '|' || j.campo_no_obb_42 || '|' || j.campo_no_obb_43 || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_ARTICOLI elaborati: ' || cont || ' record', 'SP_ARTICOLI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);

    END IF;
     
    G_LOGGER.log_info('SP_ARTICOLI finita', 'SP_ARTICOLI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ARTICOLI_ABIL:=' || G_ARTICOLI_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_ARTICOLI eccezione', 'SP_ARTICOLI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_ARTICOLI_ABIL:=' || G_ARTICOLI_ABIL);
    RAISE;
  END SP_ARTICOLI;

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_VAT_DIVISION
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_VAT_DIVISION iniziata', 'SP_VAT_DIVISION', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_VAT_DIVISION_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_VAT_DIVISION, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_ARTICOLO_X_IVA A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_ARTICOLO_X_IVA A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_ARTICOLO_X_IVA A_IN
                     WHERE A_IN.DIVISIONE = A.DIVISIONE
                     AND   A_IN.COD_ART = A.COD_ART
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                             
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.divisione || '|' || j.cod_art || '|' || j.aliquota || '|' || j.tassa || '|' || j.data_inizio || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_VAT_DIVISION elaborati: ' || cont || ' record', 'SP_VAT_DIVISION', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);

    END IF;
    
    G_LOGGER.log_info('SP_VAT_DIVISION finita', 'SP_VAT_DIVISION', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_VAT_DIVISION_ABIL:=' || G_VAT_DIVISION_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_VAT_DIVISION eccezione', 'SP_VAT_DIVISION', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_VAT_DIVISION_ABIL:=' || G_VAT_DIVISION_ABIL);
    RAISE;
  END SP_VAT_DIVISION;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_COSTI
  AS
    l_file 			    UTL_FILE.FILE_TYPE;
    l_record        VARCHAR2(4096);
    cont            NUMBER:=0;
  BEGIN
    G_LOGGER.log_info('SP_COSTI iniziata', 'SP_COSTI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
    IF (G_COSTI_ABIL = 'S')
    THEN
      l_file := UTL_FILE.FOPEN(C_DIRECTORY_PATH, C_COSTI, 'A');
      
      FOR j IN
      (SELECT * FROM PRICING_IBM_COSTI A WHERE A.LOAD_ID > G_LOAD_ID_SUB)
      /*
      (SELECT * FROM PRICING_IBM_COSTI A WHERE (NOT EXISTS (SELECT NULL FROM PRICING_IBM_COSTI A_IN
                     WHERE A_IN.COD_ART = A.COD_ART
                     AND   A_IN.PDD_CODICE = A.PDD_CODICE
                     AND   A_IN.LOAD_ID > A.LOAD_ID)                                                                             
             ) AND A.LOAD_ID > G_LOAD_ID_SUB)*/
      LOOP
        l_record := j.cod_art || '|' || j.pdd_codice || '|' || j.logistical_item_key || '|' || j.vendor_key
                              || '|' || j.primary_vendor || '|' || j.pv_conversion_factor || '|' || j.data_inizio
                              || '|' || TRIM(j.costo) || '|' || TRIM(j.costo_1) || '|' || TRIM(j.costo_2) || '|' || TRIM(j.costo_3)
                              || '|' || j.reserved1 || '|' || j.reserved2 || CHR(13);
        UTL_FILE.PUT_LINE(l_file, CONVERT(l_record, 'AL32UTF8'));
        cont     := cont + 1;
      END LOOP;
      
      UTL_FILE.FCLOSE(l_file);
      
      G_LOGGER.log_info('SP_COSTI elaborati: ' || cont || ' record', 'SP_COSTI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);
      
    END IF;
    
    G_LOGGER.log_info('SP_COSTI finita', 'SP_COSTI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_COSTI_ABIL:=' || G_COSTI_ABIL);
  
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.FCLOSE(l_file);
    G_LOGGER.log_error('SP_COSTI eccezione', 'SP_COSTI', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'G_COSTI_ABIL:=' || G_COSTI_ABIL);
    RAISE;
  END SP_COSTI;

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_RINOMINA_FILE
  AS
    l_file_name	  VARCHAR2(30);
    cont          NUMBER:=0;
  BEGIN
  G_LOGGER.log_info('SP_RINOMINA_FILE iniziata', 'SP_RINOMINA_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
  FOR i IN 1..arr_filename.count
  LOOP
    SELECT SUBSTR(arr_filename(i),0,(LENGTH(arr_filename(i))-4)) INTO l_file_name FROM DUAL;
    IF FILE_EXISTS(C_DIRECTORY_PATH,arr_filename(i))
    THEN
      UTL_FILE.FRENAME(src_location => C_DIRECTORY_PATH, src_filename => arr_filename(i), dest_location => C_DIRECTORY_PATH, dest_filename => l_file_name, overwrite => FALSE);
      cont := cont + 1;
    ELSE
      G_LOGGER.log_info('SP_RINOMINA_FILE file non trovato: ' || C_DIRECTORY_PATH || ' -- ' || arr_filename(i), 'SP_RINOMINA_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    END IF;
  END LOOP; 
	
  G_LOGGER.log_info('SP_RINOMINA_FILE rinominati: ' || cont || ' file da TMP in TXT', 'SP_RINOMINA_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);

  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('SP_RINOMINA_FILE eccezione', 'SP_RINOMINA_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    RAISE;
  END SP_RINOMINA_FILE;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_RIMUOVI_TMP_FILE
  AS
    cont          NUMBER:=0;
  BEGIN
  G_LOGGER.log_info('SP_RIMUOVI_TMP_FILE iniziata', 'SP_RIMUOVI_TMP_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    
  FOR i IN 1..arr_filename.count
  LOOP
    IF FILE_EXISTS(C_DIRECTORY_PATH,arr_filename(i))
    THEN
      UTL_FILE.FREMOVE(location => C_DIRECTORY_PATH, filename => arr_filename(i));
      cont := cont + 1;
    ELSE
      G_LOGGER.log_info('SP_RIMUOVI_TMP_FILE file non trovato: ' || C_DIRECTORY_PATH || ' -- ' || arr_filename(i), 'SP_RIMUOVI_TMP_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    END IF;
  END LOOP;
    
  G_LOGGER.log_info('SP_RIMUOVI_TMP_FILE rimossi: ' || cont || ' file TMP', 'SP_RIMUOVI_TMP_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB, 'cont:=' || cont);

  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('SP_RIMUOVI_TMP_FILE eccezione', 'SP_RIMUOVI_TMP_FILE', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
    RAISE;
  END SP_RIMUOVI_TMP_FILE;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_MAIN(p_frequenza IN CHAR)
  AS
    l_msg 		    VARCHAR2(1024);
    l_current     NUMBER:=0;
  BEGIN

  G_LOGGER.log_info('SP_MAIN iniziata', 'SP_MAIN', NULL);
    
  SELECT COUNT(*) INTO l_current
  FROM   DB_MODXLOAXTAB
  WHERE  MODULO_ID        = 'MORE2PRICING_IBM'
  AND    TABLE_NAME       = 'ANAG'
  AND    MODULO_TYPE      = 'P'
  AND    WORK_IN_PROGRESS = 'N';

  IF (l_current = 1) THEN
      SELECT LOADID INTO G_LOAD_ID_PUB
      FROM   DB_MODXLOAXTAB
      WHERE  MODULO_ID        = 'MORE2PRICING_IBM'
      AND    TABLE_NAME       = 'ANAG'
      AND    MODULO_TYPE      = 'P'
      AND    WORK_IN_PROGRESS = 'N';
    
      SELECT LOADID INTO G_LOAD_ID_SUB
      FROM   DB_MODXLOAXTAB
      WHERE  MODULO_ID       = 'DCC2PRICING_IBM'
      AND    TABLE_NAME      = 'ANAG'
      AND    MODULO_TYPE     = 'S';
      
      SP_INIT(p_frequenza);
      
      IF ((G_LOAD_ID_PUB - G_LOAD_ID_SUB) = 1) THEN
          IF (FN_COUNT_PRICING_IBM(p_frequenza) = 1) THEN
            SP_ART_X_PDD();
            SP_ALBERO_MERC();
            SP_MERCEOLOGIA();
            SP_PIAZZE_PREZZI();
            SP_RIL_CONC();
            SP_LOCATION_DATA();
            SP_PREZZI_PUBBLICO();
            SP_ARTICOLI();
            SP_VAT_DIVISION();
            SP_COSTI();
          
            SP_RINOMINA_FILE();  
          
            UPDATE DB_MODXLOAXTAB
            SET    LOADID      = G_LOAD_ID_PUB
            WHERE  MODULO_ID   = 'DCC2PRICING_IBM'
            AND    TABLE_NAME  = 'ANAG'
            AND    MODULO_TYPE = 'S';
            
            COMMIT;
            l_msg := 'Eseguita COMMIT - flussi esportati';
            G_LOGGER.log_info(l_msg, 'SP_MAIN', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
            --send_mail('[OK] FLUSSO ESTRAZIONE IBM PRICING', l_msg);
          ELSE
            l_msg := G_TAB_NVAR || CHR(10) || CHR(13) || ' - LOAD_ID_PUB:=' || G_LOAD_ID_PUB || ' - LOAD_ID_SUB:=' || G_LOAD_ID_SUB;
            G_LOGGER.log_error(l_msg, 'SP_MAIN', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
            send_mail('[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO', l_msg);
          END IF;
      
      ELSIF ((G_LOAD_ID_PUB - G_LOAD_ID_SUB) > 1) THEN
        l_msg := 'Impossibile procedere, troppi LOAD_ID - LOAD_ID_PUB:=' || G_LOAD_ID_PUB || ' - LOAD_ID_SUB:=' || G_LOAD_ID_SUB;
        G_LOGGER.log_error(l_msg, 'SP_MAIN', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
        send_mail('[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO', l_msg); 
      ELSE
        l_msg := 'Nessun LOAD_ID da esportare - LOAD_ID_PUB:=' || G_LOAD_ID_PUB || ' - LOAD_ID_SUB:=' || G_LOAD_ID_SUB;
        G_LOGGER.log_info(l_msg, 'SP_MAIN', NULL, 'G_LOAD_ID_PUB:=' || G_LOAD_ID_PUB, 'G_LOAD_ID_SUB:=' || G_LOAD_ID_SUB);
        send_mail('[OK] FLUSSO ESTRAZIONE IBM PRICING', l_msg);
      END IF;
  
  ELSE
    l_msg := 'Impossibile procedere, PUBLISHER - WORK_IN_PROGRESS <> N, non elaboro';
    G_LOGGER.log_error(l_msg, 'SP_MAIN', NULL);
    send_mail('[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO', l_msg);
  END IF;
  
  G_LOGGER.log_info('SP_MAIN finita', 'SP_MAIN', NULL, 'P_FREQUENZA:=' || p_frequenza);
  
  EXCEPTION
  WHEN OTHERS THEN
    l_msg := 'SP_MAIN eccezione ' || DBMS_UTILITY.FORMAT_ERROR_STACK;
    G_LOGGER.log_error(l_msg, 'SP_MAIN', NULL);
    send_mail('[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO', l_msg);
    
    SP_RIMUOVI_TMP_FILE();
    
    ROLLBACK;
    G_LOGGER.log_info('Eseguita ROLLBACK', 'SP_MAIN', NULL, 'P_FREQUENZA:=' || p_frequenza);
    RAISE;
  END SP_MAIN;
  
------------------------------------------------------------------------------------------------------------------

END PKG_IBM_ESTRAI_TXT;
/