create or replace PACKAGE BODY PKG_LOAD_RAPP_AMM AS

PRAGMA SERIALLY_REUSABLE;

C_NOME_MODULO                CONSTANT VARCHAR2(30)           := 'PKG_LOAD_RAPP_AMM';

C_FLAG_DA_ELAB               CONSTANT CHAR(1)                := '1';
C_FLAG_IN_ELAB               CONSTANT CHAR(1)                := 'E';
C_FLAG_OK                    CONSTANT CHAR(1)                := '2';
C_FLAG_ERR                   CONSTANT CHAR(1)                := '3';

C_SOCIETA_MAXI               CONSTANT VARCHAR2(4)            := 'B130';
C_AZIENDA_MAXI               CONSTANT VARCHAR2(4)            := '130';


C_TIPO_RAPP_AMM_CFG_VENDITA  CONSTANT VARCHAR2(6)            := 'VNDSTD';
C_TIPO_RAPP_AMM_VENDITA      CONSTANT VARCHAR2(6)            := 'RVND';  

-- Istanzio il log
G_LOGGER                TYP_LOGGER_NEW := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);                 

------------------------------------------------------------------------------------------------------------------

-- Cursore utilizzato per leggere i record dalla tabella CONDIZIONI_VENDITA sul DCC
-- e riversarli nelle tabelle dei rapporti amministrativi.
CURSOR cur_condizioni_vendita IS
    select /*+ INDEX(i, IX_FORNITORE_CODICE) */
           cv.*, 
           i.INTERLOCUTORE_ID, cs.CLASSE_SCONTO_T_ID,
           mp.MODALITA_PAGAMENTO_ID, tp.TIPO_PAGAMENTO_ID,
           merc.MERCEOLOGIA_ID,
           pf.PERSONA_FISCALE_ID,
           sa.SEDE_AMMINISTRATIVA_ID           
      from CONDIZIONI_VENDITA cv
           left join NCL_INTERLOCUTORE i on (cv.D_CODICE_CLIENTE = i.CODICE and i.RUOLO_CLIENTE = 'S')
           left join COMM_CLASSE_SCONTO_T cs on (cv.D_CODICE_CLASSE_DI_SCONTO = cs.CODICE)  
           left join TRANS_PAG trans on (cv.D_CODICE_PAGAMENTO = trans.D_PAG_IND and trans.D_SOCIETA_IND = '130') --TODO cosa fare con questo codice?
           left join COMM_MODALITA_PAGAMENTO mp on (trans.D_MOD_PAG_MORE = mp.CODICE)
           left join COMM_TIPO_PAGAMENTO tp on (trans.D_TIP_PAG_MORE = tp.CODICE)
           left join (select m.MERCEOLOGIA_ID, m.VALORE_ESTESO --, m.valore, m.descrizione, ml.progressivo 
                        from NCL_MERCEOLOGIA m, NCL_MERCEOLOGIA_LIVELLO ml, NCL_MERCEOLOGIA_CL mc
                       where m.MERCEOLOGIA_LIVELLO_ID = ml.MERCEOLOGIA_LIVELLO_ID
                         and ml.MERCEOLOGIA_CL_ID = mc.MERCEOLOGIA_CL_ID
                         and mc.CODICE = 'MAXID'
                         and ml.PROGRESSIVO >= 2
                     ) merc on ((cv.D_SETTORE || cv.D_FAMIGLIA || cv.D_SOTTOFAMIGLIA) like ('%' || merc.VALORE_ESTESO) )
           left join NCL_INTERLOCUTORE_X_PERSONA ixp on (i.INTERLOCUTORE_ID = ixp.INTERLOCUTORE_ID and sysdate BETWEEN ixp.DATA_INIZIO and NVL(ixp.DATA_FINE, to_date('01012999', 'MMDDYYYY')))
           left join NCL_PERSONA_FISCALE pf on ixp.PERSONA_FISCALE_ID = pf.PERSONA_FISCALE_ID 
           left join NCL_SEDE_AMMINISTRATIVA sa on (pf.PERSONA_FISCALE_ID = sa.PERSONA_FISCALE_ID and sa.LEGALE = 'S')
     where cv.STATO = C_FLAG_DA_ELAB
  order by cv.D_CODICE_CLIENTE, cv.D_DATA_VALIDITA_ANNO, cv.D_DATA_VALIDITA_MESE, cv.D_DATA_VALIDITA_GIORNO
  FOR UPDATE OF cv.STATO, cv.MOTIVO_SCARTO;

------------------------------------------------------------------------------------------------------------------

FUNCTION GET_RAPP_AMM_CFG(p_merceologia_id NUMBER) RETURN NUMBER
RESULT_CACHE RELIES_ON (COMM_RAPP_AMM_CFG)
IS

  l_rapp_amm_cfg_id   NUMBER;
  
BEGIN

  G_LOGGER.log_debug('Inizio recupero RAPP_AMM_CFG_ID...', 'GET_RAPP_AMM_CFG');

  if p_merceologia_id is null then
  
    select rac.RAPP_AMM_CFG_ID
      into l_rapp_amm_cfg_id
      from COMM_RAPP_AMM_CFG rac
           join COMM_TIPO_RAPP_AMM_CFG trac on (rac.TIPO_RAPP_AMM_CFG_ID = trac.TIPO_RAPP_AMM_CFG_ID)
           join NCL_AZIENDA a on (rac.AZIENDA_ID = a.AZIENDA_ID)
     where trac.CODICE = C_TIPO_RAPP_AMM_CFG_VENDITA
       and a.CODICE = C_AZIENDA_MAXI
  --     and rac.TIPO_GESTIONE_ARTICOLO_ID = ...        -- TODO: GESTIRE QUESTE CONDIZIONI
  --     and rac.TIPO_CONSEGNA_ID = ...
       and rac.MERCEOLOGIA_ID is null;
  
  else 

    select rac.RAPP_AMM_CFG_ID
      into l_rapp_amm_cfg_id
      from COMM_RAPP_AMM_CFG rac
           join COMM_TIPO_RAPP_AMM_CFG trac on (rac.TIPO_RAPP_AMM_CFG_ID = trac.TIPO_RAPP_AMM_CFG_ID)
           join NCL_AZIENDA a on (rac.AZIENDA_ID = a.AZIENDA_ID)
     where trac.CODICE = C_TIPO_RAPP_AMM_CFG_VENDITA
       and a.CODICE = C_AZIENDA_MAXI
  --     and rac.TIPO_GESTIONE_ARTICOLO_ID = ...        -- TODO: GESTIRE QUESTE CONDIZIONI
  --     and rac.TIPO_CONSEGNA_ID = ...
       and rac.MERCEOLOGIA_ID = p_merceologia_id;
       
  end if;
  
  G_LOGGER.log_debug('Recuperata RAPP_AMM_CFG_ID: ' || l_rapp_amm_cfg_id, 'GET_RAPP_AMM_CFG');

  return l_rapp_amm_cfg_id;
  
EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('Errore nel recuperare RAPP_AMM_CFG_ID: ' || SQLERRM, 'GET_RAPP_AMM_CFG');
    return null;
  
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION GET_TIPO_RAPP_AMM(p_codice VARCHAR) RETURN NUMBER
RESULT_CACHE RELIES_ON (COMM_TIPO_RAPP_AMM)
IS

  l_tipo_rapp_amm_id number;
  
BEGIN

  select TIPO_RAPP_AMM_ID
    into l_tipo_rapp_amm_id
    from COMM_TIPO_RAPP_AMM
   where CODICE = p_codice;
   
  G_LOGGER.log_debug('Recuperato TIPO_RAPP_AMM_ID: ' || l_tipo_rapp_amm_id, 'GET_TIPO_RAPP_AMM'); 
   
  return l_tipo_rapp_amm_id;
  
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE SP_TRADUZIONE(p_load_id NUMBER)
IS
BEGIN
  G_LOGGER.log_info('Inserimento record in CONDIZIONI_VENDITA...', 'SP_TRADUZIONE');

  insert into CONDIZIONI_VENDITA (
    CONDIZIONI_VENDITA_ID,
    LOAD_ID,
    STATO,
    D_CODICE_CLIENTE,
    D_DATA_VALIDITA_ANNO,
    D_DATA_VALIDITA_MESE,
    D_DATA_VALIDITA_GIORNO,
    D_SETTORE,
    D_FAMIGLIA,
    D_SOTTOFAMIGLIA,
    D_CODICE_AGENTE,
    D_CODICE_CLASSE_DI_SCONTO,
    D_CODICE_PAGAMENTO,
    D_STATO_INFORMAZIONE,
    INS_UTENTE,
    INS_MODULO,
    UPD_UTENTE,
    UPD_MODULO
  )
  select SQ_CONDIZIONI_VENDITA.nextval,
         p_load_id,
         C_FLAG_DA_ELAB,
         trim(DB9CDF),
         DB9AVA,
         lpad(DB9MVA, 2, '0'),
         lpad(DB9GVA, 2, '0'),
         lpad(trim(DB9CDR), 3, '0'),
         lpad(trim(DB9FAM), 3, '0'),
         lpad(trim(DB9SUF), 6, '0'),
         trim(DB9AGE),
         trim(DB9CDC),
         trim(DB9PAG),
         --    trim(DB9CDA),
         --    trim(DB9CDL),
         --    trim(DB9CDY),  
         trim(DB9STA),
         C_NOME_MODULO,
         C_NOME_MODULO,
         C_NOME_MODULO,
         C_NOME_MODULO
    from FDB9COVE
   where DB9ST1 = C_FLAG_IN_ELAB
     and DB9IDM = p_load_id;

  G_LOGGER.log_info('Record inseriti in CONDIZIONI_VENDITA: ' || sql%rowcount , 'SP_TRADUZIONE');

END;

------------------------------------------------------------------------------------------------------------------

FUNCTION SP_VALIDAZIONE(p_rec_cv cur_condizioni_vendita%rowtype) RETURN VARCHAR2
IS

  l_msg               VARCHAR2(255) := null;
  l_data              DATE;
  
BEGIN

  G_LOGGER.log_debug('Inizio validazione record...', 'SP_VALIDAZIONE');

  if p_rec_cv.interlocutore_id is null then
    l_msg := 'Interlocutore non trovato. ';
  else
    if p_rec_cv.PERSONA_FISCALE_ID is null then
      l_msg := l_msg || 'Persona fiscale non trovata. ';
    end if;
    if p_rec_cv.SEDE_AMMINISTRATIVA_ID is null then
      l_msg := l_msg || 'Sede amministrativa non trovata. ';
    end if;  
  end if;
  
  begin
    l_data := to_date(p_rec_cv.d_data_validita_anno || p_rec_cv.d_data_validita_mese || p_rec_cv.d_data_validita_giorno, 'YYYYMMDD');
  exception
    when others then
      l_msg := l_msg || 'Data validit� errata. ';  
  end;  
  
  if p_rec_cv.d_codice_classe_di_sconto is not null and p_rec_cv.classe_sconto_t_id is null then
    l_msg := l_msg || 'Classe di sconto non trovata. ';
  end if;
  
  if p_rec_cv.d_codice_pagamento is null then
    l_msg := l_msg || 'Codice pagamento non valorizzato. ';
  elsif p_rec_cv.modalita_pagamento_id is null or p_rec_cv.tipo_pagamento_id is null then
    l_msg := l_msg || 'Codice pagamento non trovato. ';    
  end if;

  if not (p_rec_cv.d_settore = '000' and p_rec_cv.d_famiglia = '000' and p_rec_cv.d_sottofamiglia = '000000')
     and p_rec_cv.merceologia_id is null
  then
    l_msg := l_msg || 'Merceologia non trovata. ';  
  end if;
  
  G_LOGGER.log_debug('Fine validazione record. Msg:' || l_msg, 'SP_VALIDAZIONE');
    
  return l_msg;
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE INSERT_RAPP_AMM(p_rec_cv cur_condizioni_vendita%rowtype, p_rapp_amm_cfg_id NUMBER, p_progressivo_rapporto NUMBER, p_intervallo_a DATE)
IS

  l_rapp_amm_id       NUMBER;
  l_rapp_amm_attr_id  NUMBER;
  l_rapp_amm_x_cfg_id NUMBER;
  l_codice_rapporto   COMM_RAPP_AMM.CODICE%type;
  l_tipo_rapp_amm_id  NUMBER;
  
BEGIN

  G_LOGGER.log_debug('Inizio inserimento rapporto amministrativo...', 'INSERT_RAPP_AMM');

  l_rapp_amm_id := COMM_SQ_RAPP_AMM.nextval;
  l_tipo_rapp_amm_id := GET_TIPO_RAPP_AMM(C_TIPO_RAPP_AMM_VENDITA);
  l_codice_rapporto := p_rec_cv.D_CODICE_CLIENTE || '-' || p_progressivo_rapporto;
  
  insert into COMM_RAPP_AMM ( 
    RAPP_AMM_ID,
    CODICE,
    DESCRIZIONE,
    TIPO_RAPP_AMM_ID,
    INTERLOCUTORE_ID,
    INS_UTENTE,
    INS_MODULO,
    UPD_UTENTE,
    UPD_MODULO
  )
  values (
    l_rapp_amm_id,
    l_codice_rapporto,
    'Rapporto vendita ' || p_progressivo_rapporto || ' cliente ' || p_rec_cv.D_CODICE_CLIENTE,
    l_tipo_rapp_amm_id,
    p_rec_cv.INTERLOCUTORE_ID,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO
  );

  G_LOGGER.log_debug('Inserito RAPP_AMM_ID: ' || l_rapp_amm_id, 'INSERT_RAPP_AMM');

  l_rapp_amm_attr_id := COMM_SQ_RAPP_AMM_ATTR.nextval;
  
  insert into COMM_RAPP_AMM_ATTR (
    RAPP_AMM_ATTR_ID,
    RAPP_AMM_ID,
    INTERVALLO_DA,
    INTERVALLO_A,
--    BLOCCO_FIDO_ID,
--    NUMERO_GIORNI_DATA_DEC,
    TIPO_PAGAMENTO_ID,
    MODALITA_PAGAMENTO_ID,
--    TIPO_CALCOLO_DECORRENZA_ID,
--    PERIODICITA_FATTURAZIONE_ID,
--    DISPO_FINANZIARIA_ID,
--    COORDINATE_BANCARIE_ID,
--    GESTIONE_ABBUONI,
--    DEBITI_CREDITI,
--    MASTRO_CONTABILE_ID,
    CLASSE_SCONTO_T_ID,
    PERSONA_FISCALE_ID_FATTURA,
    SEDE_AMMINISTRATIVA_ID_FATTURA,
    INS_UTENTE,
    INS_MODULO,
    UPD_UTENTE,
    UPD_MODULO
  )
  values (
    l_rapp_amm_attr_id,
    l_rapp_amm_id,
    to_date('01011900', 'DDMMYYYY'),
    null,
    p_rec_cv.TIPO_PAGAMENTO_ID,
    p_rec_cv.MODALITA_PAGAMENTO_ID,
    p_rec_cv.CLASSE_SCONTO_T_ID,
    p_rec_cv.PERSONA_FISCALE_ID,
    p_rec_cv.SEDE_AMMINISTRATIVA_ID,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO
  );

  G_LOGGER.log_debug('Inserito RAPP_AMM_ATTR_ID: ' || l_rapp_amm_attr_id, 'INSERT_RAPP_AMM');

  l_rapp_amm_x_cfg_id := COMM_SQ_RAPP_AMM_X_CFG.nextval;
  
  insert into COMM_RAPP_AMM_X_CFG (
    RAPP_AMM_X_CFG_ID,
    RAPP_AMM_CFG_ID,
    RAPP_AMM_ID,
    INTERVALLO_DA,
    INTERVALLO_A,
    ANNULLATO,
    INS_UTENTE,
    INS_MODULO,
    UPD_UTENTE,
    UPD_MODULO
  )
  values (
    l_rapp_amm_x_cfg_id,
    p_rapp_amm_cfg_id,
    l_rapp_amm_id,
    to_date(p_rec_cv.D_DATA_VALIDITA_ANNO || p_rec_cv.D_DATA_VALIDITA_MESE || p_rec_cv.D_DATA_VALIDITA_GIORNO, 'YYYYMMDD'),
    p_intervallo_a,
    case when p_rec_cv.D_STATO_INFORMAZIONE = '0' then 'S' else 'N' end,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO,
    C_NOME_MODULO
  );

  G_LOGGER.log_debug('Inserito RAPP_AMM_X_CFG_ID: ' || l_rapp_amm_x_cfg_id, 'INSERT_RAPP_AMM');
  G_LOGGER.log_debug('Fine inserimento rapporto amministrativo...', 'INSERT_RAPP_AMM');

END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE AGGIORNA_RAPP_AMM(p_rec_cv cur_condizioni_vendita%rowtype, p_rapp_amm_cfg_id NUMBER)
IS

  TYPE raxc_list_typ IS TABLE OF COMM_RAPP_AMM_X_CFG%rowtype;
  raxc_list                 raxc_list_typ;
  l_intevallo_a             DATE;
  l_intevallo_da            DATE;
  l_rapporto_trovato        BOOLEAN;
  l_idx_ultimo_rapp         NUMBER;
  
BEGIN

  G_LOGGER.log_debug('Inizio aggiornamento/inserimento rapporto amministrativo...', 'AGGIORNA_RAPP_AMM');

  -- Carico i rapporti amministrativi dell'interlocutore corrispondenti per merceologia
  select raxc.*
    bulk collect into raxc_list
    from COMM_RAPP_AMM_X_CFG raxc, COMM_RAPP_AMM ra, COMM_TIPO_RAPP_AMM tra
   where raxc.RAPP_AMM_ID = ra.RAPP_AMM_ID
     and ra.TIPO_RAPP_AMM_ID = tra.TIPO_RAPP_AMM_ID
     and tra.CODICE = C_TIPO_RAPP_AMM_VENDITA
     and raxc.RAPP_AMM_CFG_ID = p_rapp_amm_cfg_id
     and ra.INTERLOCUTORE_ID = p_rec_cv.INTERLOCUTORE_ID
   order by raxc.INTERVALLO_DA;

  G_LOGGER.log_debug('Rapporti amministrativi esistenti: ' || raxc_list.count, 'AGGIORNA_RAPP_AMM');

  l_rapporto_trovato := false;
  l_intevallo_da := to_date(p_rec_cv.d_data_validita_anno || p_rec_cv.d_data_validita_mese || p_rec_cv.d_data_validita_giorno, 'YYYYMMDD');
  
  if raxc_list.count > 0 then
    
    -- Scorro i rapporti esistenti per trovare il punto nella catena temporale in cui
    -- inserire o aggiornare il rapporto, chiudendo l'intervallo del rapporto precedente (se esiste).
    for i in 1..raxc_list.count loop
    
      if raxc_list(i).INTERVALLO_DA = l_intevallo_da then
        -- Ho trovato il rapporto da aggiornare
        
        l_rapporto_trovato := true;
        
        update COMM_RAPP_AMM_X_CFG
           set ANNULLATO = (case when p_rec_cv.D_STATO_INFORMAZIONE = '0' then 'S' else 'N' end)
         where RAPP_AMM_X_CFG_ID = raxc_list(i).RAPP_AMM_X_CFG_ID;
         
        update COMM_RAPP_AMM_ATTR
           set TIPO_PAGAMENTO_ID = p_rec_cv.TIPO_PAGAMENTO_ID,
               MODALITA_PAGAMENTO_ID = p_rec_cv.MODALITA_PAGAMENTO_ID,
               CLASSE_SCONTO_T_ID = p_rec_cv.CLASSE_SCONTO_T_ID,
               PERSONA_FISCALE_ID_FATTURA = p_rec_cv.PERSONA_FISCALE_ID,
               SEDE_AMMINISTRATIVA_ID_FATTURA = p_rec_cv.SEDE_AMMINISTRATIVA_ID
         where RAPP_AMM_ID = raxc_list(i).RAPP_AMM_ID
           and INTERVALLO_DA = (select max(INTERVALLO_DA) from COMM_RAPP_AMM_ATTR where RAPP_AMM_ID = raxc_list(i).RAPP_AMM_ID);
           
         G_LOGGER.log_debug('Trovato e aggiornato RAPP_AMM_ID: ' || raxc_list(i).RAPP_AMM_ID, 'AGGIORNA_RAPP_AMM');
        
        -- Esco dal loop perch� ho fatto tutto   
        exit;
        
      elsif raxc_list(i).INTERVALLO_DA > l_intevallo_da then
        -- Il rapporto va inserito prima del rapporto i-esimo.
        
        G_LOGGER.log_debug('Trovato punto della catena temporale in cui inserire il rapporto.' , 'AGGIORNA_RAPP_AMM');
        
        if i > 1 then
          -- Il rapporto da inserire NON e' il primo della catena temporale
          -- Devo quindi chiudere l'intervallo del rapporto precedente
          update COMM_RAPP_AMM_X_CFG
             set INTERVALLO_A = l_intevallo_da - 1
           where RAPP_AMM_X_CFG_ID = raxc_list(i-1).RAPP_AMM_X_CFG_ID;
           
          G_LOGGER.log_debug('Chiuso RAPP_AMM_X_CFG_ID: ' || raxc_list(i-1).RAPP_AMM_X_CFG_ID, 'AGGIORNA_RAPP_AMM');
        end if;
        
        -- Inserisco il rapporto
        l_intevallo_a := raxc_list(i).INTERVALLO_DA - 1;
        INSERT_RAPP_AMM(p_rec_cv, p_rapp_amm_cfg_id, raxc_list.count + 1, l_intevallo_a);
        
        l_rapporto_trovato := true;
        
        -- Esco dal loop perch� ho fatto tutto
        exit;
      
      end if;
      
    end loop;
  
  end if; -- if raxc_list.count > 0

  if not l_rapporto_trovato then
    
    if raxc_list.count > 0 then
      -- Il rapporto va inserito per ultimo   
      -- Chiudo l'intervallo dell'ultimo rapporto esistente
      l_idx_ultimo_rapp := raxc_list.count;
      
      update COMM_RAPP_AMM_X_CFG
         set INTERVALLO_A = l_intevallo_da - 1
       where RAPP_AMM_X_CFG_ID = raxc_list(l_idx_ultimo_rapp).RAPP_AMM_X_CFG_ID;
       
      G_LOGGER.log_debug('Rapporto inserito per ultimo. Chiuso RAPP_AMM_X_CFG_ID: ' || raxc_list(l_idx_ultimo_rapp).RAPP_AMM_X_CFG_ID, 'AGGIORNA_RAPP_AMM');
    end if;
    
    l_intevallo_a := null;
    INSERT_RAPP_AMM(p_rec_cv, p_rapp_amm_cfg_id, raxc_list.count + 1, l_intevallo_a);
  end if;

  G_LOGGER.log_debug('Fine aggiornamento/inserimento rapporto amministrativo.', 'AGGIORNA_RAPP_AMM');
       
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE SP_CARICAMENTO(p_num_record_ok OUT NUMBER, p_num_record_ko OUT NUMBER)
IS

  l_msg_scarto              CONDIZIONI_VENDITA.MOTIVO_SCARTO%type;
  l_rapp_amm_cfg_id         NUMBER;  
  
BEGIN

  G_LOGGER.log_info('Inizio caricamento record da CONDIZIONI_VENDITA...', 'SP_CARICAMENTO');
  
  p_num_record_ok := 0;
  p_num_record_ko := 0;
  
  for rec_cv in cur_condizioni_vendita loop
   
   begin
      -- Ad ogni loop marco l'inizio dell'elaborazione del record
      -- cos�, se ho un problema, scarto il record ed eseguo il rollback parziale
      -- di quanto eventualmente fatto per il record scartato.
      SAVEPOINT INIZIO_RECORD;
   
      -- Valida formalmente i dati
      l_msg_scarto := SP_VALIDAZIONE(rec_cv);
      
      if l_msg_scarto is null then  
        l_rapp_amm_cfg_id := GET_RAPP_AMM_CFG(rec_cv.merceologia_id); 
        if l_rapp_amm_cfg_id is null then
          l_msg_scarto := 'Configurazione rapporto non trovata. '; 
        end if;
      end if;
      
      if l_msg_scarto is not null then
      
        -- Dati non validi, scarta il record
        update CONDIZIONI_VENDITA
           set STATO = C_FLAG_ERR,
               MOTIVO_SCARTO = l_msg_scarto
         where current of cur_condizioni_vendita;
         
         p_num_record_ko := p_num_record_ko + 1;
         
         -- Passa al prossimo record del cursore
         continue;
         
      end if;
  
      -- Il record ha superato la validazione
      -- Aggiorno/inserisco il rapporto amministrativo
      AGGIORNA_RAPP_AMM(rec_cv, l_rapp_amm_cfg_id);

      update CONDIZIONI_VENDITA
         set STATO = C_FLAG_OK,
             MOTIVO_SCARTO = null
       where current of cur_condizioni_vendita;
      
      p_num_record_ok := p_num_record_ok + 1;
      
    exception
      when others then
        G_LOGGER.log_error('Errore non previsto, salto il record con CONDIZIONI_VENDITA_ID: ' || rec_cv.CONDIZIONI_VENDITA_ID  || ' - '|| SQLERRM,  'SP_CARICAMENTO');
        ROLLBACK TO INIZIO_RECORD;
        p_num_record_ko := p_num_record_ko + 1;
    end;

  end loop;

  G_LOGGER.log_info('Record caricati: ' || p_num_record_ok || '. Record scartati: ' || p_num_record_ko, 'SP_CARICAMENTO');  
  G_LOGGER.log_info('Fine caricamento record da CONDIZIONI_VENDITA', 'SP_CARICAMENTO');

END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE SP_MAIN
IS

  l_load_id         NUMBER;
  l_anno_elab       NUMBER;
  l_mese_elab       NUMBER;
  l_giorno_elab     NUMBER;
  l_ora_elab        NUMBER;
  l_num_record      NUMBER;
  l_num_record_ok   NUMBER;
  l_num_record_ko   NUMBER;
  
BEGIN  

  l_load_id := SQ_LOAD_ID_CONDIZIONI_VENDITA.nextval;
  G_LOGGER.load_id := l_load_id;
  G_LOGGER.log_info('Inizio caricamento rapporti amministrativi.', 'SP_MAIN');
  
  l_anno_elab := to_char(sysdate, 'YYYY');
  l_mese_elab := to_char(sysdate, 'MM');
  l_giorno_elab := to_char(sysdate, 'DD');
  l_ora_elab := to_char(systimestamp, 'HH24MISSFF3');
  
  -- Marca "in elaborazione" i record su AS400
  -- e gli assegna il LOAD_ID e la data/ora di elaborazione.
  -- Prende solo i record afferenti alla societ� MAXID
  update FDB9COVE
     set DB9ST1 = C_FLAG_IN_ELAB,
         DB9IDM = l_load_id,
         DB9MDA = l_anno_elab,
         DB9MDM = l_mese_elab,
         DB9MDG = l_giorno_elab,
         DB9MDO = l_ora_elab
   where DB9ST1 = C_FLAG_DA_ELAB
     and DB9CDE = C_SOCIETA_MAXI;
   
  l_num_record := sql%rowcount;
  G_LOGGER.log_info('Record marcati da elaborare su AS400: ' || l_num_record, 'SP_MAIN');
  COMMIT;

  -- Preleva i record AS400 da elaborare e li inserisce in CONDIZIONI_VENDITA sul DCC
  BEGIN
    SP_TRADUZIONE(l_load_id);
    COMMIT;
  
    -- Marca "elaborati" i record su AS400  
    update FDB9COVE
       set DB9ST1 = C_FLAG_OK
     where DB9ST1 = C_FLAG_IN_ELAB
       and DB9IDM = l_load_id;
    
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      G_LOGGER.log_error('Errore nell''inserimento su CONDIZIONI_VENDITA: ' || SQLERRM, 'SP_MAIN');
    
      ROLLBACK;
      
      -- Non si � riusciti a inserire i record su CONDIZIONI_VENDITA.
      -- Marca "in errore" i record su AS400 
      update FDB9COVE
         set DB9ST1 = C_FLAG_ERR
       where DB9ST1 = C_FLAG_IN_ELAB
         and DB9IDM = l_load_id;    
      
      COMMIT;
      G_LOGGER.log_info('Marcati record in stato ERRORE su AS400', 'SP_MAIN');
      
      RAISE;
  END;

  -- Carica i record sulle nuove tabelle dei rapporti amministrativi su MORE
  SP_CARICAMENTO(l_num_record_ok, l_num_record_ko);
  COMMIT;

  if l_num_record_ko > 0 then
    -- TODO: mandare mail
    null;
  end if;

  G_LOGGER.log_info('Procedura terminata correttamente.', 'SP_MAIN');

EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('Procedura terminata con errore: ' || SQLERRM, 'SP_MAIN');
    -- TODO: MANDARE MAIL
      
END;

END PKG_LOAD_RAPP_AMM;