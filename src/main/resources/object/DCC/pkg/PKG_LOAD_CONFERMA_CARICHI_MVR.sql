create or replace package pkg_load_conferma_carichi_mvr as 

  PRAGMA SERIALLY_REUSABLE;
  PROCEDURE sp_caricamento_comm(p_load_id IN NUMBER);
  PROCEDURE sp_caricamento_mover;
  PROCEDURE sp_main;

end pkg_load_conferma_carichi_mvr;
/