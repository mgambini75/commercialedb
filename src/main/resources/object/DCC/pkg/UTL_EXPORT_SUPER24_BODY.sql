create or replace PACKAGE BODY         UTL_EXPORT_SUPER24 IS
  /******************************************************************************
    NAME:       UTL_EXPORT_SUPER24
    PURPOSE:
  
    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.0.0.0    09/03/2017  Luca Pancaldi    Created this package.
  ******************************************************************************/

  C_MODULO    CONSTANT VARCHAR2(30) := 'MORE2SUPER24';
  C_TABELLA   CONSTANT VARCHAR2(30) := 'ANAG';
 
  PROCEDURE SP_GET_NEXT_ID(i_modulo  IN VARCHAR2,
                           i_tabella IN VARCHAR2,
                           o_loadId  OUT NUMBER) IS
    v_loadId NUMBER;
    spName   VARCHAR2(30) := 'SP_GET_NEXT_ID';
    resource_busy EXCEPTION;
    PRAGMA EXCEPTION_INIT(resource_busy, -54);
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => spName,
                                             I_INFO      => 'Modulo(' ||
                                                            i_modulo ||
                                                            ') tabella(' ||
                                                            i_tabella || ')');
  
    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(I_INFO => 'Recupero loadId da BRC_DCC.DB_MODXLOAXTAB');
      SELECT LOADID
        INTO v_loadId
        FROM BRC_DCC.DB_MODXLOAXTAB
       WHERE TABLE_NAME = i_tabella
         AND MODULO_ID = i_modulo
         FOR UPDATE NOWAIT;
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DITECH_COMMON.LOGGER.WRITE_COMMENT('LoadId non trovato in BRC_DCC.DB_MODXLOAXTAB');
      
        v_loadId := 1;
      
        DITECH_COMMON.LOGGER.START_LOG_STMT();
        INSERT INTO BRC_DCC.DB_MODXLOAXTAB
          (LOADID,
           MODULO_ID,
           TABLE_NAME,
           INS_DATA,
           UPD_DATA,
           VERSION,
           WORK_IN_PROGRESS,
           MODULO_TYPE)
        VALUES
          (v_loadId,
           i_modulo,
           i_tabella,
           SYSDATE,
           SYSDATE,
           SYSDATE,
           'S',
           'P');
        DITECH_COMMON.LOGGER.END_LOG_STMT(I_INFO => 'Inserito loadId in BRC_DCC.DB_MODXLOAXTAB. Modulo(' ||
                                                    i_modulo ||
                                                    ') tabella(' ||
                                                    i_tabella ||
                                                    ') loadId(' || v_loadId || ')');
      
      WHEN OTHERS THEN
        DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;
  
    IF v_loadId IS NOT NULL THEN
      DITECH_COMMON.LOGGER.WRITE_COMMENT('LoadId ' || v_loadId ||
                                         ' trovato in BRC_DCC.DB_MODXLOAXTAB');
    
      v_loadId := v_loadId + 1;
    
      DITECH_COMMON.LOGGER.START_LOG_STMT();
      UPDATE BRC_DCC.DB_MODXLOAXTAB
         SET LOADID           = v_loadId,
             UPD_DATA         = SYSDATE,
             VERSION          = SYSDATE,
             WORK_IN_PROGRESS = 'S',
             MODULO_TYPE      = 'P'
       WHERE TABLE_NAME = i_tabella
         AND MODULO_ID = i_modulo;
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_INFO => 'Aggiornato loadId in BRC_DCC.DB_MODXLOAXTAB. Modulo(' ||
                                                  i_modulo || ') tabella(' ||
                                                  i_tabella || ') loadId(' ||
                                                  v_loadId || ')');
    END IF;
    o_loadId := v_loadId;
    COMMIT;
  
    DITECH_COMMON.LOGGER.WRITE_COMMENT('LoadId(' || o_loadId || ')');
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE();
  
  EXCEPTION
    WHEN resource_busy THEN
      o_loadId := -1;
      DITECH_COMMON.LOGGER.END_LOG_PROCEDURE();
    WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;
  PROCEDURE SP_RELEASE_NEXT_ID(i_modulo  IN VARCHAR2,
                               i_tabella IN VARCHAR2,
                               i_loadId  IN NUMBER) IS
    vartmp NUMBER := 0;
    spName VARCHAR2(30) := 'SP_RELEASE_NEXT_ID';
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => spName,
                                             I_INFO      => 'Modulo(' ||
                                                            i_modulo ||
                                                            ') tabella(' ||
                                                            i_tabella ||
                                                            ') loadId(' ||
                                                            i_loadId || ')');
  
    DITECH_COMMON.LOGGER.START_LOG_STMT();
    UPDATE BRC_DCC.DB_MODXLOAXTAB
       SET UPD_DATA = SYSDATE, VERSION = SYSDATE, WORK_IN_PROGRESS = 'N'
     WHERE TABLE_NAME = i_tabella
       AND MODULO_ID = i_modulo
       AND LOADID = i_loadId;
    vartmp := sql%rowcount;
    DITECH_COMMON.LOGGER.END_LOG_STMT;
  
    IF vartmp = 0 THEN
      DITECH_COMMON.LOGGER.WRITE_COMMENT('LOADID non rilasciato. Modulo(' ||
                                         i_modulo || ') tabella(' ||
                                         i_tabella || ') loadId(' ||
                                         i_loadId || ')');
    END IF;
  
    COMMIT;
  
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE();
  
  EXCEPTION
    WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;
  
  FUNCTION FN_GET_LAST_ID(i_modulo IN VARCHAR2, i_tabella IN VARCHAR2)
    RETURN NUMBER IS
    v_loadId NUMBER;
  BEGIN
    SELECT LOADID
      INTO v_loadId
      FROM BRC_DCC.DB_MODXLOAXTAB
     WHERE TABLE_NAME = i_tabella
       AND MODULO_ID = i_modulo;
  
    RETURN V_LOADID;
  END FN_GET_LAST_ID;
  
  PROCEDURE ESTRAI_ANAGRAFICA (P_LOAD_ID IN NUMBER) IS
    spName VARCHAR2(30) := 'SUPER24_ESTRAI_ANAGRAFICA';
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => spName);
  
    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(I_INFO      => 'SUPER24 --> ESTRAI_ANAGRAFICA',
                                          I_SRC_TABLE => 'MORE',
                                          I_TAB_NAME  => 'SUPER24_ASSORTIMENTI');
     INSERT INTO 
        BRC_DCC.SUPER24_ASSORTIMENTO
    (
      CODICE_CEDI,
      BARCODE,
      COD_ART_INTERNO,
      MARCA,
      DESCR_ARTICOLO,
      UNITA_MIS_GESTIONALE,
      UNITA_MIS_FISCALE,
      QUANTITA_NETTA,
      PREZZO_VENDITA,
      PESO_VARIABILE,
      GIACENZA,
      LOAD_ID,
      INS_DATA,
      INS_MODULO,
      INS_UTENTE,
      VAL_MERC_1,
      DESCR_MERC_1,
      VAL_MERC_2,
      DESCR_MERC_2,
      VAL_MERC_3,
      DESCR_MERC_3,
      VAL_MERC_4,
      DESCR_MERC_4,
      CODICE_PDD
    )
    (SELECT 
        CODICE_CEDI,
        BA.VALORE BARCODE, 
        AR.CODICE COD_ART_INTERNO,
        CAST(NULL AS VARCHAR2(50)) MARCA,
        AR.DESCRIZIONE DESCR_ARTICOLO,  
        UM.CODICE UNITA_MIS_GESTIONALE,
        UM1.CODICE UNITA_MIS_DISCALE,
        abu.QUANTITA_NETTA,
        (SELECT BRC_DCC.FN_DAMMI_PREZZO_VENDITA(ar.articolo_id, pd.pdd_id, 'P') FROM DUAL )PREZZO_VENDITA,
        CASE WHEN UM.CODICE='PZ' THEN '0' ELSE '1' END PESO_VARIABILE,
        1 GIACENZA,
        p_load_Id LOAD_ID,
        SYSDATE INS_DATA,
        C_MODULO INS_MODULO,
        SPNAME   INS_UTENTE,
        VAM.M1_VALORE,
        VAM.M1_DESCRIZIONE ,
        VAM.M2_VALORE ,
        VAM.M2_DESCRIZIONE ,
        VAM.M3_VALORE ,
        VAM.M3_DESCRIZIONE ,
        VAM.M4_VALORE ,
        VAM.M4_DESCRIZIONE,
        PD.CODICE
      FROM brc_core.ARTICOLO_X_PDD_CONSIGLIATO AXPC
        JOIN BRC_CORE.ARTICOLO AR ON AR.ARTICOLO_ID=AXPC.ARTICOLO_ID
        JOIN BRC_CORE.PDD PD ON PD.PDD_ID=AXPC.PDD_ID
        JOIN BRC_CORE.BARCODE BA ON BA.ARTICOLO_ID=AR.ARTICOLO_ID
        JOIN BRC_CORE.ART_BASE_X_UNITA_MISURA ABU ON ABU.ART_BASE_ID=AR.ART_BASE_ID
        JOIN BRC_CORE.UNITA_MISURA UM ON UM.UNITA_MISURA_ID=UMG
        JOIN BRC_CORE.UNITA_MISURA UM1 ON UM1.UNITA_MISURA_ID=UMF
        JOIN BRC_CORE.ARTICOLO_X_MERCEOLOGIA AM ON (AM.ARTICOLO_ID=AR.ARTICOLO_ID)
        JOIN BRC_CORE.MERCEOLOGIA ME ON (ME.MERCEOLOGIA_ID=AM.MERCEOLOGIA_ID)
        join BRC_CORE.MERCEOLOGIA_livello ml on (ml.merceologia_livello_id=ME.MERCEOLOGIA_LIVELLO_ID AND ML.MERCEOLOGIA_CL_ID=26)
        JOIN BRC_CORE.V_MERCEOLOGIA_ALBERO VAM ON ME.VALORE=VAM.M4_VALORE
        JOIN BRC_DCC.SUPER24_MERC_ABIL MAB ON (MAB.LIVELLO_MERC_MORE_ID=VAM.M1_LIVELLO AND VAM.M1_VALORE=MAB.VALORE_MERC)
     WHERE pd.pdd_id in (select pdd_id_more from SUPER24_PDV_ABIL)
          AND BA.ANNULLATO='N'
          AND STATO_APC=1
          AND AR.STATO_ID NOT IN (1,2));


      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION
      WHEN OTHERS THEN
        DITECH_COMMON.LOGGER.END_LOG_STMT(I_IS_EXCEPTION      => TRUE,
                                          I_ON_EXCEPTION_STOP => TRUE);
    END;
  
    COMMIT;
  
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE;
  EXCEPTION
    WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_IS_EXCEPTION      => TRUE,
                                             I_ON_EXCEPTION_STOP => TRUE);
  END ESTRAI_ANAGRAFICA;

  PROCEDURE EXPORT_SUPER24_MAIN  IS
    spName      VARCHAR2(30) := 'EXPORT_SUPER24_MAIN';
    v_loadId    NUMBER;
    v_sessionId NUMBER;
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => spName);
  
    SP_GET_NEXT_ID(C_MODULO, C_TABELLA, v_loadId);
  
    ESTRAI_ANAGRAFICA (v_loadId);
  
    COMMIT;
  
    SP_RELEASE_NEXT_ID(C_MODULO, C_TABELLA, v_loadId);
  
    --Eseguo la creazione del file .csv con i dati relativi al load_id passato
    SP_EXPORT_CEDI_SUPER24(v_loadId);
  
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE;
  
  
  EXCEPTION
    WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_IS_EXCEPTION      => TRUE,
                                             I_ON_EXCEPTION_STOP => TRUE);
      ROLLBACK;
  END EXPORT_SUPER24_MAIN; 
  
END;
/