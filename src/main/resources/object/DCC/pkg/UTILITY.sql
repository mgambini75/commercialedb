create or replace PACKAGE UTILITY AS
-----------------------------------------------------------------------------
  ---------------------------------------------------------------------------
  PROCEDURE SP_DROPALL_IDX
    ( Tab           IN      CHAR
     ,Idx           IN      CHAR
     ,RetCod            OUT NUMBER);

  PROCEDURE SP_EXISTS_OBJ
    ( vOwner          IN      VARCHAR2
    , vObjectName     IN      VARCHAR2
    , vObjectType     IN      CHAR
    , ResponseSN          OUT CHAR
    , RetCod              OUT NUMBER);

  PROCEDURE SP_ANALYZE_OBJ            -- SCOPO : Do Statistics
    ( myObjTyp       IN      VARCHAR2
    , myObjNam       IN      VARCHAR2
    , myAnalizePerc  IN      NUMBER
    , RetCod             OUT NUMBER);

  PROCEDURE SP_EXECUTE_SQL
    ( StSQL           IN      VARCHAR2
    , RetCod              OUT NUMBER);

  PROCEDURE SP_TRUNCAT_TAB
    ( TableName       IN      CHAR
    , RetCod              OUT NUMBER);

  ---------------------------------------------------------------------------
  -- Log Functions
  PROCEDURE SP_LOGGA_EVENTO     (CodOpe       IN VARCHAR2,
                                 SQLOpe       IN VARCHAR2,
                                 ObjNam       IN VARCHAR2,
                                 InfAgg       IN VARCHAR2,
                                 LoadID       IN NUMBER,
                                 RetCod       IN NUMBER);

  ---------------------------------------------------------------------------
  -- Utility Functions
  FUNCTION FN_GET_DEFVAL_BY_FLD  (MyField    IN VARCHAR2)  RETURN VARCHAR2;
  FUNCTION FN_GET_STATO          (TipoStato  IN CHAR)      RETURN NUMBER;
  ---------------------------------------------------------------------------
  -----------------------------------------------------------------------------
  PROCEDURE SPEDISCI_MAIL
  (
    Oggetto    			IN VARCHAR2,
    Messaggio  			IN VARCHAR2,
    GruppoDestinatario  IN VARCHAR2,
    LoaId      			IN NUMBER := NULL
  );
END UTILITY;