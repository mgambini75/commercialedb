create or replace package body pkg_load_conferma_carichi_mvr as

  PRAGMA SERIALLY_REUSABLE;

  C_NOME_MODULO           CONSTANT VARCHAR2(30)   := 'PKG_LOAD_CONFERMA_CARICHI_MVR';
  C_INS_MODULO_MVR        CONSTANT VARCHAR2(30)   := 'MOVER2MORE_DCC_CARICHI';
  C_INS_MODULO_COMM       CONSTANT VARCHAR2(30)   := 'COMM2MORE_CARICHI';
  C_SORGENTE_FLUSSO       CONSTANT VARCHAR2(30)   := 'MOVER_CARICHI';
  
  G_LOGGER                TYP_LOGGER_NEW          := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

  G_LOAD_ID               NUMBER                  := NULL;
  G_LOAD_ID_MODX          NUMBER                  := 0;
  
  G_CONT_TESTATE          NUMBER                  := 0;
  G_CONT_RIGHE            NUMBER                  := 0;
  
 -------------------------------------------------------------------------------------------------
  
  CURSOR cur_conferma_carico_t IS
  SELECT DISTINCT t.*
  FROM mvr_dcc_conferma_carico_t t
  JOIN mvr_dcc_conferma_carico_d d ON (t.d_numero_carico = d.d_numero_carico AND t.d_codice_magazzino = d.d_codice_magazzino AND t.load_id = d.load_id)
  WHERE t.load_id > G_LOAD_ID_MODX;
  
  CURSOR cur_conferma_carico_d(p_numero_carico IN VARCHAR2, p_codice_magazzino IN VARCHAR2, p_load_id IN NUMBER) IS
  SELECT *
  FROM mvr_dcc_conferma_carico_d
  WHERE d_numero_carico = p_numero_carico 
  AND   d_codice_magazzino = p_codice_magazzino 
  AND   load_id = p_load_id;
  
  CURSOR cur_carico_log_in_t(p_load_id IN NUMBER) IS
  SELECT *
  FROM carico_log_in_t
  WHERE load_id = p_load_id;
  
  CURSOR cur_carico_log_in_d(p_numero_carico IN VARCHAR2, p_codice_magazzino IN VARCHAR2, p_load_id IN NUMBER) IS
  SELECT *
  FROM carico_log_in_d
  WHERE d_numero_carico = p_numero_carico 
  AND   d_codice_magazzino = p_codice_magazzino 
  AND   load_id = p_load_id;
  
  /*
  TYPE transcodifica_causali_type
  IS TABLE OF transcodifica_causali%ROWTYPE
  INDEX BY VARCHAR2(50);
  */ 
 -------------------------------------------------------------------------------------------------
 
  function fn_get_causale_more(p_flusso IN VARCHAR2, p_causale_ext_codice IN VARCHAR2) RETURN VARCHAR2
  is
    v_ret VARCHAR2(50);
  begin
     select d_causale_more_codice into v_ret from transcodifica_causali where d_flusso = p_flusso and d_causale_ext_codice = p_causale_ext_codice;
     return v_ret;
  exception
  when others then
    G_LOGGER.log_info('codice causale more non trovato','FN_GET_CAUSALE_MORE', NULL, 'p_flusso:=' || p_flusso, '-20012', 'p_causale_ext_codice:=' || p_causale_ext_codice);
    return null;
  end fn_get_causale_more;
  -------------------------------------------------------------------------------------------------
 
  procedure sp_insert_d2m_carico_t(p_d_carichi_t_id IN NUMBER, p_carico_log_in_t IN carico_log_in_t%ROWTYPE)
  as
  begin
    insert into comm_d2m_carichi_t
    (
      d_carichi_t_id,
      d_sorgente_flusso,
      load_id,
      codice_errore,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      VERSION,
      d_ordine_numero,
      d_ordine_data,
      d_stato_carico,
      d_numero_carico,
      d_data_inizio_ricevimento,
      d_fornitore_codice,
      d_pdd_codice,
      d_data_prevista_consegna,
      d_data_fine_ricevimento,
      d_ordine_causale
    )
    values
    (
      p_d_carichi_t_id,
      C_SORGENTE_FLUSSO,
      p_carico_log_in_t.load_id,
      p_carico_log_in_t.codice_errore,
      SYSDATE,
      C_INS_MODULO_COMM,
      C_INS_MODULO_COMM,
      SYSDATE,
      C_INS_MODULO_COMM,
      C_INS_MODULO_COMM,
      SYSDATE,
      p_carico_log_in_t.d_numero_ordine,
      p_carico_log_in_t.d_data_ordine,
      p_carico_log_in_t.d_stato_carico,
      p_carico_log_in_t.d_numero_carico,
      p_carico_log_in_t.d_data_inizio_ricevimento,
      p_carico_log_in_t.d_fornitore_codice,
      p_carico_log_in_t.d_codice_magazzino,
      p_carico_log_in_t.d_data_prevista_consegna,
      p_carico_log_in_t.d_data_fine_ricevimento,
      p_carico_log_in_t.d_causale_ordine
    );
    G_CONT_TESTATE:= G_CONT_TESTATE + 1;
  end sp_insert_d2m_carico_t;
  
 -------------------------------------------------------------------------------------------------
  
  procedure sp_insert_d2m_carico_r(p_d_carichi_t_id IN NUMBER, p_carico_log_in_t IN carico_log_in_t%ROWTYPE, p_carico_log_in_d IN carico_log_in_d%ROWTYPE)
  as
    l_d_causale_riga VARCHAR2(50);
  begin
  
    l_d_causale_riga := fn_get_causale_more(C_SORGENTE_FLUSSO,p_carico_log_in_d.d_causale_riga);
  
    insert into comm_d2m_carichi_r
    (
      d_carichi_r_id,
      d_carichi_t_id,
      d_causale,
      d_nr_doc1,
      d_data_doc1,
	    d_tipo_consegna,
      d_tipo_doc1,
      d_fornitore_codice,
      d_nr_riga,
      d_numero_carico,
      d_numero_ordine,
      d_numero_riga_ordine,
      d_pdd_codice,
      d_cessione_imballo,
      d_fornitore_imballo,
      d_artcod,
      d_pallet_piani,
      d_pallet_imballi_piano,
      d_pallet_imballi,
      d_quantita_cons_pz,
      d_quantita_cons_kg,
      d_quantita_cons_imb,
      d_quantita_ord_pz,
      d_quantita_ord_kg,
      d_quantita_ord_imb,
      d_cauzione_codice,
      d_cauzione_qta_resa,
      d_flag_sostanza_zuccherina,
      d_qta_ordinata_omaggio_colli,
      d_qta_ricevuta_omaggio_colli,
      d_qta_ricevuta_omaggio_pz_kg,
      d_username_operatore,
      d_stato_riga_carico,
      d_quantita_doc1_pz,
      d_quantita_doc1_imb,
      d_quantita_doc1_kg,
      load_id,
      codice_errore,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      VERSION
    )
    values
    (
      comm_sq_d2m_carichi_r.nextval,
      p_d_carichi_t_id,
      l_d_causale_riga,
      p_carico_log_in_d.d_numero_bolla,
      TO_CHAR(p_carico_log_in_d.d_data_bolla,'YYMMDD'),
      'D',
      NULL, --TODO TIPO DOC
      p_carico_log_in_t.d_fornitore_codice,
      p_carico_log_in_d.d_numero_riga_bolla,
      p_carico_log_in_d.d_numero_carico,
      p_carico_log_in_d.d_numero_ordine,
      p_carico_log_in_d.d_numero_riga_ordine,
      p_carico_log_in_d.d_codice_magazzino,
      p_carico_log_in_d.d_imballo_cessione,
      p_carico_log_in_d.d_imballo_fornitore,
      p_carico_log_in_d.d_articolo_codice,
      p_carico_log_in_d.d_pallet_piani,
      p_carico_log_in_d.d_pallet_imballi_piano,
      p_carico_log_in_d.d_pallet_imballi,
      p_carico_log_in_d.d_quantita_car_pz,
      p_carico_log_in_d.d_quantita_car_kg,
      p_carico_log_in_d.d_quantita_car_colli,
      p_carico_log_in_d.d_quantita_da_car_pz,
      p_carico_log_in_d.d_quantita_da_car_kg,
      p_carico_log_in_d.d_quantita_da_car_colli,
      NULL, --p_carico_log_in_d.d_cauzione_codice,
      NULL, --p_carico_log_in_d.d_cauzione_qta_resa,
      p_carico_log_in_d.d_flag_sostanza_zuccherina,
      p_carico_log_in_d.d_qta_ordinata_omaggio_colli,
      p_carico_log_in_d.d_qta_ricevuta_omaggio_colli,
      p_carico_log_in_d.d_qta_ricevuta_omaggio_pz_kg,
      p_carico_log_in_d.d_username_operatore,
      p_carico_log_in_d.d_stato_riga_carico,
      NULL, --d_quantita_doc1_pz,
      NULL, --d_quantita_doc1_imb,
      NULL, --d_quantita_doc1_kg
      p_carico_log_in_d.load_id,
      p_carico_log_in_d.codice_errore,
      SYSDATE,
      C_INS_MODULO_COMM,
      C_INS_MODULO_COMM,
      SYSDATE,
      C_INS_MODULO_COMM,
      C_INS_MODULO_COMM,
      SYSDATE
    );
    G_CONT_RIGHE:= G_CONT_RIGHE + 1;
  end sp_insert_d2m_carico_r;
  
 -------------------------------------------------------------------------------------------------
     
  procedure sp_insert_carico_log_in_t(p_conferma_carico_t IN mvr_dcc_conferma_carico_t%ROWTYPE)
  as
  begin
    insert into carico_log_in_t
    (
      d_numero_carico,
      d_data_inizio_ricevimento,
      d_numero_ordine,
      d_data_ordine,
      d_numero_bolla,
      d_fornitore_codice,
      d_codice_magazzino,
      d_stato,
      d_data_prevista_consegna,
      d_data_fine_ricevimento,
      d_commenti,
      d_causale_ordine,
      load_id,
      load_id_mvr,
      codice_errore,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      VERSION,
      d_stato_carico
    )
    values
    (
      p_conferma_carico_t.d_numero_carico,
      p_conferma_carico_t.d_data_inizio_ricevimento,
      p_conferma_carico_t.d_numero_ordine,
      p_conferma_carico_t.d_data_ordine,
      p_conferma_carico_t.d_numero_bolla,
      p_conferma_carico_t.d_fornitore_codice,
      p_conferma_carico_t.d_codice_magazzino,
      p_conferma_carico_t.d_stato,
      p_conferma_carico_t.d_data_prevista_consegna,
      p_conferma_carico_t.d_data_fine_ricevimento,
      p_conferma_carico_t.d_commenti,
      p_conferma_carico_t.d_causale_ordine,
      G_LOAD_ID,
      p_conferma_carico_t.load_id,
      p_conferma_carico_t.codice_errore,
      SYSDATE,
      C_INS_MODULO_MVR,
      C_INS_MODULO_MVR,
      SYSDATE,
      C_INS_MODULO_MVR,
      C_INS_MODULO_MVR,
      SYSDATE,
      p_conferma_carico_t.d_stato_carico
    );
    G_CONT_TESTATE:= G_CONT_TESTATE + 1;
  end sp_insert_carico_log_in_t;
 
 -------------------------------------------------------------------------------------------------
  
  procedure sp_insert_carico_log_in_d(p_conferma_carico_d IN mvr_dcc_conferma_carico_d%ROWTYPE)
  as
  begin
    insert into carico_log_in_d
    (      
      d_numero_carico,
      d_data_scadenza,
      d_numero_ordine,
      d_numero_riga_ordine,
      d_numero_bolla,
      d_data_bolla,
      d_numero_riga_bolla,
      d_codice_magazzino,
      d_articolo_codice,
      d_articolo_diff,
      d_stato,
      d_imballo_cessione,
      d_imballo_fornitore,
      d_pallet_piani,
      d_pallet_imballi_piano,
      d_pallet_imballi,
      d_quantita_car_pz,
      d_quantita_car_kg,
      d_quantita_car_colli,
      d_quantita_da_car_pz,
      d_quantita_da_car_kg,
      d_quantita_da_car_colli,
      d_cauzione_codice,
      d_cauzione_qta_resa,
      d_codice_ean,
      d_causale_testata,
      d_causale_riga,
      d_flag_sostanza_zuccherina,
      d_qta_ordinata_omaggio_colli,
      d_qta_ricevuta_omaggio_colli,
      d_qta_ricevuta_omaggio_pz_kg,
      d_costo_netto_unitario,
      load_id,
      load_id_mvr,
      codice_errore,
      ins_data,
      ins_utente,
      ins_modulo,
      upd_data,
      upd_utente,
      upd_modulo,
      VERSION,
      d_username_operatore,
      d_stato_riga_carico
    )
    values
    (
      p_conferma_carico_d.d_numero_carico,
      p_conferma_carico_d.d_data_scadenza,
      p_conferma_carico_d.d_numero_ordine,
      p_conferma_carico_d.d_numero_riga_ordine,
      p_conferma_carico_d.d_numero_bolla,
      p_conferma_carico_d.d_data_bolla,
      p_conferma_carico_d.d_numero_riga_bolla,
      p_conferma_carico_d.d_codice_magazzino,
      p_conferma_carico_d.d_articolo_codice,
      p_conferma_carico_d.d_articolo_diff,
      p_conferma_carico_d.d_stato,
      p_conferma_carico_d.d_imballo_cessione,
      p_conferma_carico_d.d_imballo_fornitore,
      p_conferma_carico_d.d_pallet_piani,
      p_conferma_carico_d.d_pallet_imballi_piano,
      p_conferma_carico_d.d_pallet_imballi,
      p_conferma_carico_d.d_quantita_car_pz,
      p_conferma_carico_d.d_quantita_car_kg,
      p_conferma_carico_d.d_quantita_car_colli,
      p_conferma_carico_d.d_quantita_da_car_pz,
      p_conferma_carico_d.d_quantita_da_car_kg,
      p_conferma_carico_d.d_quantita_da_car_colli,
      p_conferma_carico_d.d_cauzione_codice,
      p_conferma_carico_d.d_cauzione_qta_resa,
      p_conferma_carico_d.d_codice_ean,
      p_conferma_carico_d.d_causale_testata,
      p_conferma_carico_d.d_causale_riga,
      p_conferma_carico_d.d_flag_sostanza_zuccherina,
      p_conferma_carico_d.d_qta_ordinata_omaggio_colli,
      p_conferma_carico_d.d_qta_ricevuta_omaggio_colli,
      p_conferma_carico_d.d_qta_ricevuta_omaggio_pz_kg,
      p_conferma_carico_d.d_costo_netto_unitario,
      G_LOAD_ID,
      p_conferma_carico_d.load_id,
      p_conferma_carico_d.codice_errore,
      SYSDATE,
      C_INS_MODULO_MVR,
      C_INS_MODULO_MVR,
      SYSDATE,
      C_INS_MODULO_MVR,
      C_INS_MODULO_MVR,
      SYSDATE,
      p_conferma_carico_d.d_username_operatore,
      p_conferma_carico_d.d_stato_riga_carico
    );
    G_CONT_RIGHE:= G_CONT_RIGHE + 1;
  end sp_insert_carico_log_in_d;
  
 -------------------------------------------------------------------------------------------------

  procedure sp_caricamento_mover
  as
    l_msg 		        VARCHAR2(1024);
    l_last_load_id    NUMBER;
  begin
  
    IF G_LOAD_ID IS NULL
    THEN
          select SQ_LOAD_ID_CARICHI.nextval into G_LOAD_ID from dual;
          G_LOGGER.load_id := G_LOAD_ID;
    END IF;
  
    G_LOGGER.log_info('SP_CARICAMENTO_MOVER iniziata - import carichi da MOVER [CONFERMA_CARICO_T/D]', 'SP_CARICAMENTO_MOVER');
    
    select loadid into G_LOAD_ID_MODX
    from   db_modxloaxtab
    where  modulo_id        = 'MOVER2MORE_CARICHI'
    and    table_name       = 'CARICHI';
    
      for testate in cur_conferma_carico_t
      loop
      
        sp_insert_carico_log_in_t(testate);
        
        for righe in cur_conferma_carico_d(testate.d_numero_carico, testate.d_codice_magazzino, testate.load_id)
        loop
          sp_insert_carico_log_in_d(righe);
        end loop righe;
      
      end loop testate;
      
    G_LOGGER.log_info('testate caricate:= ' || G_CONT_TESTATE || ' - righe caricate:= ' || G_CONT_RIGHE, 'SP_CARICAMENTO_MOVER', NULL, 'G_CONT_TESTATE:= '|| G_CONT_TESTATE, 'G_CONT_RIGHE:= '|| G_CONT_RIGHE);
     
    G_CONT_TESTATE:=0; 
    G_CONT_RIGHE:=0;
    
    select max(load_id_mvr) into l_last_load_id from carico_log_in_t;
    update db_modxloaxtab set loadid = l_last_load_id where modulo_id = 'MOVER2MORE_CARICHI'; 
    
    commit;
    G_LOGGER.log_info('Eseguita COMMIT - IMPORT DA MOVER', 'SP_MAIN');
      
    G_LOGGER.log_info('SP_CARICAMENTO_MOVER finita', 'SP_CARICAMENTO_MOVER');
        
  exception
  when others then
    ROLLBACK;
    l_msg := 'SP_CARICAMENTO_MOVER eccezione: import carichi da MOVER [CONFERMA_CARICO_T/D]' || SQLERRM;
    G_LOGGER.log_error(l_msg, 'SP_CARICAMENTO_MOVER', NULL, NULL, NULL, '-20050');
    RAISE;   
  end sp_caricamento_mover;

  procedure sp_caricamento_comm(p_load_id IN NUMBER)
  as
    l_msg 		        VARCHAR2(1024);
    l_d_carichi_t_id  NUMBER:=0;
  begin
    G_LOGGER.load_id := p_load_id;
    G_LOGGER.log_info('SP_CARICAMENTO_COMM iniziata - import carichi da DCC [CARICHI_LOG_IN_T/D] in COMM [D2M_CARICHI_T/R]', 'SP_CARICAMENTO_COMM');
    
    IF (p_load_id IS NULL) THEN
      raise_application_error(-20001, 'LOAD_ID non pu� essere NULL');
    END IF;
    
      for testate in cur_carico_log_in_t(p_load_id)
      loop
         select comm_sq_d2m_carichi_t.nextval into l_d_carichi_t_id from DUAL;
         
         sp_insert_d2m_carico_t(l_d_carichi_t_id,testate);--inserisco le testate in D2M
         
         for righe in cur_carico_log_in_d(testate.d_numero_carico, testate.d_codice_magazzino, testate.load_id)
         loop
            sp_insert_d2m_carico_r(l_d_carichi_t_id,testate,righe);--inserisco le righe in D2M
         end loop righe;
      
      end loop testate;
      
    G_LOGGER.log_info('testate caricate:= ' || G_CONT_TESTATE || ' - righe caricate:= ' || G_CONT_RIGHE, 'SP_CARICAMENTO_COMM', NULL, 'G_CONT_TESTATE:= '|| G_CONT_TESTATE, 'G_CONT_RIGHE:= '|| G_CONT_RIGHE);
     
    G_CONT_TESTATE:=0; 
    G_CONT_RIGHE:=0;
    
    commit;
    G_LOGGER.log_info('Eseguita COMMIT - EXPORT VERSO COMM', 'SP_MAIN');
      
    G_LOGGER.log_info('SP_CARICAMENTO_COMM finita', 'SP_CARICAMENTO_COMM');
  
  exception
  when others then
    ROLLBACK;
    l_msg := 'SP_CARICAMENTO_COMM eccezione: import carichi da DCC [CARICHI_LOG_IN_T/D] in COMM [D2M_CARICHI_T/R]' || SQLERRM;
    G_LOGGER.log_error(l_msg, 'SP_CARICAMENTO_COMM', NULL, NULL, NULL, '-20070');
    RAISE;   
  end sp_caricamento_comm;

  procedure sp_main
  as
    l_msg 		        VARCHAR2(1024);
  begin
    select SQ_LOAD_ID_CARICHI.nextval into G_LOAD_ID from dual;
    G_LOGGER.load_id := G_LOAD_ID;
    
    G_LOGGER.log_info('SP_MAIN iniziata', 'SP_MAIN');
    
    SP_CARICAMENTO_MOVER;
    
    SP_CARICAMENTO_COMM(G_LOAD_ID);
    
    G_LOGGER.log_info('SP_MAIN finita', 'SP_MAIN');
    
  exception
  when others then
    
    l_msg :=  C_NOME_MODULO || ' eccezione - ' || SQLCODE || ' - ' || SQLERRM;
    G_LOGGER.log_error(l_msg, 'SP_MAIN');
    
    RAISE;
    --send_mail('[ERR] FLUSSO LOAD PARAMETRI CLIENTE BLOCCATO', l_msg);
  
  end sp_main;

end pkg_load_conferma_carichi_mvr;
/