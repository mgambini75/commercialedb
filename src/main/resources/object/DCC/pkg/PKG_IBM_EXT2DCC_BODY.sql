create or replace PACKAGE BODY PKG_IBM_EXT2DCC AS

C_NOME_MODULO                CONSTANT VARCHAR2(30)  := 'PKG_IBM_EXT2DCC';
C_INS_MODULO                 CONSTANT VARCHAR2(30)  := 'PRICING_IBM2DCC';
C_TABLE_NAME                 CONSTANT VARCHAR2(30)  := 'NSC_LST_VND';

G_LOGGER                     TYP_LOGGER_NEW         := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE send_mail(Oggetto IN VARCHAR2, Messaggio IN VARCHAR2)
  AS
    mailToAddr VARCHAR2(128);
    v_email_mittente DB_FIELDDEFVAL.FIELD_DEFVAL%TYPE := FN_GET_DEFVAL ('BRC_DCC', 'SEND_MAIL', 'MITTENTE');
    CURSOR mailToCur
    IS
      SELECT FIELD_DEFVAL
      FROM db_fielddefval
      WHERE field_name LIKE 'PRICING_IBM_%'
      AND DB_OWNER   = 'BRC_DCC'
      AND TABLE_NAME = 'SEND_MAIL';
  BEGIN
    IF FN_GET_DEFVAL('BRC_DCC','SEND_MAIL','FLAG_ORACLE_MAIL') = 'S' THEN
      OPEN mailToCur;
      LOOP
        FETCH mailToCur INTO mailToAddr;
        EXIT
      WHEN mailToCur%NOTFOUND;
        DITECH_MONITOR.SEND_MAIL(v_sender => v_email_mittente, v_receiver => mailToAddr , v_subject => Oggetto, v_message => Messaggio);
      END LOOP;
      CLOSE mailToCur;
    END IF;
  END; 

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_CARICAMENTO(p_load_id IN NUMBER)
  AS
    l_msg                    VARCHAR2(1024);
  BEGIN
      G_LOGGER.log_info('SP_CARICAMENTO iniziata - caricamento da DCC.F_PRICING_IBM2DCC a DCC.LISTINO_VEN_R','SP_CARICAMENTO');
  
      INSERT INTO LISTINO_VEN_R
        (
          d_listino_ven_r_id,
          d_listino_ven_t_codice,
          d_articolo_codice,
          d_da_data,
          d_prezzo,
          load_id,
          ins_utente,
          ins_data,
          ins_modulo,
          upd_modulo,
          upd_data,
          VERSION
        )
      SELECT
          SQ_LISTINO_VEN_R.nextval,
          listino_ven_t_codice, --Customer Location Key
          articolo_codice, --Sellable Item Key 
          TO_DATE(data_validita, 'YYYY-MM-DD'), --Price Effective Date 
          TO_NUMBER(prezzo), --Unit Base Price
          p_load_id,
          scenario,
          SYSDATE,
          C_INS_MODULO,
          C_INS_MODULO,
          SYSDATE,
          SYSDATE
      FROM F_PRICING_IBM2DCC;
      
      G_LOGGER.log_info('Eseguito INSERT DCC.LISTINO_VEN_R per: ' || SQL%ROWCOUNT || ' rec', 'SP_CARICAMENTO');
  
  EXCEPTION
  WHEN OTHERS THEN
      l_msg := 'eccezione caricamento da DCC.F_PRICING_IBM2DCC a DCC.LISTINO_VEN_R';
      G_LOGGER.log_error(l_msg, 'SP_CARICAMENTO', NULL, NULL, '-20010');
      raise_application_error(-20010, l_msg);
  END SP_CARICAMENTO;

  PROCEDURE SP_MAIN
  AS
    l_msg                    VARCHAR2(1024);
    l_load_id                NUMBER;
  BEGIN
      G_LOGGER.log_info('SP_MAIN iniziata', 'SP_MAIN');
        
      SP_SET_MODXLOAXTAB(C_INS_MODULO, C_TABLE_NAME, l_load_id);
  
      IF l_load_id IS NOT NULL
      THEN
        G_LOGGER.load_id := l_load_id;
        
        SP_CARICAMENTO(l_load_id);
        COMMIT;
        
        G_LOGGER.log_info('Eseguita COMMIT', 'SP_MAIN');
        
        SP_RELEASE_MODXLOAXTAB(C_INS_MODULO, C_TABLE_NAME);
        
        l_msg := 'importato correttamente flusso IBM nel DCC - LOAD_ID:=' || l_load_id;
        send_mail('[OK] FLUSSO CARICAMENTO [FILE] LISTINI IBM', l_msg);
      END IF;
      
      G_LOGGER.log_info('SP_MAIN finita', 'SP_MAIN');
        
  EXCEPTION
  WHEN OTHERS THEN
    l_msg := 'SP_MAIN eccezione ' || DBMS_UTILITY.FORMAT_ERROR_STACK;
    G_LOGGER.log_error(l_msg, 'SP_MAIN');
    
    ROLLBACK;
    G_LOGGER.log_info('Eseguita ROLLBACK', 'SP_MAIN');
    
    send_mail('[ERR] FLUSSO CARICAMENTO [FILE] LISTINI IBM BLOCCATO', l_msg);
    RAISE;
  END SP_MAIN;

END PKG_IBM_EXT2DCC;
/