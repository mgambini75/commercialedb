create or replace PACKAGE BODY PKG_LOAD_PARAMETRI_CLIENTE AS

C_NOME_MODULO             CONSTANT VARCHAR2(30)  := 'PKG_LOAD_PARAMETRI_CLIENTE';

G_LOGGER                  TYP_LOGGER_NEW         := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

C_FLAG_DA_PREP            CHAR(1)                := '1';
C_FLAG_OK                 CHAR(1)                := '2';
C_FLAG_ERR                CHAR(1)                := '3';
C_FLAG_IN_ELAB            CHAR(1)                := 'E';

G_CONT_RIGHE_TOTALI       NUMBER                 := 0;
G_CONT_RIGHE_CARICATE     NUMBER                 := 0;
G_CONT_RIGHE_SCARTATE     NUMBER                 := 0;

------------------------------------------------------------------------------------------------------------------
  
  TYPE mod_calc_przces_type
  IS TABLE OF comm_modalita_calc_przces%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE documento_gestionale_type
  IS TABLE OF ncl_documento_gestionale%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE tipo_divulg_etich_type
  IS TABLE OF comm_tipo_divulg_etich%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE dati_formato_elettr_type
  IS TABLE OF comm_dati_formato_elettr%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE formato_divulg_type
  IS TABLE OF comm_formato_divulgazione%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE stile_layout_listini_type
  IS TABLE OF comm_stile_layout_listini%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE tipo_copia_conforme_type
  IS TABLE OF comm_tipo_copia_conforme%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE gestione_cauzione_type
  IS TABLE OF ncl_gestione_cauzione%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE critero_ordinamento_type
  IS TABLE OF comm_critero_ordinamento%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE criterio_separazione_type
  IS TABLE OF comm_criterio_separazione%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE tipo_punto_consegna_type
  IS TABLE OF comm_tipo_punto_consegna%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  TYPE tipo_rapp_amm_cfg_type
  IS TABLE OF comm_tipo_rapp_amm_cfg%ROWTYPE
  INDEX BY VARCHAR2(50);
  
  
  CURSOR cur_parametri_cliente IS
   SELECT parametri_cliente.*, core_interloc.interlocutore_id
   FROM parametri_cliente
   LEFT JOIN ncl_interlocutore core_interloc ON (core_interloc.codice = parametri_cliente.d_interlocutore_codice AND core_interloc.ruolo_cliente = 'S' AND core_interloc.annullato = 'N') 
   WHERE flag_elab = '1'
   ORDER BY parametri_cliente.d_interlocutore_codice, TO_NUMBER(parametri_cliente.d_anno), TO_NUMBER(parametri_cliente.d_mese), TO_NUMBER(parametri_cliente.d_giorno);
   
------------------------------------------------------------------------------------------------------------------

  g_mod_calc_przces_list mod_calc_przces_type;
  g_documento_gestionale_list documento_gestionale_type;
  g_tipo_divulg_etich_list tipo_divulg_etich_type;
  g_dati_formato_elettr_list dati_formato_elettr_type;
  g_formato_divulg_list formato_divulg_type;
  g_stile_layout_listini_list stile_layout_listini_type;
  g_tipo_copia_conforme_list tipo_copia_conforme_type;
  g_gestione_cauzione_list gestione_cauzione_type;
  g_critero_ordinamento_list critero_ordinamento_type;
  g_criterio_separazione_list criterio_separazione_type;
  g_tipo_punto_consegna_list tipo_punto_consegna_type;
  g_tipo_rapp_amm_cfg_list tipo_rapp_amm_cfg_type;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE send_mail(Oggetto IN VARCHAR2, Messaggio IN VARCHAR2)
  IS
    mailToAddr VARCHAR2(128);
    v_email_mittente DB_FIELDDEFVAL.FIELD_DEFVAL%TYPE := FN_GET_DEFVAL ('BRC_DCC', 'SEND_MAIL', 'MITTENTE');
    CURSOR mailToCur
    IS
      SELECT FIELD_DEFVAL
      FROM db_fielddefval
      WHERE field_name LIKE 'ORDINI_CLIENTI_DEST_%'
      AND DB_OWNER   = 'BRC_DCC'
      AND TABLE_NAME = 'SEND_MAIL';
  BEGIN
    IF FN_GET_DEFVAL('BRC_DCC','SEND_MAIL','FLAG_ORACLE_MAIL') = 'S' THEN
      OPEN mailToCur;
      LOOP
        FETCH mailToCur INTO mailToAddr;
        EXIT
      WHEN mailToCur%NOTFOUND;
        DITECH_MONITOR.SEND_MAIL(v_sender => v_email_mittente, v_receiver => mailToAddr , v_subject => Oggetto, v_message => Messaggio);
      END LOOP;
      CLOSE mailToCur;
    END IF;
  END send_mail; 
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_INIT
  IS
  BEGIN
   G_LOGGER.log_info('SP_INIT iniziata', 'SP_INIT');

   FOR rec_mod_calc_przces IN (SELECT * FROM comm_modalita_calc_przces)
   LOOP
      g_mod_calc_przces_list(rec_mod_calc_przces.codice) := rec_mod_calc_przces;
   END LOOP;
   
   FOR rec_documento_gestionale IN (SELECT * FROM ncl_documento_gestionale)
   LOOP
      g_documento_gestionale_list(rec_documento_gestionale.codice) := rec_documento_gestionale;
   END LOOP;
   
   FOR rec_tipo_divulg_etich IN (SELECT * FROM comm_tipo_divulg_etich)
   LOOP
      g_tipo_divulg_etich_list(rec_tipo_divulg_etich.codice) := rec_tipo_divulg_etich;
   END LOOP;
   
   FOR rec_dati_formato_elettr IN (SELECT * FROM comm_dati_formato_elettr)
   LOOP
      g_dati_formato_elettr_list(rec_dati_formato_elettr.codice) := rec_dati_formato_elettr;
   END LOOP;
   
   FOR rec_formato_divulg IN (SELECT * FROM comm_formato_divulgazione)
   LOOP
      g_formato_divulg_list(rec_formato_divulg.codice) := rec_formato_divulg;
   END LOOP;
   
   FOR rec_stile_layout_listini IN (SELECT * FROM comm_stile_layout_listini)
   LOOP
      g_stile_layout_listini_list(rec_stile_layout_listini.codice) := rec_stile_layout_listini;
   END LOOP;
   
   FOR rec_tipo_copia_conforme_list IN (SELECT * FROM comm_tipo_copia_conforme)
   LOOP
      g_tipo_copia_conforme_list(rec_tipo_copia_conforme_list.codice) := rec_tipo_copia_conforme_list;
   END LOOP;
   
   FOR rec_gestione_cauzione_list IN (SELECT * FROM ncl_gestione_cauzione)
   LOOP
      g_gestione_cauzione_list(rec_gestione_cauzione_list.codice) := rec_gestione_cauzione_list;
   END LOOP;
   
   FOR rec_critero_ordinamento_list IN (SELECT * FROM comm_critero_ordinamento)
   LOOP
      g_critero_ordinamento_list(rec_critero_ordinamento_list.codice) := rec_critero_ordinamento_list;
   END LOOP;
   
   FOR rec_criterio_separazione_list IN (SELECT * FROM comm_criterio_separazione)
   LOOP
      g_criterio_separazione_list(rec_criterio_separazione_list.codice) := rec_criterio_separazione_list;
   END LOOP;
   
   FOR rec_tipo_punto_consegna_list IN (SELECT * FROM comm_tipo_punto_consegna)
   LOOP
      g_tipo_punto_consegna_list(rec_tipo_punto_consegna_list.codice) := rec_tipo_punto_consegna_list;
   END LOOP;
   
   FOR rec_tipo_rapp_amm_cfg_list IN (SELECT * FROM comm_tipo_rapp_amm_cfg)
   LOOP
      g_tipo_rapp_amm_cfg_list(rec_tipo_rapp_amm_cfg_list.codice) := rec_tipo_rapp_amm_cfg_list;
   END LOOP;
   
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('SP_INIT eccezione: errore inizializzazione variabili TYPE', 'SP_INIT');
    RAISE;
  END SP_INIT;
  
------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_MODALITA_CALC_PRZCES_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
   IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_modalita_calc_przces_codice � NULL', 'FN_GET_MODALITA_CALC_PRZCES_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
   ELSE
      RETURN g_mod_calc_przces_list(p_codice).modalita_calc_przces_id;
   END IF;
  EXCEPTION
  WHEN OTHERS THEN
   G_LOGGER.log_error('ID non trovato','FN_GET_MODALITA_CALC_PRZCES_ID', NULL, 'p_codice:=' || p_codice, '-20010', 'p_interlocutore_codice:=' || p_interlocutore_codice);
   raise_application_error(-20010, 'modalita_calc_przces_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_MODALITA_CALC_PRZCES_ID;

------------------------------------------------------------------------------------------------------------------

  -- TODO: GESTIRE DOCUMENTO GESTIONALE
  FUNCTION FN_GET_DOCUMENTO_GESTIONALE_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_documento_gestionale_codice � NULL', 'FN_GET_DOCUMENTO_GESTIONALE_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_documento_gestionale_list(p_codice).documento_gestionale_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_TIPO_DIVULG_ETICH_ID', NULL, 'p_codice:=' || p_codice, '-20011', 'p_interlocutore_codice:=' || p_interlocutore_codice);
   raise_application_error(-20011, 'documento_gestionale_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_DOCUMENTO_GESTIONALE_ID;

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_TIPO_DIVULG_ETICH_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_tipo_divulg_etich_codice � NULL', 'FN_GET_TIPO_DIVULG_ETICH_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_tipo_divulg_etich_list(p_codice).tipo_divulg_etich_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato','FN_GET_TIPO_DIVULG_ETICH_ID', NULL, 'p_codice:=' || p_codice, '-20012', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20012, 'tipo_divulg_etich_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_TIPO_DIVULG_ETICH_ID;

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_DATI_FORMATO_ELETTR_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_dati_formato_elettr_codice � NULL', 'FN_GET_DATI_FORMATO_ELETTR_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_dati_formato_elettr_list(p_codice).dati_formato_elettr_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_DATI_FORMATO_ELETTR_ID', NULL, 'p_codice:=' || p_codice, '-20013', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20013, 'dati_formato_elettr_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_DATI_FORMATO_ELETTR_ID;

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_FORMATO_DIVULGAZIONE_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      RETURN g_formato_divulg_list('N').formato_divulgazione_id;
    ELSE
      RETURN g_formato_divulg_list(p_codice).formato_divulgazione_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_FORMATO_DIVULGAZIONE_ID', NULL, 'p_codice:=' || p_codice, '-20014', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20014, 'stile_layout_listini_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_FORMATO_DIVULGAZIONE_ID; 

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_STILE_LAYOUT_LISTINI_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_stile_layout_listini_codice � NULL', 'FN_GET_STILE_LAYOUT_LISTINI_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_stile_layout_listini_list(p_codice).stile_layout_listini_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_STILE_LAYOUT_LISTINI_ID', NULL, 'p_codice:=' || p_codice, '-20015', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20015, 'stile_layout_listini_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_STILE_LAYOUT_LISTINI_ID; 

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_TIPO_COPIA_CONFORME_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL) THEN
      G_LOGGER.log_info('d_tipo_copia_conforme_codice � NULL', 'FN_GET_TIPO_COPIA_CONFORME_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_tipo_copia_conforme_list(p_codice).tipo_copia_conforme_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_TIPO_COPIA_CONFORME_ID', NULL, 'p_codice:=' || p_codice, '-20016', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20016, 'tipo_copia_conforme_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_TIPO_COPIA_CONFORME_ID;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_DATA_VALIDO_DAL(p_anno IN VARCHAR2, p_mese IN VARCHAR2, p_giorno IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN DATE
  IS
  BEGIN
    IF(p_anno IS NULL OR p_mese IS NULL OR p_giorno IS NULL)
    THEN
      G_LOGGER.log_info('d_anno o d_mese o d_giorno � NULL', 'FN_GET_DATA_VALIDO_DAL', NULL, 'p_anno:=' || p_anno || ' p_mese=' || p_mese || ' p_giorno:=' || p_giorno, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN TO_DATE(p_anno || '/' || p_mese || '/' || p_giorno, 'YYYY/mm/DD');
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('data non valida', 'FN_GET_DATA_VALIDO_DAL', NULL, 'p_anno:=' || p_anno || ' p_mese:=' || p_mese || ' p_giorno:=' || p_giorno, '-20017', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20017, 'data non valida p_anno:=' || p_anno || ' p_mese:=' || p_mese ||  ' p_giorno:=' || p_giorno || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_DATA_VALIDO_DAL;

------------------------------------------------------------------------------------------------------------------

  FUNCTION FN_GET_VALORE(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN CHAR
  IS
    BEGIN
    IF(p_codice IS NULL OR p_codice = '0' OR p_codice = 'N')
    THEN
      RETURN 'N';
    ELSIF (p_codice = '1' OR p_codice = 'S')
    THEN
      RETURN 'S';
    ELSE
      G_LOGGER.log_error('valore:=' || p_codice || ' non consentito', 'FN_GET_VALORE', NULL, 'p_codice:=' || p_codice , '-20018', 'p_interlocutore_codice:=' || p_interlocutore_codice);
      raise_application_error(-20018, 'valore:=' || p_codice || ' non consentito - interlocutore_codice:=' || p_interlocutore_codice);
    END IF;
  END FN_GET_VALORE;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_GESTIONE_CAUZIONE_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL)
    THEN
      G_LOGGER.log_info('d_gestione_cauzione � NULL', 'FN_GET_GESTIONE_CAUZIONE_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_gestione_cauzione_list(p_codice).gestione_cauzione_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_GESTIONE_CAUZIONE_ID', NULL, 'p_codice:=' || p_codice, '-20019', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20019, 'gestione_cauzione_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_GESTIONE_CAUZIONE_ID;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_CRITERO_ORDINAMENTO_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL)
    THEN
      G_LOGGER.log_info('d_critero_ordinamento � NULL', 'FN_GET_CRITERO_ORDINAMENTO_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_critero_ordinamento_list(p_codice).critero_ordinamento_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_CRITERO_ORDINAMENTO_ID', NULL, 'p_codice:=' || p_codice, '-20021', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20020, 'critero_ordinamento_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_CRITERO_ORDINAMENTO_ID;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_CRITERIO_SEPARAZIONE_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL)
    THEN
      G_LOGGER.log_info('d_criterio_separazione � NULL', 'FN_GET_CRITERIO_SEPARAZIONE_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_criterio_separazione_list(p_codice).criterio_separazione_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_CRITERIO_SEPARAZIONE_ID', NULL, 'p_codice:=' || p_codice, '-20021', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20021, 'gestione_cauzione_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_CRITERIO_SEPARAZIONE_ID;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_TIPO_PUNTO_CONSEGNA_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL)
    THEN
      G_LOGGER.log_info('d_tipo_punta_consegna � NULL', 'FN_GET_TIPO_PUNTO_CONSEGNA_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_tipo_punto_consegna_list(p_codice).tipo_punto_consegna_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_TIPO_PUNTO_CONSEGNA_ID', NULL, 'p_codice:=' || p_codice, '-20022', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20022, 'gestione_cauzione_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_TIPO_PUNTO_CONSEGNA_ID;

------------------------------------------------------------------------------------------------------------------
  
  FUNCTION FN_GET_TIPO_RAPP_AMM_CFG_ID(p_codice IN VARCHAR2, p_interlocutore_codice IN VARCHAR2) RETURN NUMBER
  IS
  BEGIN
    IF(p_codice IS NULL)
    THEN
      G_LOGGER.log_info('d_tipo_rapp_amm_cfg � NULL', 'FN_GET_TIPO_RAPP_AMM_CFG_ID', NULL, NULL, NULL, 'p_interlocutore_codice:=' || p_interlocutore_codice);
      RETURN NULL;
    ELSE
      RETURN g_tipo_rapp_amm_cfg_list(p_codice).tipo_rapp_amm_cfg_id;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    G_LOGGER.log_error('ID non trovato', 'FN_GET_TIPO_RAPP_AMM_CFG_ID', NULL, 'p_codice:=' || p_codice, '-20023', 'p_interlocutore_codice:=' || p_interlocutore_codice);
    raise_application_error(-20023, 'tipo_rapp_amm_cfg_id:=' || p_codice || ' - interlocutore_codice:=' || p_interlocutore_codice);
  END FN_GET_TIPO_RAPP_AMM_CFG_ID;

------------------------------------------------------------------------------------------------------------------

  FUNCTION SP_TRANSCODIFICA(rec_parametri_cliente IN cur_parametri_cliente%ROWTYPE) RETURN comm_parametri_cliente%ROWTYPE
  IS
     l_comm_parametri_cliente  comm_parametri_cliente%ROWTYPE;
  BEGIN
     l_comm_parametri_cliente.interlocutore_id           := rec_parametri_cliente.interlocutore_id;
     l_comm_parametri_cliente.modalita_calc_przces_id    := FN_GET_MODALITA_CALC_PRZCES_ID(rec_parametri_cliente.d_modalita_calc_przces_codice,rec_parametri_cliente.d_interlocutore_codice);
     --l_comm_parametri_cliente.documento_gestionale_id    := FN_GET_DOCUMENTO_GESTIONALE_ID(rec_parametri_cli.d_documento_gestionale_codice,rec_parametri_cliente.d_interlocutore_codice); FIX ME aggiungere NCL dato
     l_comm_parametri_cliente.tipo_divulg_etich_id       := FN_GET_TIPO_DIVULG_ETICH_ID(rec_parametri_cliente.d_tipo_divulg_etich_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.dati_formato_elettr_id     := FN_GET_DATI_FORMATO_ELETTR_ID(rec_parametri_cliente.d_dati_formato_elettr_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.fmt_listino_da_banco_id    := FN_GET_FORMATO_DIVULGAZIONE_ID(rec_parametri_cliente.d_fmt_listino_banco_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.fmt_listino_articoli_id    := FN_GET_FORMATO_DIVULGAZIONE_ID(rec_parametri_cliente.d_fmt_listino_articoli_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.fmt_listino_offerte_id     := FN_GET_FORMATO_DIVULGAZIONE_ID(rec_parametri_cliente.d_fmt_listino_offerte_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.stile_layout_listini_id    := FN_GET_STILE_LAYOUT_LISTINI_ID(rec_parametri_cliente.d_stile_layout_listini_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.tipo_copia_conforme_id     := FN_GET_TIPO_COPIA_CONFORME_ID(rec_parametri_cliente.d_tipo_copia_conforme_codice,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.valido_dal                 := FN_GET_DATA_VALIDO_DAL(rec_parametri_cliente.d_anno,rec_parametri_cliente.d_mese,rec_parametri_cliente.d_giorno,rec_parametri_cliente.d_interlocutore_codice);
      
     l_comm_parametri_cliente.flag_divulg_offerte        := FN_GET_VALORE(rec_parametri_cliente.d_flag_divulg_offerte,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_divulg_fidelity       := FN_GET_VALORE(rec_parametri_cliente.d_flag_divulg_fidelity,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_divulg_fornitori      := FN_GET_VALORE(rec_parametri_cliente.d_flag_divulg_fornitori,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_invio_fatt_email      := FN_GET_VALORE(rec_parametri_cliente.d_flag_invio_fatt_email,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_invio_inev_email      := FN_GET_VALORE(rec_parametri_cliente.d_flag_invio_inev_email,rec_parametri_cliente.d_interlocutore_codice);
      
     l_comm_parametri_cliente.flag_libro_guida_diretti   := FN_GET_VALORE(rec_parametri_cliente.d_flag_libro_guida_diretti,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_gest_varianti_ordcli  := FN_GET_VALORE(rec_parametri_cliente.d_flag_gest_varianti_ordcli,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_gest_sost_ordcli      := FN_GET_VALORE(rec_parametri_cliente.d_flag_gest_sost_ordcli,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_gest_ass_ordcli       := FN_GET_VALORE(rec_parametri_cliente.d_flag_gest_ass_ordcli,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.flag_ctr_przces_ordcli     := FN_GET_VALORE(rec_parametri_cliente.d_flag_ctr_przces_ordcli,rec_parametri_cliente.d_interlocutore_codice);
     
     l_comm_parametri_cliente.perc_plt_pieno             := rec_parametri_cliente.d_perc_plt_pieno;
     
     l_comm_parametri_cliente.gestione_cauzione_id       := FN_GET_GESTIONE_CAUZIONE_ID(rec_parametri_cliente.d_gestione_cauzione,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.critero_ordinamento_id     := FN_GET_CRITERO_ORDINAMENTO_ID(rec_parametri_cliente.d_critero_ordinamento,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.criterio_separazione_id    := FN_GET_CRITERIO_SEPARAZIONE_ID(rec_parametri_cliente.d_criterio_separazione,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.tipo_punto_consegna_id     := FN_GET_TIPO_PUNTO_CONSEGNA_ID(rec_parametri_cliente.d_tipo_punto_consegna,rec_parametri_cliente.d_interlocutore_codice);
     l_comm_parametri_cliente.tipo_rapp_amm_cfg_id       := FN_GET_TIPO_RAPP_AMM_CFG_ID(rec_parametri_cliente.d_tipo_rapp_amm_cfg,rec_parametri_cliente.d_interlocutore_codice);
     
     RETURN l_comm_parametri_cliente;
  
  END SP_TRANSCODIFICA;
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE VALIDA_PARAMETRI_CLIENTE_COMM(p_rec_comm_parametri_cliente IN comm_parametri_cliente%ROWTYPE) 
  IS
  BEGIN
      IF (p_rec_comm_parametri_cliente.modalita_calc_przces_id IS NULL
      --OR TODO l_documento_gestionale_id                 IS NULL
      OR p_rec_comm_parametri_cliente.tipo_divulg_etich_id    IS NULL
      OR p_rec_comm_parametri_cliente.dati_formato_elettr_id  IS NULL
      OR p_rec_comm_parametri_cliente.fmt_listino_da_banco_id IS NULL
      OR p_rec_comm_parametri_cliente.fmt_listino_articoli_id IS NULL
      OR p_rec_comm_parametri_cliente.fmt_listino_offerte_id  IS NULL
      --OR p_rec_comm_parametri_cliente.stile_layout_listini_id IS NULL -- TODO: PUO ESSERE NULL
      OR p_rec_comm_parametri_cliente.tipo_copia_conforme_id  IS NULL
      OR p_rec_comm_parametri_cliente.valido_dal              IS NULL)
     THEN
       G_LOGGER.log_error('un id � NULL', 'VALIDA_PARAMETRI_CLIENTE_COMM', NULL, NULL, '-20030');
       raise_application_error(-20030, 'un id � NULL '  || 'non prevista');
     END IF;
  END VALIDA_PARAMETRI_CLIENTE_COMM;

------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_TRADUZIONE(p_load_id IN NUMBER)
  IS
      l_msg                    VARCHAR2(200);
  BEGIN
      G_LOGGER.log_info('SP_TRADUZIONE iniziata - caricamento da AS400 a DCC.PARAMETRI_CLIENTE', 'SP_TRADUZIONE', NULL);

      UPDATE FDB8SEOF
      SET    DB8ST1 = C_FLAG_IN_ELAB,
             DB8IDM = p_load_id
      WHERE  DB8ST1 = C_FLAG_DA_PREP;
      G_LOGGER.log_info('Eseguito UPDATE FDB8SEOF (DB8ST1 da 1 a ' || C_FLAG_IN_ELAB || ') per: '|| SQL%ROWCOUNT || ' rec', 'SP_TRADUZIONE', NULL);
      
      COMMIT;
      G_LOGGER.log_info('Eseguita COMMIT (AS400)', 'SP_TRADUZIONE', NULL);
      
      BEGIN
        INSERT INTO parametri_cliente
          (
            d_parametri_cliente_id,
            d_interlocutore_codice,
            d_societa_cedente_codice,
            d_ente_deposito_codice,
            d_anno,
            d_mese,
            d_giorno,
            d_modalita_calc_przces_codice,
            d_documento_gestionale_codice,
            d_tipo_divulg_etich_codice,
            d_flag_divulg_offerte,
            d_flag_divulg_fidelity,
            d_flag_divulg_fornitori,
            d_dati_formato_elettr_codice,
            d_flag_invio_fatt_email,
            d_flag_invio_inev_email,
            d_fmt_listino_banco_codice,
            d_fmt_listino_articoli_codice,
            d_fmt_listino_offerte_codice,
            d_flag_libro_guida_diretti,
            d_stile_layout_listini_codice,
            d_flag_gest_varianti_ordcli,
            d_flag_gest_sost_ordcli,
            d_flag_gest_ass_ordcli,
            d_flag_ctr_przces_ordcli,
            d_tipo_copia_conforme_codice,
            d_perc_plt_pieno,
            d_stato,
            d_gestione_cauzione,
            d_critero_ordinamento,
            d_criterio_separazione,
            d_tipo_punto_consegna,
            d_tipo_rapp_amm_cfg,
            flag_elab,
            load_id,
            ins_data,
            ins_utente,
            ins_modulo,
            upd_data,
            upd_utente,
            upd_modulo,
            VERSION
          )
        SELECT
            sq_parametri_cliente.nextval,
            TRIM(DB8CDF),
            TRIM(DB8CDE),
            TRIM(DB8DEP),
            TRIM(DB8AVA),
            TRIM(DB8MVA),
            TRIM(DB8GVA),
            TRIM(DB8MOC),
            TRIM(DB8FAT),
            TRIM(DB8ETI),
            TRIM(DB8F16),
            TRIM(DB8F17),
            TRIM(DB8F18),
            TRIM(DB8FL4),
            TRIM(DB8F20),
            TRIM(DB8F19),
            TRIM(DB8F21),
            TRIM(DB8F11),
            TRIM(DB8F12),
            TRIM(DB8F13),
            TRIM(DB8F14),
            TRIM(DB8VAR),
            TRIM(DB8SOS),
            TRIM(DB8FL7),
            TRIM(DB8FL8),
            TRIM(DB8SAC),
            DB8RIE,
            TRIM(DB8STA),
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            '1',
            p_load_id,
            SYSDATE,
            C_NOME_MODULO,
            C_NOME_MODULO,
            SYSDATE,
            C_NOME_MODULO,
            C_NOME_MODULO,
            SYSDATE
            FROM  FDB8SEOF
            WHERE DB8ST1 = C_FLAG_IN_ELAB
            AND   DB8IDM = p_load_id ;
        G_LOGGER.log_info('Eseguito INSERT DCC.PARAMETRI_CLIENTE per: ' || SQL%ROWCOUNT || ' rec', 'SP_TRADUZIONE', NULL);
      
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        G_LOGGER.log_info('Eseguita ROLLBACK (ORACLE)', 'SP_TRADUZIONE', NULL);
        
        UPDATE FDB8SEOF
        SET    DB8ST1 = C_FLAG_DA_PREP,
               DB8IDM = 0
        WHERE  DB8ST1 = C_FLAG_IN_ELAB
        AND    DB8IDM = p_load_id ;
        G_LOGGER.log_info('Eseguito UPDATE FDB8SEOF (DB8ST1 da ''' || C_FLAG_IN_ELAB || ' a ' || C_FLAG_DA_PREP || ''') per: '|| SQL%ROWCOUNT || ' rec', 'SP_TRADUZIONE', NULL);
        
        COMMIT;
        G_LOGGER.log_info('Eseguita COMMIT (AS400)', 'SP_TRADUZIONE', NULL);
        RAISE;
      END;
  EXCEPTION
  WHEN OTHERS THEN
    l_msg := 'eccezione caricamento da AS400 a DCC.PARAMETRI_CLIENTE';
    G_LOGGER.log_error(l_msg, 'SP_TRADUZIONE', NULL, NULL, '-20002');
    ROLLBACK;
    raise_application_error(-20002, l_msg);
  
  END SP_TRADUZIONE;

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_INSERT_INTERLOC(p_rec_comm_parametri_cliente comm_parametri_cliente%ROWTYPE)
  IS
  BEGIN
    INSERT INTO comm_parametri_cliente
      (
         comm_parametri_cliente.parametri_cliente_id,
         comm_parametri_cliente.interlocutore_id,
         comm_parametri_cliente.modalita_calc_przces_id,
         comm_parametri_cliente.documento_gestionale_id,
         comm_parametri_cliente.tipo_divulg_etich_id,
         comm_parametri_cliente.flag_divulg_offerte,
         comm_parametri_cliente.flag_divulg_fidelity,
         comm_parametri_cliente.flag_divulg_fornitori,
         comm_parametri_cliente.flag_invio_fatt_email,
         comm_parametri_cliente.flag_invio_inev_email,
         comm_parametri_cliente.flag_libro_guida_diretti,
         comm_parametri_cliente.dati_formato_elettr_id,
         comm_parametri_cliente.fmt_listino_da_banco_id,
         comm_parametri_cliente.fmt_listino_articoli_id,
         comm_parametri_cliente.fmt_listino_offerte_id,
         comm_parametri_cliente.flag_gest_varianti_ordcli,
         comm_parametri_cliente.flag_gest_sost_ordcli,
         comm_parametri_cliente.flag_gest_ass_ordcli,
         comm_parametri_cliente.flag_ctr_przces_ordcli,
         comm_parametri_cliente.perc_plt_pieno,
         comm_parametri_cliente.stile_layout_listini_id,
         comm_parametri_cliente.tipo_copia_conforme_id,
         --comm_parametri_cliente.classe_sconto_t_id,
         comm_parametri_cliente.valido_dal,
         --comm_parametri_cliente.valido_al,
         comm_parametri_cliente.gestione_cauzione_id,
         comm_parametri_cliente.critero_ordinamento_id,
         comm_parametri_cliente.criterio_separazione_id,
         comm_parametri_cliente.tipo_punto_consegna_id,
         comm_parametri_cliente.tipo_rapp_amm_cfg_id,
         comm_parametri_cliente.ins_data,
         comm_parametri_cliente.ins_utente,
         comm_parametri_cliente.ins_modulo,
         comm_parametri_cliente.upd_data,
         comm_parametri_cliente.upd_utente,
         comm_parametri_cliente.upd_modulo,
         comm_parametri_cliente.version
      ) 
    VALUES
      (
         comm_sq_parametri_cliente.nextval,
         p_rec_comm_parametri_cliente.interlocutore_id,
         p_rec_comm_parametri_cliente.modalita_calc_przces_id,
         NULL, -- TODO l_documento_gestionale_id,
         p_rec_comm_parametri_cliente.tipo_divulg_etich_id,
         p_rec_comm_parametri_cliente.flag_divulg_offerte,
         p_rec_comm_parametri_cliente.flag_divulg_fidelity,
         p_rec_comm_parametri_cliente.flag_divulg_fornitori,
         p_rec_comm_parametri_cliente.flag_invio_fatt_email,
         p_rec_comm_parametri_cliente.flag_invio_inev_email,
         p_rec_comm_parametri_cliente.flag_libro_guida_diretti,
         p_rec_comm_parametri_cliente.dati_formato_elettr_id,
         p_rec_comm_parametri_cliente.fmt_listino_da_banco_id,
         p_rec_comm_parametri_cliente.fmt_listino_articoli_id,
         p_rec_comm_parametri_cliente.fmt_listino_offerte_id,
         p_rec_comm_parametri_cliente.flag_gest_varianti_ordcli,
         p_rec_comm_parametri_cliente.flag_gest_sost_ordcli,
         p_rec_comm_parametri_cliente.flag_gest_ass_ordcli,
         p_rec_comm_parametri_cliente.flag_ctr_przces_ordcli,
         p_rec_comm_parametri_cliente.perc_plt_pieno,
         p_rec_comm_parametri_cliente.stile_layout_listini_id,
         p_rec_comm_parametri_cliente.tipo_copia_conforme_id,
         --p_rec_comm_parametri_cliente.classe_sconto_t_id TODO come reperire la testata
         p_rec_comm_parametri_cliente.valido_dal,
         --p_rec_comm_parametri_cliente.valido_al,
         p_rec_comm_parametri_cliente.gestione_cauzione_id,
         p_rec_comm_parametri_cliente.critero_ordinamento_id,
         p_rec_comm_parametri_cliente.criterio_separazione_id,
         p_rec_comm_parametri_cliente.tipo_punto_consegna_id,
         p_rec_comm_parametri_cliente.tipo_rapp_amm_cfg_id,
         SYSDATE,
         C_NOME_MODULO,
         C_NOME_MODULO,
         SYSDATE,
         C_NOME_MODULO,
         C_NOME_MODULO,
         SYSDATE
      );
    
  END SP_INSERT_INTERLOC;
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_UPDATE_INTERLOC(p_rec_comm_parametri_cliente IN comm_parametri_cliente%ROWTYPE)
  IS
    l_parametri_cliente_id_upd            NUMBER;
  BEGIN
    UPDATE comm_parametri_cliente
    SET    comm_parametri_cliente.modalita_calc_przces_id   = p_rec_comm_parametri_cliente.modalita_calc_przces_id,
           comm_parametri_cliente.documento_gestionale_id   = NULL,
           comm_parametri_cliente.tipo_divulg_etich_id      = p_rec_comm_parametri_cliente.tipo_divulg_etich_id,
           comm_parametri_cliente.flag_divulg_offerte       = p_rec_comm_parametri_cliente.flag_divulg_offerte,
           comm_parametri_cliente.flag_divulg_fidelity      = p_rec_comm_parametri_cliente.flag_divulg_fidelity,
           comm_parametri_cliente.flag_divulg_fornitori     = p_rec_comm_parametri_cliente.flag_divulg_fornitori,
           comm_parametri_cliente.flag_invio_fatt_email     = p_rec_comm_parametri_cliente.flag_invio_fatt_email,
           comm_parametri_cliente.flag_invio_inev_email     = p_rec_comm_parametri_cliente.flag_invio_inev_email,
           comm_parametri_cliente.flag_libro_guida_diretti  = p_rec_comm_parametri_cliente.flag_libro_guida_diretti,
           comm_parametri_cliente.dati_formato_elettr_id    = p_rec_comm_parametri_cliente.dati_formato_elettr_id ,
           comm_parametri_cliente.fmt_listino_da_banco_id   = p_rec_comm_parametri_cliente.fmt_listino_da_banco_id,
           comm_parametri_cliente.fmt_listino_articoli_id   = p_rec_comm_parametri_cliente.fmt_listino_articoli_id,
           comm_parametri_cliente.fmt_listino_offerte_id    = p_rec_comm_parametri_cliente.fmt_listino_offerte_id,
           comm_parametri_cliente.flag_gest_varianti_ordcli = p_rec_comm_parametri_cliente.flag_gest_varianti_ordcli,
           comm_parametri_cliente.flag_gest_sost_ordcli     = p_rec_comm_parametri_cliente.flag_gest_sost_ordcli,
           comm_parametri_cliente.flag_gest_ass_ordcli      = p_rec_comm_parametri_cliente.flag_gest_ass_ordcli,
           comm_parametri_cliente.flag_ctr_przces_ordcli    = p_rec_comm_parametri_cliente.flag_ctr_przces_ordcli,
           comm_parametri_cliente.perc_plt_pieno            = p_rec_comm_parametri_cliente.perc_plt_pieno,
           comm_parametri_cliente.stile_layout_listini_id   = p_rec_comm_parametri_cliente.stile_layout_listini_id,
           comm_parametri_cliente.tipo_copia_conforme_id    = p_rec_comm_parametri_cliente.tipo_copia_conforme_id,
           --comm_parametri_cliente.classe_sconto_t_id
           --comm_parametri_cliente.valido_al
           comm_parametri_cliente.gestione_cauzione_id      = p_rec_comm_parametri_cliente.gestione_cauzione_id,
           comm_parametri_cliente.critero_ordinamento_id    = p_rec_comm_parametri_cliente.critero_ordinamento_id,
           comm_parametri_cliente.criterio_separazione_id   = p_rec_comm_parametri_cliente.criterio_separazione_id,
           comm_parametri_cliente.tipo_punto_consegna_id    = p_rec_comm_parametri_cliente.tipo_punto_consegna_id,
           comm_parametri_cliente.tipo_rapp_amm_cfg_id      = p_rec_comm_parametri_cliente.tipo_rapp_amm_cfg_id,
           comm_parametri_cliente.upd_data                  = SYSDATE,
           comm_parametri_cliente.upd_modulo                = C_NOME_MODULO,
           comm_parametri_cliente.version                   = SYSDATE
    WHERE  comm_parametri_cliente.interlocutore_id          = p_rec_comm_parametri_cliente.interlocutore_id
    AND    comm_parametri_cliente.valido_dal                = p_rec_comm_parametri_cliente.valido_dal; 
             
    IF (SQL%ROWCOUNT = 0)
    THEN
      BEGIN
        G_LOGGER.log_debug('nessun record trovato per interlocutore_id:=' || p_rec_comm_parametri_cliente.interlocutore_id || ' per la data validita:=' || p_rec_comm_parametri_cliente.valido_dal, 'SP_UPDATE_INTERLOC', NULL, 'comm.interlocutore_id:=' || p_rec_comm_parametri_cliente.interlocutore_id, NULL, 'comm.valido_dal:=' || p_rec_comm_parametri_cliente.valido_dal);
             
        SELECT p_id
        INTO l_parametri_cliente_id_upd
        FROM (
             SELECT   comm_parametri_cliente.parametri_cliente_id as p_id
             FROM     comm_parametri_cliente
             WHERE    comm_parametri_cliente.interlocutore_id = p_rec_comm_parametri_cliente.interlocutore_id
             ORDER BY comm_parametri_cliente.valido_dal DESC)
        WHERE ROWNUM = 1;
       
        UPDATE comm_parametri_cliente
        SET    comm_parametri_cliente.valido_al            = p_rec_comm_parametri_cliente.valido_dal - 1 
        WHERE  comm_parametri_cliente.parametri_cliente_id = l_parametri_cliente_id_upd;
        G_LOGGER.log_debug('storico trovato, eseguito UPDATE del valido_al', 'SP_UPDATE_INTERLOC', NULL, 'l_parametri_cliente_id_upd:=' || l_parametri_cliente_id_upd, NULL, 'comm.valido_al:=' || p_rec_comm_parametri_cliente.valido_dal);
       
        SP_INSERT_INTERLOC(p_rec_comm_parametri_cliente);
        G_LOGGER.log_debug('inserito nuovo storico', 'SP_UPDATE_INTERLOC', NULL, 'comm.interlocutore_id:=' || p_rec_comm_parametri_cliente.interlocutore_id, NULL, 'comm.valido_dal:=' || p_rec_comm_parametri_cliente.valido_dal);
       
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
       G_LOGGER.log_debug('storico non trovato, INSERT nuovo record', 'SP_UPDATE_INTERLOC', NULL, 'parametri_cliente.interlocutore_id:=' || p_rec_comm_parametri_cliente.interlocutore_id, NULL, 'comm.valido_dal:=' || p_rec_comm_parametri_cliente.valido_dal);
       SP_INSERT_INTERLOC(p_rec_comm_parametri_cliente);
      END;
    END IF;
    
  END SP_UPDATE_INTERLOC;
  
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_CARICAMENTO
  IS
    l_comm_parametri_cliente              comm_parametri_cliente%ROWTYPE;
    l_interlocutore_codice_prev           VARCHAR2(10):='XXXXXX';
    l_interlocutore_codice_err            BOOLEAN;
    l_msg                                 VARCHAR2(200);
  BEGIN
    G_LOGGER.log_info('SP_CARICAMENTO iniziata - caricamento da DCC.PARAMETRI_CLIENTE a COMM.PARAMETRI_CLIENTE', 'SP_CARICAMENTO', NULL);
    
    G_CONT_RIGHE_TOTALI   := 0;
    G_CONT_RIGHE_SCARTATE := 0;
    G_CONT_RIGHE_CARICATE := 0;
       
    FOR rec_parametri_cli IN cur_parametri_cliente    
    LOOP
       
       G_CONT_RIGHE_TOTALI:= G_CONT_RIGHE_TOTALI + 1;
       IF (l_interlocutore_codice_prev <> rec_parametri_cli.d_interlocutore_codice OR rec_parametri_cli.d_interlocutore_codice IS NULL) 
       THEN
          SAVEPOINT sv_interloc_OK;
          l_interlocutore_codice_err:= FALSE;
          IF(rec_parametri_cli.d_interlocutore_codice IS NOT NULL)
          THEN
              l_interlocutore_codice_prev := rec_parametri_cli.d_interlocutore_codice;
          END IF;
       END IF;
      
       IF (rec_parametri_cli.interlocutore_id IS NULL OR rec_parametri_cli.d_interlocutore_codice IS NULL)
       THEN
          l_msg := 'non esiste interlocutore_id per questo interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice;
          G_LOGGER.log_error(l_msg, 'SP_CARICAMENTO', NULL, NULL, '-20200', 'interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice);
               
          UPDATE parametri_cliente
          SET    flag_elab                                = C_FLAG_ERR
          WHERE  parametri_cliente.d_parametri_cliente_id = rec_parametri_cli.d_parametri_cliente_id;
          G_LOGGER.log_debug('update FLAG_ELAB in stato ' || C_FLAG_ERR || ' per questo d_parametri_cliente_id:=' || rec_parametri_cli.d_parametri_cliente_id, 'SP_CARICAMENTO', NULL);
          G_CONT_RIGHE_SCARTATE := G_CONT_RIGHE_SCARTATE + 1;
          CONTINUE;
       END IF;

       BEGIN
          IF(l_interlocutore_codice_err = FALSE)
          THEN
              l_comm_parametri_cliente := SP_TRANSCODIFICA(rec_parametri_cli);
              VALIDA_PARAMETRI_CLIENTE_COMM(l_comm_parametri_cliente);
              SP_UPDATE_INTERLOC(l_comm_parametri_cliente);
              
              UPDATE parametri_cliente
              SET    flag_elab              = C_FLAG_OK
              WHERE  d_parametri_cliente_id = rec_parametri_cli.d_parametri_cliente_id;
              
              G_CONT_RIGHE_CARICATE := G_CONT_RIGHE_CARICATE + 1;
          END IF;
       
       EXCEPTION
       WHEN OTHERS THEN
           l_interlocutore_codice_err := TRUE;
           ROLLBACK TO sv_interloc_OK;
           G_LOGGER.log_error('errore update/insert COMM.PARAMETRI_CLIENTE: ' || SQLERRM, 'SP_CARICAMENTO', NULL, NULL, '-20100', 'interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice);
           G_LOGGER.log_info('salto tutti i record per questo interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice, 'SP_CARICAMENTO' , NULL, NULL, NULL, 'interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice);
           
           UPDATE parametri_cliente
           SET    flag_elab                                = C_FLAG_ERR
           WHERE  parametri_cliente.d_interlocutore_codice = rec_parametri_cli.d_interlocutore_codice 
           AND    rec_parametri_cli.flag_elab              = C_FLAG_DA_PREP;
           
           G_CONT_RIGHE_SCARTATE := G_CONT_RIGHE_SCARTATE + SQL%ROWCOUNT;
           G_LOGGER.log_debug('update (FLAG_ELAB da ' || C_FLAG_DA_PREP || ' a ' || C_FLAG_ERR || ') per questo interlocutore_codice:=' || rec_parametri_cli.d_interlocutore_codice, 'SP_CARICAMENTO', NULL);
       END;
     
    END LOOP;
        
    G_LOGGER.log_debug('G_CONT_RIGHE_SCARTATE:=' || G_CONT_RIGHE_SCARTATE, 'SP_CARICAMENTO', NULL, 'G_CONT_RIGHE_SCARTATE:=' || G_CONT_RIGHE_SCARTATE); 
    G_LOGGER.log_debug('G_CONT_RIGHE_TOTALI:=' || G_CONT_RIGHE_TOTALI,'SP_CARICAMENTO', NULL, 'G_CONT_RIGHE_TOTALI:=' || G_CONT_RIGHE_TOTALI); 
    G_LOGGER.log_debug('G_CONT_RIGHE_CARICATE:=' || G_CONT_RIGHE_CARICATE, 'SP_CARICAMENTO', NULL, 'G_CONT_RIGHE_CARICATE:=' || G_CONT_RIGHE_CARICATE); 
    IF (G_CONT_RIGHE_SCARTATE > 0)
    THEN
        l_msg := 'Sono state SCARTATE:= ' || G_CONT_RIGHE_SCARTATE || ' righe SU ' || G_CONT_RIGHE_TOTALI || CHR(10) || CHR(13) || 'CARICATE:= ' || G_CONT_RIGHE_CARICATE || '  -- LOAD_ID - DCC: ' || G_LOGGER.load_id;
        send_mail('[ERR] FLUSSO LOAD PARAMETRI CLIENTE - NUMERICHE', l_msg);
    END IF;
    
  EXCEPTION
  WHEN OTHERS THEN
    l_msg := 'SP_CARICAMENTO eccezione: caricamento da DCC.PARAMETRI_CLIENTE a COMM.PARAMETRI_CLIENTE ' || SQLERRM;
    G_LOGGER.log_error(l_msg, 'SP_CARICAMENTO', NULL, NULL, '-20050');
    RAISE;
  
  END SP_CARICAMENTO;

------------------------------------------------------------------------------------------------------------------
   
  PROCEDURE SP_MAIN 
  IS
    l_load_id  NUMBER;
    l_msg 		 VARCHAR2(1024);
  BEGIN
  
    G_LOGGER.log_info('SP_MAIN iniziata', 'SP_MAIN');
    
    SELECT SQ_LOAD_ID_parametri_cliente.NEXTVAL INTO l_load_id FROM DUAL;
        
    G_LOGGER.load_id := l_load_id;
    
    SP_INIT();
      
    SP_TRADUZIONE(l_load_id);
    COMMIT;
    G_LOGGER.log_info('Eseguita COMMIT (ORACLE) - DCC', 'SP_MAIN');

    UPDATE FDB8SEOF
    SET    DB8ST1 = C_FLAG_OK,
           DB8IDM = l_load_id
    WHERE  DB8ST1 = C_FLAG_IN_ELAB;
    G_LOGGER.log_info('Eseguito UPDATE FDB8SEOF (DB8ST1 da ' || C_FLAG_IN_ELAB || ' a ' || C_FLAG_OK || ') per: '|| SQL%ROWCOUNT ||' rec', 'SP_MAIN');
    
    COMMIT;
    G_LOGGER.log_info('Eseguita COMMIT (AS400)', 'SP_MAIN');
    
    SP_CARICAMENTO;
    COMMIT;
    G_LOGGER.log_info('Eseguita COMMIT (ORACLE) - COMM', 'SP_MAIN');
    
  EXCEPTION
  WHEN OTHERS THEN
    l_msg :=  C_NOME_MODULO || ' eccezione - ' || SQLCODE || ' - ' || SQLERRM;
    G_LOGGER.log_error(l_msg, 'SP_MAIN');
    
    ROLLBACK;
    G_LOGGER.log_info('Eseguita ROLLBACK (ORACLE)', 'SP_MAIN');

    send_mail('[ERR] FLUSSO LOAD PARAMETRI CLIENTE BLOCCATO', l_msg);

  END SP_MAIN;
  
END PKG_LOAD_PARAMETRI_CLIENTE;
/