create or replace PACKAGE BODY PKG_LOAD_ORDINI_CLIENTI AS

PRAGMA SERIALLY_REUSABLE;

C_NOME_MODULO           CONSTANT VARCHAR2(30)  := 'PKG_LOAD_ORDINI_CLIENTI';
C_FILE_NAME_DORDINI2    CONSTANT VARCHAR2(100) := 'dordini2.ftp';

C_FILE_ORDINI_LOCATION  CONSTANT VARCHAR2(30)  := 'DIR_ORDINI_CLIENTI_FTP';
C_EXT_LOCATION          CONSTANT VARCHAR2(30)  := 'DIR_ORDINI_CLIENTI_EXT';
C_ERR_LOCATION          CONSTANT VARCHAR2(30)  := 'DIR_ORDINI_CLIENTI_ERR';
C_STORICO_LOCATION      CONSTANT VARCHAR2(30)  := 'DIR_ORDINI_CLIENTI_STORICO';

C_STATO_CARICATO        CONSTANT CHAR(1)        := '0';
C_STATO_ELABORATO       CONSTANT CHAR(1)        := '1';
C_STATO_ERRORE          CONSTANT CHAR(1)        := '2';

C_STATO_ONT_DA_ACQUISIRE CONSTANT VARCHAR2(3) := '1';
C_STATO_OND_DA_ACQUISIRE CONSTANT VARCHAR2(3) := '1';


G_LOAD_ID               number;

-- Istanzio il log
G_LOGGER                TYP_LOGGER_NEW := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);                 

------------------------------------------------------------------------------------------------------------------

PROCEDURE send_mail(Subject in varchar2, Msg in varchar2)
AS
BEGIN
    UTILITY.SPEDISCI_MAIL(Oggetto => 'ORDINI CLIENTI: ' || C_NOME_MODULO || ' - ' || Subject,
                          Messaggio => Msg,
                          GruppoDestinatario => 'ORDINI_CLIENTI_DEST',
                          LoaId => G_LOAD_ID);
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION FILE_EXISTS (
  p_dirname in varchar2,     -- schema object name
  p_filename in varchar2
) RETURN BOOLEAN
IS
  l_fexists boolean;
  l_flen   number;
  l_bsize  number;
BEGIN  
  UTL_FILE.FGETATTR(UPPER(p_dirname), p_filename, l_fexists, l_flen, l_bsize);
  RETURN l_fexists;
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION CHECK_MOUNT RETURN BOOLEAN
IS
    l_fmounted boolean;
  l_path VARCHAR2(500);
BEGIN
    l_fmounted := FILE_EXISTS(C_FILE_ORDINI_LOCATION,'DO_NOT_DELETE_ME');
    IF NOT l_fmounted THEN
        SELECT DIRECTORY_PATH
          INTO l_path 
          FROM ALL_DIRECTORIES
         WHERE DIRECTORY_NAME = C_FILE_ORDINI_LOCATION; 
        G_LOGGER.log_error('DIRECTORY ' || C_FILE_ORDINI_LOCATION || ' (' || l_path || ') NON MONTATA');
        send_mail('ERRORE CRITICO CARTELLA FTP NON MONTATA','CARTELLA ' || l_path || ' NON MONTATA');
    END IF;
    return l_fmounted;
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION SPOSTA_FILE_IN_EXT
RETURN BOOLEAN
IS
  l_msg            VARCHAR2(255);
  l_path           VARCHAR2(500);
BEGIN

  BEGIN
    UTL_FILE.FRENAME (
     src_location   => C_FILE_ORDINI_LOCATION,
     src_filename   => C_FILE_NAME_DORDINI2, 
     dest_location  => C_EXT_LOCATION,
     dest_filename  => C_FILE_NAME_DORDINI2,
     overwrite => FALSE);
  EXCEPTION
    WHEN OTHERS THEN    
     SELECT DIRECTORY_PATH
          INTO l_path 
          FROM ALL_DIRECTORIES
         WHERE DIRECTORY_NAME = C_EXT_LOCATION; 
         l_msg := 'FLUSSO ORDINI BLOCCATO: Errore nello spostamento del file ' || C_FILE_NAME_DORDINI2 || ' nella cartella ' || l_path || ': ' || SQLERRM;
         G_LOGGER.log_error(l_msg);
        send_mail('FLUSSO ORDINI BLOCCATO',l_msg);
      RETURN FALSE;
  END;

  RETURN TRUE;
  
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION SPOSTA_FILE_DA_EXT (
  p_destinazione  VARCHAR2
) RETURN BOOLEAN
IS
  l_nome_file      VARCHAR2(200);
  l_dest           VARCHAR2(200);
  l_msg            VARCHAR2(255);
  l_path           VARCHAR2(500);
BEGIN  

    l_nome_file := C_FILE_NAME_DORDINI2 || '_' || TO_CHAR(SYSDATE, 'YYYY-MM-DD-HH24-MI-SS');

    if p_destinazione = 'STORICO' then
      l_dest := C_STORICO_LOCATION;
      l_nome_file := 'elab_' || l_nome_file;
    elsif p_destinazione = 'ERRORE' then
      l_dest := C_ERR_LOCATION;
      l_nome_file := 'err_' || l_nome_file;
    else
      raise_application_error(-20001, 'Destinazione ' || p_destinazione || ' non prevista');
    end if;
  
    -- Rinomino il file senza spostarlo cos� se lo spostamento va in errore il file comunque non da pi�
    -- fastidio ai successivi caricamenti.
    BEGIN
      UTL_FILE.FRENAME (
       src_location   => C_EXT_LOCATION, 
       src_filename   => C_FILE_NAME_DORDINI2, 
       dest_location  => C_EXT_LOCATION,
       dest_filename  => l_nome_file,
       overwrite => FALSE);
    EXCEPTION
      WHEN OTHERS THEN   
       SELECT DIRECTORY_PATH
          INTO l_path 
          FROM ALL_DIRECTORIES
         WHERE DIRECTORY_NAME = C_EXT_LOCATION; 
        l_msg := 'FLUSSO ORDINI BLOCCATO: errore nel rename del file ' || C_FILE_NAME_DORDINI2 || ' nella cartella ' || l_path || ': ' || SQLERRM;
        G_LOGGER.log_error(l_msg);
        send_mail('FLUSSO ORDINI BLOCCATO',l_msg);
        RETURN FALSE;
    END;
    
    BEGIN
      -- Sposto il file nella cartella di archiviazione
      UTL_FILE.FRENAME (
       src_location   => C_EXT_LOCATION, 
       src_filename   => l_nome_file, 
       dest_location  => l_dest,
       dest_filename  => l_nome_file,
       overwrite => FALSE);
    EXCEPTION
      WHEN OTHERS THEN
      
      SELECT DIRECTORY_PATH
          INTO l_path 
          FROM ALL_DIRECTORIES
          WHERE DIRECTORY_NAME = C_STORICO_LOCATION; 
          l_msg := 'Errore nello spostamento del file ' || C_FILE_NAME_DORDINI2 || ' nella cartella ' || l_path || '\' || l_nome_file || ': ' || SQLERRM;
          G_LOGGER.log_error(l_msg);
          send_mail('ERRORE NELL''ARCHIVIAZIONE NEL FILE ORDINI NELLA CARTELLA DI STORICO',l_msg);
        RETURN FALSE;
    END;

    RETURN TRUE;

END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE INSERT_DORDINI(p_load_id number)
IS
l_data_caricamento DATE;
BEGIN
l_data_caricamento := SYSDATE;
  INSERT INTO DORDINI2 (
    DORDINI2_ID,
    LOAD_ID,
    DATA_CARICAMENTO,
    NUMRIGA,
    REC , 
    NEG ,
    CLI ,
    REP ,
    PID ,
    NSE ,
    EXA ,
    EXM ,
    EXG ,
    EXH ,
    EXN ,
    NRG ,
    CDA ,
    CDB ,
    QTA ,
    PRZ ,
    RIA ,
    RIM ,
    RIG ,
    CDM ,
    STA ,
    ACH ,
    MCH ,
    GCH ,
    OCH ,
    INS_DATA,
    INS_MODULO,
    UPD_MODULO,
    UPD_DATA,
    VERSION
  )
  SELECT 
    SQ_DORDINI2.NEXTVAL,
    p_load_id,
    l_data_caricamento,
    ROWNUM,
    REC , 
    NEG ,
    CLI ,
    REP ,
    PID ,
    NSE ,
    EXA ,
    EXM ,
    EXG ,
    EXH ,
    EXN ,
    NRG ,
    CDA ,
    CDB ,
    QTA ,
    PRZ ,
    RIA ,
    RIM ,
    RIG ,
    CDM ,
    STA ,
    ACH ,
    MCH ,
    GCH ,
    OCH,
    SYSDATE,
    C_NOME_MODULO,
    C_NOME_MODULO,
    SYSDATE,
    SYSDATE  
  FROM EXT_DORDINI2;
    
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE LOAD_FILE_ORDINI
IS
l_msg      VARCHAR2(200);
l_nome_file VARCHAR2(200);
l_result   BOOLEAN;


BEGIN
  G_LOAD_ID := SQ_LOAD_ID_DORDINI2.NEXTVAL;

  G_LOGGER.procedura := 'LOAD_FILE_ORDINI';
  G_LOGGER.load_id := G_LOAD_ID;
  G_LOGGER.log_info('Inizio caricamento ordini clienti da file');
  
  if not FILE_EXISTS(C_FILE_ORDINI_LOCATION, C_FILE_NAME_DORDINI2) then
    IF CHECK_MOUNT() THEN
        G_LOGGER.log_info('Nessun file da caricare. Fine');
    end IF;
    RETURN;
  end if;
  
  if SPOSTA_FILE_IN_EXT() then

    G_LOGGER.log_info('Inizio caricamento file ordini ...');
    
    begin
      INSERT_DORDINI(G_LOAD_ID);
      COMMIT;
      G_LOGGER.log_info('File ordini caricato sul DCC');
      l_result := SPOSTA_FILE_DA_EXT('STORICO');
      if l_result then 
        G_LOGGER.log_info('File ordini spostato nella cartella di storico');
      else
         G_LOGGER.log_error('Impossibile spostare file ordini nella cartella di storico');
      end if;
      G_LOGGER.log_info('Fine caricamento ordini clienti da file');
    exception
      when others then
        rollback;
        l_msg := 'ERRORE NEL CARICAMENTO DEL FILE ORDINI CLIENTI SUL DCC: ' || SQLERRM;
        G_LOGGER.log_error(l_msg);
        send_mail('ERRORE NEL CARICAMENTO DEL FILE ORDINI CLIENTI SUL DCC',l_msg);
        l_result := SPOSTA_FILE_DA_EXT('ERRORE');
        G_LOGGER.log_error('FINE Caricamento ordini clienti da file. ABORT!');
    end;
    
  end if;
  
END;

------------------------------------------------------------------------------------------------------------------

FUNCTION INS_ORDINE_NORMALIZZATO_T (p_ordine DORDINI2%ROWTYPE, p_stato_id NUMBER)
RETURN NUMBER
IS
  l_ordine_normalizzato_t_id NUMBER;
  l_tempo_trasmissione DATE;
BEGIN
  
  l_ordine_normalizzato_t_id := COMM_SQ_ORDINE_NORMALIZZATO_T.nextval;
  
  BEGIN
    l_tempo_trasmissione := TO_DATE(p_ordine.EXA || p_ordine.EXM || p_ordine.EXG || p_ordine.EXH,'YYYYMMDDHH24MISS');
  EXCEPTION
  WHEN OTHERS THEN
    l_tempo_trasmissione := TO_DATE('21991231','YYYYMMDD');
    G_LOGGER.log_error('DATA DI TRASMISSIONE NON CORRETTA, SETTATA A 31-12-2199');
  END; 
  
  INSERT
  INTO COMM_ORDINE_NORMALIZZATO_T
    (
      ORDINE_NORMALIZZATO_T_ID,
      COD_DESTINATARIO,
      COD_DISPOSITIVO,
      PROGRESSIVO_TRASMISSIONE,
      TEMPO_TRASMISSIONE,
      STATO_ORDINE_NORM_ID,
      ORIGINE,
      PROVENIENZA,
      INS_DATA,
      INS_MODULO,
      UPD_DATA,
      UPD_MODULO,
      INS_UTENTE,
      UPD_UTENTE,
      VERSION,
      LOAD_ID
    )
    VALUES
    (
      l_ordine_normalizzato_t_id,
      p_ordine.CLI,
      p_ordine.REP
      || ':'
      || p_ordine.PID
      || ':'
      || p_ordine.NSE,
      p_ordine.EXN,
      l_tempo_trasmissione,
      p_stato_id,
      C_FILE_NAME_DORDINI2,
      'PDA',
      SYSDATE,
      C_NOME_MODULO,
      SYSDATE,
      C_NOME_MODULO,
      C_NOME_MODULO,
      C_NOME_MODULO,
      SYSDATE,
      g_load_id
    );
    RETURN l_ordine_normalizzato_t_id;
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE INS_ORDINE_NORMALIZZATO_D (p_ordine DORDINI2%ROWTYPE, p_ordine_normalizzato_t_id NUMBER, p_stato_dettaglio NUMBER)
IS
l_nrg NUMBER;
BEGIN

  BEGIN
    l_nrg:= TO_NUMBER(p_ordine.NRG);
  EXCEPTION
    WHEN OTHERS THEN
    l_nrg:= 999;
    G_LOGGER.log_error('VALORE NRG: ' ||  p_ordine.NRG || ' NON VALIDO, SETTATO A ' || l_nrg);
  END;

INSERT
      INTO COMM_ORDINE_NORMALIZZATO_D
      (
        ORDINE_NORMALIZZATO_D_ID,
        ORDINE_NORMALIZZATO_T_ID,
        COD_ARTICOLO,
        NUMERO_RIGA,
        STATO_RIGA_ORDINE_NORM_ID,
        QTA_ORIGINALE,
        QTA_UM_ORDINE,
        INS_DATA,
        INS_MODULO,
        UPD_DATA,
        UPD_MODULO,
        INS_UTENTE,
        UPD_UTENTE,
        VERSION,
        LOAD_ID
      )
      VALUES
      ( 
        COMM_sq_ordine_normalizzato_d.nextval,
        p_ordine_normalizzato_t_id,
        p_ordine.CDA,
        l_nrg,
        p_stato_dettaglio,
        TO_NUMBER(p_ordine.QTA),
        TO_NUMBER(p_ordine.QTA),
        SYSDATE,
        C_NOME_MODULO,
        SYSDATE,
        C_NOME_MODULO,
        C_NOME_MODULO,
        C_NOME_MODULO,
        SYSDATE,
        g_load_id
      );
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE LOAD_ORDINE_NORMALIZZATO
IS
CURSOR cur_dordini2
IS
  SELECT *
    FROM DORDINI2
   WHERE STATO = 0
     AND REC = 'ORD'
   ORDER BY DATA_CARICAMENTO ASC, NUMRIGA ASC
     FOR UPDATE of STATO;
  
  l_ordine_normalizzato_t_id NUMBER := 0;
  l_tempo_trasmissione DATE;
  l_stato_id NUMBER;
  l_prev VARCHAR2(50);
  l_numero_righe NUMBER := 0;
  
  l_err_testata BOOLEAN;
  
  l_conta_testate NUMBER := 0;
  l_conta_righe NUMBER := 0;
  
  l_conta_testate_err NUMBER := 0;
  l_conta_righe_err NUMBER := 0;
  
  l_msg_err VARCHAR2(4096);
  l_msg VARCHAR2(4096);
  
  l_stato_dettaglio NUMBER;
  
BEGIN
  g_load_id := SQ_LOAD_ID_ORDINE_NORMALIZZATO.nextval;

  G_LOGGER.procedura := 'LOAD_ORDINE_NORMALIZZATO';
  G_LOGGER.load_id := g_load_id;
  G_LOGGER.log_info('Inizio caricamento ordini in ordine normalizzato');
  
  l_prev := 'XXXXXXXXX';
  -- TODO - verificare che i codici siano giusti
  SELECT STATO_ORDINE_NORM_ID INTO l_stato_id FROM COMM_STATO_ORDINE_NORM WHERE CODICE = C_STATO_ONT_DA_ACQUISIRE;
  SELECT STATO_RIGA_ORDINE_NORM_ID INTO l_stato_dettaglio FROM COMM_STATO_RIGA_ORDINE_NORM WHERE CODICE = C_STATO_OND_DA_ACQUISIRE;
  
  FOR REC_dordini2 IN cur_dordini2
  LOOP
    -- TESTATA
    BEGIN
      IF (REC_dordini2.EXN <> l_prev) THEN
          --COMMIT;
          l_err_testata := FALSE;
          IF (l_ordine_normalizzato_t_id <> 0) THEN
            UPDATE COMM_ORDINE_NORMALIZZATO_T
               SET NUMERO_RIGHE             = l_numero_righe
             WHERE ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
          END IF;
          l_ordine_normalizzato_t_id := INS_ORDINE_NORMALIZZATO_T(REC_dordini2,l_stato_id);
          l_prev := REC_dordini2.EXN;
          l_conta_testate := l_conta_testate + 1;
          l_numero_righe := 1;
      ELSE 
          l_numero_righe := l_numero_righe + 1;
      END IF;
    EXCEPTION
    WHEN OTHERS THEN
        l_err_testata := TRUE;
        UPDATE DORDINI2 SET stato = C_STATO_ERRORE WHERE CURRENT OF cur_dordini2;
        l_conta_testate_err := l_conta_testate_err + 1;
        l_msg_err := l_msg_err || CHR(13) || CHR(10) || SQLERRM;
    END;
    
    -- RIGA
    IF NOT l_err_testata THEN
      BEGIN
        INS_ORDINE_NORMALIZZATO_D(REC_dordini2,l_ordine_normalizzato_t_id,l_stato_dettaglio);
        UPDATE DORDINI2 SET stato = C_STATO_ELABORATO WHERE CURRENT OF cur_dordini2;
        l_conta_righe := l_conta_righe + 1;
      EXCEPTION
        WHEN OTHERS THEN
          UPDATE DORDINI2 SET stato = C_STATO_ERRORE WHERE CURRENT OF cur_dordini2;
          l_conta_righe_err := l_conta_righe_err + 1;
          l_msg_err := l_msg_err || CHR(13) || CHR(10) || SQLERRM;
      END;
    ELSE
        UPDATE DORDINI2 SET stato = C_STATO_ERRORE WHERE CURRENT OF cur_dordini2;
        l_conta_righe_err := l_conta_righe_err + 1;
    END IF;
  END LOOP;
  
  -- l'ultimo ordine
  IF (l_ordine_normalizzato_t_id <> 0) THEN
            UPDATE COMM_ORDINE_NORMALIZZATO_T
               SET NUMERO_RIGHE             = l_numero_righe
             WHERE ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
  END IF;
  
  --COMMIT;
  l_msg := 'testate caricate: ' || l_conta_testate || ', righe caricate: '
           || l_conta_righe || ', testate errore: ' || l_conta_testate_err
           || ', righe errore: ' || l_conta_righe_err  || CHR(13) || CHR(10)
           || l_msg_err;
  
  IF l_conta_testate_err > 0 OR l_conta_righe_err > 0 THEN
    G_LOGGER.log_error(l_msg);
    send_mail('ERRORE NEL CARICAMENTO DEGLI ORDINI CLIENTI NELL'' ORDINE_NORMALIZZATO_T/D',l_msg);
  ELSE
    G_LOGGER.log_info(l_msg);
  END IF;
  
  G_LOGGER.log_info('Fine caricamento ordini in ordine normalizzato');
  
END;

------------------------------------------------------------------------------------------------------------------

PROCEDURE MAIN
IS
BEGIN
  LOAD_FILE_ORDINI();
  COMMIT;
  
  LOAD_ORDINE_NORMALIZZATO();
  COMMIT;
END;

------------------------------------------------------------------------------------------------------------------

END PKG_LOAD_ORDINI_CLIENTI;
/