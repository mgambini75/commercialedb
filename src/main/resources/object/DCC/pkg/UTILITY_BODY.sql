create or replace PACKAGE BODY UTILITY AS
---------------------------------------------------------------------------
-- SCOPO    : IUtility ad uso generico
-- INPUT    : Codice Utente Web, ???altro???
-- OUTPUT   : Return Code [0=OK; 1=ER]
-- AUTORE   : GBarbagallo - 09/03/2005
-- LAST UPD :
-- NOTE     :
---------------------------------------------------------------------------

    --| Gestione Errori        |--------------------
    RC_OK                         CONSTANT  PLS_INTEGER   := 0;
    RC_ER                         CONSTANT  PLS_INTEGER   := 1;

    --| Log vars               |------------
    CodOpe                        VARCHAR2(6);
    SQLOpe                        LOG_EVENTS.SQLOPE%TYPE;
    ObjNam                        LOG_EVENTS.OBJNAM%TYPE;
    InfAgg                        LOG_EVENTS.DESOPE%TYPE;

    --| Risposte SP    |-------------
    SI            CONSTANT CHAR(1):='S';
    NO            CONSTANT CHAR(1):='N';
--***********************************************************************--
  PROCEDURE SP_LOGGA_EVENTO
  (
   CodOpe  IN varchar2,
   SQLOpe  IN varchar2,
   ObjNam  IN varchar2,
   InfAgg  IN varchar2,
   LoadID  IN number,
   RetCod  IN number
  )
  IS
  PRAGMA AUTONOMOUS_TRANSACTION;
    MiaSeq     number;
    VarLoadID  number;
    VarInfoAgg varchar2(500);
    RC_OK      number:=0;
    RC_ER      number:=-1;
    MiaData    Date;
  BEGIN

    --COMMIT;  -- Previous work
--VarLoadID := 1;
    MiaSeq:=FN_GET_NEXT_TARGET_ID();

    --dbms_Output.Put_Line(LoadID);
    --dbms_Output.Put_Line(MiaSeq);
    --select to_date(to_char(sysdate, 'DD/MM/YYYY HH24:MI')) INTO MiaData from dual;
    IF InfAgg Is Null THEN
      varInfoAgg:='.';
    ELSE
      varInfoAgg:=InfAgg;
    END IF;

    IF LoadID IS NULL THEN
      VarLoadID:=1;
    ELSE
      VarLoadID:=LoadID;
    END IF;

    IF VarLoadID > 0 AND MiaSeq > 0 THEN

      INSERT INTO LOG_EVENTS
       (EVT_ID,  LOA_ID, LOADAT,
        OBJNAM,  CODOPE,
        SQLOPE,  DESOPE)
      SELECT
         MiaSeq, VarLoadID, SYSDATE,
         ObjNam, CodOpe,
         SQLOpe, VarInfoAgg
        FROM DUAL
      ;

      --RetCod:=RC_OK;
    --ELSE
      --RetCod:=RC_ER;
   END IF;

    COMMIT;  -- ... and This one.

  EXCEPTION
    WHEN OTHERS THEN
     dbms_Output.Put_Line(SQLCODE || SUBSTR(SQLERRM,1,100));

  END SP_LOGGA_EVENTO;
  ----------------
  ----------------
  ----------------

  FUNCTION FN_GET_DEFVAL_BY_FLD

    (MyField  IN VARCHAR2)

  RETURN VARCHAR2

  IS

    MyVal   VARCHAR2(50):=-1;

  BEGIN

    SELECT FIELD_DEFVAL INTO MyVal
      FROM DB_FIELDDEFVAL
     WHERE FIELD_NAME = MyField;

    RETURN MyVal;

  END FN_GET_DEFVAL_BY_FLD;
----------------
----------------
----------------
----------------
  FUNCTION FN_GET_STATO
    (TipoStato  IN CHAR)

  RETURN NUMBER

  IS

    MySta   NUMBER(19):=-1;

  BEGIN

    IF  TipoStato = 'ART' THEN
      MySta:= 1;
    ELSIF  TipoStato = 'DIV' THEN
      MySta:= 12;
    ELSIF TipoStato = 'PDD' THEN
      MySta:= 4;
    ELSIF TipoStato = 'FOR' THEN
      MySta:= 17;
    ELSIF TipoStato = 'BAS' THEN
      MySta:= 13;
    END IF;


    RETURN MySta;

  END FN_GET_STATO;
----------------
----------------
----------------
----------------
  PROCEDURE SP_DROPALL_IDX
    (
      Tab    IN  CHAR,
      Idx    IN  CHAR,
      RetCod OUT NUMBER
    )
  IS

    --| Accesso ai dati   |------------
    CURSOR IdxCur
        IS
    SELECT  Table_Name As cTblNam
          , Index_Name As cIdxNam
      FROM USER_INDEXES
     WHERE (Table_Name = Tab OR NVL(Tab ,'1')='1' )
       AND (Index_Name = Idx OR NVL(Idx ,'1')='1' )
       AND (Index_Name Like 'IX_%' OR Index_Name Like 'IDX_%')
    ;
    IdxRec       IdxCur%ROWTYPE;

    --| Variabili d'appoggio |--------
    st              varchar2(512):='';

  BEGIN

    RetCod:=RC_OK;
    ----------------------------------------------------------------------
    CodOpe:= 'INIZIO';
    SQLope:= 'SYS';
    ObjNam:= NVL(Tab,'.');
    InfAgg:= 'Rimozione indici';
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    ----------------------------------------------------------------------
    OPEN IdxCur;

      FETCH IdxCur INTO IdxRec;
      IF NOT IdxCur%NOTFOUND THEN
        LOOP
          st:='DROP INDEX ' || IdxRec.cIdxNam;

          EXECUTE IMMEDIATE st;

          FETCH IdxCur INTO IdxRec;
          EXIT WHEN IdxCur%NOTFOUND;
        END LOOP;
      ELSE
        InfAgg :='Cursore vuoto, nessun indice esistente a queste condizioni';
      END IF;

    CLOSE IdxCur;
    ----------------------------------------------------------------
    CodOpe:='FIN_OK';
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    ----------------------------------------------------------------
    RETURN;

  EXCEPTION
    WHEN OTHERS THEN

      RetCod:=RC_ER;
      ------------------------------------------------------------------------
      InfAgg:=SUBSTR(SQLERRM,1,100);
      CodOpe:='FIN_ER';
      UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
      ------------------------------------------------------------------------
  END SP_DROPALL_IDX;
----------------
----------------
----------------
----------------
  PROCEDURE SP_EXISTS_OBJ
  (
    vOwner        IN  VARCHAR2
  , vObjectName   IN  VARCHAR2
  , vObjectType   IN  CHAR
  , ResponseSN    OUT CHAR
  , RetCod        OUT NUMBER
  )
  IS
    --| Var Appoggio      |----------------
    st             VARCHAR2(255);
    ZeroUno        NUMBER(1);
  BEGIN

    RetCod := RC_OK;

    --| casistica categorie oggetti sys

    IF    vObjectType = 'TAB' THEN
      st:='SELECT count(*) FROM ALL_TABLES      WHERE OWNER = :1 AND TABLE_NAME = :2';
    ELSIF vObjectType = 'IDX' THEN
      st:='SELECT count(*) FROM ALL_INDEXES     WHERE OWNER = :1 AND INDEX_NAME = :2';
    ELSIF vObjectType = 'VIS' THEN
      st:='SELECT count(*) FROM ALL_VIEWS       WHERE OWNER = :1 AND VIEW_NAME = :2';
    ELSIF vObjectType = 'CON' THEN
      st:='SELECT count(*) FROM ALL_CONSTRAINTS WHERE OWNER = :1 AND CONSTRAINT_NAME = :2';
    ELSE
      RetCod:=RC_ER;   -- Parametro in ingresso sbagliato
    END IF;

    IF RetCod = RC_OK THEN

      EXECUTE IMMEDIATE st INTO ZeroUno USING vOwner, vObjectName;

      IF ZeroUno = 1 THEN
        ResponseSN := SI;
      ELSE
        ResponseSN := NO;
      END IF;

    END IF;

    RETURN;

  EXCEPTION
    WHEN OTHERS THEN

    RetCod:=RC_ER;

  END;
---------------------------------------------------------------------
  PROCEDURE SP_ANALYZE_OBJ
  (
   myObjTyp       IN      VARCHAR2,
   myObjNam       IN      VARCHAR2,
   myAnalizePerc  IN      NUMBER,
   RetCod             OUT NUMBER
  )
  AS
    AnalyzePart     VARCHAR2(100);
    ComputePart     VARCHAR2(100);
  BEGIN

    RetCod := RC_OK;

    ------------------------------------------------------------------------
    CodOpe:= 'INIZIO';
    SQLope:= 'SYS';
    ObjNam:= myObjNam;
    InfAgg:='..ANALYZE ' || ObjNam;
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    ------------------------------------------------------------------------

    IF    myObjTyp = 'TAB' THEN
      AnalyzePart := 'ANALYZE TABLE ' || myObjNam;
    ELSIF myObjTyp = 'IDX' THEN
      AnalyzePart := 'ANALYZE INDEX ' || myObjNam;
    ELSE
      RetCod := RC_ER;
    END IF;

    IF myAnalizePerc = 100 THEN
      ComputePart := ' COMPUTE  STATISTICS';
    ELSE
      ComputePart := ' ESTIMATE STATISTICS SAMPLE ' || NVL(myAnalizePerc, 40) || ' PERCENT';
    END IF;

    EXECUTE IMMEDIATE AnalyzePart || ComputePart;

    ----------------------------------------------------------------------
    CodOpe := 'FIN_OK';
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    -----------------------------------------------------------------------
    RETURN;

  EXCEPTION
    WHEN OTHERS THEN
      RetCod:=RC_ER;
      ----------------------------------------------------------------------
      CodOpe := 'FIN_ER';
      InfAgg := SUBSTR(SQLERRM,1,100);
      UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
      ----------------------------------------------------------------------

  END;
-----------------------------------------------------------------------
  PROCEDURE SP_EXECUTE_SQL
  (
    StSQL     IN  VARCHAR2
  , RetCod    OUT NUMBER
  )
  AS
  BEGIN

    RetCod:=RC_OK;

    EXECUTE IMMEDIATE StSQL;

    RETURN;

  EXCEPTION
    WHEN OTHERS THEN

      RetCod:=RC_ER;
      ----------------------------------------------------------------------
      SQLope:= 'SYS';
      ObjNam:= '---';
      CodOpe:='FIN_ER';
      -- 1) Errore
      InfAgg:=SUBSTR('SP_EXECUTE_SQL: '  || SQLERRM,1,100);
      UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
      -- 2) Statement
      InfAgg:=SUBSTR('StSQL=' || StSQL,1,100);
      UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
      ----------------------------------------------------------------------

  END;
-----------------------------------------------------------------------
PROCEDURE SP_TRUNCAT_TAB
  (
    TableName       IN  CHAR,
    RetCod          OUT NUMBER
  )
  AS
    --| Var appoggio    |-------------
    st                VARCHAR2(500);

  BEGIN

    RetCod:=RC_OK;

    ----------------------------------------------------------------------
    CodOpe:= 'INIZIO';
    SQLope:= 'SYS';
    ObjNam:= TableName;
    InfAgg:= '..TRUNCATE ' || TableName;
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    ----------------------------------------------------------------------

    st:= 'TRUNCATE TABLE ' || TableName;

    EXECUTE IMMEDIATE st;

    -----------------------------------------------------------------------
    CodOpe:='FIN_OK';
    UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
    -----------------------------------------------------------------------

    RETURN;

  EXCEPTION
    WHEN OTHERS THEN

      RetCod:=RC_ER;
      ----------------------------------------------------------------------
      CodOpe:='FIN_ER';
      ObjNam:= 'SYS';
      InfAgg:=SUBSTR(SQLERRM,1,100);
      UTILITY.SP_LOGGA_EVENTO(CodOpe,SQLope,ObjNam,InfAgg,null,RetCod);
      ----------------------------------------------------------------------
  END;

-----------------------------------------------------------------------
PROCEDURE SPEDISCI_MAIL
  (
    Oggetto    			IN VARCHAR2,
    Messaggio  			IN VARCHAR2,
    GruppoDestinatario  IN VARCHAR2,
    LoaId      			IN NUMBER := NULL
  )
  IS
    mailToAddr            VARCHAR2(128);
    v_email_mittente  DB_FIELDDEFVAL.FIELD_DEFVAL%TYPE           := FN_GET_DEFVAL ('BRC_DCC', 'SEND_MAIL', 'MITTENTE');
    -- definizione cursori
    cursor mailToCur is
        SELECT FIELD_DEFVAL
         FROM db_fielddefval
         WHERE field_name LIKE GruppoDestinatario || '_%'
         AND DB_OWNER = 'BRC_DCC'
         AND TABLE_NAME = 'SEND_MAIL';

  BEGIN

  --  IF FN_GET_DEFVAL('BRC_DCC','SEND_MAIL','FLAG_ORACLE_MAIL') = 'S' THEN

      
       OPEN mailToCur;
       LOOP

          FETCH mailToCur INTO mailToAddr;
          EXIT WHEN mailToCur%NOTFOUND;

          DITECH_MONITOR.SEND_MAIL(v_sender => v_email_mittente, 
								   v_receiver => mailToAddr,
								   v_subject => Oggetto || (CASE WHEN LoaId IS NOT NULL THEN ' LoadId--> '||LoaId ELSE ' ' END),
								   v_message => Messaggio);                 
                     
       END LOOP;
       CLOSE mailToCur;
      
   -- END IF;

END;  
-----------------------------------------------------------------------

END UTILITY;