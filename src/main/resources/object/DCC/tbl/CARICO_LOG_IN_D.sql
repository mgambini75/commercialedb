CREATE TABLE CARICO_LOG_IN_D
   (	"D_CARICO_LOG_IN_D_ID" NUMBER(10,0), 
	"D_NUMERO_CARICO" VARCHAR2(20 BYTE), 
	"D_DATA_SCADENZA" DATE, 
	"D_NUMERO_ORDINE" VARCHAR2(50 BYTE), 
	"D_NUMERO_RIGA_ORDINE" NUMBER(5,0), 
	"D_NUMERO_BOLLA" VARCHAR2(50 BYTE), 
	"D_DATA_BOLLA" DATE, 
	"D_NUMERO_RIGA_BOLLA" NUMBER(5,0), 
	"D_CODICE_MAGAZZINO" VARCHAR2(50 BYTE), 
	"D_ARTICOLO_CODICE" VARCHAR2(50 BYTE), 
	"D_ARTICOLO_DIFF" VARCHAR2(2 BYTE), 
	"D_STATO" VARCHAR2(5 BYTE), 
	"D_IMBALLO_CESSIONE" NUMBER(10,0), 
	"D_IMBALLO_FORNITORE" NUMBER(10,0), 
	"D_PALLET_PIANI" NUMBER(4,0), 
	"D_PALLET_IMBALLI_PIANO" NUMBER(4,0), 
	"D_PALLET_IMBALLI" NUMBER(8,0), 
	"D_QUANTITA_CAR_PZ" NUMBER(15,5), 
	"D_QUANTITA_CAR_KG" NUMBER(15,5), 
	"D_QUANTITA_CAR_COLLI" NUMBER(15,5), 
	"D_QUANTITA_DA_CAR_PZ" NUMBER(15,5), 
	"D_QUANTITA_DA_CAR_KG" NUMBER(15,5), 
	"D_QUANTITA_DA_CAR_COLLI" NUMBER(15,5), 
	"D_CAUZIONE_CODICE" VARCHAR2(50 BYTE), 
	"D_CAUZIONE_QTA_RESA" NUMBER(10,0), 
	"D_CODICE_EAN" VARCHAR2(50 BYTE), 
	"D_CAUSALE_TESTATA" VARCHAR2(10 BYTE), 
	"D_CAUSALE_RIGA" VARCHAR2(10 BYTE), 
	"D_FLAG_SOSTANZA_ZUCCHERINA" CHAR(1 BYTE), 
	"D_QTA_ORDINATA_OMAGGIO_COLLI" NUMBER(15,5), 
	"D_QTA_RICEVUTA_OMAGGIO_COLLI" NUMBER(15,5), 
	"D_QTA_RICEVUTA_OMAGGIO_PZ_KG" NUMBER(15,5), 
	"D_COSTO_NETTO_UNITARIO" NUMBER(15,5), 
	"LOAD_ID" NUMBER(10,0), 
  "LOAD_ID_MVR" NUMBER (10,0),
	"CODICE_ERRORE" NUMBER(2,0), 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" DATE, 
	"D_USERNAME_OPERATORE" VARCHAR2(50 BYTE), 
	"D_STATO_RIGA_CARICO" VARCHAR2(50 BYTE));
  
  CREATE OR REPLACE TRIGGER BI_CARICO_LOG_IN_D BEFORE INSERT ON CARICO_LOG_IN_d
FOR EACH ROW
DECLARE
  ADESSO       DATE;
  NewID        NUMBER;
BEGIN
  SELECT SQ_CARICO_LOG_IN_D.NextVal INTO NewID FROM DUAL;
  SELECT SYSDATE INTO ADESSO FROM DUAL;
  :NEW.D_CARICO_LOG_IN_D_ID := NewID;
  :NEW.INS_DATA := ADESSO;
  :NEW.UPD_DATA := ADESSO;
  :NEW.VERSION  := ADESSO;
END BI_CARICO_LOG_IN_D;
/