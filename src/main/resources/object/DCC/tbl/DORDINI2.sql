CREATE TABLE  DORDINI2 (

    DORDINI2_ID             NUMBER(19),
    LOAD_ID                 NUMBER(10),
    STATO                   CHAR(1)  DEFAULT '0',
    DATA_CARICAMENTO        DATE,
    NUMRIGA                 NUMBER(10),

    REC                     VARCHAR2(3), 
    NEG                     VARCHAR2(4),
    CLI                     VARCHAR2(6),
    REP                     VARCHAR2(3),
    PID                     VARCHAR2(3),
    NSE                     VARCHAR2(30),
    EXA                     VARCHAR2(4),
    EXM                     VARCHAR2(2),
    EXG                     VARCHAR2(2),
    EXH                     VARCHAR2(6),
    EXN                     VARCHAR2(9),
    NRG                     VARCHAR2(12),
    CDA                     VARCHAR2(13),
    CDB                     VARCHAR2(13),
    QTA                     VARCHAR2(5),
    PRZ                     VARCHAR2(9),
    RIA                     VARCHAR2(4),
    RIM                     VARCHAR2(2),
    RIG                     VARCHAR2(2),
    CDM                     VARCHAR2(3),
    STA                     VARCHAR2(1),
    ACH                     VARCHAR2(4),
    MCH                     VARCHAR2(2),
    GCH                     VARCHAR2(2),
    OCH                     VARCHAR2(6),

    INS_DATA                DATE DEFAULT SYSDATE NOT NULL ENABLE, 
    INS_UTENTE              VARCHAR2(50 BYTE), 
    INS_MODULO              VARCHAR2(50 BYTE) NOT NULL ENABLE, 
    UPD_DATA                DATE, 
    UPD_UTENTE              VARCHAR2(50 BYTE), 
    UPD_MODULO              VARCHAR2(50 BYTE), 
    VERSION                 DATE 

);

ALTER TABLE DORDINI2 ADD CONSTRAINT PK_DORDINI2 PRIMARY KEY (DORDINI2_ID);

CREATE SEQUENCE SQ_DORDINI2 START WITH 1 CACHE 20000;

CREATE SEQUENCE SQ_LOAD_ID_DORDINI2 START WITH 1;

