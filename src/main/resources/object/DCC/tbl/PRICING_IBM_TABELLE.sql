--------------------------------------------------------
--  File creato - venerd�-febbraio-24-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ALBERO_MERC
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ALBERO_MERC" 
   (	"TIPO_RECORD" CHAR(3 BYTE), 
	"TIPO_NODO" CHAR(9 BYTE), 
	"DESCRIZIONE_NODO" VARCHAR2(50 BYTE), 
	"NODO" VARCHAR2(23 BYTE), 
	"DESCRIZIONE_ALTERNATIVA" VARCHAR2(5 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_MERC_ABIL
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_MERC_ABIL" 
   (	"LIVELLO_MERC" VARCHAR2(20 BYTE), 
	"VALORE_MERC" VARCHAR2(50 BYTE), 
	"VALORE_MERC_MORE_ID" NUMBER(19,0), 
	"LIVELLO_MERC_MORE_ID" NUMBER(19,0)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ARTICOLO_X_IVA
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ARTICOLO_X_IVA" 
   (	"DIVISIONE" VARCHAR2(50 BYTE), 
	"COD_ART" VARCHAR2(50 BYTE), 
	"ALIQUOTA" VARCHAR2(20 BYTE), 
	"TASSA" VARCHAR2(50 BYTE), 
	"DATA_INIZIO" VARCHAR2(50 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ARTICOLI
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" 
   (	"ARTICOLO" VARCHAR2(50 BYTE), 
	"TIPO_CODICE" CHAR(3 BYTE), 
	"DESC_ART" VARCHAR2(344 BYTE), 
	"CAMPO_NO_OBB_1" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_2" CHAR(2 BYTE), 
	"PADRE" VARCHAR2(50 BYTE), 
	"CAMPO_NO_OBB_3" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_4" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_5" CHAR(2 BYTE), 
	"FORN_PREV" VARCHAR2(50 BYTE), 
	"UM_GRAMMATURA" VARCHAR2(3 BYTE), 
	"QUANTITA_NETTA" VARCHAR2(40 BYTE), 
	"CAMPO_NO_OBB_6" CHAR(2 BYTE), 
	"PACK" CHAR(1 BYTE), 
	"PREZZO_PRE_STAMPATO" CHAR(1 BYTE), 
	"STAGIONALE" CHAR(1 BYTE), 
	"MARCA" VARCHAR2(40 BYTE), 
	"PRODOTTO_A_MARCHIO" CHAR(1 BYTE), 
	"CAMPO_NO_OBB_7" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_8" CHAR(2 BYTE), 
	"TIPO_ATTRIBUTO_1" CHAR(30 BYTE), 
	"ATTRIBUTO_1" CHAR(3 BYTE), 
	"TIPO_ATTRIBUTO_2" CHAR(5 BYTE), 
	"ATTRIBUTO_2" VARCHAR2(40 BYTE), 
	"CAMPO_NO_OBB_9" VARCHAR2(50 BYTE), 
	"CAMPO_NO_OBB_10" CHAR(2 BYTE), 
	"BIOLOGICO" CHAR(18 BYTE), 
	"BIOL" CHAR(9 BYTE), 
	"NO_LATT" CHAR(23 BYTE), 
	"NOLAT" CHAR(14 BYTE), 
	"NO_GLU" CHAR(22 BYTE), 
	"NOGLU" CHAR(10 BYTE), 
	"PANIERE" CHAR(34 BYTE), 
	"CAMPO_NO_OBB_11" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_12" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_13" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_14" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_15" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_16" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_17" CHAR(2 BYTE), 
	"NODO_1" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO_1" VARCHAR2(50 BYTE), 
	"NODO_2" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO_2" VARCHAR2(50 BYTE), 
	"NODO_3" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO_3" VARCHAR2(50 BYTE), 
	"NODO_4" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO_4" VARCHAR2(50 BYTE), 
	"CAMPO_NO_OBB_18" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_19" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_20" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_21" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_22" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_23" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_24" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_25" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_26" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_27" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_28" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_29" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_30" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_31" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_32" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_33" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_34" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_35" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_36" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_37" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_38" CHAR(2 BYTE), 
	"COD_SOTFAM" VARCHAR2(23 BYTE), 
	"CAMPO_NO_OBB_39" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_40" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_41" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_42" CHAR(2 BYTE), 
	"CAMPO_NO_OBB_43" CHAR(2 BYTE), 
	"LOAD_ID" VARCHAR2(50 BYTE), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ART_X_PDD
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ART_X_PDD" 
   (	"COD_ART" VARCHAR2(50 BYTE), 
	"COD_PDD" VARCHAR2(50 BYTE), 
	"STATO_ART" VARCHAR2(20 BYTE), 
	"DATA_INIZIO_MARK" DATE, 
	"DATA_FINE_MARK" DATE, 
	"COD_MARK" VARCHAR2(50 BYTE), 
	"WEEK" VARCHAR2(50 BYTE), 
	"LOAD_ID" VARCHAR2(50 BYTE), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT 0, 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_LOCATION_DATA
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_LOCATION_DATA" 
   (	"PDD_CODICE" VARCHAR2(50 BYTE), 
	"ORG_CODICE" VARCHAR2(50 BYTE), 
	"STATO" NUMBER(5,0), 
	"PDD_DESCRIZIONE" VARCHAR2(128 BYTE), 
	"CODICE_ZONA" VARCHAR2(50 BYTE), 
	"CODICE_DESCRIZIONE" VARCHAR2(200 BYTE), 
	"RISERVATO_1" VARCHAR2(50 BYTE), 
	"DATA_INIZIO" VARCHAR2(50 BYTE), 
	"DATA_FINE" VARCHAR2(50 BYTE), 
	"CENTRO_DISTRIBUZIONE" VARCHAR2(50 BYTE), 
	"INDIRIZZO_1" VARCHAR2(255 BYTE), 
	"INDIRIZZO_2" VARCHAR2(255 BYTE), 
	"CITTA" VARCHAR2(50 BYTE), 
	"REGIONE" VARCHAR2(50 BYTE), 
	"PROVINCIA" VARCHAR2(50 BYTE), 
	"CAP" VARCHAR2(255 BYTE), 
	"NAZIONE" VARCHAR2(50 BYTE), 
	"PDD_FORMATO" VARCHAR2(50 BYTE), 
	"PDD_METRATURA" VARCHAR2(50 BYTE), 
	"ETA_MEDIA" NUMBER(10,5), 
	"RICAVATO_MEDIO" NUMBER(10,5), 
	"PERC_ETNIA" NUMBER(10,5), 
	"DATA_ULTIMA_LOC" DATE, 
	"COD_LIV_1" VARCHAR2(50 BYTE), 
	"DESC_LIV_1" VARCHAR2(250 BYTE), 
	"COD_LIV_2" VARCHAR2(50 BYTE), 
	"DESC_LIV_2" VARCHAR2(250 BYTE), 
	"COD_LIV_3" VARCHAR2(50 BYTE), 
	"DESC_LIV_3" VARCHAR2(250 BYTE), 
	"COD_LIV_4" VARCHAR2(50 BYTE), 
	"DESC_LIV_4" VARCHAR2(250 BYTE), 
	"COD_LIV_5" VARCHAR2(50 BYTE), 
	"DESC_LIV_5" VARCHAR2(250 BYTE), 
	"COD_LIV_6" VARCHAR2(50 BYTE), 
	"DESC_LIV_6" VARCHAR2(250 BYTE), 
	"COD_LIV_7" VARCHAR2(50 BYTE), 
	"DESC_LIV_7" VARCHAR2(250 BYTE), 
	"COD_LIV_8" VARCHAR2(50 BYTE), 
	"DESC_LIV_8" VARCHAR2(250 BYTE), 
	"COD_LIV_9" VARCHAR2(50 BYTE), 
	"DESC_LIV_9" VARCHAR2(250 BYTE), 
	"COD_LIV_10" VARCHAR2(50 BYTE), 
	"DESC_LIV_10" VARCHAR2(250 BYTE), 
	"COD_LIV_11" VARCHAR2(50 BYTE), 
	"DESC_LIV_11" VARCHAR2(250 BYTE), 
	"COD_LIV_12" VARCHAR2(50 BYTE), 
	"DESC_LIV_12" VARCHAR2(250 BYTE), 
	"MARKET" VARCHAR2(50 BYTE), 
	"DIVISIONE" VARCHAR2(50 BYTE), 
	"ATTR_1" VARCHAR2(128 BYTE), 
	"ATTR_2" VARCHAR2(128 BYTE), 
	"ATTR_3" VARCHAR2(128 BYTE), 
	"ATTR_4" VARCHAR2(128 BYTE), 
	"RISERVATO_2" VARCHAR2(50 BYTE), 
	"TIPO_LOCAZIONE" VARCHAR2(50 BYTE), 
	"TIMEZONE" VARCHAR2(75 BYTE), 
	"SORGENTE_PDD_CHIAVE" VARCHAR2(50 BYTE), 
	"RISERVATO_3" VARCHAR2(50 BYTE), 
	"RISERVATO_4" VARCHAR2(50 BYTE), 
	"FLAG_FRANCHISE" NUMBER(5,0), 
	"LATITUDINE" NUMBER(15,10), 
	"LONGITUDINE" NUMBER(15,10), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_MERCEOLOGIA
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_MERCEOLOGIA" 
   (	"TIPO_RECORD" CHAR(3 BYTE), 
	"TIPO_MERCEOLOGIA" CHAR(1 BYTE), 
	"PADRE" VARCHAR2(23 BYTE), 
	"NODO" VARCHAR2(23 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_COSTI
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_COSTI" 
   (	"COD_ART" VARCHAR2(50 BYTE), 
	"PDD_CODICE" VARCHAR2(20 BYTE), 
	"LOGISTICAL_ITEM_KEY" VARCHAR2(20 BYTE), 
	"VENDOR_KEY" VARCHAR2(20 BYTE), 
	"PRIMARY_VENDOR" VARCHAR2(1 BYTE) DEFAULT '1', 
	"PV_CONVERSION_FACTOR" VARCHAR2(1 BYTE) DEFAULT '0', 
	"DATA_INIZIO" VARCHAR2(10 BYTE), 
	"COSTO" VARCHAR2(50 BYTE), 
	"COSTO_1" VARCHAR2(50 BYTE), 
	"COSTO_2" VARCHAR2(50 BYTE), 
	"COSTO_3" VARCHAR2(50 BYTE), 
	"RESERVED1" VARCHAR2(50 BYTE), 
	"RESERVED2" VARCHAR2(50 BYTE), 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(32 BYTE), 
	"INS_MODULO" VARCHAR2(128 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(32 BYTE), 
	"UPD_MODULO" VARCHAR2(128 BYTE), 
	"VERSION" TIMESTAMP (3) DEFAULT SYSTIMESTAMP, 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0'
   )

   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."COD_ART" IS 'Same Item Identifier as contained in the Sellable Item Interface.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."PDD_CODICE" IS 'Location Key as found in the Location File.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."LOGISTICAL_ITEM_KEY" IS 'For Customers who are interested in price management functionality only, this field is not applicable.
          Currently, not being supported for promotional customers.  Functionality to fully integrate needs to be completed.  This is to be designed to integrate with the Logistical Item Interface.  If logistical item key is an important consideration for promotional customer then discussions with product management, professional services and integration services are needed to complete the design of the functionality.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."VENDOR_KEY" IS 'For Customers who are interested in price management functionality only, this field is not applicable.
          Currently, not being supported for promotional customers.  Functionality to fully integrate needs to be completed.  This is to be designed to integrate with the Vendor Master.  If vendor key is an important consideration for promotional customer then discussions with product management, professional services and integration services are needed to complete the design of the functionality.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."PRIMARY_VENDOR" IS '1 = Yes; 0 = No
          For Customers who are interested in price management functionality only, this field is not applicable.
          Currently, not being supported for promotional customers.  When vendor key is supported this flag will provide the ability to associate the Primary Vendor for an item when multiple vendors exist.
          The default value needs to be 1 if only 1 vendor cost is being represented.  If vendor field is being populated this needs to be maintained to indicate the primary vendor cost that should be used.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."PV_CONVERSION_FACTOR" IS 'Integer value capturing cardinality of logistical items that make up a larger vendor shipping configuration.
          For Customers who are interested in price management functionality only, this field is not applicable.
          Currently, not being supported for promotional customers.  Functionality needs to be developed for the vendor to enable this data feed.  ';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."DATA_INIZIO" IS 'Dates should be provided in MM/DD/YYYY format.
          The Effective Date for an item indicates the date upon which the cost provided becomes effective. We can receive past, current and future effective dates. Typically, once all historical cost data has been seeded, we would only expect to receive current and future cost values.
          Note that when sending delta records, we require the complete schedule of Effective Dates for the product/store be sent when a change in any one of those records takes place. See note below.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."COSTO" IS 'Costo netto/netto';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."COSTO_1" IS 'Netto';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."COSTO_2" IS 'Fattura';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."COSTO_3" IS 'Commerciale';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."RESERVED1" IS 'Currently not used by the application.';
   COMMENT ON COLUMN "BRC_DCC"."PRICING_IBM_COSTI"."RESERVED2" IS 'Currently not used by the application.';
--------------------------------------------------------
--  DDL for Table PRICING_IBM_PDD_X_ZONA
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_PDD_X_ZONA" 
   (	"ENTE" VARCHAR2(50 BYTE), 
	"ZONA_PREZZI" VARCHAR2(50 BYTE), 
	"CODICE_PDD" VARCHAR2(50 BYTE), 
	"CODICE_ZONA" VARCHAR2(10 BYTE), 
	"LOAD_ID" VARCHAR2(50 BYTE), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT 0, 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_PIAZZE_PREZZI
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_PIAZZE_PREZZI" 
   (	"COD_SOCIETA" CHAR(6 BYTE), 
	"TIPO_PIAZZA" CHAR(11 BYTE), 
	"ZONA_PREZZI" VARCHAR2(50 BYTE), 
	"CODICE_ZONA" VARCHAR2(10 BYTE), 
	"CODICE_PDD" VARCHAR2(50 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_PREZZI_PUBBLICO
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_PREZZI_PUBBLICO" 
   (	"COD_ART" VARCHAR2(50 BYTE), 
	"PDD_CODICE" VARCHAR2(20 BYTE), 
	"DATA_INIZIO" VARCHAR2(10 BYTE), 
	"UNITA_PREZZO" VARCHAR2(50 BYTE), 
	"RAPP_CONV" NUMBER, 
	"LOCALE" NUMBER, 
	"USER_LOCK" NUMBER, 
	"FLAG_1" CHAR(1 BYTE), 
	"FLAG_2" CHAR(1 BYTE), 
	"FLAG_3" CHAR(1 BYTE), 
	"FLAG_4" CHAR(1 BYTE), 
	"FLAG_5" CHAR(1 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_RIL_CONC
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_RIL_CONC" 
   (	"COD_ART" VARCHAR2(50 BYTE), 
	"COD_PDV" VARCHAR2(50 BYTE), 
	"COD_CONCORRENTE" VARCHAR2(50 BYTE), 
	"PREZZO_RILEVATO" VARCHAR2(50 BYTE), 
	"QTA_RIFERIMENTO_PREZZO" NUMBER, 
	"TIPO_RILEVAZIONE" VARCHAR2(40 BYTE), 
	"DATA_RILEVAZIONE" VARCHAR2(10 BYTE), 
	"LOAD_ID" NUMBER(19,0), 
	"FLAG_ELAB" CHAR(1 BYTE) DEFAULT '0', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (6)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_PDV_ABIL
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_PDV_ABIL" 
   (	"PDD_CODICE" VARCHAR2(20 BYTE), 
	"PDD_CODICE_CEDI" VARCHAR2(20 BYTE), 
	"PDD_ID_MORE" NUMBER(19,0)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ART_DELTA
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ART_DELTA" 
   (	"ARTICOLO_ID" NUMBER(19,0), 
	"UPD_DATA" DATE, 
	"ORIG" NUMBER(19,0)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_ZONA_PREZZO_ABIL
--------------------------------------------------------

  CREATE TABLE "BRC_DCC"."PRICING_IBM_ZONA_PREZZO_ABIL" 
   (	"ZONA_PREZZI" VARCHAR2(50 BYTE), 
	"CODICE_ZONA" VARCHAR2(10 BYTE)
   )
--------------------------------------------------------
--  DDL for Table PRICING_IBM_TMP_ARTICOLI
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" 
   (	"ARTICOLO" VARCHAR2(50 BYTE), 
	"TIPO_CODICE" CHAR(3 BYTE), 
	"DESC_ART" VARCHAR2(344 BYTE), 
	"NULL1" VARCHAR2(100 BYTE), 
	"NULL2" VARCHAR2(100 BYTE), 
	"PADRE" VARCHAR2(50 BYTE), 
	"NULL3" VARCHAR2(100 BYTE), 
	"NULL4" VARCHAR2(100 BYTE), 
	"NULL5" VARCHAR2(100 BYTE), 
	"FORN_PREV" VARCHAR2(50 BYTE), 
	"UM_GRAMMATURA" VARCHAR2(3 BYTE), 
	"QUANTITA_NETTA" VARCHAR2(40 BYTE), 
	"NULL6" VARCHAR2(100 BYTE), 
	"PACK" CHAR(1 BYTE), 
	"PREZZO_PRE_STAMPATO" CHAR(1 BYTE), 
	"STAGIONALE" CHAR(1 BYTE), 
	"MARCA" VARCHAR2(40 BYTE), 
	"PRODOTTO_A_MARCHIO" CHAR(1 BYTE), 
	"NULL7" VARCHAR2(100 BYTE), 
	"NULL8" VARCHAR2(100 BYTE), 
	"TIPO_ATTRIBUTO_1" CHAR(30 BYTE), 
	"ATTRIBUTO_1" CHAR(3 BYTE), 
	"TIPO_ATTRIBUTO_2" CHAR(5 BYTE), 
	"ATTRIBUTO_2" VARCHAR2(40 BYTE), 
	"NULL8A" VARCHAR2(100 BYTE), 
	"NULL8B" VARCHAR2(100 BYTE), 
	"PRODOTTI_BIOLOGICI" CHAR(18 BYTE), 
	"BIOL" CHAR(9 BYTE), 
	"PRODOTTI_SENZA_LATTOSIO" CHAR(23 BYTE), 
	"NOLAT" CHAR(14 BYTE), 
	"PRODOTTI_SENZA_GLUTINE" CHAR(22 BYTE), 
	"NOGLU" CHAR(10 BYTE), 
	"PANIERE" CHAR(34 BYTE), 
	"NULL9" VARCHAR2(100 BYTE), 
	"NULL10" VARCHAR2(100 BYTE), 
	"NULL11" VARCHAR2(100 BYTE), 
	"NULL12" VARCHAR2(100 BYTE), 
	"NULL13" VARCHAR2(100 BYTE), 
	"NULL14" VARCHAR2(100 BYTE), 
	"NULL15" VARCHAR2(100 BYTE), 
	"NODO1" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO1" VARCHAR2(50 BYTE), 
	"NODO2" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO2" VARCHAR2(50 BYTE), 
	"NODO3" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO3" VARCHAR2(50 BYTE), 
	"NODO4" VARCHAR2(16 BYTE), 
	"DESCRIZIONE_NODO4" VARCHAR2(50 BYTE), 
	"NULL16" VARCHAR2(100 BYTE), 
	"NULL17" VARCHAR2(100 BYTE), 
	"NULL18" VARCHAR2(100 BYTE), 
	"NULL19" VARCHAR2(100 BYTE), 
	"NULL20" VARCHAR2(100 BYTE), 
	"NULL21" VARCHAR2(100 BYTE), 
	"NULL22" VARCHAR2(100 BYTE), 
	"NULL23" VARCHAR2(100 BYTE), 
	"NULL24" VARCHAR2(100 BYTE), 
	"NULL25" VARCHAR2(100 BYTE), 
	"NULL26" VARCHAR2(100 BYTE), 
	"NULL27" VARCHAR2(100 BYTE), 
	"NULL28" VARCHAR2(100 BYTE), 
	"NULL29" VARCHAR2(100 BYTE), 
	"NULL30" VARCHAR2(100 BYTE), 
	"NULL31" VARCHAR2(100 BYTE), 
	"NULL32" VARCHAR2(100 BYTE), 
	"NULL33" VARCHAR2(100 BYTE), 
	"NULL34" VARCHAR2(100 BYTE), 
	"NULL35" VARCHAR2(100 BYTE), 
	"NULL36" VARCHAR2(100 BYTE), 
	"COD_SOTFAM" VARCHAR2(23 BYTE), 
	"NULL37" VARCHAR2(100 BYTE), 
	"NULL38" VARCHAR2(100 BYTE), 
	"NULL39" VARCHAR2(100 BYTE), 
	"NULL40" VARCHAR2(100 BYTE), 
	"NULL41" VARCHAR2(100 BYTE), 
	"ARTICOLO_ID" NUMBER(19,0), 
	"ART_BASE_ID" NUMBER(19,0), 
	"FORNITORE_PREVALENTE_ID" NUMBER(19,0), 
	"SESSION_ID" NUMBER(19,0)
   ) ON COMMIT DELETE ROWS ;
--------------------------------------------------------
--  DDL for Index IX_IBM_ALB_MERC_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ALB_MERC_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_ALBERO_MERC" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IX_IBM_ALBMERC_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ALBMERC_FE" ON "BRC_DCC"."PRICING_IBM_ALBERO_MERC" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_AXI_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_AXI_FE" ON "BRC_DCC"."PRICING_IBM_ARTICOLO_X_IVA" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_ART_LOADID
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ART_LOADID" ON "BRC_DCC"."PRICING_IBM_ARTICOLI" ("LOAD_ID") 

--------------------------------------------------------
--  DDL for Index IX_IBM_ART_ART
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ART_ART" ON "BRC_DCC"."PRICING_IBM_ARTICOLI" ("ARTICOLO") 

--------------------------------------------------------
--  DDL for Index IX_IBM_ART_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ART_FE" ON "BRC_DCC"."PRICING_IBM_ARTICOLI" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_ART_X_PDD_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_ART_X_PDD_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_ART_X_PDD" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IDX_IBM_ARTXPDD_LOA
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IDX_IBM_ARTXPDD_LOA" ON "BRC_DCC"."PRICING_IBM_ART_X_PDD" ("LOAD_ID") 

--------------------------------------------------------
--  DDL for Index IX_IBM_AXP_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_AXP_FE" ON "BRC_DCC"."PRICING_IBM_ART_X_PDD" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_MERCEOLOGIA_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_MERCEOLOGIA_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_MERCEOLOGIA" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IX_IBM_MERC_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_MERC_FE" ON "BRC_DCC"."PRICING_IBM_MERCEOLOGIA" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_COSTI_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_COSTI_FE" ON "BRC_DCC"."PRICING_IBM_COSTI" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_COSTI_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_COSTI_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_COSTI" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IX_IBM_PXZ_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_PXZ_FE" ON "BRC_DCC"."PRICING_IBM_PDD_X_ZONA" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IX_IBM_PIAZ_PREZZI_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_PIAZ_PREZZI_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_PIAZZE_PREZZI" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IDX_IBM_PRZ_PUBB_PDD_ART_DATA
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IDX_IBM_PRZ_PUBB_PDD_ART_DATA" ON "BRC_DCC"."PRICING_IBM_PREZZI_PUBBLICO" ("COD_ART", "PDD_CODICE", "DATA_INIZIO") 

--------------------------------------------------------
--  DDL for Index IX_IBM_PREZZI_PUB_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_PREZZI_PUB_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_PREZZI_PUBBLICO" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IX_IBM_PP_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_PP_FE" ON "BRC_DCC"."PRICING_IBM_PREZZI_PUBBLICO" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Index IDX_IBM_PRZ_PUBB_LOA
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IDX_IBM_PRZ_PUBB_LOA" ON "BRC_DCC"."PRICING_IBM_PREZZI_PUBBLICO" ("LOAD_ID") 

--------------------------------------------------------
--  DDL for Index IX_IBM_RIL_CONC_FLAG_ELAB
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_RIL_CONC_FLAG_ELAB" ON "BRC_DCC"."PRICING_IBM_RIL_CONC" (NVL("FLAG_ELAB",'0')) 

--------------------------------------------------------
--  DDL for Index IX_IBM_RC_FE
--------------------------------------------------------

  CREATE INDEX "BRC_DCC"."IX_IBM_RC_FE" ON "BRC_DCC"."PRICING_IBM_RIL_CONC" ("FLAG_ELAB") 

--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_ALBERO_MERC
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_ALBERO_MERC" BEFORE
  INSERT ON PRICING_IBM_ALBERO_MERC FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_ALBERO_MERC ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_ALBERO_MERC" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_ART_X_PDD
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_ART_X_PDD" BEFORE
  INSERT ON PRICING_IBM_ART_X_PDD FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_ART_X_PDD ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_ART_X_PDD" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_LOCATION_DATA
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_LOCATION_DATA" BEFORE
  INSERT ON "PRICING_IBM_LOCATION_DATA" FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_LOCATION_DATA ;
/
ALTER TRIGGER "BRC_DCC"."BI_LOCATION_DATA" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_MERCEOLOGIA
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_MERCEOLOGIA" BEFORE
  INSERT ON PRICING_IBM_MERCEOLOGIA FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_MERCEOLOGIA ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_MERCEOLOGIA" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_COSTI
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_COSTI" BEFORE
  INSERT ON PRICING_IBM_COSTI FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_COSTI;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_COSTI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_PIAZZE_PREZZI
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_PIAZZE_PREZZI" BEFORE
  INSERT ON PRICING_IBM_PIAZZE_PREZZI FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_PIAZZE_PREZZI ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_PIAZZE_PREZZI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_PREZZI_PUBBLICO
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_PREZZI_PUBBLICO" BEFORE
  INSERT ON PRICING_IBM_PREZZI_PUBBLICO FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_PREZZI_PUBBLICO ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_PREZZI_PUBBLICO" ENABLE;
--------------------------------------------------------
--  DDL for Trigger BI_PRICING_IBM_RIL_CONC
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BRC_DCC"."BI_PRICING_IBM_RIL_CONC" BEFORE
  INSERT ON PRICING_IBM_RIL_CONC FOR EACH ROW DECLARE ADESSO DATE;
  BEGIN
    SELECT SYSDATE INTO ADESSO FROM DUAL;
    :NEW.INS_DATA                  := ADESSO;
    :NEW.UPD_DATA                  := ADESSO;
    :NEW.VERSION                   := ADESSO;
  END BI_PRICING_IBM_RIL_CONC ;
/
ALTER TRIGGER "BRC_DCC"."BI_PRICING_IBM_RIL_CONC" ENABLE;
--------------------------------------------------------
--  Constraints for Table PRICING_IBM_ARTICOLI
--------------------------------------------------------

  ALTER TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" MODIFY ("DESCRIZIONE_NODO_4" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" MODIFY ("DESCRIZIONE_NODO_3" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" MODIFY ("DESCRIZIONE_NODO_2" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" MODIFY ("DESCRIZIONE_NODO_1" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_ARTICOLI" MODIFY ("FORN_PREV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PRICING_IBM_COSTI
--------------------------------------------------------

  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("UPD_MODULO" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("UPD_UTENTE" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("UPD_DATA" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("INS_MODULO" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("INS_UTENTE" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("INS_DATA" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("COSTO" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("DATA_INIZIO" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("PDD_CODICE" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_COSTI" MODIFY ("COD_ART" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PRICING_IBM_TMP_ARTICOLI
--------------------------------------------------------

  ALTER TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" MODIFY ("DESCRIZIONE_NODO4" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" MODIFY ("DESCRIZIONE_NODO3" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" MODIFY ("DESCRIZIONE_NODO2" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" MODIFY ("DESCRIZIONE_NODO1" NOT NULL ENABLE);
  ALTER TABLE "BRC_DCC"."PRICING_IBM_TMP_ARTICOLI" MODIFY ("FORN_PREV" NOT NULL ENABLE);
  
  -- CFG_PRICING_IBM
  CREATE TABLE "BRC_DCC"."CFG_PRICING_IBM" 
    ("CFG_PRICING_IBM_ID" NUMBER(19,0) NOT NULL ENABLE, 
	"CODICE" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"NOME_TABELLA" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"ABILITATO" CHAR(1 BYTE) DEFAULT 'N', 
	"FREQUENZA" CHAR(1 BYTE) DEFAULT 'S', 
	 CONSTRAINT "CFG_PRICING_IBM_PK" PRIMARY KEY ("CFG_PRICING_IBM_ID"));
	 
  CREATE TABLE F_PRICING_IBM2DCC
  (
    ARTICOLO_CODICE    VARCHAR2(50),
    LISTINO_VEN_T_CODICE VARCHAR2(50),
    PREZZO             NUMBER(19,4),
    DATA_VALIDITA      DATE,
    SCENARIO           VARCHAR2(50)
  )
  ORGANIZATION EXTERNAL
  (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EXTERNAL_DIR_IBM
    ACCESS PARAMETERS 
    (
      RECORDS DELIMITED BY NEWLINE
      FIELDS TERMINATED BY '|'
      MISSING FIELD VALUES ARE NULL
    (
      ARTICOLO_CODICE,
      LISTINO_VEN_T_CODICE,
      PREZZO,
      DATA_VALIDITA   DATE "YYYY-MM-DD",
      SCENARIO
      )
    )
    
    LOCATION (EXTERNAL_DIR_IBM: 'dtp1001_maxidi.txt')
  )
  REJECT LIMIT 0;
  
/