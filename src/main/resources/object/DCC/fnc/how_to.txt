Aggiungere sql file contente l'oggetto di database (con lo stesso nome - p.e UTL_RICHIEST.sql)
Ricordarsi di aggiungere sempre in testa (prime 2 righe) le seguenti opportunamente configurate

--liquibase formatted sql
--changeset @@author@@:@@description@@ logicalFilePath:@@object_name@@ runOnChange:true runInTransaction:true failOnError:true

customizzandone i seguenti valori:

@@author@@ = autore dell'ultima modifica
@@description@@ = descrizione della modifica (o riferimento al task di tracking)
@@object_name@@ = nome dell'oggetto - tipicamente stesso nome dell file.
