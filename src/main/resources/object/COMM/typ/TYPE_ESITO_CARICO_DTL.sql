create or replace type TYPE_ESITO_CARICO_DTL  IS OBJECT
(
  ESITO                       VARCHAR2 (32) , 
  DATA_ELABORAZIONE           DATE,
  SESSIONE_ID                 VARCHAR2 (32) ,
  MESSAGGIO                   VARCHAR2 (1024) ,  
  MESSAGGIO_UTENTE            VARCHAR2 (1024)  
)
;  
/