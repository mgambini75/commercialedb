create or replace type TYPE_ESITO_CARICO  IS OBJECT
(
  LOAD_ID                     NUMBER ,
  SESSIONE_ID                 VARCHAR2 (32) ,
  NUMERO_DOC                  VARCHAR2 (32) , 
  DATA_DOC                    DATE , 
  ESITO                       VARCHAR2 (32) , 
  CODICE                      VARCHAR2 (32) ,   
  DATA_ELABORAZIONE           DATE,
  CODICE_NEGOZIO              VARCHAR2 (32) ,   
  FORNITORE_FATTURA           VARCHAR2 (32) ,    
  FORNITORE_ORDINE            VARCHAR2 (32) ,  
  NUMERO_ORD                  VARCHAR2 (32) ,  
  PREF_ORD                    VARCHAR2 (32) ,  
  suff_ord                    VARCHAR2 (32) ,  
  FILE_NAME                   VARCHAR2 (64) ,  
  ESISTE_DETTAGLIO            NUMBER
)
;
/