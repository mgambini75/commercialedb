create or replace view v_criterio_listino_vnd
as
select (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'PDD'
       ) as pdd_id,
       (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'CNL'
       ) as canale_id,
       (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'ZNA'
       ) as zona_id,
       (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'TLC'
       ) as tipo_licenza_id,
       (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'INS'
       ) as insegna_id,
       (select vc.valore
          from criterio_regola cr, criterio c, valore_criterio vc
         where lvr.regola_id = cr.regola_id
           and cr.criterio_id = c.criterio_id
           and cr.criterio_regola_id = vc.criterio_regola_id
           and c.codice = 'DIV'
       ) as divisione_id,
       fr.priorita as priorita_regola, 
       lvr.priorita as priorita_listino, lvr.listino_vnd_id, lvr.linea_prezzo_id, lvr.tipo_listino, lvr.annullato, 
       r.data_inizio, r.data_fine, 
       r.regola_id, fr.formato_regola_id, fr.codice as formato_codice, fr.descrizione as formato_descrizione
  from listino_vnd_x_regola lvr, regola r, formato_regola fr, contesto_uso_regola cur
 where lvr.regola_id = r.regola_id  
   and r.formato_regola_id = fr.formato_regola_id
   and fr.contesto_uso_regola_id = cur.contesto_uso_regola_id
   and cur.codice in ('LSTPBL', 'LSTCSS')
 ;