create or replace PACKAGE      UTL_CARICHI AS
/******************************************************************************
   NAME:       UTL_CARICHI
   PURPOSE:
   DEPENDENCY: NCL_UTL_MESSAGGIO

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0.0    -			  -			 	              Created this package.
   1.1.0.0    27/10/2016  C.Carretti 	      Riorganizzazione e aggiunta stati testata
   1.1.0.1    28/11/2016  C.Carretti        TaskDitech #24590: Gestire nuovi errori artcioli e tipo consegna su import carichi
   1.1.0.2    28/11/2016  C.Carretti        TaskDitech #24573: Aggiunta intercettazione costi importazione da fornitore
   1.1.0.3    27/01/2017  C.Carretti        TaskDitech #24863: Miglioramento manutenzine carichi
   1.1.0.4    10/02/2017  C.Carretti        TaskDitech #24863: Refactory interno (split CLEANING e VALID)
******************************************************************************/
  CODE_STATO_TESTATA_VALIDA					    CONSTANT VARCHAR2(5 BYTE) := 'VALID';
  CODE_STATO_TESTATA_TRASFERITA         CONSTANT VARCHAR2(5 BYTE) := 'TRANS';
  CODE_STATO_TESTATA_TR_MOV         CONSTANT VARCHAR2(5 BYTE) := 'TRMOV';
  CODE_STATO_TESTATA_TR_CAR         CONSTANT VARCHAR2(5 BYTE) := 'TRCAR';
  CODE_STATO_TESTATA_ERRATA  		        CONSTANT VARCHAR2(5 BYTE) := 'ERROR';
  MESSAGGIO_CONTESTO                    CONSTANT VARCHAR2(5 BYTE) := 'OC';

 /*
  * Prepara l'elaborazione del carico (e la relativa verifica) agendo su anagrafica ed assortimento fornitori
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE PREPARA( P_LOAD_ID NUMBER, P_INS_MODULO VARCHAR2);

  /*
  * Esegue intera validazione di righe e testate
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE VALIDA( P_LOAD_ID NUMBER, P_INS_MODULO VARCHAR2);

  /*
  * Effettua l'elaborazione del carico sul DCC creando le testate (ed effettuando alcune validazioni ulteriori)
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE ELABORA_TESTATE_SU_DCC( P_LOAD_ID NUMBER, P_INS_MODULO VARCHAR2);
  
  /*
  * Effettua l'elaborazione del carico sul DCC decorando le righe con le info necessarie
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE ELABORA_RIGHE_SU_DCC( P_LOAD_ID NUMBER, P_INS_MODULO VARCHAR2);

  /*
  * Effettua il caricamento di un LOAD ID dei carichi, effettuando le varie fasi necessari di preparazione, verifica ed elaborazione
  * CEDI
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE LOAD_ONE_ID (P_LOAD_ID     IN NUMBER , P_INS_MODULO     IN VARCHAR2) ;
  
  /*
  * Effettua il caricamento di un LOAD ID dei carichi, effettuando le varie fasi necessari di preparazione, verifica ed elaborazione
  * per fonti diverse come l'EXCEL
  * 
  * TODO: da uniformare con la LOAD_ONE_ID
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  */
  PROCEDURE LOAD_ONE_ID_EXC (P_LOAD_ID     IN NUMBER , P_INS_MODULO     IN VARCHAR2) ;

/*
  * Calcola (su tabella temporanea) i carichi presenti sul DCC da trasferiere in COMM
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  * P_SESSION_ID Sessione
  */
  PROCEDURE ESTRAI_DA_DCC_INT (P_LOAD_ID   NUMBER, P_MODULO_ID VARCHAR2, P_SESSION_ID NUMBER, P_NUMDOCACC VARCHAR2 DEFAULT NULL, P_PDD_ID NUMBER DEFAULT NULL );
  
  /*
  * Calcola (su tabella temporanea) i carichi presenti sul DCC da trasferiere in COMM e li estrai
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  * P_NUMDOCACC Numero documento accompagnatorio da estrarre
  * P_PDD_ID Identificativo del PDD
  * P_SESSION_ID Sessione
  */
  FUNCTION ESTRAI_DA_DCC (P_LOAD_ID   NUMBER, P_MODULO_ID VARCHAR2, P_NUMDOCACC VARCHAR2 DEFAULT NULL, P_PDD_ID NUMBER DEFAULT NULL, P_SESSION_ID NUMBER) RETURN SYS_REFCURSOR;
  
  /*
  * Resetta l'intero stato di validazione del carichi presenti sul DCC da trasferiere in COMM
  *
  * P_LOAD_ID LOAD ID del carico
  * P_INS_MODULO Codice fornitore (o modulo) di inserimento
  * P_NUMDOCACC Numero documento accompagnatorio da estrarre
  */
  PROCEDURE RESET_STATO (P_LOAD_ID  IN NUMBER, P_INS_MODULO VARCHAR2, P_NUMDOCACC VARCHAR2 DEFAULT NULL);

  -- viene chiamata se il flag DCC2MORE.DB_MODXLOAXTAB_NSC.COMPLETA_CARICO  � attivo;
  -- viene richiamata, alla fine dell�intero caricamento (non quindi alla fine del caricamento di ogni testata) la stored procedure
  PROCEDURE COMPLETA_CARICO (P_LOAD_ID  IN NUMBER, P_INS_MODULO VARCHAR2);

  PROCEDURE ELIMINA_CARICHI_PDD (P_PDD_CODICE  IN VARCHAR2, D_DATA_DA DATE, P_DATA_A DATE, P_BACKUP IN CHAR DEFAULT 'S');

  FUNCTION GET_NUMERO_DOC ( P_PREF_DOC1 IN VARCHAR2, P_NR_DOC1 IN VARCHAR2,  P_SUF_DOC1 IN VARCHAR2,
                            P_PREF_DOC2 IN VARCHAR2, P_NR_DOC2 IN VARCHAR2,  P_SUF_DOC2 IN VARCHAR2   )  RETURN VARCHAR2;

  FUNCTION IS_FORNITORE_DIRETTO ( P_DIVISIONE_ID NUMBER,P_FORNITORE_ID IN NUMBER  )  RETURN NUMBER;

  FUNCTION VERIFICA_ESISTENZA_CARICO ( P_DATA_DOC IN VARCHAR2,
                                       P_PDD_CODICE  IN VARCHAR2,
                                       P_NUMERO_DOC IN VARCHAR2,
                                       P_SUF_DOC IN VARCHAR2,
                                       P_FORNITORE_CODICE IN VARCHAR2,
                                       P_CARICO_T_ID  OUT NUMBER,
                                       P_RIGHE  OUT NUMBER,
                                       P_QUANTITA OUT NUMBER,
                                       P_VALORE OUT NUMBER )  RETURN NUMBER;

  FUNCTION GET_ESITO_CARICO ( P_LOAD_ID IN NUMBER, P_INS_MODULO  in varchar2 )
    RETURN TBL_ESITO_CARICO  PIPELINED;  
    
  FUNCTION GET_ESITO_CARICO_DTL ( P_LOAD_ID IN NUMBER, P_NUMERO_DOC  in varchar2 )
    RETURN TBL_ESITO_CARICO_DTL  PIPELINED; 
    
END UTL_CARICHI;    
/