create or replace PACKAGE BODY PKG_UNITA_MISURA AS

/******************************************************************************
 PURPOSE  : Fornisce una serie di procedure per convertire fra le varie  
            unita' di misura. 
 LAST UPD : 06/02/2017
 
 REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0.0.0    06/02/2017  Ivan Lo Castro   Creato il package

 ******************************************************************************/

--| Costanti globali     |---------------------------------------------------------------------------
C_NOME_MODULO     CONSTANT VARCHAR2(30)  := 'PKG_UNITA_MISURA';

--| Tipi e Variabili globali    |---------------------------------------------------------------------
type um_type is record (
  unita_misura_tipo_id      number,
  unita_misura_tipo_codice  varchar2(10),
  convertibile              varchar2(1),
  unita_misura_id           number,
  codice                    varchar2(10),
  conversione               float,
  unita_misura_base_id      number
);

type um_list_type is table of um_type index by PLS_INTEGER;


--| Procedure Private    |----------------------------------------------------------------------------

/*
  Costruisce e ritorna un array associativo con le unit� di misura.
  L'indice dell'array e' l'UNITA_MISURA_ID.
  La funzione e' in cache, cosicche' viene esegutita solo la prima volta (vedere
  documentazione oracle su RESULT_CACHE), mentre le volte successive ritorna l'array prendendolo 
  dalla cache. 
*/
FUNCTION GET_UM_LIST 
RETURN um_list_type
RESULT_CACHE RELIES_ON (ncl_unita_misura, ncl_unita_misura_tipo)
IS

l_um_list um_list_type;

BEGIN

  for recUM in (select umt.unita_misura_tipo_id, umt.codice as unita_misura_tipo_codice, 
                       umt.convertibile, um.unita_misura_id, um.codice,
                       um.conversione, umt.unita_misura_base_id
                  from NCL_UNITA_MISURA um, NCL_UNITA_MISURA_TIPO umt
                 where um.unita_misura_tipo_id = umt.unita_misura_tipo_id
                 order by um.unita_misura_id)
  loop
    l_um_list(recUM.unita_misura_id) := recUM;
  end loop;

  return l_um_list;
  
END;


--| Procedure Pubbliche    |--------------------------------------------------------------------------

/*
  Dato il codice dell'unita' di misura, ritorna il corrispondente ID.
*/
FUNCTION GET_UM_ID(p_um_codice NCL_UNITA_MISURA.CODICE%type) 
RETURN NCL_UNITA_MISURA.UNITA_MISURA_ID%type
RESULT_CACHE RELIES_ON (ncl_unita_misura, ncl_unita_misura_tipo)
IS
  l_um_list um_list_type;
BEGIN
  -- Recupera l'array associativo con le UM. L'indice dell'array e' l'ID dell'UM.
  l_um_list := get_um_list();
  
  for i in l_um_list.first..l_um_list.last loop
    if l_um_list(i).codice = p_um_codice then 
      return l_um_list(i).unita_misura_id;
    end if;
  end loop;

  return null;
END;

-------------------------------------------------------------------------------------------------------

/*
  Data l'ID dell'unita' di misura, ritorna il corrispondente codice.
*/
FUNCTION GET_UM_CODICE(p_um_ID NCL_UNITA_MISURA.UNITA_MISURA_ID%type) 
RETURN NCL_UNITA_MISURA.CODICE%type
RESULT_CACHE
IS
BEGIN
  return get_um_list()(p_um_ID).CODICE;
END;


/*
  Converte nell'unita' di misura di arrivo una quantita' espressa nell'unita' di
  misura di partenza. Le due unit� di misura devono essere dello stesso 
  tipo (ad esempio: di peso, di lunghezza, etc.), il flag TIPO_UNITA_MISURA.CONVERTIBILE
  deve essere impostato a 'S' ed il campo UNITA_MISURA.CONVERSIONE deve essere diverso
  da zero. 
  Se la conversione fra le due unit� di misura non � possibile la procedura lancia una
  eccezione.
  Per convertire fra KG, PZ e IC NON usare questa funzione, ma bensi' la funzione
  CONVERTI_UM_LOGISTICA (vedi).    
*/
FUNCTION CONVERTI_UM(p_qta number,
                     p_um_id_partenza NCL_UNITA_MISURA.UNITA_MISURA_ID%type, 
                     p_um_id_arrivo NCL_UNITA_MISURA.UNITA_MISURA_ID%type)  
RETURN float
IS

l_um_list um_list_type;
l_um_partenza um_type;
l_um_arrivo   um_type;

BEGIN
  
  if p_um_id_partenza = p_um_id_arrivo then
    return p_qta;
  end if;
  
  -- Recupera l'array associativo con le UM. L'indice dell'array e' l'ID dell'UM.
  l_um_list := get_um_list();
  
  -- Preleva dall'array l'UM di partenza e lancia un'eccezione se non la trova
  begin
    l_um_partenza := l_um_list(p_um_id_partenza);
  exception
    when others then
      raise_application_error(-20001, 'L''UM id ' || p_um_id_partenza || ' non esiste nella tabella NCL_UNITA_MISURA');
  end;
    
  -- Preleva dall'array l'UM di arrivo e lancia un'eccezione se non la trova
  begin  
    l_um_arrivo := l_um_list(p_um_id_arrivo);
  exception
    when others then
      raise_application_error(-20001, 'L''UM id ' || p_um_id_arrivo || ' non esiste nella tabella NCL_UNITA_MISURA');
  end;
  
  -- Controlla che le due UM sia convertibili fra loro e lancia un'eccezione in caso contrario
  if l_um_partenza.unita_misura_tipo_id != l_um_arrivo.unita_misura_tipo_id then
    raise_application_error(-20002, 'Non e'' possibile convertire l''UM id ' || p_um_id_partenza || ' nell''UM id ' || p_um_id_arrivo || ' perche'' sono di tipi diversi');
  elsif l_um_partenza.convertibile = 'N' then
    raise_application_error(-20003, 'L''UM id ' || p_um_id_partenza || ' non e'' convertibile nell''UM id ' || p_um_id_arrivo || '. Controllare NCL_UNITA_MISURA_TIPO.CONVERTIBILE');
  elsif l_um_partenza.conversione = 0 or l_um_arrivo.conversione = 0 then
    raise_application_error(-20004, 'Non e'' possibile convertire l''UM id ' || p_um_id_partenza || ' nell''UM id ' || p_um_id_arrivo 
                            || ' perche'' i rapporti di conversione sono a zero. Controllare il campo NCL_UNITA_MISURA.CONVERSIONE per le due unita'' di misura');
  end if;
  
  return p_qta * (l_um_arrivo.conversione / l_um_partenza.conversione);

END;

-----------------------------------------------------------------------------------------------------

/*
  Converte fra le unita' di misura logistiche KG (o altri pesi: GR, HG, etc), PZ e IC.
  Se si passano unit� di misura diverse da queste tre, oppure i parametri passati non
  sono coerenti (peso a zero o null, um di peso non valorizzata, imballi cessione non valorizzati
  o a zero) viene lanciata un'eccezione.
*/
FUNCTION CONVERTI_UM_LOGISTICA(p_qta_partenza number,
                               p_um_id_partenza NCL_UNITA_MISURA.UNITA_MISURA_ID%type, 
                               p_um_id_arrivo NCL_UNITA_MISURA.UNITA_MISURA_ID%type,
                               p_imballo_cessione number,
                               p_peso_medio_pezzo float,
                               p_um_id_peso_medio_pezzo NCL_UNITA_MISURA.UNITA_MISURA_ID%type,
                               p_eccezione_se_errore char := 'S')  
RETURN float
IS

l_um_list um_list_type;
l_um_partenza um_type;
l_um_arrivo   um_type;
l_conversione_non_supportata boolean; 
l_qta_arrivo float;

BEGIN
  
  if p_um_id_partenza = p_um_id_arrivo then
    return p_qta_partenza;
  end if;
  
  l_conversione_non_supportata := false;
  
  -- Recupera l'array associativo con le UM. L'indice dell'array e' l'ID dell'UM.
  l_um_list := get_um_list();

  -- Preleva dall'array l'UM di partenza e lancia un'eccezione se non la trova
  begin
    l_um_partenza := l_um_list(p_um_id_partenza);
  exception
    when others then
      raise_application_error(-20001, 'L''UM id ' || p_um_id_partenza || ' non esiste nella tabella NCL_UNITA_MISURA');
  end;
    
  -- Preleva dall'array l'UM di arrivo e lancia un'eccezione se non la trova
  begin  
    l_um_arrivo := l_um_list(p_um_id_arrivo);
  exception
    when others then
      raise_application_error(-20002, 'L''UM id ' || p_um_id_arrivo || ' non esiste nella tabella NCL_UNITA_MISURA');
  end;

  -- Se l'UM di partenza e quella di arrivo sono dello stesso tipo (pesi, misure, volumi, etc.)  
  if l_um_partenza.unita_misura_tipo_id = l_um_arrivo.unita_misura_tipo_id 
     and l_um_partenza.convertibile = 'S'
  then
    return CONVERTI_UM(p_qta_partenza, p_um_id_partenza, p_um_id_arrivo);
  end if;
  
  if l_um_partenza.codice = C_UM_CODICE_IMBALLI_CESSIONE then
    
    if l_um_arrivo.codice = C_UM_CODICE_PEZZI then
      -- Da IC a PZ
      l_qta_arrivo := p_qta_partenza * p_imballo_cessione;
    elsif l_um_arrivo.unita_misura_tipo_codice = C_TIPO_UM_CODICE_PESO then
      -- Da IC a KG (o altri pesi): (Quantita IC) * (IMBALLO CESSIONE) * (Peso medio pezzo convertito in KG o altri pesi)
      l_qta_arrivo := p_qta_partenza * p_imballo_cessione * CONVERTI_UM(p_peso_medio_pezzo, p_um_id_peso_medio_pezzo, p_um_id_arrivo);
    else
      -- Conversione non supportata
      l_conversione_non_supportata := true;
    end if;
  
  elsif l_um_partenza.codice = C_UM_CODICE_PEZZI then
  
    if l_um_arrivo.codice = C_UM_CODICE_IMBALLI_CESSIONE then
      -- Da PZ a IC
      if p_imballo_cessione is null or p_imballo_cessione = 0 then
        raise_application_error(-20003, 'Impossibile convertire da PZ a IC perche'' l''imballo cessione e'' zero o null');
      else
        l_qta_arrivo := p_qta_partenza / p_imballo_cessione;
      end if;
    elsif l_um_arrivo.unita_misura_tipo_codice = C_TIPO_UM_CODICE_PESO then
      -- Da PZ a KG (o altri pesi): (Quantita PZ) * (Peso medio pezzo convertito in KG o altri pesi)
      l_qta_arrivo := p_qta_partenza * CONVERTI_UM(p_peso_medio_pezzo, p_um_id_peso_medio_pezzo, p_um_id_arrivo);
    else
      -- Conversione non supportata
      l_conversione_non_supportata := true;
    end if;  
    
  elsif l_um_partenza.unita_misura_tipo_codice = C_TIPO_UM_CODICE_PESO then
  
    if l_um_arrivo.codice = C_UM_CODICE_IMBALLI_CESSIONE then
      -- Da KG (o altri pesi) a IC: (Quantita KG o altri pesi) / ( (Peso medio pezzo convertito in KG o altri pesi) * ((IMBALLO CESSIONE) )
      if p_peso_medio_pezzo is null or p_peso_medio_pezzo = 0 then
        raise_application_error(-20004, 'Impossibile convertire da ' || l_um_partenza.codice || ' a IC perche'' il peso medio pezzo e'' zero o null');
      else
        l_qta_arrivo := p_qta_partenza / (CONVERTI_UM(p_peso_medio_pezzo, p_um_id_peso_medio_pezzo, p_um_id_partenza) * p_imballo_cessione);
      end if;
    elsif l_um_arrivo.codice = C_UM_CODICE_PEZZI then
      -- Da KG (o altri pesi) a PZ: (Quantita KG o altri pesi) / (Peso medio pezzo convertito in KG o altri pesi)
      if p_peso_medio_pezzo is null or p_peso_medio_pezzo = 0 then
        raise_application_error(-20005, 'Impossibile convertire da ' || l_um_partenza.codice || ' a PZ perche'' il peso medio pezzo e'' zero o null');
      else
        l_qta_arrivo := p_qta_partenza / CONVERTI_UM(p_peso_medio_pezzo, p_um_id_peso_medio_pezzo, p_um_id_partenza);
      end if;
    else
      -- Conversione non supportata
      l_conversione_non_supportata := true;    
    end if;

  else
  
    -- Conversione non supportata
    l_conversione_non_supportata := true;
    
  end if;
  
  if l_conversione_non_supportata then
    raise_application_error(-20001, 'Conversione non supportata fra l''UM id ' || p_um_id_partenza || ' e l''UM id ' || p_um_id_arrivo);
  end if;

  return l_qta_arrivo;

exception
  when others then
    if p_eccezione_se_errore = 'S' then
      raise;
    else
      return 0;
    end if;

END;


END PKG_UNITA_MISURA;
/