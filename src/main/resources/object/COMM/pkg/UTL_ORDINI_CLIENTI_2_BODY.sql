create or replace PACKAGE BODY UTL_ORDINI_CLIENTI_2 
IS
PRAGMA SERIALLY_REUSABLE;

--| Costanti globali     |---------------------------------------------------------------------------
C_VERSION         CONSTANT VARCHAR2(100) := 'V:1.0.0 LASTUPDATE:11/11/2016';
C_NOME_MODULO     CONSTANT VARCHAR2(30)  := 'UTL_ORDINI_CLIENTI_2';

--| Log    |-----------------------------------------------------------------------------------------
G_LOGGER  TYP_LOGGER_NEW := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

--| Codici Messaggi      |---------------------------------------------------------------------------
C_MSG_NO_COD_INTERLOCUTORE  CONSTANT VARCHAR2(5) := 'VE005';  -- Il Codice Interlocutore $P{codice} per il ruolo $P{ruolo} non � stato riconosciuto  - NEW
C_MSG_NO_COD_ARTICOLO       CONSTANT VARCHAR2(5) := 'VW005';  -- Il codice articolo $P{codice} di tipo $P{tipoCodice} non � stato riconosciuto
C_MSG_NO_PDD_CLIENTE        CONSTANT VARCHAR2(5) := 'XXXXX';  -- L'interlocutore (cliente) non ha alcun PDV censito nell'anagrafica dei punti vendita 
C_MSG_ERR_INTER_MAG_SERV    CONSTANT VARCHAR2(5) := 'XXXXX';  -- Non e' stato possibile intercettare il magazzino di servizio per l''articolo codice: $P{codice}
C_MSG_ERR_CALC_QTA          CONSTANT VARCHAR2(5) := 'XXXXX';  -- Non e' stato possibile calcolare la quantita in $P{UM} per l''articolo codice: $P{codice}.
                                                              -- Controllare la correttezza dell'anagrafica dell'articolo, in particolare:
                                                              -- l'Imballo Cessione, il Peso Medio Pezzo e la sua Unita di Misura, l'Unit� Misura dell'Ordine Cliente.   

--| Codici Stati Ordine  |---------------------------------------------------------------------------
C_STATO_ORDINE_BLOCCATO  CONSTANT VARCHAR2(2) := '3';
C_STATO_ORDINE_VALIDATO  CONSTANT VARCHAR2(2) := '5';

--| Codici Stati Righe Ordine  |---------------------------------------------------------------------
C_STATO_RIGA_ORDINE_BLOCCATO  CONSTANT VARCHAR2(1) := '3';
C_STATO_RIGA_ORDINE_VALIDATO  CONSTANT VARCHAR2(1) := '5';

--| Codici Tipi Listini  |---------------------------------------------------------------------
C_TIPO_LISTINO_CESSIONE   CONSTANT VARCHAR2(1) := 'C';
C_TIPO_LISTINO_PUBBLICO   CONSTANT VARCHAR2(1) := 'P';

--| Codici Modalit� Calcolo Prezzi Cessione  |---------------------------------------------------------------------
C_TIPO_CALCOLO_PRZ_DA_LISTINO CONSTANT VARCHAR2(1) := 'L';
C_TIPO_CALCOLO_PRZ_DA_COSTI   CONSTANT VARCHAR2(1) := 'C';
 
--| Tipi e Variabili globali    |--------------------------------------------------------------------
type MESSAGGIO_TYPE is record (
  MESSAGGIO_ID  number,
  MESSAGGIO     NCL_MESSAGGIO.CONTENUTO%type
);

type MSG_VAR_LIST is table of varchar2(255);
----------------------------------------------------------------------------------------------------
 
FUNCTION GET_VERSION RETURN VARCHAR2 IS
BEGIN
  RETURN C_VERSION;
END;

----------------------------------------------------------------------------------------------------

/*
  Dato un codice di messaggio (censito nella tabella NCL_MESSAGGIO e valido per il
  contesto ordini clienti) ritorna il suo ID ed il contenuto del messaggio.
*/
FUNCTION GET_MESSAGGIO(p_cod_msg varchar2) RETURN MESSAGGIO_TYPE
RESULT_CACHE RELIES_ON (NCL_MESSAGGIO)
IS
  l_messaggio MESSAGGIO_TYPE;
BEGIN

  SELECT M.MESSAGGIO_ID, M.CONTENUTO
    INTO l_messaggio
    FROM NCL_MESSAGGIO M, NCL_CONTESTO_MESSAGGIO CM
   WHERE M.CONTESTO_MESSAGGIO_ID = CM.CONTESTO_MESSAGGIO_ID
     AND CM.CODICE = 'ORC'
     AND M.CODICE = p_cod_msg;

  return l_messaggio;
  
END GET_MESSAGGIO;

----------------------------------------------------------------------------------------------------

/*
  Dato un codice di messaggio (censito nella tabella NCL_MESSAGGIO e valido per il
  contesto ordini clienti) ritorna il suo ID ed il contenuto del messaggio.
  Il messaggio pu� contenere dei segnaposto da sostituire, nella forma $P{nome_segnaposto}.
  Ad esempio:
    "Il Codice Interlocutore $P{codice} per il ruolo $P{ruolo} non � stato riconosciuto"
  L'array p_vars contiene con le variabili da sostituire per i segnaposto presenti 
  sul messaggio. Ogni elemento dell'array p_vars deve essere una stringa della forma
    variabile=valore
  in cui "variabile" � il nome segnaposto definito nel testo del messaggio.
  Ad esempio, per il messaggio precedente l'array p_vars dovr� contenere 2 elementi:
    codice=363622
    ruolo=MIO RUOLO 
*/
FUNCTION GET_MESSAGGIO(p_cod_msg varchar2, p_vars MSG_VAR_LIST) 
RETURN MESSAGGIO_TYPE
IS
l_messaggio MESSAGGIO_TYPE;
l_par_codice varchar2(255);
l_par_valore varchar2(255);
n integer;
BEGIN

  l_messaggio := GET_MESSAGGIO(P_COD_MSG);
  if p_vars is not null and p_vars.count > 0 then
    for i in 1..p_vars.count loop
      n := INSTR(p_vars(i), '='); 
      if n > 0 then
        l_par_codice := SUBSTR(p_vars(i), 1, n-1);
        l_par_valore := SUBSTR(p_vars(i), n+1);
        l_messaggio.MESSAGGIO := REPLACE(l_messaggio.MESSAGGIO, '$P{' || l_par_codice || '}', l_par_valore);
      end if;
    end loop;
  end if;
  
  return l_messaggio;
  
END;

----------------------------------------------------------------------------------------------------

FUNCTION GET_STATO_ORDINE_ID(p_codice varchar2) RETURN number
RESULT_CACHE RELIES_ON (NCL_STATO_ORDINE)
IS
  l_stato_ordine_id number;
BEGIN

  SELECT STATO_ORDINE_NORM_ID 
    INTO l_stato_ordine_id
    FROM STATO_ORDINE_NORM 
   WHERE CODICE = p_codice;
   
  return l_stato_ordine_id;
   
END GET_STATO_ORDINE_ID;

----------------------------------------------------------------------------------------------------

FUNCTION GET_STATO_RIGA_ORDINE_ID(p_codice varchar2) RETURN number
RESULT_CACHE RELIES_ON (STATO_DETT_ORD_DST)
IS
  l_stato_riga_ordine_id number;
BEGIN

  SELECT STATO_RIGA_ORDINE_NORM_ID 
    INTO l_stato_riga_ordine_id
    FROM STATO_RIGA_ORDINE_NORM 
   WHERE CODICE = p_codice;
   
  return l_stato_riga_ordine_id;
   
END GET_STATO_RIGA_ORDINE_ID;

----------------------------------------------------------------------------------------------------

PROCEDURE ADD_MESSAGGIO_ONT(p_ordine_normalizzato_t_id number, p_cod_msg varchar2, p_vars MSG_VAR_LIST := null) 
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  
  l_messaggio MESSAGGIO_TYPE;
BEGIN

  l_messaggio := GET_MESSAGGIO(P_COD_MSG, p_vars);

  INSERT INTO ORDINE_N_T_X_MESS (
    MESSAGGIO_ID,
    COD_MESSAGGIO,
    MESSAGGIO,
    COD_CONTESTO_MESSAGGIO,
    COD_LINGUA,
    ORDINE_NORMALIZZATO_T_ID,
    ORDINE_N_T_X_MESS_ID,
    UPD_DATA,
    UPD_UTENTE,
    UPD_MODULO,
    INS_DATA,
    INS_UTENTE,
    INS_MODULO,
    VERSION)
  VALUES (
    l_messaggio.MESSAGGIO_ID,
    p_cod_msg,
    l_messaggio.MESSAGGIO,
    'ORC', -- COD_CONTESTO_MESSAGGIO,
    'ITA', -- COD_LINGUA,
    p_ordine_normalizzato_t_id,
    SQ_ORDINE_N_T_X_MESS.NEXTVAL,
    SYSDATE,
    NULL,
    C_NOME_MODULO,
    SYSDATE,
    NULL,
    C_NOME_MODULO,
    CURRENT_TIMESTAMP
  );

  COMMIT;

END ADD_MESSAGGIO_ONT;

----------------------------------------------------------------------------------------------------

PROCEDURE ADD_MESSAGGIO_OND(p_ordine_normalizzato_d_id number, p_cod_msg varchar2, p_vars MSG_VAR_LIST := null) 
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  
  l_messaggio MESSAGGIO_TYPE;
BEGIN

  l_messaggio := GET_MESSAGGIO(p_cod_msg, p_vars);

  INSERT INTO ORDINE_N_D_X_MESS ( 
    ORDINE_N_D_X_MESS_ID,
    ORDINE_NORMALIZZATO_D_ID,
    COD_MESSAGGIO,
    MESSAGGIO,
    COD_CONTESTO_MESSAGGIO,
    COD_LINGUA,
    INS_DATA,
    INS_UTENTE,
    INS_MODULO,
    UPD_DATA,
    UPD_UTENTE,
    UPD_MODULO,
    VERSION,
    MESSAGGIO_ID)
  VALUES (
     SQ_ORDINE_N_D_X_MESS.NEXTVAL,
     p_ordine_normalizzato_d_id,
     p_cod_msg,
     l_messaggio.MESSAGGIO,
     'ORC',
     'ITA',
     SYSDATE,
     NULL,
     C_NOME_MODULO,
     SYSDATE,
     NULL,
     C_NOME_MODULO,
     CURRENT_TIMESTAMP,
     l_messaggio.MESSAGGIO_ID
  );
  
  COMMIT;
   
END ADD_MESSAGGIO_OND;

----------------------------------------------------------------------------------------------------


FUNCTION DECODIFICA_CODICI(p_ordine_normalizzato_t_id number) RETURN BOOLEAN
IS
  TYPE OND_LIST_TYPE IS TABLE OF ORDINE_NORMALIZZATO_D%rowtype; 
  listOND  OND_LIST_TYPE;
  recONT  ORDINE_NORMALIZZATO_T%rowtype;
  l_interlocutore_id number := null;
  l_pdd_id number := null;
  l_pdd_codice varchar2(50);
  l_divisione_id number;
  l_persona_fiscale_id number := null;
  --l_validazione_ok boolean := true;
  l_msg varchar2(1000);
  l_stato_ordine_id number;
  l_stato_riga_ordine_id number;
  l_provenienza_ordine_dst_id number;

BEGIN
  G_LOGGER.procedura := 'DECODIFICA_CODICI';
  G_LOGGER.log_debug('Inizio procedura');
    
  --
  -- DECODIFICA CODICI DI TESTATA
  --
  
  SELECT * 
    INTO recONT
    FROM ORDINE_NORMALIZZATO_T
   WHERE ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id;
   
  -- Decodifica provenienza ordine
  G_LOGGER.log_debug('Decodifico provenienza ordine');
  BEGIN
    SELECT PROVENIENZA_ORDINE_DST_ID
      INTO l_provenienza_ordine_dst_id
      FROM PROVENIENZA_ORDINE_DST
     WHERE CODICE = recONT.PROVENIENZA;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      G_LOGGER.log_warn('Codice provenienza ordine (' || recONT.PROVENIENZA || ') non trovato nella tabella PROVENIENZA_ORDINE_DST per l''ordine ID ' || p_ordine_normalizzato_t_id);  
      l_provenienza_ordine_dst_id := null;
      -- TODO aggiungere anche messaggio utente?
  END;
  
  -- Decodifica dati di base relativi all'INTERLOCUTORE
  -- TODO la PERSONA_FISCALE_ID non � usata, togliere
  G_LOGGER.log_debug('Decodifica dati di base relativi all''INTERLOCUTORE');
  BEGIN
  
    SELECT *
      INTO l_interlocutore_id, l_pdd_id, l_pdd_codice, l_divisione_id, l_persona_fiscale_id --, l_azienda_id
      FROM (SELECT i.INTERLOCUTORE_ID, 
                   p.PDD_ID, p.CODICE, p.DIVISIONE_ID,
                   ixp.PERSONA_FISCALE_ID --, 
       --            ta.AZIENDA_ID
              FROM NCL_INTERLOCUTORE i, NCL_PDD p, 
                   NCL_INTERLOCUTORE_X_PERSONA ixp --, NCL_TITOLARE_AZIENDA ta
             WHERE i.CODICE = recONT.COD_DESTINATARIO  -- TODO: controllare se � necessario fare lpad del codice chiamando la GET_CODICE_PADDED
            -- AND i.RUOLO_FORNITORE = 'N' 
               AND i.RUOLO_CLIENTE = 'S'
               AND i.INTERLOCUTORE_ID = p.INTERLOCUTORE_ID(+)
               AND i.INTERLOCUTORE_ID = ixp.INTERLOCUTORE_ID(+)
               AND (SYSDATE BETWEEN ixp.DATA_INIZIO(+) AND NVL(ixp.DATA_FINE, TO_DATE('31-12-2999','DD-MM-YYYY')))
 --              AND ixp.PERSONA_FISCALE_ID = ta.PERSONA_FISCALE_ID(+)
             ORDER BY ixp.DATA_INIZIO DESC
           )
     WHERE ROWNUM = 1;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- Codice destinatario non trovato
      
      G_LOGGER.log_error('Errore bloccante. ORDINE ID: ' || p_ordine_normalizzato_t_id 
                          || ' il codice destinatario: ' || recONT.COD_DESTINATARIO 
                          || ' non � stato trovato nella tabella INTERLOCUTORE con RUOLO_CLIENTE=S');
    
      -- Aggiungo un messaggio di errore
      ADD_MESSAGGIO_ONT(p_ordine_normalizzato_t_id, C_MSG_NO_COD_INTERLOCUTORE, MSG_VAR_LIST('codice=' || recONT.COD_DESTINATARIO, 'ruolo=CLIENTE'));
      
      -- Non avendo decodificato l'interlocutore non si puo' proseguire
      return false;
  END;
  -- FINE Decodifica dati di base relativi all'INTERLOCUTORE
  
  -- Aggiorno l'ordine con gli ID decodificati
  G_LOGGER.log_debug('Aggiorno l''ordine con gli ID decodificati');
  UPDATE ORDINE_NORMALIZZATO_T 
     SET PROVENIENZA_ORDINE_DST_ID = l_provenienza_ordine_dst_id,
         INTERLOCUTORE_ID = l_interlocutore_id,
         PDD_ID = l_pdd_id,
         DIVISIONE_ID = l_divisione_id
         -- PDD_CODICE = l_pdd_codice,
         -- PERSONA_FISCALE_ID = l_persona_fiscale_id,
   WHERE ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id;
  
  if l_pdd_id is null then
    ADD_MESSAGGIO_ONT(p_ordine_normalizzato_t_id, C_MSG_NO_PDD_CLIENTE);
    
    -- Non avendo decodificato il PDV dell'interlocutore non si puo' proseguire
     return false;    
  end if;

  -- TODO: per gli gli altri ID non trovati (l_persona_fiscale_id) aggiungere messaggi di errore?
  -- Bloccare l'ordine?

  
  --
  -- DECODIFICA CODICI SULLE RIGHE 
  --
  
  -- Decodifica ARTICOLI
  G_LOGGER.log_debug('Decodifica ARTICOLI');
  MERGE
   INTO ORDINE_NORMALIZZATO_D a 
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, axd.ARTICOLO_ID, ab.ARTICOLO_CESSIONE_ID,
                ts.TIPO_SERVIZIO_ID, ts.CODICE as TIPO_SERVIZIO_CODICE
           FROM ORDINE_NORMALIZZATO_D ond, NCL_ARTICOLO_X_DIVISIONE axd,
                NCL_ARTICOLO art, NCL_TIPO_SERVIZIO ts, NCL_ART_BASE ab
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ond.COD_ARTICOLO = axd.CODICE
            AND axd.DIVISIONE_ID = l_divisione_id
            AND axd.ARTICOLO_ID = art.ARTICOLO_ID
            AND art.ART_BASE_ID = ab.ART_BASE_ID
            AND ab.TIPO_SERVIZIO_ID = ts.TIPO_SERVIZIO_ID(+)
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE 
    SET a.ARTICOLO_ID = b.ARTICOLO_ID,
        a.ARTICOLO_CESSIONE_ID = b.ARTICOLO_CESSIONE_ID,
        a.TIPO_SERVIZIO_ID = b.TIPO_SERVIZIO_ID,
        a.TIPO_SERVIZIO_CODICE = b.TIPO_SERVIZIO_CODICE,
        a.PDD_ID = l_pdd_id; -- TODO: controllare
  
  G_LOGGER.log_debug('Decodificati ARTICOLI n: ' || SQL%ROWCOUNT);
  
  -- Controlla se ci sono righe per cui non � stato possibile
  -- decodificare l'articolo ed in tal caso inserisce un messaggio di errore
  -- per ciascuna di esse e le marca bloccate.
  SELECT *
    BULK COLLECT INTO listOND
    FROM ORDINE_NORMALIZZATO_D ond
   WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
     AND ond.ARTICOLO_ID IS NULL;
   
  if listOND.count > 0 then
    G_LOGGER.log_warn('Articoli NON decodificati: ' || listOND.count);
  
    for i in 1..listOND.count loop
      --TODO c'� anche un $P{tipoCodice} che non so cosa sia
      ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, C_MSG_NO_COD_ARTICOLO, MSG_VAR_LIST('codice=' || listOND(i).COD_ARTICOLO));
    end loop;
    
    l_stato_riga_ordine_id := GET_STATO_RIGA_ORDINE_ID(C_STATO_RIGA_ORDINE_BLOCCATO);
       
    FORALL i IN 1..listOND.count
    UPDATE ORDINE_NORMALIZZATO_D ond
       SET ond.STATO_RIGA_ORDINE_NORM_ID = l_stato_riga_ordine_id
     WHERE ond.ORDINE_NORMALIZZATO_D_ID = listOND(i).ORDINE_NORMALIZZATO_D_ID;       
       
     G_LOGGER.log_debug('Righe Ordine Bloccate per ARTICOLO_ID null: ' || SQL%ROWCOUNT);
  end if;
  -- FINE Decodifica Articoli
    
  -- TODO  verificare se ci sono articoli senza tipo_servizio_id
    
  return true;
  
END DECODIFICA_CODICI;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_RAPPORTI_AMMINISTRATIVI(p_ordine_normalizzato_t_id number, p_data_riferimento date := SYSDATE)
IS
  l_tipo_consegna_id number;
BEGIN

  G_LOGGER.procedura := 'SET_RAPPORTI_AMMINISTRATIVI';
  G_LOGGER.log_debug('Inizio procedura');
  
  SELECT TIPO_CONSEGNA_ID 
    INTO l_tipo_consegna_id 
    FROM NCL_TIPO_CONSEGNA 
   WHERE DFT = 'S' 
     AND ROWNUM = 1;
  
  -- TODO: la join con NCL_INTERLOCUTORE_X_PERSONA pu� tornare N record: che fare?
  -- TODO: la join con NCL_TITOLARE_AZIENDA pu� tornare N record: che fare?
  -- TODO: la join con PDD_X_TIPO_CONSEGNA pu� tornare N record: che fare?
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, ond.MAGAZZINO_ID, ta.AZIENDA_ID, pxtc.TIPO_CONSEGNA_ID
           FROM ORDINE_NORMALIZZATO_D ond, NCL_PDD p, NCL_INTERLOCUTORE_X_PERSONA ixp, PDD_X_TIPO_CONSEGNA pxtc, NCL_TITOLARE_AZIENDA ta
          WHERE ond.MAGAZZINO_ID = p.PDD_ID
            AND p.INTERLOCUTORE_ID = ixp.INTERLOCUTORE_ID
            AND (p_data_riferimento BETWEEN ixp.DATA_INIZIO(+) AND NVL(ixp.DATA_FINE, TO_DATE('31-12-2999','DD-MM-YYYY')))
            AND ixp.PERSONA_FISCALE_ID = ta.PERSONA_FISCALE_ID
            AND ond.MAGAZZINO_ID = pxtc.PDD_ID(+)
            AND pxtc.PRIORITA(+) = 1
            AND ond.MAGAZZINO_ID IS NOT NULL
            AND ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.AZIENDA_ID = b.AZIENDA_ID,
        a.TIPO_CONSEGNA_ID = NVL(b.TIPO_CONSEGNA_ID, l_tipo_consegna_id);
  
  -- TODO: togliere codice cablato 'MAXID'            
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, 
                cram.CONTENUTO_RAPP_AMM_ID, cram.CONTESTO, cram.REGOLA_ID, cram.PRIORITA_FORMATO, cram.DATA_INIZIO, cram.DATA_FINE,
                cra.TIPO_PUNTO_CONSEGNA_ID, cra.TIPO_PAGAMENTO_ID, cra.MODALITA_PAGAMENTO_ID, cra.DISPO_FINANZIARIA_ID, cra.TIPO_CALCOLO_DECORRENZA_ID,
                ra.RAPPORTO_AMMINISTRATIVO_ID,
                ROW_NUMBER() OVER (PARTITION BY ond.ORDINE_NORMALIZZATO_D_ID  
                                       ORDER BY cram.PRIORITA_FORMATO DESC) RNUM
           FROM ORDINE_NORMALIZZATO_T ont, ORDINE_NORMALIZZATO_D ond,
                CRITERIO_RAPP_AMM cram, CONTENUTO_RAPP_AMM cra, RAPPORTO_AMMINISTRATIVO ra,
                NCL_ARTICOLO_X_MERCEOLOGIA axm, NCL_MERCEOLOGIA m, NCL_MERCEOLOGIA_LIVELLO ml, NCL_MERCEOLOGIA_CL mc
          WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ont.ORDINE_NORMALIZZATO_T_ID = ond.ORDINE_NORMALIZZATO_T_ID         
            AND ond.ARTICOLO_ID IS NOT NULL                                    -- ARTICOLO_ID, AZIENDA_ID e TIPO_CONSEGNA_ID
            AND ond.AZIENDA_ID IS NOT NULL                                     -- sono necessari per individuare i
            AND ond.TIPO_CONSEGNA_ID IS NOT NULL                               -- rapporti amministrativi
            AND ond.ARTICOLO_ID = axm.ARTICOLO_ID                                   --
            AND axm.MERCEOLOGIA_ID = m.MERCEOLOGIA_ID                               --
            AND m.MERCEOLOGIA_LIVELLO_ID = ml.MERCEOLOGIA_LIVELLO_ID                --  INDIVIDUAZIONE MERCEOLOGIA ARTICOLO
            AND ml.MERCEOLOGIA_CL_ID = mc.MERCEOLOGIA_CL_ID                         --
            AND mc.CODICE = 'MAXID'                                                 --
            AND ont.INTERLOCUTORE_ID = cram.INTERLOCUTORE_ID                                                      --
            AND (axm.MERCEOLOGIA_ID = cram.MERCEOLOGIA_ID OR cram.MERCEOLOGIA_ID is null)                         -- 
            AND (ond.AZIENDA_ID = cram.AZIENDA_ID OR cram.AZIENDA_ID = -1)                                        --
            AND (ond.TIPO_CONSEGNA_ID = cram.TIPO_CONSEGNA_ID OR cram.TIPO_CONSEGNA_ID = -1)                      -- INDIVIDUAZIONE RAPPORTO AMMINISTRATIVO
            AND p_data_riferimento BETWEEN cram.DATA_INIZIO AND NVL(cram.DATA_FINE, TO_DATE('31-12-2999', 'DD-MM-YYYY'))     --
            AND cram.CONTENUTO_RAPP_AMM_ID = cra.CONTENUTO_RAPP_AMM_ID                                            --
            AND cra.CONTENUTO_RAPP_AMM_ID = ra.CONTENUTO_RAPP_AMM_ID                                              --
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID AND b.RNUM = 1)
   WHEN MATCHED THEN UPDATE
    SET a.CONTENUTO_RAPP_AMM_ID = b.CONTENUTO_RAPP_AMM_ID,   
        a.TIPO_PUNTO_CONSEGNA_ID = b.TIPO_PUNTO_CONSEGNA_ID,   
        a.TIPO_PAGAMENTO_ID = b.TIPO_PAGAMENTO_ID,  
        a.MODALITA_PAGAMENTO_ID = b.MODALITA_PAGAMENTO_ID,  
        a.DISPO_FINANZIARIA_ID = b.DISPO_FINANZIARIA_ID, 
        a.TIPO_CALCOLO_DECORRENZA_ID = b.TIPO_CALCOLO_DECORRENZA_ID, 
        a.RAPPORTO_AMMINISTRATIVO_ID = b.RAPPORTO_AMMINISTRATIVO_ID;  
           
  -- TODO: verificare se ci sono righe senza rapporti amministrativi?
  -- Cosa fare in quel caso? Aggiungere messaggio? Bloccare riga?

END SET_RAPPORTI_AMMINISTRATIVI;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_CAUSALI_COMMERCIALI(p_ordine_normalizzato_t_id number)
IS
BEGIN

  G_LOGGER.procedura := 'SET_CAUSALI_COMMERCIALI';
  G_LOGGER.log_debug('Inizio procedura');

  -- TODO E' necessario che sulle OND ci sia TIPO_PUNTO_CONSEGNA_ID e ARTICOLO_CESSIONE_ID?
  -- Cosa fare se non ci sono, visto che la query seguente li usa per le join?

  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, ond.TIPO_PUNTO_CONSEGNA_ID, ond.ARTICOLO_CESSIONE_ID,
                pca.LOOKUP_ID AS CAUSALE_COMMERCIALE_ID,
                cc.CODICE,
                ROW_NUMBER () OVER (PARTITION BY ond.ORDINE_NORMALIZZATO_D_ID
                                        ORDER BY pca.TIPO_PUNTO_CONSEGNA_ID, pca.ARTICOLO_CESSIONE_ID, pca.PRIORITA) AS RNUM
           FROM ORDINE_NORMALIZZATO_D ond, PARAMETRI_CICLO_ATTIVO pca, NCL_CAUSALE_COMMERCIALE cc
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id 
            AND (ond.TIPO_PUNTO_CONSEGNA_ID = pca.TIPO_PUNTO_CONSEGNA_ID OR pca.TIPO_PUNTO_CONSEGNA_ID IS NULL)
            AND (ond.ARTICOLO_CESSIONE_ID = pca.ARTICOLO_CESSIONE_ID OR PCA.ARTICOLO_CESSIONE_ID IS NULL)
            AND pca.LOOKUP = 'CAUSALE_COMMERCIALE'
            AND pca.LOOKUP_ID = cc.CAUSALE_COMMERCIALE_ID
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID AND b.RNUM = 1)
   WHEN MATCHED THEN UPDATE
    SET a.CAUSALE_COMMERCIALE_ID = b.CAUSALE_COMMERCIALE_ID,
        a.CAUSALE_MOV = b.CODICE;
     
  -- TODO: verificare se ci sono righe senza causali commerciali?
  -- Cosa fare in quel caso? Aggiungere messaggio? Bloccare riga?
  
END SET_CAUSALI_COMMERCIALI;

----------------------------------------------------------------------------------------------------

-- Verifica quali articoli dell'ordine sono in ordine diretto (rifatturazione)
-- cio� sono ordinati dal PDD direttamente al fornitore.
-- Popola di conseguenza il flag ARTICOLO_DIRETTO.
--
PROCEDURE CHECK_ARTICOLI_DIRETTI(p_ordine_normalizzato_t_id number) 
IS
BEGIN

  G_LOGGER.procedura := 'CHECK_ARTICOLI_DIRETTI';
  G_LOGGER.log_debug('Inizio procedura');

  -- Imposta a ARTICOLO_DIRETTO = 'N' tutte le righe dell'ordine
  UPDATE ORDINE_NORMALIZZATO_D
     SET ARTICOLO_DIRETTO = 'N'
   WHERE ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
     AND ARTICOLO_ID IS NOT NULL;

  -- Aggiorno a ARTICOLO_DIRETTO = 'S' le righe dell'ordine che trovo in ASSORTIMENTO_FORNITORE_PDD
  G_LOGGER.log_debug('Aggiorno a ARTICOLO_DIRETTO = S le righe dell''ordine che trovo in ASSORTIMENTO_FORNITORE_PDD');
  MERGE
   INTO ORDINE_NORMALIZZATO_D a 
  USING (SELECT DISTINCT ORDINE_NORMALIZZATO_D_ID
           FROM ORDINE_NORMALIZZATO_D ond,
                ASSORTIMENTO_FORNITORE_PDD afp,
                NCL_ASSORTIMENTO_FORNITORE af,
                NCL_STATO_PRODOTTO nsp
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ond.ARTICOLO_ID IS NOT NULL
            AND ond.PDD_ID IS NOT NULL
            AND ond.PDD_ID = afp.PDD_ID
            AND afp.STATO_ID = nsp.STATO_ID
            AND nsp.CODICE = 'AF'       -- TODO: CONTROLLARE
            AND afp.ASSORTIMENTO_FORNITORE_ID = af.ASSORTIMENTO_FORNITORE_ID
            AND ond.ARTICOLO_ID = af.ARTICOLO_ID
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.ARTICOLO_DIRETTO = 'S';  

  G_LOGGER.log_info('Righe aggiornate a ARTICOLO_DIRETTO = S : ' || SQL%ROWCOUNT);

END CHECK_ARTICOLI_DIRETTI;

----------------------------------------------------------------------------------------------------

--
-- Verifica quali articoli dell'ordine sono in assortimento per il PDD_ID 
-- dell'interlocutore. 
-- Popola di conseguenza il flag ART_IN_ASSORTIMENTO_X_PDD
--
PROCEDURE CHECK_ASSORTIMENTO_DST(p_ordine_normalizzato_t_id number) 
IS
BEGIN
  G_LOGGER.procedura := 'CHECK_ASSORTIMENTO_DST';
  G_LOGGER.log_debug('Inizio procedura');

  -- Effettua una left join fra le righe dell'ordine e NCL_ARTICOLO_X_PDD_CONSIGLIATO
  -- e imposta ART_IN_ASSORTIMENTO_X_PDD = 'S' sulle righe che ha trovato nella tabella in join
  -- mentre le righe non trovate vengono impostate a 'N'.
  G_LOGGER.log_debug('Impostazione ART_IN_ASSORTIMENTO_X_PDD');  
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, axpc.ARTICOLO_X_PDD_CONSIGLIATO_ID 
           FROM ORDINE_NORMALIZZATO_D ond, NCL_ARTICOLO_X_PDD_CONSIGLIATO axpc
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ond.ARTICOLO_ID IS NOT NULL
            AND ond.PDD_ID IS NOT NULL
            AND ond.PDD_ID = axpc.PDD_ID(+)
            AND ond.ARTICOLO_ID = axpc.ARTICOLO_ID(+)
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.ART_IN_ASSORTIMENTO_X_PDD = DECODE(b.ARTICOLO_X_PDD_CONSIGLIATO_ID, NULL, 'N', 'S');

  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);  
           
END CHECK_ASSORTIMENTO_DST;

----------------------------------------------------------------------------------------------------

--
-- Intercetta il magazzino di servizio (MAGAZZINO_ID) che preparer� l'articolo
-- Utilizza la vista V_PDV_TIPO_SERVIZIO per individuare per PDD_ID e TIPO_SERVIZIO e ARTICOLO_ID i magazzini candidati
-- Nel caso di N magazzini prende il primo in base al campo ORDINE della vista.
--
PROCEDURE INTERCETTA_MAGAZZINO_SERVIZIO(p_ordine_normalizzato_t_id number) 
IS
  TYPE OND_LIST_TYPE IS TABLE OF ORDINE_NORMALIZZATO_D%rowtype; 
  listOND  OND_LIST_TYPE;
  l_stato_riga_ordine_id number;
BEGIN

  G_LOGGER.procedura := 'INTERCETTA_MAGAZZINO_SERVIZIO';
  G_LOGGER.log_debug('Inizio procedura');

  -- TODO: sostituire brc_divu e brc_logi con sinonimi.
  -- Spostare la vista V_PDV_TIPO_SERVIZIO sul core?
  
  -- Intercettazione magazzino di servizio
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, v.MAG_ID, p.CODICE as COD_MAGAZZINO, axp.ARTICOLO_X_PDD_ID,
                ROW_NUMBER() OVER (PARTITION BY ond.ORDINE_NORMALIZZATO_D_ID ORDER BY v.ORDINE) RNUM
           FROM ORDINE_NORMALIZZATO_D ond,
                ORDINE_NORMALIZZATO_T ont,
                brc_divu.V_PDV_TIPO_SERVIZIO v,
                NCL_ARTICOLO_X_DIVISIONE axd,
                NCL_ARTICOLO_X_PDD axp,
                --brc_logi.ARTICOLO_MAGAZZINO_LOGISTICO aml,
                NCL_PDD p
          WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ont.ORDINE_NORMALIZZATO_T_ID = ond.ORDINE_NORMALIZZATO_T_ID
            AND ond.ARTICOLO_DIRETTO = 'N'          -- Per gli articoli diretti non va intercettato il magazzino di servizio
            AND ond.TIPO_SERVIZIO_ID IS NOT NULL    -- perch� per questi articoli il negozio fa l'ordine direttamente al fornitore
            AND ond.PDD_ID IS NOT NULL
            AND ond.ARTICOLO_ID IS NOT NULL
            AND ond.TIPO_SERVIZIO_ID = v.TIPO_SERVIZIO_ID                   --
            AND ond.PDD_ID = v.PDD_ID                                       -- Intercettazione magazzino
            AND ond.ARTICOLO_ID = axd.ARTICOLO_ID                           -- di servizio
            AND axd.ARTICOLO_X_DIVISIONE_ID = axp.ARTICOLO_X_DIVISIONE_ID   --
            AND axd.DIVISIONE_ID = ont.DIVISIONE_ID                         --
            AND axp.PDD_ID = v.MAG_ID                                       --
            AND axp.PDD_ID = p.PDD_ID   -- per prendere il codice magazzino
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID AND b.RNUM = 1)
   WHEN MATCHED THEN UPDATE
    SET a.MAGAZZINO_ID = b.MAG_ID,
        a.COD_MAGAZZINO = b.COD_MAGAZZINO,
        a.ARTICOLO_X_PDD_ID = b.ARTICOLO_X_PDD_ID;

  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);

  --
  -- VALIDAZIONE
  --

  -- Controlla se ci sono righe per cui non � stato possibile
  -- intercettare il magazzino di servizio ed in tal caso inserisce un messaggio di errore
  -- per ciascuna di esse e le marca bloccate.
  SELECT *
    BULK COLLECT INTO listOND
    FROM ORDINE_NORMALIZZATO_D ond
   WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
     AND ARTICOLO_DIRETTO = 'N'
     AND ond.ARTICOLO_ID IS NOT NULL
     AND ond.MAGAZZINO_ID IS NULL;
   
  if listOND.count > 0 then
    G_LOGGER.log_warn('Magazzino di servizio NON intercettato per : ' || listOND.count || ' articoli. Controllare il TIPO_SERVIZIO_ID e la vista V_PDV_TIPO_SERVIZIO');
  
    for i in 1..listOND.count loop
      ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, C_MSG_ERR_INTER_MAG_SERV, MSG_VAR_LIST('codice=' || listOND(i).COD_ARTICOLO));
    end loop;
    
    l_stato_riga_ordine_id := GET_STATO_RIGA_ORDINE_ID(C_STATO_RIGA_ORDINE_BLOCCATO);

    FORALL i IN 1..listOND.count
    UPDATE ORDINE_NORMALIZZATO_D ond
       SET ond.STATO_RIGA_ORDINE_NORM_ID = l_stato_riga_ordine_id
     WHERE ond.ORDINE_NORMALIZZATO_D_ID = listOND(i).ORDINE_NORMALIZZATO_D_ID;
       
     G_LOGGER.log_warn('Righe Ordine Bloccate per MAGAZZINO_ID non intercettato: ' || listOND.count);
  end if;
 
/*       
  -- Valorizzazzione ARTICOLO_X_PDD_ID             
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, axp.ARTICOLO_X_PDD_ID
           FROM ORDINE_NORMALIZZATO_D ond, 
                NCL_ARTICOLO_X_DIVISIONE axd,
                NCL_ARTICOLO_X_PDD axp
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = P_ORDINE_NORMALIZZATO_T_ID
            AND ond.MAGAZZINO_ID IS NOT NULL
            AND ond.ARTICOLO_ID IS NOT NULL
            AND ond.ARTICOLO_ID = axd.ARTICOLO_ID
            AND axd.ARTICOLO_X_DIVISIONE_ID = axp.ARTICOLO_X_DIVISIONE_ID
            AND axp.PDD_ID = ond.MAGAZZINO_ID
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.ARTICOLO_X_PDD_ID = b.ARTICOLO_X_PDD_ID;
*/        
END INTERCETTA_MAGAZZINO_SERVIZIO;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_QUANTITA(p_ordine_normalizzato_t_id number) 
IS
  TYPE OND_LIST_TYPE IS TABLE OF ORDINE_NORMALIZZATO_D%rowtype; 
  listOND  OND_LIST_TYPE;
  l_stato_riga_ordine_id number;

  l_um_id_pz  number;
  l_um_id_kg  number;
  l_um_id_ic  number;

BEGIN

  G_LOGGER.procedura := 'SET_QUANTITA';
  G_LOGGER.log_debug('Inizio procedura');
 
  -- TODO: COME TRATTARE GLI ARTICOLI DIRETTI PER I QUALI IL MAGAZZINO_ID E' NULL?
  
  -- Intercetta l'UM dell'ordine e la QTA MAX e MIN ammessa. Se la QTA dell'ordine e'
  -- esterna < del MIN viene portata al MIN, mentre se � > del MAX viene portata al MAX.
  -- L'intercettazione avviene sulle tabelle degli articoli dello schema BRC_LOGI perch� solo
  -- l� sono presenti i campi dell'UM dell'ordine.
  -- TODO: USA LE TABELLE DI BRC_LOGI. SPOSTARE SUL CORE?
  G_LOGGER.log_debug('Intercettazione UM dell''ordine e la QTA MAX e MIN ammessa');
  MERGE
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, a.ARTICOLO_ID,
                nvl(aml.UMI_ORDINE_ID, nvl(adl.UMI_ORDINE_ID, abl.UMI_ORDINE_ID)) as UMI_ORDINE_ID,
               -- nvl(aml.IMBALLO_CESSIONE, nvl(adl.IMBALLO_CESSIONE, abl.IMBALLO_CESSIONE)) as IMBALLO_CESSIONE,
                nvl(aml.ORDINE_MINIMO, nvl(adl.ORDINE_MINIMO, abl.ORDINE_MINIMO)) as ORDINE_MINIMO,
                nvl(aml.ORDINE_MASSIMO, nvl(adl.ORDINE_MASSIMO, abl.ORDINE_MASSIMO)) as ORDINE_MASSIMO       
           FROM ORDINE_NORMALIZZATO_T ont, ORDINE_NORMALIZZATO_d ond,  
                NCL_ARTICOLO a, 
                MVR_ART_BASE_LOGISTICO abl, MVR_ARTICOLO_X_DIV_LOGI adl, MVR_ARTICOLO_MAGAZZINO_LOGI aml
          WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ont.ORDINE_NORMALIZZATO_T_ID = ond.ORDINE_NORMALIZZATO_T_ID
            AND ond.ARTICOLO_ID IS NOT NULL
            AND ond.MAGAZZINO_ID IS NOT NULL
            AND ond.ARTICOLO_ID = a.ARTICOLO_ID
            AND a.ART_BASE_ID = abl.ART_BASE_ID
            AND ond.ARTICOLO_ID = adl.ARTICOLO_ID
            AND ont.DIVISIONE_ID = adl.DIVISIONE_ID
            AND ond.ARTICOLO_ID = aml.ARTICOLO_ID
            AND ond.MAGAZZINO_ID = aml.MAGAZZINO_ID
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.UM_ORDINE_ID = b.UMI_ORDINE_ID,
--        a.IMBALLO_CESSIONE = b.IMBALLO_CESSIONE,
        a.QTA_UM_ORDINE = (CASE 
                             WHEN b.ORDINE_MINIMO IS NOT NULL AND a.QTA_ORIGINALE <= b.ORDINE_MINIMO THEN b.ORDINE_MINIMO
                             WHEN b.ORDINE_MASSIMO IS NOT NULL AND a.QTA_ORIGINALE >= b.ORDINE_MASSIMO THEN b.ORDINE_MASSIMO
                             ELSE a.QTA_ORIGINALE
                           END
                          );
  
  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);
  
  -- Ricava gli ID delle UM dati i codici
  l_um_id_pz := PKG_UNITA_MISURA.GET_UM_ID(PKG_UNITA_MISURA.C_UM_CODICE_PEZZI);     
  l_um_id_kg := PKG_UNITA_MISURA.GET_UM_ID(PKG_UNITA_MISURA.C_UM_CODICE_KG);     
  l_um_id_ic := PKG_UNITA_MISURA.GET_UM_ID(PKG_UNITA_MISURA.C_UM_CODICE_IMBALLI_CESSIONE);     

  -- Popola l'Imballo Cessione e le QTA IC, PZ, e KG sulle righe dell'ordine normalizzato.
  -- Se ci sono dei problemi (pesi, imballi cessione, um mancanti) le quantit� vengono messe a zero 
  -- perche' viene passato a 'N' il parametro apposito della procedura PKG_UNITA_MISURA.CONVERTI_UM_LOGISTICA (vedi). 
  G_LOGGER.log_debug('Valorizzazione Imballo Cessione e le QTA IC, PZ, e KG sulle righe');
  MERGE 
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT s.*,
                PKG_UNITA_MISURA.CONVERTI_UM_LOGISTICA(s.QTA_UM_ORDINE, s.UM_ORDINE_ID, l_um_id_pz, s.IMBALLO_CESSIONE, s.PESO_MEDIO_PEZZO, s.UMI_PESO_MEDIO_ID, 'N') as QTA_PZ,
                PKG_UNITA_MISURA.CONVERTI_UM_LOGISTICA(s.QTA_UM_ORDINE, s.UM_ORDINE_ID, l_um_id_kg, s.IMBALLO_CESSIONE, s.PESO_MEDIO_PEZZO, s.UMI_PESO_MEDIO_ID, 'N') as QTA_KG,
                PKG_UNITA_MISURA.CONVERTI_UM_LOGISTICA(s.QTA_UM_ORDINE, s.UM_ORDINE_ID, l_um_id_ic, s.IMBALLO_CESSIONE, s.PESO_MEDIO_PEZZO, s.UMI_PESO_MEDIO_ID, 'N') as QTA_IC,
                PKG_UNITA_MISURA.CONVERTI_UM_LOGISTICA(s.QTA_UM_ORDINE, s.UM_ORDINE_ID, s.UM_RIF_PREZZO_ID, s.IMBALLO_CESSIONE, s.PESO_MEDIO_PEZZO, s.UMI_PESO_MEDIO_ID, 'N') as QTA_UM_RIF_PREZZO
           FROM (SELECT ond.ORDINE_NORMALIZZATO_D_ID, ond.UM_ORDINE_ID, ond.QTA_UM_ORDINE, ond.UM_RIF_PREZZO_ID,
                        nvl(axp.UMI_PESO_MEDIO_ID, nvl(axd.UMI_PESO_MEDIO_ID, ab.UMI_PESO_MEDIO_ID)) as UMI_PESO_MEDIO_ID,
                        nvl(axp.PESO_MEDIO_PEZZO, nvl(axd.PESO_MEDIO_PEZZO, ab.PESO_MEDIO_PEZZO)) as PESO_MEDIO_PEZZO,
                        nvl(axp.IMBALLO_CESSIONE, nvl(axd.IMBALLO_CESSIONE, ab.IMBALLO_CESSIONE)) as IMBALLO_CESSIONE
                   FROM ORDINE_NORMALIZZATO_T ont, ORDINE_NORMALIZZATO_d ond,  
                        NCL_ARTICOLO a, 
                        NCL_ART_BASE ab, NCL_ARTICOLO_X_DIVISIONE axd, NCL_ARTICOLO_X_PDD axp
                  WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
                    AND ont.ORDINE_NORMALIZZATO_T_ID = ond.ORDINE_NORMALIZZATO_T_ID
                    AND ond.ARTICOLO_ID IS NOT NULL
                    AND ond.MAGAZZINO_ID IS NOT NULL
                    AND ond.ARTICOLO_ID = a.ARTICOLO_ID
                    AND a.ART_BASE_ID = ab.ART_BASE_ID
                    AND ond.ARTICOLO_ID = axd.ARTICOLO_ID
                    AND ont.DIVISIONE_ID = axd.DIVISIONE_ID
                    AND axd.ARTICOLO_X_DIVISIONE_ID = axp.ARTICOLO_X_DIVISIONE_ID
                    AND ond.MAGAZZINO_ID = axp.PDD_ID
               ) s
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID)
   WHEN MATCHED THEN UPDATE
    SET a.QTA_PZ = b.QTA_PZ,
        a.QTA_KG = b.QTA_KG,
        a.QTA_IC = b.QTA_IC,
        a.QTA_UM_RIF_PREZZO = b.QTA_UM_RIF_PREZZO,
        a.IMBALLO_CESSIONE = b.IMBALLO_CESSIONE,
        a.UM_PESO_MEDIO_PEZZO_ID = b.UMI_PESO_MEDIO_ID,
        a.PESO_MEDIO_PEZZO = b.PESO_MEDIO_PEZZO;
  
  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);  
 
  -- Valorizza la Quanti� totale IC dell'ordine sulla testata  
  G_LOGGER.log_debug('Valorizzazione della Quanti� totale IC dell''ordine sulla testata ');
  UPDATE ORDINE_NORMALIZZATO_T ont
     SET ont.QTA_TOTALE_ORD = (SELECT SUM(ond.QTA_IC) 
                                 FROM ORDINE_NORMALIZZATO_D ond
                                WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
                              )
   WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id;
      
 
  --
  -- VALIDAZIONE
  --
  
  -- Controlla se ci sono righe per cui non � stato possibile
  -- calcolare la QTA_PZ, QTA_KG, QTA_IC o QTA_UM_RIF_PREZZO 
  -- ed inserisce dei messaggi utente per ciascuna di esse.
  SELECT *
    BULK COLLECT INTO listOND
    FROM ORDINE_NORMALIZZATO_D ond
   WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
     AND ond.ARTICOLO_ID IS NOT NULL
     AND ond.MAGAZZINO_ID IS NOT NULL
     AND (QTA_PZ = 0 OR QTA_KG = 0 OR QTA_IC = 0 OR QTA_UM_RIF_PREZZO = 0);
   
  if listOND.count > 0 then
    G_LOGGER.log_warn('Problemi nel calcolo delle QUANTITA per: ' || listOND.count || ' articoli. ' 
                      || 'Controllare PESO_MEDIO_PEZZO, UM_PESO_MEDIO_PEZZO_ID, IMBALLO_CESSIONE, UM_ORDINE_ID nelle anagrafiche dell''articolo sul CORE, '
                      || 'UNITA_MISURA_ID nella riga del listino (per la QTA_UM_RIF_PREZZO) e la tabella NCL_UNITA_MISURA');
  
    -- Inserisco i messaggi per l'utente
    for i in 1..listOND.count loop
      if listOND(i).QTA_PZ = 0 then
        ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, 
                          C_MSG_ERR_CALC_QTA, 
                          MSG_VAR_LIST('UM=' || PKG_UNITA_MISURA.C_UM_CODICE_PEZZI, 'codice=' || listOND(i).COD_ARTICOLO));
      end if;

      if listOND(i).QTA_KG = 0 then
        ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, 
                          C_MSG_ERR_CALC_QTA, 
                          MSG_VAR_LIST('UM=' || PKG_UNITA_MISURA.C_UM_CODICE_KG, 'codice=' || listOND(i).COD_ARTICOLO));
      end if;
 
      if listOND(i).QTA_IC = 0 then
        ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, 
                          C_MSG_ERR_CALC_QTA, 
                          MSG_VAR_LIST('UM=' || PKG_UNITA_MISURA.C_UM_CODICE_IMBALLI_CESSIONE, 'codice=' || listOND(i).COD_ARTICOLO));
      end if;

      if listOND(i).QTA_UM_RIF_PREZZO = 0 then
        ADD_MESSAGGIO_OND(listOND(i).ORDINE_NORMALIZZATO_D_ID, 
                          C_MSG_ERR_CALC_QTA, 
                          MSG_VAR_LIST('UM=' || PKG_UNITA_MISURA.GET_UM_CODICE(listOND(i).UM_RIF_PREZZO_ID) , 'codice=' || listOND(i).COD_ARTICOLO));
      end if;
    end loop;
    
    l_stato_riga_ordine_id := GET_STATO_RIGA_ORDINE_ID(C_STATO_RIGA_ORDINE_BLOCCATO);

    -- TODO: bloccare solo le righe con IC = 0? e/o PZ = 0?
    FORALL i IN 1..listOND.count
    UPDATE ORDINE_NORMALIZZATO_D ond
       SET ond.STATO_RIGA_ORDINE_NORM_ID = l_stato_riga_ordine_id
     WHERE ond.ORDINE_NORMALIZZATO_D_ID = listOND(i).ORDINE_NORMALIZZATO_D_ID;
       
     G_LOGGER.log_warn('Righe Ordine Bloccate per MAGAZZINO_ID non intercettato: ' || listOND.count);
  end if;

END SET_QUANTITA;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_PREZZI_DA_LISTINO(p_ordine_normalizzato_t_id number, p_data_riferimento date := SYSDATE) 
IS
BEGIN
  G_LOGGER.procedura := 'SET_PREZZI_DA_LISTINO';
  G_LOGGER.log_debug('Inizio procedura');

  -- Intecettazione prezzi da listini di cessione
  -- TODO: va ottimizzata la vista V_PDD_COM perch� provoca dei full table scan
  G_LOGGER.log_debug('Intecettazione prezzi da listini di cessione');
  MERGE 
   INTO ORDINE_NORMALIZZATO_D a   
  USING (SELECT ond.ORDINE_NORMALIZZATO_D_ID, 
                lvr.LISTINO_VND_ID, lvr.PREZZO, lvr.LINEA_PREZZO_ID, lvr.LISTINO_VND_RIGA_ID, lvr.UNITA_MISURA_ID,
                ROW_NUMBER() OVER (PARTITION BY ond.ORDINE_NORMALIZZATO_D_ID 
                                       ORDER BY CLV.PRIORITA_REGOLA ASC , CLV.PRIORITA_LISTINO ASC) RNUM    -- 1 � LA PRIORIT� MAX 
           FROM ORDINE_NORMALIZZATO_D ond, V_PDD_COM pdd, V_CRITERIO_LISTINO_VND clv,
                NCL_UNITA_MISURA um,
                LISTINO_VND lv, LISTINO_VND_RIGA lvr
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ond.PDD_ID = pdd.PDD_ID       -- E' il PDD_ID del negozio dell'interlocutore. 
            AND (pdd.PDD_ID = clv.PDD_ID
                 OR pdd.CANALE_ID = clv.CANALE_ID 
                 OR pdd.ZONA_ID = clv.ZONA_ID
                 OR pdd.TIPO_LICENZA_ID = clv.TIPO_LICENZA_ID
                 OR pdd.INSEGNA_ID = clv.INSEGNA_ID
                 OR pdd.DIVISIONE_ID = clv.DIVISIONE_ID)          
            AND clv.TIPO_LISTINO = C_TIPO_LISTINO_CESSIONE
            AND clv.ANNULLATO = 'N'
            AND p_data_riferimento BETWEEN clv.DATA_INIZIO AND NVL(clv.DATA_FINE, TO_DATE('31122999', 'DDMMYYYY'))
            AND clv.LISTINO_VND_ID = lv.LISTINO_VND_ID
            AND p_data_riferimento BETWEEN lv.DATA_INIZIO AND NVL(lv.DATA_FINE, TO_DATE('31122999', 'DDMMYYYY'))   
            AND clv.LISTINO_VND_ID = lvr.LISTINO_VND_ID
            AND p_data_riferimento BETWEEN lvr.DATA_INIZIO AND NVL(lvr.DATA_FINE, TO_DATE('31122999', 'DDMMYYYY'))   
            AND ond.ARTICOLO_ID = lvr.ARTICOLO_ID
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID AND b.RNUM = 1)
   WHEN MATCHED THEN UPDATE 
    SET a.PREZZO_LIST = b.PREZZO,
--        a.COD_LISTINO = b.COD_LISTINO,
        a.LISTINO_VND_ID = b.LISTINO_VND_ID,
        --a.LINEA_PREZZO_ID = b.LINEA_PREZZO_ID, 
        a.LISTINO_VND_RIGA_ID = b.LISTINO_VND_RIGA_ID,
        a.UM_RIF_PREZZO_ID = b.UNITA_MISURA_ID,
--      a.UM_RIF_PREZZO = b.UM_CODICE,
        a.DATA_RIF_PREZZO = p_data_riferimento;

  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);  
  
  -- TODO controllare se ci sono righe senza listini? Bloccarle?

END SET_PREZZI_DA_LISTINO;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_PREZZI_DA_COSTI(p_ordine_normalizzato_t_id number, p_data_riferimento date := SYSDATE) 
IS
BEGIN
  G_LOGGER.procedura := 'SET_PREZZI_DA_COSTI';
  G_LOGGER.log_debug('Inizio procedura');
  
  


  NULL;
END SET_PREZZI_DA_COSTI;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_PREZZI(p_ordine_normalizzato_t_id number, p_data_riferimento date := SYSDATE) 
IS
l_modalita_calc_prezzi  varchar2(50);
BEGIN
  G_LOGGER.procedura := 'SET_PREZZI';
  G_LOGGER.log_debug('Inizio procedura');

  -- Controlla il parametro di tipo calcolo sulla PARAMETRI_CLIENTE per decidere
  -- se per quel cliente il calcolo dei prezzi � da listino o da costi.
  -- TODO: ma questo parametro � sempre presente per tutti i clienti?
  G_LOGGER.log_debug('Controlla del parametro di tipo calcolo MODALITA_CALC_PRZCES_ID sulla PARAMETRI_CLIENTE');
  begin
    SELECT mcp.CODICE
      INTO l_modalita_calc_prezzi
      FROM ORDINE_NORMALIZZATO_T ont, PARAMETRI_CLIENTE pc, MODALITA_CALC_PRZCES mcp
     WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
       AND ont.INTERLOCUTORE_ID = pc.INTERLOCUTORE_ID
       AND p_data_riferimento BETWEEN pc.VALIDO_DAL AND NVL(pc.VALIDO_AL, TO_DATE('31122999', 'DDMMYYYY'))
       AND pc.MODALITA_CALC_PRZCES_ID = mcp.MODALITA_CALC_PRZCES_ID;
  exception
    when others then
       G_LOGGER.log_error('Nessun record valido trovato nella tabella PARAMETRI_CLIENTE');
       
      -- TODO aggiungere messaggio utente
      
      raise;  -- oppure saltare il calcolo dei prezzi e basta?
  end;

  if l_modalita_calc_prezzi = C_TIPO_CALCOLO_PRZ_DA_LISTINO then
    SET_PREZZI_DA_LISTINO(p_ordine_normalizzato_t_id, p_data_riferimento);
  elsif l_modalita_calc_prezzi = C_TIPO_CALCOLO_PRZ_DA_COSTI then
    SET_PREZZI_DA_COSTI(p_ordine_normalizzato_t_id, p_data_riferimento);
  else
    -- todo: LOGGARE ERRORE e aggiungere messaggio
    null;
  end if;

END SET_PREZZI;

----------------------------------------------------------------------------------------------------

PROCEDURE SET_SCONTI(p_ordine_normalizzato_t_id number, p_data_riferimento date := SYSDATE) 
IS

l_classe_sconto_t_id  number;

BEGIN
  G_LOGGER.procedura := 'SET_SCONTI';
  G_LOGGER.log_debug('Inizio procedura');


  -- Ricavo la classe di sconto da applicare al cliente in base ai parametri
  G_LOGGER.log_debug('Ricavo la classe di sconto da applicare al cliente in base ai PARAMETRI_CLIENTE');
  begin
    SELECT pc.CLASSE_SCONTO_T_ID
      INTO l_classe_sconto_t_id
      FROM ORDINE_NORMALIZZATO_T ont, PARAMETRI_CLIENTE pc
     WHERE ont.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
       AND ont.INTERLOCUTORE_ID = pc.INTERLOCUTORE_ID
       AND p_data_riferimento BETWEEN pc.VALIDO_DAL AND NVL(pc.VALIDO_AL, TO_DATE('31122999', 'DDMMYYYY'));
  exception
    when others then
      G_LOGGER.log_error('Nessun record valido trovato nella tabella PARAMETRI_CLIENTE');
      
      -- TODO aggiungere messaggio utente
      
      raise;  -- TODO oppure saltare il calcolo dei prezzi e basta?
  end;
  
  -- Se non ci sono sconti da applicare termino
  if l_classe_sconto_t_id is null then
    -- TODO: loggare che non ci sono classi di sconto da applicare
    G_LOGGER.log_info('Nessuna classe di sconto da applicare');
    
    -- TODO: verificare che sia corretto
    UPDATE ORDINE_NORMALIZZATO_D
       SET VAL_SCONTO = 0,
           PREZZO_UNIT_SCN = PREZZO_LIST
     WHERE ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id;

    return;
  end if;
  
  -- Intercettazione delle classi di sconto e calcolo del prezzo unitario scontato o maggiorato
  G_LOGGER.log_debug('Intercettazione delle classi di sconto e calcolo del prezzo unitario scontato o maggiorato');
  MERGE 
   INTO ORDINE_NORMALIZZATO_D a
  USING (SELECT sconti.SCONTO,
                cst.CLASSE_SCONTO_T_ID,
                cst.DFT_VALORE as SCONTO_DEFAULT,
                sconti.CLASSE_SCONTO_D_ID,
                ROW_NUMBER() OVER (PARTITION BY ond.ORDINE_NORMALIZZATO_D_ID ORDER BY sconti.PROGRESSIVO DESC) RNUM, 
                ond.ORDINE_NORMALIZZATO_D_ID
           FROM ORDINE_NORMALIZZATO_D ond,
                NCL_ARTICOLO_X_MERCEOLOGIA axm, NCL_MERCEOLOGIA m, NCL_MERCEOLOGIA_LIVELLO ml, NCL_MERCEOLOGIA_CL mc,   -- Per ricavare la merceologia dell'articolo
                CLASSE_SCONTO_T cst,
                (SELECT csd.CLASSE_SCONTO_D_ID, csd.VALORE as SCONTO, 
                        m2.VALORE_ESTESO AS MERCEOLOGIA, ml2.PROGRESSIVO
                   FROM CLASSE_SCONTO_D csd, NCL_MERCEOLOGIA m2, NCL_MERCEOLOGIA_LIVELLO ml2
                  WHERE csd.MERCEOLOGIA_ID = m2.MERCEOLOGIA_ID
                    AND m2.MERCEOLOGIA_LIVELLO_ID = ml2.MERCEOLOGIA_LIVELLO_ID     
                    AND csd.CLASSE_SCONTO_T_ID = l_classe_sconto_t_id
                ) sconti
          WHERE ond.ORDINE_NORMALIZZATO_T_ID = p_ordine_normalizzato_t_id
            AND ond.ARTICOLO_ID IS NOT NULL                                   
            AND ond.ARTICOLO_ID = axm.ARTICOLO_ID                                   --
            AND axm.MERCEOLOGIA_ID = m.MERCEOLOGIA_ID                               --
            AND m.MERCEOLOGIA_LIVELLO_ID = ml.MERCEOLOGIA_LIVELLO_ID                --  INDIVIDUAZIONE MERCEOLOGIA ARTICOLO
            AND ml.MERCEOLOGIA_CL_ID = mc.MERCEOLOGIA_CL_ID                         --
            AND mc.CODICE = 'MAXID'                                                 --
            AND cst.CLASSE_SCONTO_T_ID = l_classe_sconto_t_id
            AND m.VALORE_ESTESO like (sconti.MERCEOLOGIA(+) || '%')
        ) b
     ON (a.ORDINE_NORMALIZZATO_D_ID = b.ORDINE_NORMALIZZATO_D_ID AND b.RNUM = 1)
   WHEN MATCHED THEN UPDATE
    SET a.CLASSE_SCONTO_T_ID = b.CLASSE_SCONTO_T_ID,
        a.CLASSE_SCONTO_D_ID = b.CLASSE_SCONTO_D_ID,
        a.VAL_SCONTO = NVL(NVL(b.SCONTO, b.SCONTO_DEFAULT), 0),
        a.PREZZO_UNIT_SCN = a.PREZZO_LIST + a.PREZZO_LIST * NVL(NVL(b.SCONTO, b.SCONTO_DEFAULT), 0) / 100;

  G_LOGGER.log_debug('Righe aggiornate: ' || SQL%ROWCOUNT);  

END SET_SCONTI;

----------------------------------------------------------------------------------------------------

PROCEDURE PROCESSA_ORDINE_NORMALIZZATO(p_richiesta_id NUMBER)
IS
  --PRAGMA AUTONOMOUS_TRANSACTION;

  l_ordine_normalizzato_t_id number;
  l_validazione_ok boolean;
  l_stato_ordine_id number;
  l_stato_riga_ordine_id number;
BEGIN
  
  -- TODO staccare un load_id?
  
  -- Recupera l'ordine normalizzato a partire dalla richiesta
  SELECT ont.ORDINE_NORMALIZZATO_T_ID
    INTO l_ordine_normalizzato_t_id
    FROM ORDINE_NORMALIZZATO_T ont
   WHERE ont.RICHIESTA_ID = p_richiesta_id;
  
  G_LOGGER.log_info('Elaboro Ordine Normalizzato ID: ' || l_ordine_normalizzato_t_id);
  
  -- Pulisco le tabelle dei messaggi utente 
  G_LOGGER.log_debug('Pulisco le tabelle dei messaggi utente');
  DELETE FROM ORDINE_N_T_X_MESS WHERE ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
 
  DELETE 
    FROM ORDINE_N_D_X_MESS 
   WHERE ORDINE_NORMALIZZATO_D_ID IN (SELECT ORDINE_NORMALIZZATO_D_ID
                                        FROM ORDINE_NORMALIZZATO_D
                                       WHERE ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id);
  
  -- Mette la ONT e tutte le OND in stato validato, verranno bloccate man mano dai controlli ko
  G_LOGGER.log_debug('Resetto la testata e le righe in stato validato');
  l_stato_ordine_id := GET_STATO_ORDINE_ID(C_STATO_ORDINE_VALIDATO);
  UPDATE ORDINE_NORMALIZZATO_T ont
     SET ont.STATO_ORDINE_NORM_ID = l_stato_ordine_id
   WHERE ont.ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
  
  l_stato_riga_ordine_id := GET_STATO_RIGA_ORDINE_ID(C_STATO_RIGA_ORDINE_VALIDATO);
  UPDATE ORDINE_NORMALIZZATO_D ond
     SET ond.STATO_RIGA_ORDINE_NORM_ID = l_stato_riga_ordine_id
   WHERE ond.ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
   
  l_validazione_ok := DECODIFICA_CODICI(l_ordine_normalizzato_t_id);
  
  if l_validazione_ok then
    CHECK_ARTICOLI_DIRETTI(l_ordine_normalizzato_t_id);
    INTERCETTA_MAGAZZINO_SERVIZIO(l_ordine_normalizzato_t_id);
    SET_RAPPORTI_AMMINISTRATIVI(l_ordine_normalizzato_t_id);
    SET_CAUSALI_COMMERCIALI(l_ordine_normalizzato_t_id);
    CHECK_ASSORTIMENTO_DST(l_ordine_normalizzato_t_id);
    
    -- L'intercettazione dei prezzi va eseguita prima di quella delle quantit� perch�
    -- una delle quantit� da calcolare (QTA_UM_RIF_PREZZO) � riferita all'UM in cui � espresso il prezzo
    -- intercettato sul listino.
    SET_PREZZI(l_ordine_normalizzato_t_id);     
    
    SET_QUANTITA(l_ordine_normalizzato_t_id);
    SET_SCONTI(l_ordine_normalizzato_t_id);
  end if;

  -- TODO: bloccare le righe che hanno ARTICOLO_DIRETTO = 'N' AND ART_IN_ASSORTIMENTO_X_PDD = 'N' 
  -- TODO: bloccare le righe che hanno ARTICOLO_DIRETTO = 'N' AND MAGAZINO_ID IS NULL (intercettazione fallita)
  
  if not l_validazione_ok then
  
    -- Marco l'ordine come BLOCCATO
    l_stato_ordine_id := GET_STATO_ORDINE_ID(C_STATO_ORDINE_BLOCCATO);
    UPDATE ORDINE_NORMALIZZATO_T 
       SET STATO_ORDINE_NORM_ID = l_stato_ordine_id
     WHERE ORDINE_NORMALIZZATO_T_ID = l_ordine_normalizzato_t_id;
       
   end if;



END PROCESSA_ORDINE_NORMALIZZATO;

----------------------------------------------------------------------------------------------------

END UTL_ORDINI_CLIENTI_2;
/