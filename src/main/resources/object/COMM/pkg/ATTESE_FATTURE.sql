create or replace PACKAGE ATTESE_FATTURE AS
/******************************************************************************
   NAME:       ATTESE_FATTURE
   PURPOSE:
   DEPENDENCY: 

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0.0    -			  -			 	              Created this package.
******************************************************************************/
  PROCEDURE CANCELLA_CARICHI (P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S');

  PROCEDURE CANCELLA_ATTESA_FATTURA (P_CARICO_T_ID IN VARCHAR2, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S');

  PROCEDURE CANCELLA_MOVIMENTO (P_TIPO_ORIGINE IN VARCHAR2, P_ORIGINE_ID IN NUMBER, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S', P_SALVA_STORNO IN CHAR DEFAULT 'S' );

  PROCEDURE CANCELLA_CARICO (P_CARICO_T_ID IN VARCHAR2, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S');

  
  
 /*
  * Effettua la cancellazione delle attese fatture generate da un carico
  *
  * P_CARICO_T_ID ID del carico
  * P_COMMIT
  * P_BACKUP
  * P_RET_CODE Restituisce 'OK' nel caso la cancellazione sia avvenuta correttamente o un codice che identifica la motivazione dell'errore
  *
  * stati di prosecuzione lato java partono con la srtringa OK
  */
  PROCEDURE CANCELLA_ATTESA_FATTURA (P_CARICO_T_ID IN VARCHAR2, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S', P_RET_CODE OUT VARCHAR2);

 /*
  * Effettua la cancellazione dei movimenti generati da un carico
  *
  * P_TIPO_ORIGINE 
  * P_ORIGINE_ID
  * P_BACKUP
  * P_SALVA_STORNO
  * P_RET_CODE Restituisce 'OK' nel caso la cancellazione sia avvenuta correttamente o un codice che identifica la motivazione dell'errore
  *
  * stati di prosecuzione lato java partono con la srtringa OK
  */
  PROCEDURE CANCELLA_MOVIMENTO (P_TIPO_ORIGINE IN VARCHAR2, P_ORIGINE_ID IN NUMBER, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S', P_SALVA_STORNO IN CHAR DEFAULT 'S',  P_RET_CODE  OUT VARCHAR2);

 /*
  * Effettua la cancellazione dei movimenti generati da un carico
  *
  * P_CARICO_T_ID ID del carico
  * P_COMMIT
  * P_BACKUP
  * P_RET_CODE Restituisce 'OK' nel caso la cancellazione sia avvenuta correttamente o un codice che identifica la motivazione dell'errore
  *
  * stati di prosecuzione lato java partono con la srtringa OK
  */
  PROCEDURE CANCELLA_CARICO  (P_CARICO_T_ID IN VARCHAR2, P_COMMIT IN CHAR DEFAULT 'N', P_BACKUP IN CHAR DEFAULT 'S', P_RET_CODE  OUT VARCHAR2);


END ATTESE_FATTURE;
/