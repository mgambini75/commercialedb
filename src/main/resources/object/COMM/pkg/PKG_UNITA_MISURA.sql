create or replace PACKAGE PKG_UNITA_MISURA AS 

/******************************************************************************
 PURPOSE  : Fornisce una serie di procedure per convertire fra le varie  
            unita' di misura. 
 LAST UPD : 06/02/2017
 
 REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0.0.0    06/02/2017  Ivan Lo Castro   Creato il package

 ******************************************************************************/

--| Tipi e Variabili globali    |--------------------------------------------------------------------
C_UM_CODICE_PEZZI            constant varchar2(3) := 'PZ';
C_UM_CODICE_KG               constant varchar2(3) := 'KG';
C_UM_CODICE_IMBALLI_CESSIONE constant varchar2(3) := 'IC';

C_TIPO_UM_CODICE_PESO        constant varchar2(3) := 'PS'; 


--| Procedure Pubbliche    |--------------------------------------------------------------------------

/*
  Dato il codice dell'unita' di misura, ritorna il corrispondente ID.
*/
FUNCTION GET_UM_ID(p_um_codice NCL_UNITA_MISURA.CODICE%type) 
RETURN NCL_UNITA_MISURA.UNITA_MISURA_ID%type
RESULT_CACHE;

/*
  Data l'ID dell'unita' di misura, ritorna il corrispondente codice.
*/
FUNCTION GET_UM_CODICE(p_um_ID NCL_UNITA_MISURA.UNITA_MISURA_ID%type) 
RETURN NCL_UNITA_MISURA.CODICE%type
RESULT_CACHE;

/*
  Converte nell'unita' di misura di arrivo una quantita' espressa nell'unita' di
  misura di partenza. Le due unit� di misura devono essere dello stesso 
  tipo (ad esempio: di peso, di lunghezza, etc.), il flag TIPO_UNITA_MISURA.CONVERTIBILE
  deve essere impostato a 'S' ed il campo UNITA_MISURA.CONVERSIONE deve essere diverso
  da zero. 
  Se la conversione fra le due unit� di misura non � possibile la procedura lancia una
  eccezione.
  Per convertire fra KG, PZ e IC NON usare questa funzione, ma bensi' la funzione
  CONVERTI_UM_LOGISTICA (vedi).
*/
FUNCTION CONVERTI_UM(p_qta number,
                     p_um_id_partenza NCL_UNITA_MISURA.UNITA_MISURA_ID%type, 
                     p_um_id_arrivo NCL_UNITA_MISURA.UNITA_MISURA_ID%type)  
RETURN float;



/*
  Converte fra le unita' di misura logistiche KG (o altri pesi: GR, HG, etc), PZ e IC.
  Se si passano unit� di misura diverse da queste tre, oppure i parametri passati non
  sono coerenti (peso a zero o null, um di peso non valorizzata, imballi cessione non valorizzati
  o a zero) viene lanciata un'eccezione. Se non si vuole che la procedura lanci un'eccezione, impostare
  il parametro p_eccezione_se_errore a 'N': in questo caso la procedura ritorner� zero.
*/
FUNCTION CONVERTI_UM_LOGISTICA(p_qta_partenza number,
                               p_um_id_partenza NCL_UNITA_MISURA.UNITA_MISURA_ID%type, 
                               p_um_id_arrivo NCL_UNITA_MISURA.UNITA_MISURA_ID%type,
                               p_imballo_cessione number,
                               p_peso_medio_pezzo float,
                               p_um_id_peso_medio_pezzo NCL_UNITA_MISURA.UNITA_MISURA_ID%type,
                               p_eccezione_se_errore char := 'S')  
RETURN float;

END PKG_UNITA_MISURA;
/