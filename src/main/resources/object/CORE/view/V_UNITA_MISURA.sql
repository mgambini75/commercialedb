create or replace view v_unita_misura
as
select umt.unita_misura_tipo_id, umt.codice as um_tipo_codice, umt.descrizione as um_tipo_descrizione, umt.convertibile,
       um.unita_misura_id, um.codice, um.descrizione, um.conversione,
       umb.unita_misura_id as um_base_id, umb.codice as um_base_codice, umb.descrizione as um_base_descrizione
  from unita_misura um, unita_misura_tipo umt, unita_misura umb
 where um.unita_misura_tipo_id = umt.unita_misura_tipo_id
   and umt.unita_misura_base_id = umb.unita_misura_id; 
   