-- BRC_CORE
create or replace FUNCTION FN_GET_PALLETTIZZATI(p_qta IN NUMBER, p_pallet_imballi IN NUMBER)
RETURN NUMBER
AS
  v_int         NUMBER;
  v_resto       NUMBER;
  v_progressivo NUMBER:=0;
BEGIN
  v_int           := TRUNC(p_qta / p_pallet_imballi);
  v_progressivo   := v_int;
  v_resto         := MOD(p_qta,p_pallet_imballi);
  IF (v_resto = 0) THEN
    RETURN v_progressivo;
  END IF;
RETURN v_int; --v_resto;
EXCEPTION
WHEN OTHERS THEN
  RETURN -123321;
END FN_GET_PALLETTIZZATI;