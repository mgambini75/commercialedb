create or replace PACKAGE BODY PKG_GENERA_LISTA_PREPARAZIONE AS

C_NOME_MODULO CONSTANT VARCHAR2(30) := 'PKG_GENERA_LISTA_PREPARAZIONE';
  
G_LOGGER TYP_LOGGER_NEW             := TYP_LOGGER_NEW(sys_context('userenv','current_schema'), C_NOME_MODULO);

------------------------------------------------------------------------------------------------------------------
/*
  PROCEDURE send_mail(Subject in varchar2, Msg in varchar2)
  AS
  BEGIN
    UTILITY.SPEDISCI_MAIL(Oggetto => 'ORDINI CLIENTI: ' || C_NOME_MODULO || ' - ' || Subject,
                Messaggio => Msg,
                GruppoDestinatario => 'ORDINI_CLIENTI_DEST',
                LoaId => 0);
  END;
*/
------------------------------------------------------------------------------------------------------------------

  PROCEDURE SP_PRE_CARICAMENTO
  IS
    l_lista_preparazione_t_id_PP NUMBER;
    l_lista_preparazione_t_id_PK NUMBER;
    
    l_progressivo_listaPK        NUMBER;
    l_progressivo_listaPP        NUMBER;
    
    l_qta_richiesta_tot_ord      NUMBER;
    l_qta_pk                     NUMBER;

    l_num_riga_PP                NUMBER;
    l_num_riga_PK                NUMBER;
    
    l_pallet_richiesti           NUMBER:=0;
    
    l_tipo_preparazione_id       NUMBER;

  BEGIN
  
       G_LOGGER.log_info('SP_PRE_CARICAMENTO iniziata','SP_PRE_CARICAMENTO');
       G_LOGGER.log_info('Inizio caricamento testata_ordini da ORDINE_DST_T','SP_PRE_CARICAMENTO');
  
       FOR testata_ordini IN
      (SELECT
        ord_dst_t.ordine_dst_t_codice       AS codice,
        ord_dst_t.cliente_id                AS cliente_id,
        ord_dst_t.destinatario_id           AS destinazione_id,
        ord_dst_t.data_ricezione_ordine     AS data_ordine,
        ord_dst_t.tipo_consegna_id          AS tipo_consegna_id,
        ord_dst_t.annullato                 AS annullato,
        ord_dst_t.ordine_dst_t_id           AS ordine_dst_t_id,
        'RINE'                              AS prefisso,
        NULL                                AS suffisso                      
       FROM ordine_dst_t ord_dst_t  
          JOIN destinazione core_dest ON (ord_dst_t.destinatario_id = core_dest.destinazione_id)
          JOIN stato_ordine ON (ord_dst_t.stato_ordine_id = stato_ordine.stato_ordine_id)
       WHERE ord_dst_t.stato_ordine_id = 1 AND ord_dst_t.annullato = 'N')
    
      LOOP
      
      -- azzero i progressivi
      l_progressivo_listaPK := 0;
      l_progressivo_listaPP := 0;
      l_num_riga_PK := 0;
      l_num_riga_PP := 0;
        
        G_LOGGER.log_info('Inizio caricamento righe_ordini da ORDINE_DST_D','SP_PRE_CARICAMENTO',NULL,'testata_ordini.ordine_dst_t_id:= ' || testata_ordini.ordine_dst_t_id);
        
        FOR righe_ordini IN (
        SELECT 
          ord_dst_d.numero_riga                AS numero_riga,
          ord_dst_d.articolo_id                AS articolo_id,
          ord_dst_d.flag_stralcio              AS flag_stralcio,
          ord_dst_d.imballo_cessione           AS imballo_cessione,
          art_x_pdd.pallet_imballi             AS pallet_imballi,
          NULL                                 AS quantita_ic, -- TODO verificare che sia corretto
          ord_dst_d_umi.quantita_ordinata      AS qta_ordine,
          unita_misura.unita_misura_id         AS um_ordine_id,
          unita_misura.unita_misura_id         AS um_preparazione_id,
          ord_dst_d.ordine_dst_t_id            AS ordine_dst_t_id,
          ord_dst_d.ordine_dst_d_id            AS ordine_dst_d_id,
          ord_dst_d.stato_evadibilita_id       AS stato_evadibilita_id
        FROM ordine_dst_d ord_dst_d 
          JOIN ordine_dstd_x_umi ord_dst_d_umi ON (ord_dst_d_umi.ordine_dst_d_id = ord_dst_d.ordine_dst_d_id and ord_dst_d_umi.unita_misura_id = 3)
          JOIN articolo_x_divisione art_x_div ON (ord_dst_d.articolo_id = art_x_div.articolo_id)
          JOIN articolo_x_pdd art_x_pdd ON (art_x_pdd.articolo_x_divisione_id = art_x_div.articolo_x_divisione_id and art_x_pdd.pdd_id = ord_dst_d.magazzino_id)
          JOIN unita_misura ON (ord_dst_d_umi.unita_misura_id = unita_misura.unita_misura_id AND unita_misura.codice = 'IC')
        WHERE ord_dst_d.ordine_dst_t_id = testata_ordini.ordine_dst_t_id)
           
           LOOP
           
             -- calcolo i PALLET necessari
             l_pallet_richiesti := FN_GET_PALLETTIZZATI(righe_ordini.qta_ordine,righe_ordini.pallet_imballi);
             
             l_qta_richiesta_tot_ord := righe_ordini.qta_ordine;

             -- ordine di tipo PP
             IF(l_pallet_richiesti > 0) THEN
             
             l_num_riga_PP := 1;
             l_progressivo_listaPP := l_progressivo_listaPP + 1;
             
             FOR cont_prog in 1 .. l_pallet_richiesti
             LOOP
              SELECT sq_lista_preparazione_t.nextval INTO l_lista_preparazione_t_id_PP FROM DUAL;    
                   SELECT tipo_preparazione_id
                   INTO l_tipo_preparazione_id
                   FROM brc_logi.tipo_preparazione
                   WHERE codice = 'PP'; -- FIXME (� corretto reperire da MOVER?)
                      
                      INSERT INTO tmp_lista_preparazione_d
                        (
                          lista_preparazione_d_id,
                          lista_preparazione_t_id,
                          numero_riga,
                          articolo_id,
                          flag_stralcio,
                          imballo_cessione,
                          pallet_imballi,
                          quantita_ic,
                          qta_ordine,
                          um_ordine_id,
                          qta_preparata,
                          um_preparazione_id,
                          peso_prodotto,
                          ean,
                          dettaglio_sostitutivo_id,
                          barcode_tipo_id,
                          ordine_dst_t_id,
                          ordine_dst_d_id,
                          prefisso, 
                          progressivo, 
                          suffisso,
                          stato_evadibilita_id
                        )
                      VALUES
                        (
                          sq_lista_preparazione_d.nextval,
                          l_lista_preparazione_t_id_PP,
                          l_num_riga_PP,
                          righe_ordini.articolo_id,
                          righe_ordini.flag_stralcio,
                          righe_ordini.imballo_cessione,
                          righe_ordini.pallet_imballi,
                          righe_ordini.quantita_ic,
                          righe_ordini.pallet_imballi, 
                          righe_ordini.um_ordine_id,
                          0,
                          righe_ordini.um_preparazione_id,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          righe_ordini.ordine_dst_t_id,
                          righe_ordini.ordine_dst_d_id,
                          testata_ordini.prefisso,
                          l_progressivo_listaPP,
                          testata_ordini.suffisso,
                          righe_ordini.stato_evadibilita_id
                        );
                        
                      -- sottraggo alla qta totale dell'ordine, il pallet imballo
                      l_qta_richiesta_tot_ord := l_qta_richiesta_tot_ord - righe_ordini.pallet_imballi;
                      
                      INSERT INTO tmp_lista_preparazione_t
                        (
                          lista_preparazione_t_id,
                          codice,
                          cliente_id,
                          destinazione_id,
                          documento_gestionale_id,
                          prefisso,
                          progressivo,
                          suffisso,
                          data_ordine,
                          data_acquisizione,
                          data_inizio_preparazione,
                          --magazzino_id,
                          tipo_consegna_id,
                          preparatore_id,
                          stato_preparazione_id,
                          annullato,
                          priorita,
                          ordine_dst_t_id,
                          tipo_preparazione_id
                        )
                      VALUES
                        (
                          l_lista_preparazione_t_id_PP,
                          testata_ordini.codice,
                          testata_ordini.cliente_id,
                          testata_ordini.destinazione_id,
                          456, -- TODO verificare che sia corretto
                          'RINE',
                          l_progressivo_listaPP,
                          NULL,
                          testata_ordini.data_ordine,
                          NULL, -- TODO da correggere?
                          NULL,
                          --ord_dst_d.magazzino_id ,
                          testata_ordini.tipo_consegna_id,
                          NULL,
                          1, -- TODO verificare che sia corretto
                          testata_ordini.annullato,
                          NULL, --testata_ordini.priorita,
                          testata_ordini.ordine_dst_t_id,
                          l_tipo_preparazione_id  
                    );
               END LOOP;
             END IF;
                 
               -- ordine di tipo PK
               IF (l_qta_richiesta_tot_ord > 0) THEN
                 l_num_riga_PK:= l_num_riga_PK + 1;
               
                 -- inserisco la prima testata (unica) delle righe di tipo PK
                 IF(l_progressivo_listaPK = 0) THEN
                   l_progressivo_listaPK:= l_progressivo_listaPP + 1;
                   l_lista_preparazione_t_id_PK := sq_lista_preparazione_t.nextval;
                   l_progressivo_listaPP := l_progressivo_listaPK;
                   
                   SELECT tipo_preparazione_id
                   INTO l_tipo_preparazione_id
                   FROM brc_logi.tipo_preparazione
                   WHERE codice = 'PK'; -- FIXME (E' corretto reperire da MOVER?)

                   INSERT INTO tmp_lista_preparazione_t
                     (
                        lista_preparazione_t_id,
                        codice,
                        cliente_id,
                        destinazione_id,
                        documento_gestionale_id,
                        prefisso,
                        progressivo,
                        suffisso,
                        data_ordine,
                        data_acquisizione,
                        data_inizio_preparazione,
                        --magazzino_id,
                        tipo_consegna_id,
                        preparatore_id,
                        stato_preparazione_id,
                        annullato,
                        priorita,
                        ordine_dst_t_id,
                        tipo_preparazione_id
                     )
                   VALUES
                     (
                        l_lista_preparazione_t_id_PK,
                        testata_ordini.codice,
                        testata_ordini.cliente_id,
                        testata_ordini.destinazione_id,
                        456, -- TODO verificare che sia corretto
                        'RINE',
                        l_progressivo_listaPK,
                        NULL,
                        testata_ordini.data_ordine,
                        NULL, -- TODO da correggere?
                        NULL,
                        --ord_dst_d.magazzino_id ,
                        testata_ordini.tipo_consegna_id,
                        NULL,
                        1, -- TODO verificare che sia corretto
                        testata_ordini.annullato,
                        NULL, --testata_ordini.priorita,
                        testata_ordini.ordine_dst_t_id,
                        l_tipo_preparazione_id
                     );
                 END IF;
                 
                    -- inserisco le righe PK
                   INSERT INTO tmp_lista_preparazione_d
                     (
                       lista_preparazione_d_id,
                       lista_preparazione_t_id,
                       numero_riga,
                       articolo_id,
                       flag_stralcio,
                       imballo_cessione,
                       pallet_imballi,
                       quantita_ic,
                       qta_ordine,
                       um_ordine_id,
                       qta_preparata,
                       um_preparazione_id,
                       peso_prodotto,
                       ean,
                       dettaglio_sostitutivo_id,
                       barcode_tipo_id,
                       ordine_dst_t_id,
                       ordine_dst_d_id,
                       prefisso, 
                       progressivo, 
                       suffisso,
                       stato_evadibilita_id
                     ) 
                   VALUES
                     (
                       sq_lista_preparazione_d.nextval,
                       l_lista_preparazione_t_id_PK,
                       l_num_riga_PK,
                       righe_ordini.articolo_id,
                       righe_ordini.flag_stralcio,
                       righe_ordini.imballo_cessione,
                       righe_ordini.pallet_imballi,
                       righe_ordini.quantita_ic,
                       l_qta_richiesta_tot_ord, 
                       righe_ordini.um_ordine_id,
                       0,
                       righe_ordini.um_preparazione_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       righe_ordini.ordine_dst_t_id,
                       righe_ordini.ordine_dst_d_id,
                       testata_ordini.prefisso,
                       l_progressivo_listaPK,
                       testata_ordini.suffisso,
                       righe_ordini.stato_evadibilita_id
                     );
               END IF;
         END LOOP;
    END LOOP;
    
    -- update magazzino_id
    MERGE INTO tmp_lista_preparazione_t USING
      (
        SELECT DISTINCT ord_dst_t.ordine_dst_t_id, ord_dst_d.magazzino_id
        FROM ordine_dst_d ord_dst_d
        JOIN ordine_dst_t ord_dst_t ON (ord_dst_d.ordine_dst_t_id = ord_dst_t.ordine_dst_t_id)
        JOIN tmp_lista_preparazione_t tmp_lpt ON (ord_dst_d.ordine_dst_t_id = tmp_lpt.ordine_dst_t_id)
      ) A
    ON (tmp_lista_preparazione_t.ordine_dst_t_id = A.ordine_dst_t_id) 
    WHEN MATCHED THEN UPDATE SET tmp_lista_preparazione_t.magazzino_id = A.magazzino_id;
      
  END SP_PRE_CARICAMENTO;

------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_CARICAMENTO
  IS
  
  l_rapporto_ic     NUMBER;
  
  BEGIN
  
   G_LOGGER.log_info('SP_CARICAMENTO iniziata','SP_CARICAMENTO');
   G_LOGGER.log_info('Inizio INSERT in LISTA_PREPARAZIONE_T da TMP_LISTA_PREPARAZIONE_T','SP_CARICAMENTO');
  
        INSERT INTO LISTA_PREPARAZIONE_T
          (
            lista_preparazione_t_id,
            codice,
            cliente_id,
            destinazione_id,
            documento_gestionale_id,
            prefisso,
            progressivo,
            suffisso,
            data_ordine,
            data_acquisizione,
            data_inizio_preparazione,
            magazzino_id,
            tipo_consegna_id,
            preparatore_id,
            stato_preparazione_id,
            annullato,
            ins_data,
            ins_utente,
            ins_modulo,
            upd_data,
            upd_utente,
            upd_modulo,
            version,
            priorita,
            flag_trasmesso,
            tipo_preparazione_id
          )
        SELECT
            lista_preparazione_t_id,
            tmp_lpt.codice,
            tmp_lpt.cliente_id,
            tmp_lpt.destinazione_id,
            tmp_lpt.documento_gestionale_id,
            tmp_lpt.prefisso,
            tmp_lpt.progressivo,
            tmp_lpt.suffisso,
            tmp_lpt.data_ordine,
            tmp_lpt.data_acquisizione,
            tmp_lpt.data_inizio_preparazione,
            tmp_lpt.magazzino_id,
            tmp_lpt.tipo_consegna_id,
            tmp_lpt.preparatore_id,
            tmp_lpt.stato_preparazione_id,
            tmp_lpt.annullato,
            SYSDATE                          AS ins_data,
            'PKG_GENERA_LISTA_PREPAZIONE'    AS ins_utente,
            'PKG_GENERA_LISTA_PREPAZIONE'    AS ins_modulo,
            SYSDATE                          AS upd_data,
            'PKG_GENERA_LISTA_PREPAZIONE'    AS upd_utente,
            'PKG_GENERA_LISTA_PREPAZIONE'    AS upd_modulo,
            SYSDATE                          AS version,
            tmp_lpt.priorita,
            'N'                              AS flag_trasmesso,
            tmp_lpt.tipo_preparazione_id
        FROM tmp_lista_preparazione_t tmp_lpt;
        
   G_LOGGER.log_info('Eseguito INSERT LISTA_PREPARAZIONE_T per: '|| SQL%ROWCOUNT ||' rec','SP_CARICAMENTO');
   -------------------------------------------------------------------------------------------
   
   G_LOGGER.log_info('Inizio INSERT in LISTA_PREPARAZIONE_D da TMP_LISTA_PREPARAZIONE_D','SP_CARICAMENTO');
   
      INSERT INTO LISTA_PREPARAZIONE_D
        (
          lista_preparazione_d_id,
          lista_preparazione_t_id,
          numero_riga,
          articolo_id,
          flag_stralcio,
          imballo_cessione,
          pallet_imballi,
          quantita_ic,
          qta_ordine,
          um_ordine_id,
          qta_preparata,
          um_preparazione_id,
          peso_prodotto,
          ins_data,
          ins_utente,
          ins_modulo,
          upd_data,
          upd_utente,
          upd_modulo,
          version,
          ean,
          dettaglio_sostitutivo_id,
          barcode_tipo_id,
          prefisso, 
          progressivo, 
          suffisso,
          stato_evadibilita_id
        )
      SELECT
          tmp_lpd.lista_preparazione_d_id,
          tmp_lpd.lista_preparazione_t_id,
          tmp_lpd.numero_riga,
          tmp_lpd.articolo_id,
          tmp_lpd.flag_stralcio,
          tmp_lpd.imballo_cessione,
          tmp_lpd.pallet_imballi,
          tmp_lpd.quantita_ic,
          tmp_lpd.qta_ordine,
          tmp_lpd.um_ordine_id,
          tmp_lpd.qta_preparata,
          tmp_lpd.um_preparazione_id,
          tmp_lpd.peso_prodotto,
          SYSDATE                          AS ins_data,
          'PKG_GENERA_LISTA_PREPAZIONE'    AS ins_utente,
          'PKG_GENERA_LISTA_PREPAZIONE'    AS ins_modulo,
          SYSDATE                          AS upd_data,
          'PKG_GENERA_LISTA_PREPAZIONE'    AS upd_utente,
          'PKG_GENERA_LISTA_PREPAZIONE'    AS upd_modulo,
          SYSDATE                          AS version,
          tmp_lpd.ean,
          tmp_lpd.dettaglio_sostitutivo_id,
          tmp_lpd.barcode_tipo_id,
          tmp_lpd.prefisso, 
          tmp_lpd.progressivo, 
          tmp_lpd.suffisso,
          tmp_lpd.stato_evadibilita_id
      FROM tmp_lista_preparazione_d tmp_lpd;
        
   G_LOGGER.log_info('Eseguito INSERT LISTA_PREPARAZIONE_D per: '|| SQL%ROWCOUNT ||' rec','SP_CARICAMENTO');
   -------------------------------------------------------------------------------------------
    
    FOR righe_ordini IN (
        SELECT
          tmp_lpd.lista_preparazione_d_id         AS lista_preparazione_d_id,
          tmp_lpd.ordine_dst_d_id                 AS ordine_dst_d_id,
          dstx_umi.unita_misura_id                AS unita_misura_id,
          tmp_lpd.qta_ordine                      AS qta_ordine,
          tmp_lpd.qta_preparata                   AS qta_preparata,
          dstx_umi.quantita_ordinata              AS quantita_ordinata
        FROM tmp_lista_preparazione_d tmp_lpd
        JOIN ordine_dstd_x_umi dstx_umi ON (dstx_umi.ordine_dst_d_id = tmp_lpd.ordine_dst_d_id)
        WHERE dstx_umi.unita_misura_id = 3)

    LOOP
        
    	l_rapporto_ic:= righe_ordini.qta_ordine / righe_ordini.quantita_ordinata;
          
          INSERT INTO LISTA_PREPARAZIONE_D_X_UMI
            ( 
              lista_preparazione_d_id,
              unita_misura_id,
              quantita_preparata,
              quantita_da_preparare,
              pezzi_sfusi,
              pezzi_sfusi_da_preparare,
              ins_data,
              ins_utente,
              ins_modulo,
              upd_data,
              upd_utente,
              upd_modulo,
              version
            )
          SELECT
              righe_ordini.lista_preparazione_d_id                      AS lista_preparazione_d_id,
              dstx_umi.unita_misura_id                                  AS unita_misura_id,
              righe_ordini.qta_preparata                                AS quantita_preparata,
              dstx_umi.quantita_ordinata * l_rapporto_ic                AS quantita_da_preparare,
              0                                                         AS pezzi_sfusi,
              0                                                         AS pezzi_sfusi_da_preparare,
              SYSDATE                                                   AS ins_data,
              'PKG_GENERA_LISTA_PREPAZIONE'                             AS ins_utente,
              'PKG_GENERA_LISTA_PREPAZIONE'                             AS ins_modulo,
              SYSDATE                                                   AS upd_data,
              'PKG_GENERA_LISTA_PREPAZIONE'                             AS upd_utente,
              'PKG_GENERA_LISTA_PREPAZIONE'                             AS upd_modulo,
              SYSDATE                                                   AS version
          FROM ordine_dstd_x_umi dstx_umi
          WHERE dstx_umi.ordine_dst_d_id = righe_ordini.ordine_dst_d_id;

    END LOOP;
    
         INSERT INTO LP_D_X_ORDINE_DST_D
            (
              lista_preparazione_d_id,
              ordine_dst_d_id,
              ins_data,
              ins_utente,
              ins_modulo,
              upd_data,
              upd_utente,
              upd_modulo,
              version
            )
          SELECT
              tmp_lpd.lista_preparazione_d_id,
              tmp_lpd.ordine_dst_d_id,
              SYSDATE,
              'PKG_GENERA_LISTA_PREPAZIONE',
              'PKG_GENERA_LISTA_PREPAZIONE',
              SYSDATE,
              'PKG_GENERA_LISTA_PREPAZIONE',
              'PKG_GENERA_LISTA_PREPAZIONE',
              SYSDATE
          FROM tmp_lista_preparazione_d tmp_lpd;
          
   G_LOGGER.log_info('Eseguito INSERT LP_D_X_ORDINE_DST_D per: '|| SQL%ROWCOUNT ||' rec','SP_CARICAMENTO');
   -------------------------------------------------------------------------------------------
          
          UPDATE ordine_dst_t ord_dst_t
          SET stato_ordine_id = 2
          WHERE ord_dst_t.ordine_dst_t_id
          IN (SELECT tmp_lpt.ordine_dst_t_id FROM tmp_lista_preparazione_t tmp_lpt);
              
   G_LOGGER.log_info('Eseguito UPDATE ORDINE_DST_T (stato_ordine_id da 1 a 2) per: '|| SQL%ROWCOUNT ||' rec','SP_CARICAMENTO');
   -------------------------------------------------------------------------------------------

  EXCEPTION
  WHEN OTHERS THEN
   G_LOGGER.log_error('Eccezione SP_CARICAMENTO: ' || SQLERRM,'SP_CARICAMENTO');
  RAISE;
  END;
  
------------------------------------------------------------------------------------------------------------------
  
  PROCEDURE SP_MAIN
  IS
  l_msg 			VARCHAR2(255);
  BEGIN
    G_LOGGER.log_info('SP_MAIN iniziata','SP_MAIN');
    SP_PRE_CARICAMENTO();
    SP_CARICAMENTO();
    COMMIT;
    G_LOGGER.log_info('Eseguita COMMIT','SP_MAIN');
    EXCEPTION
    WHEN OTHERS THEN
      l_msg := 'GENERA LISTA PREPARAZIONE ORDINI BLOCCATO ' || DBMS_UTILITY.FORMAT_ERROR_STACK || '  ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
      --send_mail('GENERA LISTA PREPARAZIONE ORDINI BLOCCATO',l_msg,0);
      G_LOGGER.log_error('Eccezione SP_MAIN: ' || SQLERRM,'SP_MAIN');   
    RAISE;
    ROLLBACK;
    G_LOGGER.log_info('Eseguita ROLLBACK','SP_MAIN');
  END SP_MAIN;

------------------------------------------------------------------------------------------------------------------ 
  
END PKG_GENERA_LISTA_PREPARAZIONE;
/