create or replace PACKAGE UTL_MESSAGGIO
AS
  /******************************************************************************
  NAME:       UTL_MESSAGGIO
  PURPOSE:
  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0.0.0    27/10/2016  C.Carretti       Created this package.
  1.0.0.1    04/11/2016  C.Carretti       Add function GET_MSG_LIST.
  ******************************************************************************/
  SEVERITY_ERROR    CONSTANT CHAR(1) := 'E';
  SEVERITY_WARN     CONSTANT CHAR(1) := 'W';
  SEVERITY_INFO     CONSTANT CHAR(1) := 'I';
  SEVERITY_INATTESO CONSTANT CHAR(1) := 'U'; --?????????
  
  
  TYPE TYPE_MESSAGGIO IS RECORD
  (
    MSG_ID          MESSAGGIO.MESSAGGIO_ID%TYPE,
    MSG_CODICE      MESSAGGIO.CODICE%TYPE,
    MSG_DESCRIZIONE MESSAGGIO.DESCRIZIONE%TYPE,
    CTX_CODICE      CONTESTO_MESSAGGIO.CODICE%TYPE,
    CTX_DESCRIZIONE CONTESTO_MESSAGGIO.DESCRIZIONE%TYPE,
    SEVERITY_CODE   TIPO_MESSAGGIO.CODICE%TYPE,       
    SEVERITY_DESC   TIPO_MESSAGGIO.DESCRIZIONE%TYPE
  );

  TYPE TBL_MESSAGGIO IS TABLE OF TYPE_MESSAGGIO;

  
  
  /*
  * Restituisce il numero di messaggi ERROR indicata contenuti nella maschera di bit, 0 altrimenti
  *
  * P_CONTESTO Contesto di ricerca del codice
  * P_BIT_MASK Codice a maschera di bit DEI messaggi
  */
  FUNCTION HAS_ERROR_SEVERITY(
      P_CONTESTO IN VARCHAR,
      P_BIT_MASK IN NUMBER)
    RETURN NUMBER;
  /*
  * Restituisce il numero di messaggi alla severity indicata contenuti nella maschera di bit, 0 altrimenti
  *
  * P_SEVERITY Severity (vedi costanti)
  * P_CONTESTO Contesto di ricerca del codice
  * P_BIT_MASK Codice a maschera di bit DEI messaggi
  */
  FUNCTION HAS_MSG_SEVERITY(
      P_SEVERITY IN CHAR,
      P_CONTESTO IN VARCHAR,
      P_BIT_MASK IN NUMBER)
    RETURN NUMBER;
 /*
  * Restituisce la maschera di bit associata a questo messaggio, 0 se non presente
  *
  * P_CODICE Codice del messaggio
  */
  FUNCTION GET_BIT_MASK(
      P_CODICE IN VARCHAR)
    RETURN NUMBER;
    
 /*
  * Restituisce la lista dei messaggi presenti nella maschera di bit
  *
  * P_CONTESTO Contesto di ricerca del codice
  * P_BIT_MASK Codice a maschera di bit DEI messaggi
  * P_NOMSG_ROW a 1 se anche nel caso di nessun messaggio si deve ottenere un messaggio fittizio, 0 altrimenti
  */  
  FUNCTION GET_MSG_LIST(
      P_CONTESTO IN VARCHAR,
      P_BIT_MASK IN NUMBER,
      P_NOMSG_ROW IN CHAR DEFAULT 'S')
    RETURN TBL_MESSAGGIO PIPELINED;
    

END UTL_MESSAGGIO;
/