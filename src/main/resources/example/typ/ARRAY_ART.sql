--liquibase formatted sql
--changeset ccarretti:creazione_iniziale logicalFilePath:ARRAY_ART.sql runOnChange:true runInTransaction:true failOnError:true 
create or replace TYPE ARRAY_ART AS TABLE OF VARCHAR2(100);
/