--liquibase formatted sql
--changeset ccarretti:creazione_iniziale logicalFilePath:UTL_RICHIESTE.sql runOnChange:true failOnError:true 

create or replace PACKAGE UTL_RICHIESTE IS
/******************************************************************************
   NAME:       UTL_RICHIESTE
   PURPOSE:
   DEPENDENCY: DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0.2    20161222    A. Acinapura     Added CHECK_ABIL in RICERCA_FASE: 
                                           skip working if client is not enabled.
   1.0.0.1    20161222    A. Acinapura     Added IS_CLIENT_ABIL function.
   1.0.0.0    -			  -			 	   Porting this package.
******************************************************************************/

  C_VERSION VARCHAR2(100):='V:1.0.0.0 LASTUPDATE:22/12/2016';

  FUNCTION GET_VERSION RETURN VARCHAR2;
  PROCEDURE SET_FASE_X_PROCESSO(P_PROCESSO IN VARCHAR2,
                                P_FASE IN VARCHAR2,
                                P_STATO IN NUMBER,
                                P_RICHIESTA_ID IN NUMBER,
                                P_START IN NUMBER:=0,
                                P_ERRORE IN CLOB:=NULL,
                                P_FLAG_INIT_CALLER BOOLEAN := FALSE,
                                P_CLIENT_INFO_ABIL IN VARCHAR2:=NULL,
                                P_CHECK_CHILD IN NUMBER :=0);
  PROCEDURE INIT_FASE_X_PROCESSO(P_RICHIESTA_ID IN NUMBER);
  FUNCTION CHECK_FASE_X_PROCESSO(P_RICHIESTA_ID IN NUMBER,
                                 P_PROCESSO                  IN VARCHAR2:=NULL,
                                 P_FASE IN VARCHAR2:=NULL) RETURN NUMBER;
  PROCEDURE SET_FILTRI_XML(P_RICHIESTA_ID IN NUMBER, P_XML IN CLOB);
  PROCEDURE SET_END_RICHIESTA(P_RICHIESTA_ID IN NUMBER);
  FUNCTION CHECK_TIMEOUT_RICHIESTE(P_RICHIESTA_ID IN NUMBER) RETURN NUMBER;
  PROCEDURE CHECK_COMPLETED(P_RICHIESTA_ID IN NUMBER);
  FUNCTION CHECK_STEP_X_STATO(P_STATO_OLD IN NUMBER, P_STATO_NEW IN NUMBER) RETURN NUMBER;
  FUNCTION  RICERCA_FASE(P_LIST IN ARRAY_ART) RETURN SYS_REFCURSOR;
  PROCEDURE SERIALIZZA_SESSIONE(P_CONTESTO_SESSIONE IN VARCHAR2);
  FUNCTION CHECK_COMPLETED_CHILD(P_RICHIESTA_ID IN NUMBER) RETURN NUMBER;
  FUNCTION IS_CLIENT_ABIL(P_CLIENT_INFO IN VARCHAR2, P_CLIENT_INFO_ABIL IN VARCHAR2) RETURN VARCHAR2;  
  FUNCTION INSERT_RICHIESTA(P_CODICE IN VARCHAR2, P_LOAD_ID IN NUMBER, P_MODULE IN VARCHAR2 DEFAULT 'DIRECT-API') RETURN NUMBER;
  -- notifica dal DITECH_INTEGRAZIONE per PUB/SUB
  PROCEDURE RECEIVE_NOTIFY(P_DESTINATARIO IN VARCHAR2, P_PROCESSO IN VARCHAR2, P_MITTENTE IN VARCHAR2, P_LOAD_ID IN NUMBER, P_CODICE_ESTERNO IN VARCHAR2);
  PROCEDURE CALLBACK_NOTIFY(P_RICHIESTA_ID IN NUMBER, P_DESTINATARIO IN VARCHAR2, P_PROCESSO IN VARCHAR2, P_MITTENTE IN VARCHAR2, P_LOAD_ID IN NUMBER);
END UTL_RICHIESTE;
/

create or replace PACKAGE BODY UTL_RICHIESTE IS

  FUNCTION GET_VERSION RETURN VARCHAR2 IS
  BEGIN
    RETURN C_VERSION;
  END;

  PROCEDURE SET_FASE_X_PROCESSO(P_PROCESSO IN VARCHAR2,
                                P_FASE IN VARCHAR2,
                                P_STATO IN NUMBER,
                                P_RICHIESTA_ID IN NUMBER,
                                P_START IN NUMBER:=0,
                                P_ERRORE IN CLOB:=NULL,
                                P_FLAG_INIT_CALLER BOOLEAN := FALSE,
                                P_CLIENT_INFO_ABIL IN VARCHAR2:=NULL,
                                P_CHECK_CHILD IN NUMBER :=0) IS
   V_CHECK_RECORD NUMBER;
   PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN

    IF P_FLAG_INIT_CALLER = FALSE THEN
      INIT_FASE_X_PROCESSO(P_RICHIESTA_ID => P_RICHIESTA_ID);
    END IF;

    SELECT COUNT(1) INTO V_CHECK_RECORD FROM RICHIESTE_FASE_X_PROCESSO
    WHERE PROCESSO = P_PROCESSO
    AND FASE = P_FASE
    AND RICHIESTA_ID= P_RICHIESTA_ID;

    IF V_CHECK_RECORD = 0 THEN
      INSERT INTO RICHIESTE_FASE_X_PROCESSO(
        FASE,
        PROCESSO,
        STATO,
        RICHIESTA_ID,
        ERRORE,
        INS_UTENTE,
        INS_MODULO,
        INS_DATA,
        UPD_UTENTE,
        UPD_MODULO,
        UPD_DATA,
        CLIENT_INFO,
        CLIENT_INFO_ABIL,
        CHECK_RICHIESTA_X_DIPENDENZA
      )
      VALUES(
        P_FASE,
        P_PROCESSO,
        P_STATO,
        P_RICHIESTA_ID,
        P_ERRORE,
        'SET_FASE_X_PROCESSO',
        'SET_FASE_X_PROCESSO',
        SYSDATE,
        'SET_FASE_X_PROCESSO',
        'SET_FASE_X_PROCESSO',
        SYSDATE,
        SYS_CONTEXT('USERENV','CLIENT_INFO'),
        P_CLIENT_INFO_ABIL,
        P_CHECK_CHILD
      );
    ELSE
      UPDATE RICHIESTE_FASE_X_PROCESSO R SET 
        STATO =P_STATO,
        START_DATE = DECODE(P_STATO,1,SYSDATE, START_DATE),
        END_DATE = DECODE(P_STATO,2,SYSDATE, END_DATE),
        ERRORE = NVL(P_ERRORE,ERRORE),
        UPD_UTENTE='SET_FASE_X_PROCESSO',
        UPD_MODULO='SET_FASE_X_PROCESSO',
        UPD_DATA=SYSDATE,
        CLIENT_INFO = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),CLIENT_INFO)
      WHERE FASE = P_FASE
      AND PROCESSO = P_PROCESSO
      AND RICHIESTA_ID = P_RICHIESTA_ID
      AND CHECK_STEP_X_STATO(P_STATO_OLD=> R.STATO, P_STATO_NEW =>P_STATO) =1
      ;
   END IF;

   CHECK_COMPLETED(P_RICHIESTA_ID => P_RICHIESTA_ID);
   COMMIT;
  END;

  PROCEDURE INIT_FASE_X_PROCESSO(P_RICHIESTA_ID IN NUMBER) IS
    V_TIPO_RICHIESTA_ID NUMBER;
    CURSOR V_CUR IS 
      SELECT * FROM CONFIGURAZIONE_PROCESSI C
      WHERE C.TIPO_RICHIESTA_ELABORAZIONE_ID = V_TIPO_RICHIESTA_ID
      AND C.STATO ='A';
    V_CUR_ROW V_CUR%ROWTYPE;

    V_PROCESSO VARCHAR2(255);
    V_FASE VARCHAR2(255);
    V_CHECK_INIT NUMBER;
  BEGIN

    SELECT COUNT(1) INTO V_CHECK_INIT FROM  RICHIESTE_FASE_X_PROCESSO
    WHERE RICHIESTE_FASE_X_PROCESSO.RICHIESTA_ID = P_RICHIESTA_ID;

    IF V_CHECK_INIT = 0 THEN
      SELECT R.TIPO_RICHIESTA_ELABORAZIONE_ID
        INTO V_TIPO_RICHIESTA_ID
        FROM RICHIESTE_DA_PROCESSARE R
       WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;

    OPEN V_CUR;
    LOOP
     FETCH V_CUR INTO V_CUR_ROW;
     EXIT WHEN V_CUR%NOTFOUND;
       SET_FASE_X_PROCESSO(P_PROCESSO => V_CUR_ROW.PROCESSO,
                           P_FASE => V_CUR_ROW.FASE,
                           P_STATO => 0,
                           P_RICHIESTA_ID => P_RICHIESTA_ID,
                           P_START => 0,
                           P_FLAG_INIT_CALLER => TRUE,
                           P_CLIENT_INFO_ABIL => V_CUR_ROW.CLIENT_INFO_ABIL,
                           P_CHECK_CHILD => V_CUR_ROW.CHECK_RICHIESTE_X_DIPENDENZA);
    END LOOP;
    CLOSE V_CUR;
    END IF;
  END;

  FUNCTION CHECK_FASE_X_PROCESSO(P_RICHIESTA_ID IN NUMBER,
                                 P_PROCESSO IN VARCHAR2:=NULL,
                                 P_FASE IN VARCHAR2:=NULL) RETURN NUMBER IS
    V_RETURN NUMBER;
    V_CHECK_DIPENDENZA NUMBER;
    V_DIPENDENZA VARCHAR2(255);
    V_TIPO_RICHIESTA_ID NUMBER;
    V_TIPO_DIPENDENZA VARCHAR2(10);
    V_TIME_OUT NUMBER;
    V_CHECK_ABIL NUMBER;
    V_FLAG_INIT VARCHAR2(5);
    v_TOT_ROW number;
    v_CHECK_ROW number;
    V_CHECK_CHILD NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    -- logger.START_LOG_PROCEDURE(I_PROC_NAME =>'CHECK_FASE_X_PROCESSO' ,I_INFO =>P_RICHIESTA_ID ||'-'||P_PROCESSO ||'-'||P_FASE );
    -- logger.END_LOG_PROCEDURE;
    begin
      SELECT R.FLAG_INIT INTO V_FLAG_INIT FROM RICHIESTE_DA_PROCESSARE R
      WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID AND R.FLAG_INIT ='N'
      FOR UPDATE;

      IF V_FLAG_INIT = 'N' THEN
        INIT_FASE_X_PROCESSO(P_RICHIESTA_ID => P_RICHIESTA_ID);
        UPDATE RICHIESTE_DA_PROCESSARE R SET R.FLAG_INIT = 'S' WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;
      END IF;
  
      COMMIT;

    exception when others then
      rollback;
    end;

    SELECT COUNT(1)
    INTO V_CHECK_ABIL
     FROM RICHIESTE_FASE_X_PROCESSO
     WHERE RICHIESTA_ID = P_RICHIESTA_ID;

    IF V_CHECK_ABIL > 0 THEN
      BEGIN
        SELECT COUNT(1)
        INTO V_CHECK_ABIL FROM (
          SELECT CLIENT_INFO_ABIL
          FROM RICHIESTE_FASE_X_PROCESSO
          WHERE RICHIESTA_ID = P_RICHIESTA_ID
          AND ((1=DECODE(P_FASE,NULL, 1, 0) ) OR (0 =DECODE(P_FASE,NULL, 1, 0) AND FASE = P_FASE ))
          AND ((1=DECODE(P_PROCESSO,NULL, 1, 0) ) OR (0 = DECODE(P_PROCESSO,NULL, 1, 0) AND PROCESSO = P_PROCESSO ))
          --AND INSTR(CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
          AND UTL_RICHIESTE.IS_CLIENT_ABIL(SYS_CONTEXT('USERENV','CLIENT_INFO'),CLIENT_INFO_ABIL) = 'S'
        );
        
        IF V_CHECK_ABIL = 0 THEN
          RETURN 0;
        END IF;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN 0;
      END;
    END IF;

    IF P_PROCESSO IS NULL AND P_FASE IS NULL THEN
      V_TIME_OUT := CHECK_TIMEOUT_RICHIESTE(P_RICHIESTA_ID);
      IF V_TIME_OUT = 0 THEN
    
        CHECK_COMPLETED(P_RICHIESTA_ID);
        
        SELECT TOT_ROW, CHECK_ROW
        INTO v_TOT_ROW, v_CHECK_ROW
        FROM (
          SELECT COUNT(1) AS TOT_ROW, SUM(DECODE(STATO,2,1,0)) AS CHECK_ROW
          FROM RICHIESTE_FASE_X_PROCESSO R
          WHERE RICHIESTA_ID = P_RICHIESTA_ID
          --AND INSTR(R.CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
          AND UTL_RICHIESTE.IS_CLIENT_ABIL(SYS_CONTEXT('USERENV','CLIENT_INFO'),R.CLIENT_INFO_ABIL) = 'S'
        );

        if ((v_TOT_ROW = v_CHECK_ROW)) then
          v_return := 0;
        else
          v_return := 1;
        end if;

      ELSE
        RETURN 0;
      END IF;
      RETURN NVL(V_RETURN,1);
    END IF;

    BEGIN
      SELECT R.CHECK_RICHIESTA_X_DIPENDENZA
      INTO V_CHECK_CHILD
      FROM RICHIESTE_FASE_X_PROCESSO R
      WHERE R.RICHIESTA_ID = P_RICHIESTA_ID
      AND R.PROCESSO = P_PROCESSO
      AND R.FASE = P_FASE;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN V_CHECK_CHILD:=0;
    END;

    IF V_CHECK_CHILD = 1 THEN
      V_CHECK_CHILD:=UTL_RICHIESTE.CHECK_COMPLETED_CHILD(P_RICHIESTA_ID => P_RICHIESTA_ID);
      IF V_CHECK_CHILD = 0 THEN
        RETURN 0;
      END IF;
    END IF;

    SELECT TIPO_RICHIESTA_ELABORAZIONE_ID
    INTO V_TIPO_RICHIESTA_ID
    FROM RICHIESTE_DA_PROCESSARE R
    WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;

    IF P_FASE IS NOT NULL THEN

      BEGIN
        SELECT CONFIGURAZIONE_PROCESSI.DIPENDENZA,CONFIGURAZIONE_PROCESSI.TIPO_DIPENDENZA
        INTO V_DIPENDENZA ,V_TIPO_DIPENDENZA
        FROM CONFIGURAZIONE_PROCESSI
        WHERE CONFIGURAZIONE_PROCESSI.PROCESSO=P_PROCESSO
        AND CONFIGURAZIONE_PROCESSI.FASE = P_FASE
        AND CONFIGURAZIONE_PROCESSI.STATO='A'
        AND CONFIGURAZIONE_PROCESSI.TIPO_RICHIESTA_ELABORAZIONE_ID =V_TIPO_RICHIESTA_ID;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN 0;
      END;

      IF V_DIPENDENZA IS NOT NULL THEN
        SELECT DECODE(TOT_ROW, CHECK_ROW, 1, 0)
        INTO V_CHECK_DIPENDENZA
        FROM (
          SELECT COUNT(1) AS TOT_ROW, SUM(DECODE(STATO,2,1,0)) AS CHECK_ROW
          FROM RICHIESTE_FASE_X_PROCESSO
          WHERE RICHIESTA_ID = P_RICHIESTA_ID
          -- AND INSTR(CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
          AND ((1=DECODE(V_TIPO_DIPENDENZA, 'PROCESSO', 0, 1) ) OR (0 = DECODE(V_TIPO_DIPENDENZA,'PROCESSO',0, 1) AND PROCESSO = V_DIPENDENZA ))
          AND ((1=DECODE(V_TIPO_DIPENDENZA, 'FASE', 0, 1) ) OR (0 = DECODE(V_TIPO_DIPENDENZA, 'FASE', 0, 1) AND FASE = V_DIPENDENZA ))
          AND ((1=DECODE(V_TIPO_DIPENDENZA, 'FASE', 0, 1) ) OR (0 = DECODE(V_TIPO_DIPENDENZA, 'FASE', 0, 1) AND PROCESSO = P_PROCESSO ))
        );

        IF V_CHECK_DIPENDENZA = 0 THEN
          RETURN 0;
        END IF;
      END IF;
    ELSE
      BEGIN
        SELECT CONFIGURAZIONE_PROCESSI.DIPENDENZA,CONFIGURAZIONE_PROCESSI.TIPO_DIPENDENZA
        INTO V_DIPENDENZA ,V_TIPO_DIPENDENZA
        FROM CONFIGURAZIONE_PROCESSI
        WHERE CONFIGURAZIONE_PROCESSI.PROCESSO=P_PROCESSO
        AND CONFIGURAZIONE_PROCESSI.TIPO_RICHIESTA_ELABORAZIONE_ID =V_TIPO_RICHIESTA_ID
        AND TIPO_DIPENDENZA ='PROCESSO'
        AND ROWNUM = 1;
      EXCEPTION WHEN NO_DATA_FOUND THEN V_DIPENDENZA :=NULL;
      END;

      IF V_DIPENDENZA IS NOT NULL THEN
        SELECT DECODE(TOT_ROW, CHECK_ROW, 1, 0)
        INTO V_CHECK_DIPENDENZA
        FROM (
          SELECT COUNT(1) AS TOT_ROW, SUM(DECODE(STATO,2,1,0)) AS CHECK_ROW
          FROM RICHIESTE_FASE_X_PROCESSO
          WHERE RICHIESTA_ID = P_RICHIESTA_ID
          --AND INSTR(CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
          AND PROCESSO = V_DIPENDENZA
        );

        IF V_CHECK_DIPENDENZA = 0 THEN
          RETURN 0;
        END IF;
      END IF;
    END IF;

    IF P_FASE IS NULL THEN
      SELECT DECODE(TOT_ROW, CHECK_ROW, 0,1)
      INTO V_RETURN
      FROM (
        SELECT COUNT(1) AS TOT_ROW, SUM(DECODE(STATO,2,1,0)) AS CHECK_ROW
        FROM RICHIESTE_FASE_X_PROCESSO
        WHERE RICHIESTA_ID = P_RICHIESTA_ID
        --AND INSTR(CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
        AND UTL_RICHIESTE.IS_CLIENT_ABIL(SYS_CONTEXT('USERENV','CLIENT_INFO'),CLIENT_INFO_ABIL) = 'S'
        AND PROCESSO = P_PROCESSO
      );
    ELSE
      SELECT DECODE(STATO,0,1,0)  
      INTO V_RETURN
      FROM RICHIESTE_FASE_X_PROCESSO
      WHERE RICHIESTA_ID = P_RICHIESTA_ID
      --AND INSTR(CLIENT_INFO_ABIL,SYS_CONTEXT('USERENV','CLIENT_INFO')) > 0
      AND UTL_RICHIESTE.IS_CLIENT_ABIL(SYS_CONTEXT('USERENV','CLIENT_INFO'),CLIENT_INFO_ABIL) = 'S'
      AND PROCESSO = P_PROCESSO
      AND FASE = P_FASE;
    END IF;
    commit;
    RETURN NVL(V_RETURN,1);

  EXCEPTION WHEN NO_DATA_FOUND THEN
    commit;
    RETURN 1;
  END;

  PROCEDURE SET_FILTRI_XML(P_RICHIESTA_ID IN NUMBER, P_XML IN CLOB) IS
  BEGIN
    UPDATE RICHIESTE_DA_PROCESSARE R SET 
      R.FILTRI_XML = XMLTYPE(P_XML),
      DATA_ELABORAZIONE = SYSDATE
    WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;
  END;

  PROCEDURE SET_END_RICHIESTA(P_RICHIESTA_ID IN NUMBER) IS
    V_TIPO_RICHIESTA_ID NUMBER;
    V_CHECK NUMBER;
  BEGIN

    SELECT R.TIPO_RICHIESTA_ELABORAZIONE_ID
      INTO V_TIPO_RICHIESTA_ID
      FROM RICHIESTE_DA_PROCESSARE R
     WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;

    UPDATE RICHIESTE_DA_PROCESSARE R
       SET R.STATO_RICHIESTA_ID = 2, R.END_DATE = SYSDATE
     WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;

  END;

  FUNCTION CHECK_TIMEOUT_RICHIESTE(P_RICHIESTA_ID IN NUMBER) RETURN NUMBER IS
   V_RETURN NUMBER;
   V_TIMEOUT NUMBER;
   V_DATA_ELABORAZIONE DATE;
  PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    SELECT NVL(RP.MAX_LOAD_TIME_IN_MINUTE,240),
           RP.DATA_ELABORAZIONE
     INTO V_TIMEOUT,
          V_DATA_ELABORAZIONE
     FROM RICHIESTE_DA_PROCESSARE RP
     WHERE RP.RICHIESTE_DA_PROCESSARE_ID =  P_RICHIESTA_ID;

    IF V_DATA_ELABORAZIONE + (1/24/60 * V_TIMEOUT) < SYSDATE THEN
        UPDATE RICHIESTE_DA_PROCESSARE R SET R.STATO_RICHIESTA_ID = 3 , R.END_DATE = SYSDATE,R.ERRORI ='TIMEOUT RICHIESTA SUPERATO'
          WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;
      V_RETURN:=3;
    ELSE
      V_RETURN:=0;
    END IF;

    COMMIT;
    RETURN V_RETURN;
  END;

  PROCEDURE CHECK_COMPLETED(P_RICHIESTA_ID IN NUMBER)  IS
   V_RETURN NUMBER;
   V_TIMEOUT NUMBER;
   V_START_DATE DATE;
  PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN

    SELECT DECODE(TOT_ROW, CHECK_ROW, 1, 0)
    INTO V_RETURN
    FROM (
      SELECT COUNT(1) AS TOT_ROW, SUM(DECODE(STATO,2,1,0)) AS CHECK_ROW
      FROM RICHIESTE_FASE_X_PROCESSO
      WHERE RICHIESTA_ID = P_RICHIESTA_ID
    );

    IF V_RETURN = 1 THEN
      UPDATE RICHIESTE_DA_PROCESSARE R SET R.STATO_RICHIESTA_ID = 2 , R.END_DATE = SYSDATE
      WHERE R.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID;
    END IF;

    COMMIT;
  END;

  FUNCTION CHECK_STEP_X_STATO(P_STATO_OLD IN NUMBER, P_STATO_NEW IN NUMBER) RETURN NUMBER IS
    V_RET NUMBER;
  BEGIN
    IF P_STATO_OLD = 0 AND P_STATO_NEW = 1 THEN
       V_RET:=1;
    ELSIF P_STATO_OLD = 0 AND P_STATO_NEW = 2 THEN
       V_RET:=0;
    ELSE
       V_RET:=1;
    END IF;
    RETURN V_RET;
  END;

  FUNCTION RICERCA_FASE(P_LIST IN ARRAY_ART) RETURN SYS_REFCURSOR IS
    V_RETURN SYS_REFCURSOR;
    pragma autonomous_transaction;
    V_LIST_TIPI VARCHAR2(4000);
    V_CLIENT_INFO VARCHAR2(1000);
    V_P_LIST_STR VARCHAR2(1000);
    V_ELEMENT VARCHAR2(1000);
    V_CHECK_ABIL NUMBER;
  BEGIN

    V_P_LIST_STR:=' [';
     FOR counter IN P_LIST.FIRST .. P_LIST.LAST
      LOOP
        SELECT P_LIST( counter ) INTO V_ELEMENT FROM DUAL;
        V_P_LIST_STR := V_P_LIST_STR || V_ELEMENT || ',';
      END loop;
    V_P_LIST_STR:=V_P_LIST_STR||']';

    DITECH_COMMON.LOGGER.RESET_DEFAUL_VALUE ;
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => 'RICERCA_FASE', I_INFO => 'START '||trim(V_P_LIST_STR));

    -- controllo se sono abilitato a proseguire

    SELECT COUNT(1)
    INTO V_CHECK_ABIL 
    FROM (
      SELECT cp.CLIENT_INFO_ABIL
      FROM CONFIGURAZIONE_PROCESSI cp 
      JOIN TIPO_RICHIESTA_ELABORAZIONE tp ON tp.TIPO_RICHIESTA_ELABORAZIONE_ID = cp.TIPO_RICHIESTA_ELABORAZIONE_ID
      where tp.CODICE in (SELECT T.COLUMN_VALUE FROM TABLE(P_LIST) T)
      AND UTL_RICHIESTE.IS_CLIENT_ABIL(SYS_CONTEXT('USERENV','CLIENT_INFO'),cp.CLIENT_INFO_ABIL) = 'S'
    );

    if(V_CHECK_ABIL = 0) then
      -- se non sono abilitato, esco 
      OPEN V_RETURN FOR
        select 
          richieste_da_processare_id,
          fase,
          processo,
          tipo_fase,
          fase_apllicativa,
          tipo_richiesta_elaborazione_id,
          tr_codice
        from  richieste_fase_x_processo_new rn
        where 1 = 2;
      return V_RETURN;
    end if;

    SERIALIZZA_SESSIONE('PARALLELIZZATORE') ;

   -- LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => 'RICERCA_FASE',I_INFO => 'V_LIST_TIPI:'||V_LIST_TIPI||',V_CLIENT_INFO'||V_CLIENT_INFO);

    -- cancellazione richieste nuove da eseguire (per le tipologie richieste)

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'cancellazione richieste nuove da eseguire (per le tipologie richieste)'||V_P_LIST_STR
        , I_SRC_TABLE => 'richieste_fase_x_processo_new'
        , I_TAB_NAME => 'richieste_fase_x_processo_new'
      );
      delete from richieste_fase_x_processo_new rn where rn.TR_CODICE IN (SELECT T.COLUMN_VALUE FROM TABLE(P_LIST) T);
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;

    -- cancellazione richieste inviate finite (correttamente o con errore)

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'cancellazione richieste inviate finite (correttamente o con errore)'||V_P_LIST_STR
        , I_SRC_TABLE => 'rICHIESTE_FASE_X_PROCESSO_send'
        , I_TAB_NAME => 'rICHIESTE_FASE_X_PROCESSO_send'
      );
      delete from rICHIESTE_FASE_X_PROCESSO_send rs where rs.richieste_da_processare_id in (
      select distinct r.richieste_da_processare_id
        from rICHIESTE_FASE_X_PROCESSO_send s,
             richieste_da_processare r
       where s.richieste_da_processare_id = r.richieste_da_processare_id
         and r.stato_richiesta_id in (2,3));
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;

    -- cancellazione richieste inviate non in stato "da eseguire"

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'cancellazione richieste inviate non in stato da eseguire'||V_P_LIST_STR
        , I_SRC_TABLE => 'rICHIESTE_FASE_X_PROCESSO_send'
        , I_TAB_NAME => 'rICHIESTE_FASE_X_PROCESSO_send'
      );
      delete from rICHIESTE_FASE_X_PROCESSO_send rs where (rs.richieste_da_processare_id, rs.fase, rs.processo) in (
      select r.richiesta_id,r.fase,r.processo
        from rICHIESTE_FASE_X_PROCESSO_send s,
             richieste_fase_x_processo r
       where s.richieste_da_processare_id = r.richiesta_id
         and s.fase = r.fase
         and s.processo = r.processo
         and r.stato > 1 
      );
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;

    -- cancellazione richieste inviate in timout

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'cancellazione richieste inviate in timout'||V_P_LIST_STR
        , I_SRC_TABLE => 'rICHIESTE_FASE_X_PROCESSO_send'
        , I_TAB_NAME => 'rICHIESTE_FASE_X_PROCESSO_send'
      );
      delete from richieste_fase_x_processo_send s
      where s.data_send + ((1/24/60) * 5)  <=  sysdate;
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;

    -- calcolo ed inserisco le nuove richieste da eseguire

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'calcolo ed inserisco le nuove richieste da eseguire'||V_P_LIST_STR
        , I_SRC_TABLE => 'richieste_fase_x_processo_new'
        , I_TAB_NAME => 'richieste_fase_x_processo_new'
      );
      insert into richieste_fase_x_processo_new(
        richieste_da_processare_id,
        fase,
        processo,
        tipo_fase,
        fase_apllicativa,
        tipo_richiesta_elaborazione_id,
        tr_codice
      )
      select 
        richieste_da_processare_id,
        fase,
        processo,
        tipo_fase,
        fase_apllicativa,
        tipo_richiesta_elaborazione_id,
        tr_codice 
      from (
        select 
          t.richieste_da_processare_id,
          t.fase,
          t.processo,
          t.tipo_fase,
          t.fase_apllicativa,
          t.tipo_richiesta_elaborazione_id,
          tr_codice,
          row_number() over(partition by t.fase, t.processo order by richieste_da_processare_id) as num,
          cf.max_num_istanze
        from (
          select 
            r.richieste_da_processare_id,
            fase,
            processo,
            tipo_fase,
            fase_apllicativa,
            tr.tipo_richiesta_elaborazione_id,
            tr.codice as tr_codice
          from 
            CONFIGURAZIONE_PROCESSI cp,
            tipo_richiesta_elaborazione tr,
            richieste_da_processare r
          where cp.tipo_richiesta_elaborazione_id = tr.tipo_richiesta_elaborazione_id
          and cp.stato = 'A'
          and UTL_RICHIESTE.CHECK_FASE_X_PROCESSO(P_RICHIESTA_ID => r.richieste_da_processare_id,
                                                  P_PROCESSO     => cp.processo,
                                                  P_FASE         => cp.fase) = 1
          and r.tipo_richiesta_elaborazione_id = CP.TIPO_RICHIESTA_ELABORAZIONE_ID
          and UTL_RICHIESTE.CHECK_FASE_X_PROCESSO(P_RICHIESTA_ID => r.richieste_da_processare_id,
                                                  P_PROCESSO     => null,
                                                  P_FASE         => null) = 1
          and r.stato_richiesta_id < 2
          --and tr.codice IN ('GTSOF', 'ACQOF', 'ACDIL', 'ACQDL');
          -- ggamberini 20151022 fix for ClassCast Exception in DTOs
          AND TR.CODICE IN (SELECT T.COLUMN_VALUE FROM TABLE(P_LIST) T)
          minus
          select 
            richieste_da_processare_id,
            fase,
            processo,
            tipo_fase,
            fase_apllicativa,
            tipo_richiesta_elaborazione_id,
            tr_codice
          from richieste_fase_x_processo_send 
        ) t, configurazione_processi cp, CLASSIFICAZIONE_FASE cf
        where cp.tipo_richiesta_elaborazione_id = t.tipo_richiesta_elaborazione_id
        and cp.processo = t.processo
        and cp.fase = t.fase
        AND CP.CLASSIFICAZIONE_FASE_ID = CF.CLASSIFICAZIONE_FASE_ID
      ) 
      where num <= NVL(max_num_istanze, 1000) ;
      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;

    BEGIN
      DITECH_COMMON.LOGGER.START_LOG_STMT(
        I_INFO => 'copio le nuove richieste da eseguire nelle richieste inviate'||V_P_LIST_STR
        , I_SRC_TABLE => 'richieste_fase_x_processo_send'
        , I_TAB_NAME => 'richieste_fase_x_processo_send'
      );
      -- copio le nuove richieste da eseguire nelle richieste inviate
      insert into richieste_fase_x_processo_send(
        richieste_da_processare_id,
        fase,
        processo,
        tipo_fase,
        fase_apllicativa,
        tipo_richiesta_elaborazione_id,
        tr_codice,
        data_send,client_info
      )
      select 
        richieste_da_processare_id,
        fase,
        processo,
        tipo_fase,
        fase_apllicativa,
        tipo_richiesta_elaborazione_id,
        tr_codice,
        sysdate,
        SYS_CONTEXT('USERENV','CLIENT_INFO')
      from richieste_fase_x_processo_new rn
      where rn.TR_CODICE IN (SELECT T.COLUMN_VALUE FROM TABLE(P_LIST) T);

      DITECH_COMMON.LOGGER.END_LOG_STMT();
    EXCEPTION WHEN OTHERS THEN
      DITECH_COMMON.LOGGER.END_LOG_STMT(I_ON_EXCEPTION_STOP => TRUE);
    END;
    commit;

    -- restituisco le richieste nuove da eseguire
    OPEN V_RETURN FOR
      select 
        richieste_da_processare_id,
        fase,
        processo,
        tipo_fase,
        fase_apllicativa,
        tipo_richiesta_elaborazione_id,
        tr_codice
      from  richieste_fase_x_processo_new rn
      where rn.TR_CODICE IN (SELECT T.COLUMN_VALUE FROM TABLE(P_LIST) T);

    -- LOGGER.END_LOG_PROCEDURE();

  	DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_INFO => 'END '||trim(V_P_LIST_STR));
    RETURN V_RETURN;
  EXCEPTION
    WHEN OTHERS THEN
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;

  PROCEDURE SERIALIZZA_SESSIONE(P_CONTESTO_SESSIONE IN VARCHAR2) IS
  BEGIN
    update serializza_sessioni set 
      serializza_sessioni.ultima_richiesta_sessione = sysdate, 
      serializza_sessioni.id_sessione = serializza_sessioni.id_sessione + 1
    where contesto_SESSIONE = P_CONTESTO_SESSIONE;

    IF SQL%ROWCOUNT = 0 THEN
      update serializza_sessioni set 
        serializza_sessioni.ultima_richiesta_sessione = sysdate, 
        serializza_sessioni.id_sessione = serializza_sessioni.id_sessione + 1
      where contesto_SESSIONE = 'GLOBALE';

      INSERT INTO serializza_sessioni(id_sessione,ultima_richiesta_sessione,contesto_sessione)
      VALUES (1,SYSDATE,P_CONTESTO_SESSIONE);

    END IF;
  END;

  FUNCTION CHECK_COMPLETED_CHILD(P_RICHIESTA_ID IN NUMBER) RETURN NUMBER IS
    V_RET NUMBER;
  BEGIN

    SELECT DECODE(SUM(decode(r.stato_richiesta_id,2,1,3,1,0)), COUNT(*), 1, 0)
      INTO V_RET
      FROM RICHIESTE_DA_PROCESSARE R
     WHERE R.PADRE_RICHIESTA_DA_PROCES_ID = P_RICHIESTA_ID;

    RETURN V_RET;

  END;

  FUNCTION IS_CLIENT_ABIL(P_CLIENT_INFO IN VARCHAR2, P_CLIENT_INFO_ABIL IN VARCHAR2) RETURN VARCHAR2 IS
    V_RET VARCHAR2(1 CHAR) := 'N';
  BEGIN

    select decode(count(1), 0, 'N', 'S') 
    into V_RET
    from dual
    where P_CLIENT_INFO in (select * from table(ditech_integrazione.split(P_CLIENT_INFO_ABIL)));

    return V_RET;       

  END;

--<?xml version = '1.0' encoding = 'UTF-8' standalone = 'yes'?>
--<dcc-pub-sub>
--	<destinationCode>MAGI</destinationCode>
--	<mittenteCode>CONNETTORE_CARICHI</mittenteCode>
--	<internalCode>IMCAR</internalCode>
--	<code>IMPORTCARICHI</code>
--	<loadId>5</loadId>
--</dcc-pub-sub>
  PROCEDURE RECEIVE_NOTIFY(P_DESTINATARIO IN VARCHAR2, P_PROCESSO IN VARCHAR2, P_MITTENTE IN VARCHAR2, P_LOAD_ID IN NUMBER, P_CODICE_ESTERNO IN VARCHAR2) IS
   filtro_xml XMLTYPE;
   richiestaID NUMBER;
   BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => 'RECEIVE_NOTIFY');
    BEGIN
      SELECT XMLELEMENT("message-receiver",
        XMLELEMENT("destinationCode", P_DESTINATARIO),
        XMLELEMENT("mittenteCode", P_MITTENTE),
        XMLELEMENT("internalCode", P_CODICE_ESTERNO),
        XMLELEMENT("code", P_PROCESSO),
        XMLELEMENT("loadId", P_LOAD_ID))
      INTO   filtro_xml
      FROM   dual;

      richiestaID := INSERT_RICHIESTA(P_CODICE => 'SSUB', P_LOAD_ID => P_LOAD_ID, P_MODULE => CONCAT('DI.UTL_PUBBLICAZIONI.',P_MITTENTE));

      SET_FILTRI_XML(richiestaID, filtro_xml.getClobVal());
    END;
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE;
    EXCEPTION WHEN OTHERS THEN
        DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;

  PROCEDURE CALLBACK_NOTIFY(P_RICHIESTA_ID IN NUMBER, P_DESTINATARIO IN VARCHAR2, P_PROCESSO IN VARCHAR2, P_MITTENTE IN VARCHAR2, P_LOAD_ID IN NUMBER) IS
    tipoRichiesta VARCHAR2(5);
    callerStato NUMBER;
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => 'CALLBACK_NOTIFY');

    SELECT tre.CODICE
    INTO tipoRichiesta
    FROM RICHIESTE_DA_PROCESSARE rdp, TIPO_RICHIESTA_ELABORAZIONE tre
    WHERE rdp.RICHIESTE_DA_PROCESSARE_ID = P_RICHIESTA_ID
    AND rdp.TIPO_RICHIESTA_ELABORAZIONE_ID = tre.TIPO_RICHIESTA_ELABORAZIONE_ID;

    IF tipoRichiesta='SSUB'
      THEN
      callerStato := DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI.GC_STATO_IN_ELABORAZIONE;

    ELSIF tipoRichiesta='ESUB'
      THEN
        -- solo il primo
        SELECT r.stato_richiesta_id
        INTO callerStato
        FROM RICHIESTE_DA_PROCESSARE R
        WHERE R.PADRE_RICHIESTA_DA_PROCES_ID = P_RICHIESTA_ID;

       IF callerStato=2
        THEN
          callerStato := DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI.GC_STATO_ELABORATO_OK;
        ELSE
          callerStato := DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI.GC_STATO_ELABORATO_KO;
      END IF;
    END IF;

    DITECH_INTEGRAZIONE.UTL_PUBBLICAZIONI.SET_LOAD_ID_STATUS(P_DESTINATARIO => P_DESTINATARIO, P_PROCESSO => P_PROCESSO, P_MITTENTE => P_MITTENTE, P_LOAD_ID => P_LOAD_ID, P_STATO => callerStato);

    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE;
    EXCEPTION WHEN OTHERS THEN
        DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;

  FUNCTION INSERT_RICHIESTA(P_CODICE IN VARCHAR2, P_LOAD_ID IN NUMBER, P_MODULE IN VARCHAR2 DEFAULT 'DIRECT-API') RETURN NUMBER IS
    richiestaID NUMBER;
    tipologiaID NUMBER;
  BEGIN
    DITECH_COMMON.LOGGER.START_LOG_PROCEDURE(I_PROC_NAME => 'INSERT_RICHIESTA');

    BEGIN
      select tre.TIPO_RICHIESTA_ELABORAZIONE_ID into tipologiaID
      from TIPO_RICHIESTA_ELABORAZIONE tre
      where tre.CODICE = P_CODICE;

      richiestaID := SQ_RICHIESTE_DA_PROCESSARE.NEXTVAL;

      INSERT INTO RICHIESTE_DA_PROCESSARE (RICHIESTE_DA_PROCESSARE_ID,LOAD_ID,SESSION_ID,FILTRI_XML,INS_UTENTE,INS_DATA,INS_MODULO,UPD_UTENTE,UPD_DATA,UPD_MODULO,STATO_RICHIESTA_ID,DATA_ELABORAZIONE,TIPO_RICHIESTA_ELABORAZIONE_ID,PADRE_RICHIESTA_DA_PROCES_ID,START_DATE,END_DATE,MAX_LOAD_TIME_IN_MINUTE,FLAG_INIT,FILTRI_XSD)
      VALUES (richiestaID,P_LOAD_ID,null,null,'UTL_RICHIESTE.INSERT_RICHIESTA',SYSDATE,P_MODULE,'UTL_RICHIESTE.INSERT_RICHIESTA',SYSDATE,P_MODULE,0,null,tipologiaID,null,null,null,null,null,null);

      return richiestaID;
    END;
    DITECH_COMMON.LOGGER.END_LOG_PROCEDURE;
    EXCEPTION WHEN OTHERS THEN
        DITECH_COMMON.LOGGER.END_LOG_PROCEDURE(I_ON_EXCEPTION_STOP => TRUE);
  END;

END UTL_RICHIESTE;
/