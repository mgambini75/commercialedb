--liquibase formatted sql
--changeset ilocastro:Rapporti_amministrativi logicalFilePath:v-1.0/0003-Rapporti_amministrativi.sql runOnChange:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql

-- Connessione a COMM --------------------------------------------------------------------------------------------------------------
CONNECT &DB_COMM

--
-- ER/Studio 8.0 SQL Code Generation
-- Company :      DI.TECH
-- Project :      interlocutore.DM1
-- Author :       gbarbagallo/ilocastro
--
-- Date Created : Tuesday, March 07, 2017 12:05:35
-- Target DBMS : Oracle 11g
--

CREATE SEQUENCE SQ_RAPP_AMM
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: RAPP_AMM 
--

CREATE TABLE RAPP_AMM(
    RAPP_AMM_ID         NUMBER(19, 0)    NOT NULL,
    CODICE              VARCHAR2(50)     NOT NULL,
    DESCRIZIONE         VARCHAR2(256)    NOT NULL,
    TIPO_RAPP_AMM_ID    NUMBER(19, 0)    NOT NULL,
    INTERLOCUTORE_ID    NUMBER(19, 0)    NOT NULL,
    INS_DATA            DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE          VARCHAR2(32)     NOT NULL,
    INS_MODULO          VARCHAR2(128)    NOT NULL,
    UPD_DATA            DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE          VARCHAR2(32)     NOT NULL,
    UPD_MODULO          VARCHAR2(128)    NOT NULL,
    VERSION             TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_RAPP_AMM PRIMARY KEY (RAPP_AMM_ID),
    CONSTRAINT AK_RAPP_AMM  UNIQUE (CODICE)
)
;



CREATE SEQUENCE SQ_RAPP_AMM_ATTR
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: RAPP_AMM_ATTR 
--

CREATE TABLE RAPP_AMM_ATTR(
    RAPP_AMM_ATTR_ID               NUMBER(19, 0)    NOT NULL,
    RAPP_AMM_ID                    NUMBER(19, 0)    NOT NULL,
    INTERVALLO_DA                  DATE             NOT NULL,
    INTERVALLO_A                   DATE,
    BLOCCO_FIDO_ID                 NUMBER(19, 0),
    NUMERO_GIORNI_DATA_DEC         NUMBER(38, 0),
    TIPO_PAGAMENTO_ID              NUMBER(19, 0),
    TIPO_CALCOLO_DECORRENZA_ID     NUMBER(19, 0),
    MODALITA_PAGAMENTO_ID          NUMBER(19, 0),
    PERIODICITA_FATTURAZIONE_ID    NUMBER(19, 0),
    DISPO_FINANZIARIA_ID           NUMBER(19, 0),
    COORDINATE_BANCARIE_ID         NUMBER(19, 0),
    GESTIONE_ABBUONI               CHAR(1)          DEFAULT 'N' NOT NULL,
    DEBITI_CREDITI                 CHAR(1)          DEFAULT 'N' NOT NULL,
    MASTRO_CONTABILE_ID            NUMBER(19, 0),
    CLASSE_SCONTO_T_ID             NUMBER(19, 0),
    PERSONA_FISCALE_ID_FATTURA     NUMBER(19, 0)    NOT NULL,
    SEDE_AMMINISTRATIVA_ID_FATTURA NUMBER(19, 0)    NOT NULL,    
    INS_DATA                       DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE                     VARCHAR2(32)     NOT NULL,
    INS_MODULO                     VARCHAR2(128)    NOT NULL,
    UPD_DATA                       DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE                     VARCHAR2(32)     NOT NULL,
    UPD_MODULO                     VARCHAR2(128)    NOT NULL,
    VERSION                        TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT CK_RAPPAMM_GESTIONE_ABBUONI CHECK (GESTIONE_ABBUONI in ('S', 'N')),
    CONSTRAINT CK_RAPPAMM_DEBITI_CREDITI CHECK (DEBITI_CREDITI in ('S', 'N')),
    CONSTRAINT PK_RAPP_AMM_ATTR PRIMARY KEY (RAPP_AMM_ATTR_ID),
    CONSTRAINT AK_RAPP_AMM_ATTR UNIQUE (RAPP_AMM_ID, INTERVALLO_DA)
)
;



CREATE SEQUENCE SQ_RAPP_AMM_CFG
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: RAPP_AMM_CFG 
--

CREATE TABLE RAPP_AMM_CFG(
    RAPP_AMM_CFG_ID              NUMBER(19, 0)    NOT NULL,
    TIPO_RAPP_AMM_CFG_ID         NUMBER(19, 0)    NOT NULL,
    AZIENDA_ID                   NUMBER(19, 0)    NOT NULL,
    TIPO_GESTIONE_ARTICOLO_ID    NUMBER(19, 0),
    TIPO_CONSEGNA_ID             NUMBER(10, 0),
    MERCEOLOGIA_ID               NUMBER(19, 0),
    PRIORITA                     NUMBER(5, 0)     NOT NULL,
    RAPP_AMM_DFT_ID              NUMBER(19, 0)    NOT NULL,
    INS_DATA                     DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE                   VARCHAR2(32)     NOT NULL,
    INS_MODULO                   VARCHAR2(128)    NOT NULL,
    UPD_DATA                     DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE                   VARCHAR2(32)     NOT NULL,
    UPD_MODULO                   VARCHAR2(128)    NOT NULL,
    VERSION                      TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_RAPP_AMM_CFG PRIMARY KEY (RAPP_AMM_CFG_ID),
    CONSTRAINT AK_RAPP_AMM_CFG  UNIQUE (TIPO_RAPP_AMM_CFG_ID, AZIENDA_ID, TIPO_GESTIONE_ARTICOLO_ID, TIPO_CONSEGNA_ID, MERCEOLOGIA_ID)
)
;



CREATE SEQUENCE SQ_RAPP_AMM_DFT
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: RAPP_AMM_DFT 
--

CREATE TABLE RAPP_AMM_DFT(
    RAPP_AMM_DFT_ID          NUMBER(19, 0)    NOT NULL,
    CODICE                   VARCHAR2(50)     NOT NULL,
    DESCRIZIONE              VARCHAR2(256)    NOT NULL,
    TIPO_PAGAMENTO_ID        NUMBER(19, 0),
    MODALITA_PAGAMENTO_ID    NUMBER(19, 0),
    INS_DATA                 DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE               VARCHAR2(32)     NOT NULL,
    INS_MODULO               VARCHAR2(128)    NOT NULL,
    UPD_DATA                 DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE               VARCHAR2(32)     NOT NULL,
    UPD_MODULO               VARCHAR2(128)    NOT NULL,
    VERSION                  TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_RAPP_AMM_DFT PRIMARY KEY (RAPP_AMM_DFT_ID),
    CONSTRAINT AK_RAPP_AMM_DFT UNIQUE (CODICE)
)
;



CREATE SEQUENCE SQ_RAPP_AMM_X_CFG
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: RAPP_AMM_X_CFG 
--

CREATE TABLE RAPP_AMM_X_CFG(
    RAPP_AMM_X_CFG_ID    NUMBER(19, 0)    NOT NULL,
    RAPP_AMM_CFG_ID      NUMBER(19, 0)    NOT NULL,
    RAPP_AMM_ID          NUMBER(19, 0)    NOT NULL,
    INTERVALLO_DA        DATE             NOT NULL,
    INTERVALLO_A         DATE,
    INS_DATA             DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE           VARCHAR2(32)     NOT NULL,
    INS_MODULO           VARCHAR2(128)    NOT NULL,
    UPD_DATA             DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE           VARCHAR2(32)     NOT NULL,
    UPD_MODULO           VARCHAR2(128)    NOT NULL,
    VERSION              TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_RAPP_AMM_X_CFG PRIMARY KEY (RAPP_AMM_X_CFG_ID)
)
;



CREATE SEQUENCE SQ_TIPO_RAPP_AMM
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: TIPO_RAPP_AMM 
--

CREATE TABLE TIPO_RAPP_AMM(
    TIPO_RAPP_AMM_ID    NUMBER(19, 0)    NOT NULL,
    CODICE              VARCHAR2(50)     NOT NULL,
    DESCRIZIONE         VARCHAR2(256)    NOT NULL,
    INS_DATA            DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE          VARCHAR2(32)     NOT NULL,
    INS_MODULO          VARCHAR2(128)    NOT NULL,
    UPD_DATA            DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE          VARCHAR2(32)     NOT NULL,
    UPD_MODULO          VARCHAR2(128)    NOT NULL,
    VERSION             TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_TIPO_RAPP_AMM PRIMARY KEY (TIPO_RAPP_AMM_ID),
    CONSTRAINT AK_TIPO_RAPP_AMM  UNIQUE (CODICE)
)
;



CREATE SEQUENCE SQ_TIPO_RAPP_AMM_CFG
    INCREMENT BY 1
    START WITH 1000
;
-- 
-- TABLE: TIPO_RAPP_AMM_CFG 
--

CREATE TABLE TIPO_RAPP_AMM_CFG(
    TIPO_RAPP_AMM_CFG_ID    NUMBER(19, 0)    NOT NULL,
    CODICE                  VARCHAR2(50)     NOT NULL,
    DESCRIZIONE             VARCHAR2(256)    NOT NULL,
    INS_DATA                DATE             DEFAULT SYSDATE NOT NULL,
    INS_UTENTE              VARCHAR2(32)     NOT NULL,
    INS_MODULO              VARCHAR2(128)    NOT NULL,
    UPD_DATA                DATE             DEFAULT SYSDATE NOT NULL,
    UPD_UTENTE              VARCHAR2(32)     NOT NULL,
    UPD_MODULO              VARCHAR2(128)    NOT NULL,
    VERSION                 TIMESTAMP(6)     DEFAULT SYSTIMESTAMP NOT NULL,
    CONSTRAINT PK_TIPO_RAPP_AMM_CFG PRIMARY KEY (TIPO_RAPP_AMM_CFG_ID),
    CONSTRAINT AK_TIPO_RAPP_AMM_CFG  UNIQUE (CODICE)
)
;



-- 
-- INDEX: IX_RAPPAMM_INTERLOCUTORE 
--

CREATE INDEX IX_RAPPAMM_INTERLOCUTORE ON RAPP_AMM(INTERLOCUTORE_ID)
;
-- 
-- INDEX: IX_RAPPAMMATTR_RAPP_AMM_ID 
--

CREATE INDEX IX_RAPPAMMATTR_RAPP_AMM_ID ON RAPP_AMM_ATTR(RAPP_AMM_ID)
;
-- 
-- INDEX: IX_RAXC_RAPP_AMM_ID 
--

CREATE INDEX IX_RAXC_RAPP_AMM_ID ON RAPP_AMM_X_CFG(RAPP_AMM_ID)
;
-- 
-- INDEX: IX_RAXC_RAPP_AMM_CFG_ID 
--

CREATE INDEX IX_RAXC_RAPP_AMM_CFG_ID ON RAPP_AMM_X_CFG(RAPP_AMM_CFG_ID)
;
-- 
-- TABLE: RAPP_AMM 
--

ALTER TABLE RAPP_AMM ADD CONSTRAINT FK_RAPPAMM2INTERLOCUTORE 
    FOREIGN KEY (INTERLOCUTORE_ID)
    REFERENCES NCL_INTERLOCUTORE(INTERLOCUTORE_ID)
;

ALTER TABLE RAPP_AMM ADD CONSTRAINT FK_RAPPAMM2TIPORAPPAMM 
    FOREIGN KEY (TIPO_RAPP_AMM_ID)
    REFERENCES TIPO_RAPP_AMM(TIPO_RAPP_AMM_ID)
;


-- 
-- TABLE: RAPP_AMM_ATTR 
--

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2BLOCCO_FIDO 
    FOREIGN KEY (BLOCCO_FIDO_ID)
    REFERENCES BLOCCO_FIDO(BLOCCO_FIDO_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2CLASSE_SCONTO_T 
    FOREIGN KEY (CLASSE_SCONTO_T_ID)
    REFERENCES CLASSE_SCONTO_T(CLASSE_SCONTO_T_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2COORDINATE_BANCARIE 
    FOREIGN KEY (COORDINATE_BANCARIE_ID)
    REFERENCES COORDINATE_BANCARIE(COORDINATE_BANCARIE_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2DISPO_FINANZIARIA 
    FOREIGN KEY (DISPO_FINANZIARIA_ID)
    REFERENCES DISPO_FINANZIARIA(DISPO_FINANZIARIA_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2MASTRO_CONTABILE 
    FOREIGN KEY (MASTRO_CONTABILE_ID)
    REFERENCES MASTRO_CONTABILE(MASTRO_CONTABILE_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2MODALITA_PAGAMENTO 
    FOREIGN KEY (MODALITA_PAGAMENTO_ID)
    REFERENCES MODALITA_PAGAMENTO(MODALITA_PAGAMENTO_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2PERIODICITA_FATTURAZION 
    FOREIGN KEY (PERIODICITA_FATTURAZIONE_ID)
    REFERENCES PERIODICITA_FATTURAZIONE(PERIODICITA_FATTURAZIONE_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2RAPP_AMM 
    FOREIGN KEY (RAPP_AMM_ID)
    REFERENCES RAPP_AMM(RAPP_AMM_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2TIPO_CALCOLO_DECORRENZA 
    FOREIGN KEY (TIPO_CALCOLO_DECORRENZA_ID)
    REFERENCES TIPO_CALCOLO_DECORRENZA(TIPO_CALCOLO_DECORRENZA_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2TIPO_PAGAMENTO 
    FOREIGN KEY (TIPO_PAGAMENTO_ID)
    REFERENCES TIPO_PAGAMENTO(TIPO_PAGAMENTO_ID)
;

ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2PERSONA_FISCALE 
    FOREIGN KEY (PERSONA_FISCALE_ID_FATTURA)
    REFERENCES NCL_PERSONA_FISCALE(PERSONA_FISCALE_ID)
;
    
ALTER TABLE RAPP_AMM_ATTR ADD CONSTRAINT FK_RAA2SEDE_AMMINISTRATIVA 
    FOREIGN KEY (SEDE_AMMINISTRATIVA_ID_FATTURA)
    REFERENCES NCL_SEDE_AMMINISTRATIVA(SEDE_AMMINISTRATIVA_ID)
;

-- 
-- TABLE: RAPP_AMM_CFG 
--

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2AZIENDA 
    FOREIGN KEY (AZIENDA_ID)
    REFERENCES NCL_AZIENDA(AZIENDA_ID)
;

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2MERCEOLOGIA 
    FOREIGN KEY (MERCEOLOGIA_ID)
    REFERENCES NCL_MERCEOLOGIA(MERCEOLOGIA_ID)
;

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2RAPPAMMDFT 
    FOREIGN KEY (RAPP_AMM_DFT_ID)
    REFERENCES RAPP_AMM_DFT(RAPP_AMM_DFT_ID)
;

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2TIPO_CONSEGNA 
    FOREIGN KEY (TIPO_CONSEGNA_ID)
    REFERENCES NCL_TIPO_CONSEGNA(TIPO_CONSEGNA_ID)
;

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2TIPO_GESTIONE_ARTICOLO 
    FOREIGN KEY (TIPO_GESTIONE_ARTICOLO_ID)
    REFERENCES NCL_TIPO_GESTIONE_ARTICOLO(TIPO_GESTIONE_ARTICOLO_ID)
;

ALTER TABLE RAPP_AMM_CFG ADD CONSTRAINT FK_RAC2TIPORAPPAMMCFG 
    FOREIGN KEY (TIPO_RAPP_AMM_CFG_ID)
    REFERENCES TIPO_RAPP_AMM_CFG(TIPO_RAPP_AMM_CFG_ID)
;


-- 
-- TABLE: RAPP_AMM_DFT 
--

ALTER TABLE RAPP_AMM_DFT ADD CONSTRAINT FK_RAD2MODALITA_PAGAMENTO 
    FOREIGN KEY (MODALITA_PAGAMENTO_ID)
    REFERENCES MODALITA_PAGAMENTO(MODALITA_PAGAMENTO_ID)
;

ALTER TABLE RAPP_AMM_DFT ADD CONSTRAINT FK_RAD2TIPO_PAGAMENTO 
    FOREIGN KEY (TIPO_PAGAMENTO_ID)
    REFERENCES TIPO_PAGAMENTO(TIPO_PAGAMENTO_ID)
;


-- 
-- TABLE: RAPP_AMM_X_CFG 
--

ALTER TABLE RAPP_AMM_X_CFG ADD CONSTRAINT FK_RAXC2RAPP_AMM 
    FOREIGN KEY (RAPP_AMM_ID)
    REFERENCES RAPP_AMM(RAPP_AMM_ID)
;

ALTER TABLE RAPP_AMM_X_CFG ADD CONSTRAINT FK_RAXC2RAPPAMMCFG 
    FOREIGN KEY (RAPP_AMM_CFG_ID)
    REFERENCES RAPP_AMM_CFG(RAPP_AMM_CFG_ID)
;


------------------------------
-- DATI TABELLE DI LOOKUP
------------------------------

Insert into TIPO_RAPP_AMM (TIPO_RAPP_AMM_ID,CODICE,DESCRIZIONE,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('1','RVND','Rapporto amministrativo vendita','SETUP','SETUP','SETUP','SETUP');
Insert into TIPO_RAPP_AMM (TIPO_RAPP_AMM_ID,CODICE,DESCRIZIONE,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('2','RACQ','Rapporto amministrativo acquisto','SETUP','SETUP','SETUP','SETUP');

Insert into TIPO_RAPP_AMM_CFG (TIPO_RAPP_AMM_CFG_ID,CODICE,DESCRIZIONE,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('1','VNDSTD','Configurazione standard intercettazione rapporto Vendita','SETUP','SETUP','SETUP','SETUP');
Insert into TIPO_RAPP_AMM_CFG (TIPO_RAPP_AMM_CFG_ID,CODICE,DESCRIZIONE,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('2','ACQSTD','Configurazione standard intercettazione rapporto Acquisto','SETUP','SETUP','SETUP','SETUP');

Insert into RAPP_AMM_DFT (RAPP_AMM_DFT_ID,CODICE,DESCRIZIONE,TIPO_PAGAMENTO_ID,MODALITA_PAGAMENTO_ID,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('1','VND30GG','Vendita con pag. a 30 gg.','12','428','SETUP','SETUP','SETUP','SETUP');
Insert into RAPP_AMM_DFT (RAPP_AMM_DFT_ID,CODICE,DESCRIZIONE,TIPO_PAGAMENTO_ID,MODALITA_PAGAMENTO_ID,INS_UTENTE,INS_MODULO,UPD_UTENTE,UPD_MODULO) values ('2','VND60GG','Vendita con pag. a 60 gg.','12','434','SETUP','SETUP','SETUP','SETUP');

COMMIT;




