--liquibase formatted sql
--changeset ccarretti:Porting_CARICHI_da_SAIT_a_MAXIDI logicalFilePath:v-1.0/0007-TaskDitech24918-Porting_carichi.sql runInTransaction:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql

CONNECT &DB_COMM

ALTER TABLE CARICO_DOCACC DROP COLUMN SELEZIONATO;

commit;

-- 762
CONNECT &DB_CORE

@\CORE\pkg\PKG_GET_COLUMNS_CORE.sql
@\CORE\pkg\PKG_GET_COLUMNS_CORE_BODY.sql

commit;

--776
CONNECT &DB_COMM

ALTER TABLE CARICO_DOCACC ADD 
( 
	BARCODE_ARCH_OPT VARCHAR2(128),
	N_PAGINE NUMBER(5,0)
);
COMMENT ON COLUMN CARICO_DOCACC.BARCODE_ARCH_OPT 
     IS 'Barcode archiviazione ottica';
COMMENT ON COLUMN CARICO_DOCACC.N_PAGINE
     IS 'Numero di pagine del documento';
	 
CREATE TABLE BARCODE_TIPO_OTHER 
   (	"BARCODE_TIPO_ID" NUMBER(19,0), 
	"CODICE" VARCHAR2(3 CHAR), 
	"DESCRIZIONE_LUNGA" VARCHAR2(256 CHAR), 
	"NUMERO_CARATTERI" NUMBER(3,0), 
	"STRUTTURA" VARCHAR2(256 CHAR), 
	"ESTERNO" CHAR(1 CHAR) DEFAULT 'N', 
	"AUTOMATICO" CHAR(1 CHAR) DEFAULT 'N', 
	"CODICE_VARIABILE" CHAR(1 CHAR), 
	"DFT_X_LUNGHEZZA" CHAR(1 CHAR) DEFAULT 'N', 
	"ANNULLATO" CHAR(1 CHAR) DEFAULT 'N', 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(32 CHAR), 
	"INS_MODULO" VARCHAR2(128 CHAR), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(32 CHAR), 
	"UPD_MODULO" VARCHAR2(128 CHAR), 
	"VERSION" TIMESTAMP (3) DEFAULT systimestamp, 
	"PLU_BILANCE" CHAR(1 CHAR) DEFAULT 'N', 
	"PRIORITA" NUMBER(3,0), 
	"ACCETTA_MOLTIPLICATORE" CHAR(1 CHAR) DEFAULT 'S', 
	"GESTIONE_GENERALE" CHAR(1 CHAR) DEFAULT 'S', 
	"GESTIONE_DIVISIONE" CHAR(1 CHAR) DEFAULT 'N', 
	"GESTIONE_PDD" CHAR(1 CHAR) DEFAULT 'N', 
  "CATEGORIA" VARCHAR2(15 CHAR) NOT NULL,
	"UNIVOCO" CHAR(1 BYTE) DEFAULT 'S', 
	 CONSTRAINT "CHK_ACC_MOLT" CHECK (ACCETTA_MOLTIPLICATORE IN ('N','S')) ENABLE, 
   CONSTRAINT "CHK_CATEGORIA" CHECK (CATEGORIA IN ('CARICO')) ENABLE, 
	 CONSTRAINT "PK_BARCODE_TIPO" PRIMARY KEY ("BARCODE_TIPO_ID"));
   
  CREATE TABLE BARCODE_RANGE_OTHER 
   (	"BARCODE_RANGE_ID" NUMBER(19,0), 
	"BARCODE_TIPO_ID" NUMBER(19,0), 
	"RANGE_DA" VARCHAR2(128 CHAR), 
	"RANGE_A" VARCHAR2(128 CHAR), 
	"INS_DATA" DATE, 
	"INS_UTENTE" VARCHAR2(32 CHAR), 
	"INS_MODULO" VARCHAR2(128 CHAR), 
	"UPD_DATA" DATE, 
	"UPD_UTENTE" VARCHAR2(32 CHAR), 
	"UPD_MODULO" VARCHAR2(128 CHAR), 
	"VERSION" TIMESTAMP (3) DEFAULT systimestamp, 
  CONSTRAINT PK_BARCODE_RANGE PRIMARY KEY ("BARCODE_RANGE_ID"),
  CONSTRAINT REF_BARCODE_TIPO_OTHER FOREIGN KEY (BARCODE_TIPO_ID)
  REFERENCES BARCODE_TIPO_OTHER (BARCODE_TIPO_ID) ENABLE);
  
CREATE SEQUENCE  NSQ_BARCODE_TIPO_OTHER  MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100 NOORDER  NOCYCLE  NOPARTITION ;
	 
Insert into BARCODE_TIPO_OTHER (BARCODE_TIPO_ID,CODICE,DESCRIZIONE_LUNGA,NUMERO_CARATTERI,STRUTTURA,ESTERNO,AUTOMATICO,CODICE_VARIABILE,DFT_X_LUNGHEZZA,ANNULLATO,INS_DATA,INS_UTENTE,INS_MODULO,UPD_DATA,UPD_UTENTE,UPD_MODULO,VERSION,PLU_BILANCE,PRIORITA,ACCETTA_MOLTIPLICATORE,GESTIONE_GENERALE,GESTIONE_DIVISIONE,GESTIONE_PDD,CATEGORIA,UNIVOCO) 
values ('1','0','EAN 13 OCR','13','NNNNNNNNNNNNC','S','N','N','S','N',TO_DATE('10082009', 'ddmmyyyy'),'LP','SQL',TO_DATE('10082009', 'ddmmyyyy'),'LP','SQL',to_timestamp('10082009 13:35:16,000000000','ddmmyyyy HH24:MI:SSXFF'),'N',null,'S','S','N','N','CARICO','S');

Insert into BARCODE_RANGE_OTHER (BARCODE_RANGE_ID,BARCODE_TIPO_ID,RANGE_DA,RANGE_A,INS_DATA,INS_UTENTE,INS_MODULO,UPD_DATA,UPD_UTENTE,UPD_MODULO,VERSION) 
values ('1','1','2099000000000','2099999999999',TO_DATE('10082009', 'ddmmyyyy'),'LP','SQL',TO_DATE('10082009', 'ddmmyyyy'),'LP','SQL',to_timestamp('10082009 13:35:16,000000000','ddmmyyyy HH24:MI:SSXFF'));
	 
Insert into STATO_CARICO (STATO_CARICO_ID,DESCRIZIONE,CODICE,VISIBILE_FORN,VISIBILE_GEST_FORN,DEFAULT_IMP_GEST_FORN) values ('7','CARICO PRIMO RICEVIMENTO','PR','S','N','N');

EXEC MORE_PARAM.ADDCONFIGPARAM('server','GD','Divulgazioni','Carichi','primo_ricevimento','Valori posibile : Si / No','No',null,null);

--782-785
EXEC MORE_PARAM.ADDCONFIGPARAM('server','GD','Divulgazioni','Carichi','cauzioni_veloci','Valori posibile : Si / No','No',null,null);
CREATE OR REPLACE SYNONYM PKG_BARCODE_INFO FOR &USR_CORE..PKG_BARCODE_INFO;
CREATE OR REPLACE SYNONYM PKG_GET_COLUMNS_CORE FOR &USR_CORE..PKG_GET_COLUMNS_CORE;

--792
DECLARE
P_TIPO_RICHIESTA_ID NUMBER;
BEGIN
SELECT NVL(MAX(TIPO_RICHIESTA_ELABORAZIONE_ID),0)+1 INTO P_TIPO_RICHIESTA_ID FROM TIPO_RICHIESTA_ELABORAZIONE;
Insert into TIPO_RICHIESTA_ELABORAZIONE (TIPO_RICHIESTA_ELABORAZIONE_ID,CODICE,DESCRIZIONE,PROCESSO_ID) values (P_TIPO_RICHIESTA_ID,'SSUB','Richiesta di gestione di inizio SUBSCRIBE dati sul DCC',null);
Insert into CONFIGURAZIONE_PROCESSI (TIPO_RICHIESTA_ELABORAZIONE_ID,PROCESSO,FASE,DIPENDENZA,TIPO_DIPENDENZA,STATO,TIPO_FASE,FASE_APLLICATIVA,DESCRIZIONE,CLIENT_INFO_ABIL,CLASSIFICAZIONE_FASE_ID,CHECK_RICHIESTE_X_DIPENDENZA) values (P_TIPO_RICHIESTA_ID,'START_IMPORT_FROM_DCC','START_SUBSCRIBE',null,null,'A','JAVA','java:app/comm-ejb/MessageReceiverDccPubSubImpl!it.ditech.comm.jms.receiver.system.MessageReceiverDccPubSub','Notifica al meccanismo PUB/SUB del DCC la presa in carico',' ','1',null);
SELECT NVL(MAX(TIPO_RICHIESTA_ELABORAZIONE_ID),0)+1 INTO P_TIPO_RICHIESTA_ID FROM TIPO_RICHIESTA_ELABORAZIONE;
Insert into TIPO_RICHIESTA_ELABORAZIONE (TIPO_RICHIESTA_ELABORAZIONE_ID,CODICE,DESCRIZIONE,PROCESSO_ID) values (P_TIPO_RICHIESTA_ID,'ESUB','Richiesta di gestione di fine SUBSCRIBE dati sul DCC',null);
Insert into CONFIGURAZIONE_PROCESSI (TIPO_RICHIESTA_ELABORAZIONE_ID,PROCESSO,FASE,DIPENDENZA,TIPO_DIPENDENZA,STATO,TIPO_FASE,FASE_APLLICATIVA,DESCRIZIONE,CLIENT_INFO_ABIL,CLASSIFICAZIONE_FASE_ID,CHECK_RICHIESTE_X_DIPENDENZA) values (P_TIPO_RICHIESTA_ID,'END_IMPORT_FROM_DCC','END_SUBSCRIBE',null,null,'A','JAVA','java:app/comm-ejb/MessageReceiverDccPubSubImpl!it.ditech.comm.jms.receiver.system.MessageReceiverDccPubSub','Notifica al meccanismo PUB/SUB del DCC la fine della gestione',' ','1','1');
SELECT NVL(MAX(TIPO_RICHIESTA_ELABORAZIONE_ID),0)+1 INTO P_TIPO_RICHIESTA_ID FROM TIPO_RICHIESTA_ELABORAZIONE;
Insert into TIPO_RICHIESTA_ELABORAZIONE (TIPO_RICHIESTA_ELABORAZIONE_ID,CODICE,DESCRIZIONE,PROCESSO_ID) values (P_TIPO_RICHIESTA_ID,'IMCAR','Richiesta di importazione carichi',null);
Insert into CONFIGURAZIONE_PROCESSI (TIPO_RICHIESTA_ELABORAZIONE_ID,PROCESSO,FASE,DIPENDENZA,TIPO_DIPENDENZA,STATO,TIPO_FASE,FASE_APLLICATIVA,DESCRIZIONE,CLIENT_INFO_ABIL,CLASSIFICAZIONE_FASE_ID,CHECK_RICHIESTE_X_DIPENDENZA) values (P_TIPO_RICHIESTA_ID,'IMPORT_CARICHI','COMPLETA_ONE_ID','APRI_ART_X_PDD','FASE','A','JAVA','java:app/comm-ejb/MessageReceiverImportaCaricoImpl!it.ditech.comm.jms.receiver.carichi.MessageReceiverImportaCarico','Esegue l''importazione dei carichi dal DCC',' ','1',null);
Insert into CONFIGURAZIONE_PROCESSI (TIPO_RICHIESTA_ELABORAZIONE_ID,PROCESSO,FASE,DIPENDENZA,TIPO_DIPENDENZA,STATO,TIPO_FASE,FASE_APLLICATIVA,DESCRIZIONE,CLIENT_INFO_ABIL,CLASSIFICAZIONE_FASE_ID,CHECK_RICHIESTE_X_DIPENDENZA) values (P_TIPO_RICHIESTA_ID,'IMPORT_CARICHI','LOAD_ONE_ID',null,null,'A','JAVA','java:app/comm-ejb/MessageReceiverImportaCaricoImpl!it.ditech.comm.jms.receiver.carichi.MessageReceiverImportaCarico','Esegue l''importazione dei carichi dal DCC',' ','1',null);
Insert into CONFIGURAZIONE_PROCESSI (TIPO_RICHIESTA_ELABORAZIONE_ID,PROCESSO,FASE,DIPENDENZA,TIPO_DIPENDENZA,STATO,TIPO_FASE,FASE_APLLICATIVA,DESCRIZIONE,CLIENT_INFO_ABIL,CLASSIFICAZIONE_FASE_ID,CHECK_RICHIESTE_X_DIPENDENZA) values (P_TIPO_RICHIESTA_ID,'IMPORT_CARICHI','APRI_ART_X_PDD','LOAD_ONE_ID','FASE','A','JAVA','java:app/comm-ejb/MessageReceiverImportaCaricoImpl!it.ditech.comm.jms.receiver.carichi.MessageReceiverImportaCarico','Esegue l''importazione dei carichi dal DCC',' ','1',null);
END;

call MORE_PARAM.ADDCONFIGPARAM('server', 'all', 'JMS_Timers', 'Launcher_management', 'CAR_Timer_Active', 'Si/No', 'Si', null);
call MORE_PARAM.ADDCONFIGPARAM('server', 'all', 'JMS_Timers', 'Launcher_management', 'CAR_Timer_Interval_seconds', '0-59', '30', null);
call MORE_PARAM.ADDCONFIGPARAM('server', 'all', 'JMS_Timers', 'Launcher_management', 'PSD_Timer_Active', 'Si/No', 'Si', null);
call MORE_PARAM.ADDCONFIGPARAM('server', 'all', 'JMS_Timers', 'Launcher_management', 'PSD_Timer_Interval_seconds', '0-59', '30', null);

GRANT EXECUTE ON UTL_RICHIESTE TO DITECH_INTEGRAZIONE;

commit;

--793
CONNECT &DB_DI
Insert into PUBBLICAZIONI (DESCRIZIONE,MITTENTE_CODICE,PROCESSO_CODICE,LOAD_ID,MITTENTE_STATO,CODICE_ESTERNO,TIPO_CODICE_ESTERNO,INS_DATA,UPD_DATA,CORRELATED_ID,AGGREGATE_ID) values ('IMPORTAZIONE CARICHI','000001','IMPORTCARICHI','1','0',null,null,to_date('26-SET-16','DD-MON-RR'),to_date('29-SET-16','DD-MON-RR'),null,null);
Insert into SOTTOSCRIZIONI (DESCRIZIONE,DESTINATARIO_CODICE,PROCESSO_CODICE,MITTENTE_CODICE,MOD_GET_LOAD_ID,MOD_STATO_LOAD_ID,LOAD_ID,DESTINATARIO_STATO,CODICE_ESTERNO,TIPO_CODICE_ESTERNO,INS_DATA,UPD_DATA) values ('INMPORTAZIONE CARICHI ','MAGI','IMPORTCARICHI','000001','2','1','-1','0','IMCAR','MAGI',to_date('26-APR-16','DD-MON-RR'),to_date('26-APR-16','DD-MON-RR'));

commit;

--808
CONNECT &DB_CORE

ALTER TABLE MESSAGGIO
MODIFY  CODICE VARCHAR2(7 BYTE);

COMMENT ON COLUMN MESSAGGIO.CODICE
     IS 'Codice Univoco Alfanumerico - 2 LETTERE INIZIALI';
     
ALTER TABLE MESSAGGIO
ADD BIT_MASK VARCHAR2(10 BYTE);

COMMENT ON COLUMN MESSAGGIO.CODICE
     IS 'Maschera di bit per codifica a bit di errori multipli';
     
DELETE 
FROM MESSAGGIO
WHERE CODICE IN (1,2,4,8,16,32,64);

UPDATE MESSAGGIO
SET CODICE = 'CE'||CODICE
WHERE LENGTH(TRIM(TRANSLATE(CODICE, '0123456789', ' '))) is null;


INSERT INTO MESSAGGIO
  (
    MESSAGGIO_ID,
    CODICE,
    BIT_MASK,
    CONTESTO_MESSAGGIO_ID,
    LINGUA_ID,
    DESCRIZIONE,
    TIPO_MESSAGGIO_ID,
    CONTENUTO,
    INS_DATA,
    INS_UTENTE,
    INS_MODULO,
    UPD_DATA,
    UPD_UTENTE,
    UPD_MODULO,
    VERSION
  )
select
    SQ_MESSAGGIO.NEXTVAL,
    M.CODICE,
    M.BIT_MASK,
    CM.CONTESTO_MESSAGGIO_ID,
    1,
    M.DESCRIZIONE,
    TM.TIPO_MESSAGGIO_ID,
    M.DESCRIZIONE,
    sysdate,
    'SETUP',
    'SETUP',
    sysdate,
    'SETUP',
    'SETUP',
    sysdate
FROM NCL_CONTESTO_MESSAGGIO CM
CROSS JOIN (
            SELECT 'OC001' AS CODICE, 1 AS BIT_MASK, 'Righe doppie' AS DESCRIZIONE, 'W' AS TIPO_MESSAGGIO FROM DUAL
      UNION SELECT 'OC002', 2, 'Righe senza documento' AS DESCRIZIONE, 'E' AS TIPO_MESSAGGIO FROM DUAL
      UNION SELECT 'OC003', 4, 'Causali comm non mappate' , 'E'  FROM DUAL
      UNION SELECT 'OC004', 8, 'Righe senza testata' , 'E'  FROM DUAL
      UNION SELECT 'OC005', 16,'Re-import' , 'W'  FROM DUAL
      UNION SELECT 'OC006', 32,'Fornitore non presente in more' , 'E'  FROM DUAL
      UNION SELECT 'OC007', 64,'Assortimento fornitore non presente in more' , 'E'  FROM DUAL
      )  M
JOIN NCL_TIPO_MESSAGGIO  TM   ON TM.CODICE = M.TIPO_MESSAGGIO
WHERE CM.CODICE = 'OC';

commit;

CONNECT &DB_DCC

CREATE TABLE STATO_CARICO_T (
  "ST_CARICO_T_ID" NUMBER(19,0), 
	"ST_CARICO_T_CODICE" VARCHAR2(5 BYTE), 
	"ST_CARICO_T_DESC" VARCHAR2(50 BYTE), 
	"INS_DATA" TIMESTAMP (6), 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" TIMESTAMP (6), 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (3) DEFAULT systimestamp
);

ALTER TABLE STATO_CARICO_T
ADD UNIQUE (ST_CARICO_T_CODICE);

Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (1,'VALID','Valido');
Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (2,'TRANS','Trasferito');
Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (3,'ERROR','Errato');

ALTER TABLE CARICHI_T
MODIFY  STATO VARCHAR2(5 BYTE);

COMMENT ON COLUMN CARICHI_T.STATO
     IS 'Codice stato da STATO_CARICO_T';

UPDATE CARICHI_T
SET STATO = 'VALID'
WHERE STATO = 'V';

ALTER TABLE CARICHI_T
ADD CONSTRAINT FK_STATO_CARICHI_T
FOREIGN KEY (STATO)
REFERENCES STATO_CARICO_T(ST_CARICO_T_CODICE);

commit;

CONNECT &DB_CORE

@\CORE\pkg\UTL_MESSAGGIO.sql
@\CORE\pkg\UTL_MESSAGGIO_BODY.sql

GRANT EXECUTE ON UTL_MESSAGGIO TO PUBLIC;

CONNECT &DB_COMM

CREATE SYNONYM NCL_UTL_MESSAGGIO
FOR &USR_CORE..UTL_MESSAGGIO;


-- 819
CONNECT &DB_CORE

INSERT INTO MESSAGGIO
  (
    MESSAGGIO_ID,
    CODICE,
    BIT_MASK,
    CONTESTO_MESSAGGIO_ID,
    LINGUA_ID,
    DESCRIZIONE,
    TIPO_MESSAGGIO_ID,
    CONTENUTO,
    INS_DATA,
    INS_UTENTE,
    INS_MODULO,
    UPD_DATA,
    UPD_UTENTE,
    UPD_MODULO,
    VERSION
  )
select
    SQ_MESSAGGIO.NEXTVAL,
    M.CODICE,
    M.BIT_MASK,
    CM.CONTESTO_MESSAGGIO_ID,
    1,
    M.DESCRIZIONE,
    TM.TIPO_MESSAGGIO_ID,
    M.DESCRIZIONE,
    sysdate,
    'SETUP',
    'SETUP',
    sysdate,
    'SETUP',
    'SETUP',
    sysdate
FROM NCL_CONTESTO_MESSAGGIO CM
CROSS JOIN (
            SELECT 'OC008' AS CODICE, 128 AS BIT_MASK, 'Tipo Consegna Obbligatorio' AS DESCRIZIONE, 'E' AS TIPO_MESSAGGIO FROM DUAL
            UNION SELECT 'OC009', 256, 'Articolo non censito' AS DESCRIZIONE, 'W' AS TIPO_MESSAGGIO FROM DUAL
            UNION SELECT 'OC010' AS CODICE, 512 AS BIT_MASK, 'Causale non importabile' AS DESCRIZIONE, 'W' AS TIPO_MESSAGGIO FROM DUAL
      )  M
JOIN NCL_TIPO_MESSAGGIO  TM   ON TM.CODICE = M.TIPO_MESSAGGIO
WHERE CM.CODICE = 'OC';

commit;

CONNECT &DB_DCC

ALTER TABLE CARICHI_R
  MODIFY CODICE_ERRORE NUMBER(10,0);
  
commit;

--820 
CONNECT &DB_COMM

Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (4,'ELAB','In Elaborazione');
  
--822
CONNECT &DB_COMM

@\COMM\pkg\ATTESE_FATTURE.sql
@\COMM\pkg\ATTESE_FATTURE_BODY.sql

commit;

--826
CONNECT &DB_COMM

ALTER TABLE FORNITORE_COM
ADD GESTIONE_IMPCAR_INTERC CHAR(1 BYTE) DEFAULT 'N';

COMMENT ON COLUMN FORNITORE_COM.GESTIONE_IMPCAR_INTERC IS 'Abilita o meno la possibilitÓ di intercettare i costi sull''importazione dei carichi';

commit;

--827

--827-2
CONNECT &DB_DCC

Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (5,'TRMOV','Trasferito carico e movimenti');
Insert into STATO_CARICO_T (ST_CARICO_T_ID,ST_CARICO_T_CODICE,ST_CARICO_T_DESC) values (6,'TRCAR','Trasferito solo carico');

--@\DCC\pkg\LOADER.sql
--@\DCC\pkg\LOADER_BODY.sql

commit;

--830
CONNECT &DB_D2M

ALTER TABLE ESITO_LOAD_CARICHI
ADD FORNITORE VARCHAR2(50 BYTE);

DROP TABLE TMP_VARIAZIONE_CARICHI ;

CREATE TABLE TMP_VARIAZIONE_CARICHI 
   (	"ID" NUMBER(10,0), 
	"INTERLOCUTORE_ID" NUMBER(19,0), 
	"INTERLOCUTORE_CODICE" VARCHAR2(50 BYTE), 
	"INTERLOCUTORE_FAT_ID" NUMBER(19,0), 
	"INTERLOCUTORE_FAT_CODICE" VARCHAR2(50 BYTE), 
	"PDD_ID" NUMBER(19,0), 
	"PDD_CODICE" VARCHAR2(50 BYTE), 
	"DIVISIONE_ID" NUMBER(19,0), 
	"PDD_INTERLOCUTORE_ID" NUMBER(19,0), 
	"PDD_TIPO_CESSIONE" CHAR(1 BYTE), 
	"D_CODICE_NEGOZIO" VARCHAR2(6 BYTE), 
	"DATA" DATE, 
	"ARTICOLO_ID" NUMBER(19,0), 
	"ARTICOLO_CODICE" VARCHAR2(50 BYTE), 
	"QTACONS" NUMBER, 
	"QTABOLLA" NUMBER, 
	"IMBFCONS" NUMBER(5,0), 
	"TOTALECONSEGNATO" NUMBER, 
	"TOTALEKG" NUMBER(7,2), 
	"QTAORD" NUMBER, 
	"IMBFORD" NUMBER, 
	"CAUSALE_COMMERCIALE_ID" NUMBER(19,0), 
	"CAUSALE_COMMERCIALE_CODICE" VARCHAR2(5 BYTE), 
	"PRRIGA" NUMBER(7,2), 
	"PREZZOVENDITA" NUMBER(7,2), 
	"NUMERODOCACC" VARCHAR2(128 BYTE), 
	"DATADOCACC" DATE, 
	"DG_ID" NUMBER, 
	"GESTIONESTRALCIO" CHAR(1 BYTE), 
	"LOAD_ID" NUMBER(10,0), 
	"D_PREF_DOC1" VARCHAR2(3 BYTE), 
	"D_SUF_DOC1" VARCHAR2(3 BYTE), 
	"D_NR_RIGA" NUMBER(10,0), 
	"NUMEROFATTFORN" VARCHAR2(15 BYTE), 
	"DATAFATTFORN" DATE, 
	"DGFATTFORN_ID" NUMBER, 
	"D_PREF_DOC2" VARCHAR2(3 BYTE), 
	"D_SUF_DOC2" VARCHAR2(3 BYTE), 
	"D_NR_RIGA2" VARCHAR2(5 BYTE), 
	"D_TRE_CORD" CHAR(1 BYTE), 
	"VALOREUNITARIO" NUMBER, 
	"VOCEVALOREID" NUMBER, 
	"CAUSALECOMMERCIALEID" NUMBER, 
	"BASECALCOLOID" NUMBER, 
	"GESTIONEVOCEVALOREID" NUMBER, 
	"D_CARICHI_R_ID" NUMBER(10,0), 
	"PRE_ORD" CHAR(3 BYTE), 
	"NUM_ORD" VARCHAR2(15 BYTE), 
	"SUF_ORD" CHAR(3 BYTE), 
	"DATA_ORD" DATE, 
	"D_CAT_DIS" VARCHAR2(10 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UNITA_MISURA_ID" NUMBER(19,0), 
	"CONVERSIONE" FLOAT(126), 
	"OPERATORE" CHAR(1 BYTE), 
	"OP_CONVERSIONE" FLOAT(126), 
	"SESSIONE_ID" NUMBER(19,0)
   );
   
CREATE INDEX TMP_VARIAZIONE_CARICHI_IX1 ON TMP_VARIAZIONE_CARICHI (SESSIONE_ID);
CREATE INDEX TMP_VARIAZIONE_CARICHI_IX2 ON TMP_VARIAZIONE_CARICHI (LOAD_ID, INS_MODULO, SESSIONE_ID);

commit;

CONNECT &DB_COMM

Insert into NCL_MESSAGGIO (MESSAGGIO_ID,CODICE,CONTESTO_MESSAGGIO_ID,LINGUA_ID,DESCRIZIONE,TIPO_MESSAGGIO_ID,CONTENUTO,INS_DATA,INS_UTENTE,INS_MODULO,UPD_DATA,UPD_UTENTE,UPD_MODULO,VERSION,BIT_MASK) 
select 
  &USR_CORE..sq_MESSAGGIO.nextval,
  'OC011',
  CM.CONTESTO_MESSAGGIO_ID, 
  1, 
  'Articolo non in assortimento al pdv',
  1 AS TIPO_MESSAGGIO_ID,
  'Articolo non in assortimento al pdv',
  sysdate,'SETUP','SETUP',sysdate,'SETUP','SETUP',SYSDATE, 1024  
from NCL_CONTESTO_MESSAGGIO CM
WHERE 
  CM.CODICE = 'OC'
  ;

Insert into NCL_MESSAGGIO (MESSAGGIO_ID,CODICE,CONTESTO_MESSAGGIO_ID,LINGUA_ID,DESCRIZIONE,TIPO_MESSAGGIO_ID,CONTENUTO,INS_DATA,INS_UTENTE,INS_MODULO,UPD_DATA,UPD_UTENTE,UPD_MODULO,VERSION,BIT_MASK) 
select 
  &USR_CORE..sq_MESSAGGIO.nextval,
  'OC012',
  CM.CONTESTO_MESSAGGIO_ID, 
  1, 
  'Testata duplicata',
  1 AS TIPO_MESSAGGIO_ID,
  'Testata duplicata',
  sysdate,'SETUP','SETUP',sysdate,'SETUP','SETUP',SYSDATE, 2048  
from NCL_CONTESTO_MESSAGGIO CM
WHERE 
  CM.CODICE = 'OC'
  ;
  

@\COMM\typ\TYPE_ESITO_CARICO.sql
@\COMM\typ\TBL_ESITO_CARICO.sql


--842
CONNECT &DB_D2M

ALTER TABLE DB_MODXLOAXTAB_NSC
ADD FL_LOCK VARCHAR2(3 BYTE);

COMMENT ON COLUMN DB_MODXLOAXTAB_NSC.FL_LOCK 
     IS 'Indica CONCETTUALMENTE un lock del record. Unocked per valore NULL - Locked (solo per retrocompatibilitÓ con gestione stato WP) contiene STATO_ELABORAZIONE precedente';

commit;


/*only MAXIDI*/
CONNECT &DB_DCC

CREATE GLOBAL TEMPORARY TABLE "CARICHI_DA_CANCELLARE" 
   ("PDD_MORE" VARCHAR2(50 BYTE), 
	"D_DATA_DOC1" VARCHAR2(128 BYTE), 
	"D_NR_DOC1" VARCHAR2(128 BYTE), 
	"D_SUF_DOC1" VARCHAR2(3 BYTE), 
	"CODICE_FORN" VARCHAR2(50 BYTE), 
	"CARICO_T_ID" NUMBER
   ) ON COMMIT PRESERVE ROWS ;

DROP TABLE "CARICHI_R" ;
   
CREATE TABLE "CARICHI_R" 
   (	"D_CARICHI_R_ID" NUMBER(10,0), 
	"D_CARICHI_T_ID" NUMBER(10,0), 
	"D_CAUSALE" VARCHAR2(5 BYTE), 
	"D_CODICE_NEGOZIO" VARCHAR2(6 BYTE), 
	"D_NR_DOC1" VARCHAR2(128 BYTE), 
	"D_DATA_DOC1" VARCHAR2(6 BYTE), 
	"D_NR_DOC2" VARCHAR2(128 BYTE), 
	"D_DATA_DOC2" VARCHAR2(6 BYTE), 
	"D_CODCONS" VARCHAR2(8 BYTE), 
	"D_CODARTFOR" VARCHAR2(50 BYTE), 
	"D_CODARTCONS" VARCHAR2(15 BYTE), 
	"D_UNITA_IMBALLO" VARCHAR2(2 BYTE), 
	"D_IMBALLO" NUMBER(5,0), 
	"D_QTA" NUMBER(7,2), 
	"D_PREZZO_ACQ" NUMBER(7,2), 
	"D_PREZZO_VEND_CONS" NUMBER(7,2), 
	"D_ALIQUOTA" NUMBER(3,1), 
	"D_TIPO_RAPPORTO_CONVERSIONE" VARCHAR2(1 BYTE), 
	"D_VAL_RAPPORTO_CONVERSIONE" NUMBER(18,5), 
	"D_TIPO_CONSEGNA" VARCHAR2(1 BYTE) DEFAULT 'D', 
	"D_TIPO_DOC1" VARCHAR2(1 BYTE) DEFAULT 'B', 
	"D_TIPO_DOC2" VARCHAR2(1 BYTE), 
	"D_DIVISIONE_FORNITORE" VARCHAR2(1 BYTE), 
	"D_FORNITORE_FATTURA" VARCHAR2(8 BYTE), 
	"LOAD_ID" NUMBER(10,0), 
	"CODICE_ERRORE" NUMBER(10,0), 
	"INS_DATA" TIMESTAMP (6) DEFAULT SYSDATE NOT NULL ENABLE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" TIMESTAMP (6), 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (3), 
	"D_QTA_TEMP" VARCHAR2(9 BYTE), 
	"D_VALORAPP_TEMP" VARCHAR2(8 BYTE), 
	"D_SUF_DOC1" CHAR(3 BYTE), 
	"D_PREF_DOC1" CHAR(3 BYTE), 
	"D_NR_RIGA" NUMBER(10,0), 
	"D_DESCRIZIONE_ARTFOR" VARCHAR2(256 BYTE), 
	"D_BARCODE_FOR" VARCHAR2(256 BYTE), 
	"D_DOC_GES2" VARCHAR2(3 BYTE), 
	"D_QTA_CONS" NUMBER(7,2), 
	"D_UM_RP" VARCHAR2(2 BYTE), 
	"D_NR_RIGA2" VARCHAR2(5 BYTE), 
	"D_IVA_CONS" VARCHAR2(16 BYTE), 
	"D_CAUSALE_SEGNO" CHAR(1 BYTE), 
	"D_PREF_DOC2" VARCHAR2(3 BYTE), 
	"D_CAUSALE_DESC" VARCHAR2(60 BYTE), 
	"D_DOC_GES1" VARCHAR2(3 BYTE), 
	"D_IVA_LEGGE" VARCHAR2(6 BYTE), 
	"D_CAT_DIS" VARCHAR2(10 BYTE), 
	"D_TRE_CORD" CHAR(1 BYTE) DEFAULT 'M', 
	"D_QTA_ORD" NUMBER(7,2), 
	"D_SUF_DOC2" VARCHAR2(3 BYTE), 
	"D_MULTI_IVA" CHAR(1 BYTE), 
	"D_OFF_CESS" CHAR(1 BYTE), 
	"D_OFF_ANNO" VARCHAR2(4 BYTE), 
	"D_TIPO_ART" CHAR(1 BYTE), 
	"D_OFF_PROMO" VARCHAR2(5 BYTE), 
	"D_CAT_CODE" VARCHAR2(2 BYTE), 
	"D_OFF_PUBB" CHAR(1 BYTE), 
	"D_DATA_DOC1_TEMP" VARCHAR2(9 BYTE), 
	"D_DATA_DOC2_TEMP" VARCHAR2(9 BYTE), 
	"D_NR_RIGA_TEMP" VARCHAR2(5 BYTE), 
	"D_PERSONA_EC" VARCHAR2(6 BYTE), 
	"D_TIPO_VEN" CHAR(1 BYTE), 
	"D_PROVENIENZA" VARCHAR2(5 BYTE), 
	"D_COD_MAG_USC" VARCHAR2(2 BYTE), 
	"D_CAL_MAX" VARCHAR2(5 BYTE), 
	"D_LOTTO" VARCHAR2(20 BYTE), 
	"D_CAL_MIN" VARCHAR2(5 BYTE), 
	"TIPO_ART" CHAR(1 BYTE), 
	"D_IMBALLO_BCK" NUMBER, 
	"D_ARTCOD_MORE_ID" NUMBER(19,0), 
	"D_ARTCOD_MORE_CODICE" VARCHAR2(50 BYTE), 
	"D_NR_DOC1_BCK" VARCHAR2(128 BYTE), 
	"D_PREF_DOC1_BCK" VARCHAR2(128 BYTE), 
	"D_DATA_DOC1_BCK" VARCHAR2(128 BYTE), 
	"D_SUF_DOC1_BCK" VARCHAR2(128 BYTE), 
	"PREZZO_ACQ" NUMBER(7,2), 
	"ARTICOLO_ID" NUMBER(19,0), 
	"ARTICOLO_CODICE" VARCHAR2(50 BYTE)
   );
   
CREATE INDEX "IX_CARICHI_R_CARICHI_T_ID" ON "CARICHI_R" ("D_CARICHI_T_ID");

CREATE INDEX "IX_CARICHI_R_CAUSALE" ON "CARICHI_R" ("D_CAUSALE", "INS_DATA");

CREATE INDEX "IX_CARICHI_R_FORNITORE" ON "CARICHI_R" ("D_FORNITORE_FATTURA", "INS_DATA");

CREATE INDEX "IX_CARICHI_R_INS_DATA" ON "CARICHI_R" ("INS_DATA");

CREATE INDEX "IX_CARICHI_R_LOADMOD" ON "CARICHI_R" ("LOAD_ID", "INS_MODULO", "INS_DATA");

CREATE INDEX "IX_CARICHI_R_LOAD_ID" ON "CARICHI_R" ("LOAD_ID", "INS_DATA");

CREATE INDEX "IX_CARICHI_R_SELECT" ON "CARICHI_R" ("LOAD_ID", TRIM("D_NR_DOC1"), "D_DATA_DOC1", "D_DATA_DOC2", TRIM("D_NR_DOC2"), "D_CARICHI_T_ID", "D_FORNITORE_FATTURA", "INS_DATA");

CREATE INDEX "IX_D_CARICHI_R_ID" ON "CARICHI_R" ("D_CARICHI_R_ID");

@\DCC\trg\BI_CARICHI_R.sql
ALTER TRIGGER "BI_CARICHI_R" ENABLE;

@\DCC\trg\AFTER_INSERT_ON_CARICHI_R.sql
ALTER TRIGGER "AFTER_INSERT_ON_CARICHI_R" ENABLE;


GRANT ALL ON CARICHI_R TO &USR_COMM;
GRANT ALL ON CARICHI_DA_CANCELLARE TO &USR_COMM;

CREATE TABLE "TMP_CARICHI_R" 
   (	"D_CARICHI_R_ID" NUMBER(10,0), 
	"D_CARICHI_T_ID" NUMBER(10,0), 
	"D_CAUSALE" VARCHAR2(5 BYTE), 
	"D_CODICE_NEGOZIO" VARCHAR2(6 BYTE), 
	"D_NR_DOC1" VARCHAR2(15 BYTE), 
	"D_DATA_DOC1" VARCHAR2(6 BYTE), 
	"D_NR_DOC2" VARCHAR2(15 BYTE), 
	"D_DATA_DOC2" VARCHAR2(6 BYTE), 
	"D_CODCONS" VARCHAR2(8 BYTE), 
	"D_CODARTFOR" VARCHAR2(50 BYTE), 
	"D_CODARTCONS" VARCHAR2(15 BYTE), 
	"D_UNITA_IMBALLO" VARCHAR2(2 BYTE), 
	"D_IMBALLO" NUMBER(5,0), 
	"D_QTA" NUMBER(7,2), 
	"D_PREZZO_ACQ" NUMBER(7,2), 
	"D_PREZZO_VEND_CONS" NUMBER(7,2), 
	"D_ALIQUOTA" NUMBER(3,1), 
	"D_TIPO_RAPPORTO_CONVERSIONE" VARCHAR2(1 BYTE), 
	"D_VAL_RAPPORTO_CONVERSIONE" NUMBER(5,3), 
	"D_TIPO_CONSEGNA" VARCHAR2(1 BYTE) DEFAULT 'D', 
	"D_TIPO_DOC1" VARCHAR2(1 BYTE) DEFAULT 'B', 
	"D_TIPO_DOC2" VARCHAR2(1 BYTE), 
	"D_DIVISIONE_FORNITORE" VARCHAR2(1 BYTE), 
	"D_FORNITORE_FATTURA" VARCHAR2(8 BYTE), 
	"LOAD_ID" NUMBER(10,0), 
	"CODICE_ERRORE" NUMBER(2,0), 
	"INS_DATA" TIMESTAMP (6) NOT NULL ENABLE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" TIMESTAMP (6), 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (3), 
	"D_QTA_TEMP" VARCHAR2(9 BYTE), 
	"D_VALORAPP_TEMP" VARCHAR2(8 BYTE), 
	"D_SUF_DOC1" CHAR(3 BYTE), 
	"D_PREF_DOC1" CHAR(3 BYTE), 
	"D_NR_RIGA" NUMBER(10,0), 
	"D_DESCRIZIONE_ARTFOR" VARCHAR2(256 BYTE), 
	"D_BARCODE_FOR" VARCHAR2(256 BYTE), 
	"D_DOC_GES2" VARCHAR2(3 BYTE), 
	"D_QTA_CONS" NUMBER(7,2), 
	"D_UM_RP" VARCHAR2(2 BYTE), 
	"D_NR_RIGA2" VARCHAR2(5 BYTE), 
	"D_IVA_CONS" VARCHAR2(16 BYTE), 
	"D_CAUSALE_SEGNO" CHAR(1 BYTE), 
	"D_PREF_DOC2" VARCHAR2(3 BYTE), 
	"D_CAUSALE_DESC" VARCHAR2(60 BYTE), 
	"D_DOC_GES1" VARCHAR2(3 BYTE), 
	"D_IVA_LEGGE" VARCHAR2(6 BYTE), 
	"D_CAT_DIS" VARCHAR2(10 BYTE), 
	"D_TRE_CORD" CHAR(1 BYTE) DEFAULT 'M', 
	"D_QTA_ORD" NUMBER(7,2), 
	"D_SUF_DOC2" VARCHAR2(3 BYTE), 
	"D_MULTI_IVA" CHAR(1 BYTE), 
	"D_OFF_CESS" CHAR(1 BYTE), 
	"D_OFF_ANNO" VARCHAR2(4 BYTE), 
	"D_TIPO_ART" CHAR(1 BYTE), 
	"D_OFF_PROMO" VARCHAR2(5 BYTE), 
	"D_CAT_CODE" VARCHAR2(2 BYTE), 
	"D_OFF_PUBB" CHAR(1 BYTE), 
	"D_DATA_DOC1_TEMP" VARCHAR2(9 BYTE), 
	"D_DATA_DOC2_TEMP" VARCHAR2(9 BYTE), 
	"D_NR_RIGA_TEMP" VARCHAR2(5 BYTE), 
	"D_PERSONA_EC" VARCHAR2(6 BYTE), 
	"D_TIPO_VEN" CHAR(1 BYTE), 
	"D_PROVENIENZA" VARCHAR2(5 BYTE), 
	"D_COD_MAG_USC" VARCHAR2(2 BYTE), 
	"D_CAL_MAX" VARCHAR2(5 BYTE), 
	"D_LOTTO" VARCHAR2(20 BYTE), 
	"D_CAL_MIN" VARCHAR2(5 BYTE), 
	"TIPO_ART" CHAR(1 BYTE), 
	"D_IMBALLO_BCK" NUMBER
   ) ;
   

  CREATE GLOBAL TEMPORARY TABLE "TMPCR_CARICHI_R" 
   (	"D_CARICHI_R_ID" NUMBER(10,0), 
	"D_CARICHI_T_ID" NUMBER(10,0), 
	"D_CAUSALE" VARCHAR2(5 BYTE), 
	"D_CODICE_NEGOZIO" VARCHAR2(6 BYTE), 
	"D_NR_DOC1" VARCHAR2(128 BYTE), 
	"D_DATA_DOC1" VARCHAR2(6 BYTE), 
	"D_NR_DOC2" VARCHAR2(15 BYTE), 
	"D_DATA_DOC2" VARCHAR2(6 BYTE), 
	"D_CODCONS" VARCHAR2(8 BYTE), 
	"D_CODARTFOR" VARCHAR2(50 BYTE), 
	"D_CODARTCONS" VARCHAR2(15 BYTE), 
	"D_UNITA_IMBALLO" VARCHAR2(2 BYTE), 
	"D_IMBALLO" NUMBER(5,0), 
	"D_QTA" NUMBER(7,2), 
	"D_PREZZO_ACQ" NUMBER(7,2), 
	"D_PREZZO_VEND_CONS" NUMBER(7,2), 
	"D_ALIQUOTA" NUMBER(3,1), 
	"D_TIPO_RAPPORTO_CONVERSIONE" VARCHAR2(1 BYTE), 
	"D_VAL_RAPPORTO_CONVERSIONE" NUMBER(18,5), 
	"D_TIPO_CONSEGNA" VARCHAR2(1 BYTE), 
	"D_TIPO_DOC1" VARCHAR2(1 BYTE), 
	"D_TIPO_DOC2" VARCHAR2(1 BYTE), 
	"D_DIVISIONE_FORNITORE" VARCHAR2(1 BYTE), 
	"D_FORNITORE_FATTURA" VARCHAR2(8 BYTE), 
	"LOAD_ID" NUMBER(10,0), 
	"CODICE_ERRORE" NUMBER(2,0), 
	"INS_DATA" TIMESTAMP (6) NOT NULL ENABLE, 
	"INS_UTENTE" VARCHAR2(50 BYTE), 
	"INS_MODULO" VARCHAR2(50 BYTE), 
	"UPD_DATA" TIMESTAMP (6), 
	"UPD_UTENTE" VARCHAR2(50 BYTE), 
	"UPD_MODULO" VARCHAR2(50 BYTE), 
	"VERSION" TIMESTAMP (3), 
	"D_QTA_TEMP" VARCHAR2(9 BYTE), 
	"D_VALORAPP_TEMP" VARCHAR2(8 BYTE), 
	"D_SUF_DOC1" CHAR(3 BYTE), 
	"D_PREF_DOC1" CHAR(3 BYTE), 
	"D_NR_RIGA" NUMBER(10,0), 
	"D_DESCRIZIONE_ARTFOR" VARCHAR2(256 BYTE), 
	"D_BARCODE_FOR" VARCHAR2(256 BYTE), 
	"D_DOC_GES2" VARCHAR2(3 BYTE), 
	"D_QTA_CONS" NUMBER(7,2), 
	"D_UM_RP" VARCHAR2(2 BYTE), 
	"D_NR_RIGA2" VARCHAR2(5 BYTE), 
	"D_IVA_CONS" VARCHAR2(16 BYTE), 
	"D_CAUSALE_SEGNO" CHAR(1 BYTE), 
	"D_PREF_DOC2" VARCHAR2(3 BYTE), 
	"D_CAUSALE_DESC" VARCHAR2(60 BYTE), 
	"D_DOC_GES1" VARCHAR2(3 BYTE), 
	"D_IVA_LEGGE" VARCHAR2(6 BYTE), 
	"D_CAT_DIS" VARCHAR2(10 BYTE), 
	"D_TRE_CORD" CHAR(1 BYTE), 
	"D_QTA_ORD" NUMBER(7,2), 
	"D_SUF_DOC2" VARCHAR2(3 BYTE), 
	"D_MULTI_IVA" CHAR(1 BYTE), 
	"D_OFF_CESS" CHAR(1 BYTE), 
	"D_OFF_ANNO" VARCHAR2(4 BYTE), 
	"D_TIPO_ART" CHAR(1 BYTE), 
	"D_OFF_PROMO" VARCHAR2(5 BYTE), 
	"D_CAT_CODE" VARCHAR2(2 BYTE), 
	"D_OFF_PUBB" CHAR(1 BYTE), 
	"D_DATA_DOC1_TEMP" VARCHAR2(9 BYTE), 
	"D_DATA_DOC2_TEMP" VARCHAR2(9 BYTE), 
	"D_NR_RIGA_TEMP" VARCHAR2(5 BYTE), 
	"D_PERSONA_EC" VARCHAR2(6 BYTE), 
	"D_TIPO_VEN" CHAR(1 BYTE), 
	"D_PROVENIENZA" VARCHAR2(5 BYTE), 
	"D_COD_MAG_USC" VARCHAR2(2 BYTE), 
	"D_CAL_MAX" VARCHAR2(5 BYTE), 
	"D_LOTTO" VARCHAR2(20 BYTE), 
	"D_CAL_MIN" VARCHAR2(5 BYTE), 
	"TIPO_ART" CHAR(1 BYTE), 
	"ARTICOLO_ID" NUMBER(19,0), 
	"ARTICOLO_CODICE" VARCHAR2(50 BYTE), 
	"PREZZO_ACQ" NUMBER(7,2)
   ) ON COMMIT DELETE ROWS ;

  CREATE INDEX "TIX_CARICHI_R_CAUSALE" ON "TMPCR_CARICHI_R" ("D_CAUSALE") ;

  CREATE INDEX "TIX_CARICHI_R_CODARTFOR" ON "TMPCR_CARICHI_R" ("D_CODARTFOR") ;

  CREATE INDEX "TIX_CARICHI_R_FORNITORE" ON "TMPCR_CARICHI_R" ("D_FORNITORE_FATTURA") ;

  CREATE INDEX "TIX_CARICHI_R_INS_DATA" ON "TMPCR_CARICHI_R" ("INS_DATA") ;

  CREATE INDEX "TIX_CARICHI_R_LOADMOD" ON "TMPCR_CARICHI_R" ("LOAD_ID", "INS_MODULO", "INS_DATA") ;

  CREATE INDEX "TIX_CARICHI_R_LOAD_ID" ON "TMPCR_CARICHI_R" ("LOAD_ID", "INS_DATA") ;

  CREATE INDEX "TIX_CARICHI_R_SELECT" ON "TMPCR_CARICHI_R" ("LOAD_ID", TRIM("D_NR_DOC1"), "D_DATA_DOC1", "D_DATA_DOC2", TRIM("D_NR_DOC2"), "D_CARICHI_T_ID", "D_FORNITORE_FATTURA", "INS_DATA") ;
  

  CREATE TABLE "ERR$_CARICHI_R" 
   (	"ORA_ERR_NUMBER$" NUMBER, 
	"ORA_ERR_MESG$" VARCHAR2(2000 BYTE), 
	"ORA_ERR_ROWID$" UROWID (4000), 
	"ORA_ERR_OPTYP$" VARCHAR2(2 BYTE), 
	"ORA_ERR_TAG$" VARCHAR2(2000 BYTE), 
	"D_CARICHI_R_ID" VARCHAR2(4000 BYTE), 
	"D_CARICHI_T_ID" VARCHAR2(4000 BYTE), 
	"D_CAUSALE" VARCHAR2(4000 BYTE), 
	"D_CODICE_NEGOZIO" VARCHAR2(4000 BYTE), 
	"D_NR_DOC1" VARCHAR2(4000 BYTE), 
	"D_DATA_DOC1" VARCHAR2(4000 BYTE), 
	"D_NR_DOC2" VARCHAR2(4000 BYTE), 
	"D_DATA_DOC2" VARCHAR2(4000 BYTE), 
	"D_CODCONS" VARCHAR2(4000 BYTE), 
	"D_CODARTFOR" VARCHAR2(4000 BYTE), 
	"D_CODARTCONS" VARCHAR2(4000 BYTE), 
	"D_UNITA_IMBALLO" VARCHAR2(4000 BYTE), 
	"D_IMBALLO" VARCHAR2(4000 BYTE), 
	"D_QTA" VARCHAR2(4000 BYTE), 
	"D_PREZZO_ACQ" VARCHAR2(4000 BYTE), 
	"D_PREZZO_VEND_CONS" VARCHAR2(4000 BYTE), 
	"D_ALIQUOTA" VARCHAR2(4000 BYTE), 
	"D_TIPO_RAPPORTO_CONVERSIONE" VARCHAR2(4000 BYTE), 
	"D_VAL_RAPPORTO_CONVERSIONE" VARCHAR2(4000 BYTE), 
	"D_TIPO_CONSEGNA" VARCHAR2(4000 BYTE), 
	"D_TIPO_DOC1" VARCHAR2(4000 BYTE), 
	"D_TIPO_DOC2" VARCHAR2(4000 BYTE), 
	"D_DIVISIONE_FORNITORE" VARCHAR2(4000 BYTE), 
	"D_FORNITORE_FATTURA" VARCHAR2(4000 BYTE), 
	"LOAD_ID" VARCHAR2(4000 BYTE), 
	"CODICE_ERRORE" VARCHAR2(4000 BYTE), 
	"INS_DATA" VARCHAR2(4000 BYTE), 
	"INS_UTENTE" VARCHAR2(4000 BYTE), 
	"INS_MODULO" VARCHAR2(4000 BYTE), 
	"UPD_DATA" VARCHAR2(4000 BYTE), 
	"UPD_UTENTE" VARCHAR2(4000 BYTE), 
	"UPD_MODULO" VARCHAR2(4000 BYTE), 
	"VERSION" VARCHAR2(4000 BYTE), 
	"D_QTA_TEMP" VARCHAR2(4000 BYTE), 
	"D_VALORAPP_TEMP" VARCHAR2(4000 BYTE), 
	"D_SUF_DOC1" VARCHAR2(4000 BYTE), 
	"D_PREF_DOC1" VARCHAR2(4000 BYTE), 
	"D_NR_RIGA" VARCHAR2(4000 BYTE), 
	"D_DESCRIZIONE_ARTFOR" VARCHAR2(4000 BYTE), 
	"D_BARCODE_FOR" VARCHAR2(4000 BYTE), 
	"D_DOC_GES2" VARCHAR2(4000 BYTE), 
	"D_QTA_CONS" VARCHAR2(4000 BYTE), 
	"D_UM_RP" VARCHAR2(4000 BYTE), 
	"D_NR_RIGA2" VARCHAR2(4000 BYTE), 
	"D_IVA_CONS" VARCHAR2(4000 BYTE), 
	"D_CAUSALE_SEGNO" VARCHAR2(4000 BYTE), 
	"D_PREF_DOC2" VARCHAR2(4000 BYTE), 
	"D_CAUSALE_DESC" VARCHAR2(4000 BYTE), 
	"D_DOC_GES1" VARCHAR2(4000 BYTE), 
	"D_IVA_LEGGE" VARCHAR2(4000 BYTE), 
	"D_CAT_DIS" VARCHAR2(4000 BYTE), 
	"D_TRE_CORD" VARCHAR2(4000 BYTE), 
	"D_QTA_ORD" VARCHAR2(4000 BYTE), 
	"D_SUF_DOC2" VARCHAR2(4000 BYTE), 
	"D_MULTI_IVA" VARCHAR2(4000 BYTE), 
	"D_OFF_CESS" VARCHAR2(4000 BYTE), 
	"D_OFF_ANNO" VARCHAR2(4000 BYTE), 
	"D_TIPO_ART" VARCHAR2(4000 BYTE), 
	"D_OFF_PROMO" VARCHAR2(4000 BYTE), 
	"D_CAT_CODE" VARCHAR2(4000 BYTE), 
	"D_OFF_PUBB" VARCHAR2(4000 BYTE), 
	"D_DATA_DOC1_TEMP" VARCHAR2(4000 BYTE), 
	"D_DATA_DOC2_TEMP" VARCHAR2(4000 BYTE), 
	"D_NR_RIGA_TEMP" VARCHAR2(4000 BYTE), 
	"D_PERSONA_EC" VARCHAR2(4000 BYTE), 
	"D_TIPO_VEN" VARCHAR2(4000 BYTE), 
	"D_PROVENIENZA" VARCHAR2(4000 BYTE), 
	"D_COD_MAG_USC" VARCHAR2(4000 BYTE), 
	"D_CAL_MAX" VARCHAR2(4000 BYTE), 
	"D_LOTTO" VARCHAR2(4000 BYTE), 
	"D_CAL_MIN" VARCHAR2(4000 BYTE), 
	"TIPO_ART" VARCHAR2(4000 BYTE), 
	"D_IMBALLO_BCK" VARCHAR2(4000 BYTE), 
	"D_ARTCOD_MORE_ID" VARCHAR2(4000 BYTE), 
	"D_ARTCOD_MORE_CODICE" VARCHAR2(4000 BYTE), 
	"D_NR_DOC1_BCK" VARCHAR2(4000 BYTE), 
	"D_PREF_DOC1_BCK" VARCHAR2(4000 BYTE), 
	"D_DATA_DOC1_BCK" VARCHAR2(4000 BYTE), 
	"D_SUF_DOC1_BCK" VARCHAR2(4000 BYTE)
   ) ;

   COMMENT ON TABLE "ERR$_CARICHI_R"  IS 'DML Error Logging table for "CARICHI_R"';


GRANT ALL ON TMP_CARICHI_R TO &USR_COMM;
GRANT ALL ON TMPCR_CARICHI_R TO &USR_COMM;
GRANT ALL ON ERR$_CARICHI_R TO &USR_COMM;
   
commit;
   
CONNECT &DB_COMM

CREATE OR REPLACE SYNONYM DCC_CARICHI_R FOR &USR_DCC..CARICHI_R;
CREATE OR REPLACE SYNONYM DCC_CARICHI_T FOR &USR_DCC..CARICHI_T;
CREATE OR REPLACE SYNONYM DCC_CARICHI_DA_CANCELLARE FOR &USR_DCC..CARICHI_DA_CANCELLARE;
CREATE OR REPLACE SYNONYM DCC_TMP_CARICHI_R FOR &USR_DCC..TMP_CARICHI_R;
CREATE OR REPLACE SYNONYM DCC_ERR$_CARICHI_R FOR &USR_DCC..ERR$_CARICHI_R;
CREATE OR REPLACE SYNONYM NPKG_BARCODE_INFO FOR &USR_CORE..PKG_BARCODE_INFO;

commit;


CONNECT &DB_CORE

ALTER TABLE GRANT_INTERLOC_X_DIVISIONE
ADD INS_MODULO VARCHAR2(128 BYTE);


COMMIT;


CONNECT &DB_COMM

/*modify MAXIDI*/@\COMM\pkg\UTL_CARICHI.sql
/*modify MAXIDI*/@\COMM\pkg\UTL_CARICHI_BODY.sql

COMMIT;

CONNECT &DB_D2M

alter table DB_MODXLOAXTAB_NSC drop constraint CHK_DB_MODXLOAXTAB_NSC ;
alter table DB_MODXLOAXTAB_NSC add constraint CHK_DB_MODXLOAXTAB_NSC CHECK (STATO_ELABORAZIONE IN 
      (
       'A',
       'S',
       'WP',
       'T',
       'PL',
       'E',
       'E2',
       'PT'
      ));