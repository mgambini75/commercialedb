--liquibase formatted sql
--changeset fburghila:Caricamento_parametri_cliente logicalFilePath:v-1.0/0004-Caricamento_parametri_cliente.sql runInTransaction:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql

-- Connessione a DCC --------------------------------------------------------------------------------------------------------------
CONNECT &DB_DCC

CREATE SEQUENCE SQ_PARAMETRI_CLIENTE START WITH 1;
CREATE SEQUENCE SQ_LOAD_ID_PARAMETRI_CLIENTE START WITH 1;

-- 
-- TABLE: PARAMETRI_CLIENTE
--
@\DCC\tbl\PARAMETRI_CLIENTE.sql

CREATE SYNONYM ncl_documento_gestionale FOR &USR_CORE..documento_gestionale;
CREATE SYNONYM comm_parametri_cliente FOR &USR_COMM..parametri_cliente;
CREATE SYNONYM comm_modalita_calc_przces FOR &USR_COMM..modalita_calc_przces;
CREATE SYNONYM comm_tipo_divulg_etich FOR &USR_COMM..tipo_divulg_etich;
CREATE SYNONYM comm_formato_divulgazione FOR &USR_COMM..formato_divulgazione;
CREATE SYNONYM comm_dati_formato_elettr FOR &USR_COMM..dati_formato_elettr;
CREATE SYNONYM comm_stile_layout_listini FOR &USR_COMM..stile_layout_listini;
CREATE SYNONYM comm_tipo_copia_conforme FOR &USR_COMM..tipo_copia_conforme;
CREATE SYNONYM comm_sq_parametri_cliente FOR &USR_COMM..sq_parametri_cliente;
CREATE SYNONYM ncl_gestione_cauzione FOR &USR_CORE..gestione_cauzione;
CREATE SYNONYM comm_critero_ordinamento FOR &USR_COMM..critero_ordinamento;
CREATE SYNONYM comm_criterio_separazione FOR &USR_COMM..criterio_separazione;
CREATE SYNONYM comm_tipo_punto_consegna FOR &USR_COMM..tipo_punto_consegna;
CREATE SYNONYM comm_tipo_rapp_amm_cfg FOR &USR_COMM..tipo_rapp_amm_cfg;


@\DCC\pkg\PKG_LOAD_PARAMETRI_CLIENTE.sql
@\DCC\pkg\PKG_LOAD_PARAMETRI_CLIENTE_BODY.sql

INSERT INTO DB_FIELDDEFVAL (DB_OWNER,TABLE_NAME,FIELD_NAME,FIELD_DEFVAL) VALUES ('BRC_DCC','SEND_MAIL','SERVIZI_CLIENTI_DEST_01','ivan.locastro@ditechspa.it');
INSERT INTO DB_FIELDDEFVAL (DB_OWNER,TABLE_NAME,FIELD_NAME,FIELD_DEFVAL) VALUES ('BRC_DCC','SEND_MAIL','SERVIZI_CLIENTI_DEST_02','flavius.burghila@ditechspa.it');

COMMIT;