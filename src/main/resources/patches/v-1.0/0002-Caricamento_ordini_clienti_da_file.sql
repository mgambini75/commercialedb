--liquibase formatted sql
--changeset ilocastro:Caricamento_ordini_clienti_da_file logicalFilePath:v-1.0/0002-Caricamento_ordini_clienti_da_file.sql runOnChange:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql


-- Connessione a COMM --------------------------------------------------------------------------------------------------------------
CONNECT &DB_COMM

ALTER TABLE ORDINE_NORMALIZZATO_T ADD PROGRESSIVO_TRASMISSIONE VARCHAR2(50);
ALTER TABLE ORDINE_NORMALIZZATO_T ADD PROVENIENZA_ORDINE_DST_ID NUMBER(19); 

ALTER SEQUENCE SQ_ORDINE_NORMALIZZATO_T CACHE 1000;
ALTER SEQUENCE SQ_ORDINE_NORMALIZZATO_D CACHE 5000;

-- Connessione  DBA ---------------------------------------------------------------------------------------------------------------
CONNECT &DB_DBA

GRANT CREATE ANY DIRECTORY TO &USR_DCC;


-- Connessione a DCC ---------------------------------------------------------------------------------------------------------------
CONNECT &DB_DCC

@\DCC\tbl\EXT_DORDINI2.sql
@\DCC\tbl\DORDINI2.sql

CREATE SEQUENCE SQ_LOAD_ID_ORDINE_NORMALIZZATO INCREMENT BY 1 START WITH 1;

CREATE SYNONYM COMM_ORDINE_NORMALIZZATO_D FOR &USR_COMM..ORDINE_NORMALIZZATO_D;
CREATE SYNONYM COMM_ORDINE_NORMALIZZATO_T FOR &USR_COMM..ORDINE_NORMALIZZATO_T;
CREATE SYNONYM COMM_SQ_ORDINE_NORMALIZZATO_T FOR &USR_COMM..SQ_ORDINE_NORMALIZZATO_T;
CREATE SYNONYM COMM_SQ_ORDINE_NORMALIZZATO_D FOR &USR_COMM..SQ_ORDINE_NORMALIZZATO_D;
CREATE SYNONYM COMM_STATO_ORDINE_NORM FOR &USR_COMM..STATO_ORDINE_NORM;
CREATE SYNONYM COMM_STATO_RIGA_ORDINE_NORM FOR &USR_COMM..STATO_RIGA_ORDINE_NORM;

@\DCC\pkg\UTILITY.sql
@\DCC\pkg\UTILITY_BODY.sql
@\DCC\pkg\PKG_LOAD_ORDINI_CLIENTI.sql
@\DCC\pkg\PKG_LOAD_ORDINI_CLIENTI_BODY.sql

Insert into DB_FIELDDEFVAL (DB_OWNER,TABLE_NAME,FIELD_NAME,FIELD_DEFVAL,FIELD_LOOKUP,FILED_ISDEFAULT,ENV_VAR_NAME,DESCRIZIONE) 
values ('BRC_DCC','SEND_MAIL','ORDINI_CLIENTI_DEST_01','ivan.locastro@ditechspa.it',null,null,'$NULL',null);
Insert into DB_FIELDDEFVAL (DB_OWNER,TABLE_NAME,FIELD_NAME,FIELD_DEFVAL,FIELD_LOOKUP,FILED_ISDEFAULT,ENV_VAR_NAME,DESCRIZIONE) 
values ('BRC_DCC','SEND_MAIL','ORDINI_CLIENTI_DEST_02','flavius.burghila@ditechspa.it',null,null,'$NULL',null);

commit;



