create or replace PACKAGE PRICING AS

  PROCEDURE FILL_EXEC_ARTICOLO      ( P_PRICING_MODEL_ID     IN PRICING_MODEL.PRICING_MODEL_ID%TYPE ,
                                      P_PRICING_EXEC_ID      IN PRICING_EXEC.PRICING_EXEC_ID%TYPE ,
                                      P_USERNAME               IN VARCHAR2 ,
                                      P_COMPLETA_BASE        IN BOOLEAN DEFAULT FALSE );

  FUNCTION CALCOLA_MARGINE(P_ALGORITMO_PRICING_ID IN NUMBER,
                                  P_RC_ORIGINE  IN NUMBER ,
                                  P_ORIGINE IN NUMBER,
                                  P_PREZZO  IN NUMBER,
                                  P_RC_PREZZO  in number,
                                  P_ALIQUOTA_IVA  in number,
                                  P_CALO_PESO in number,
                                  P_APPLICA_CALO_PESO in varchar2,
                                  P_DECIMALI IN NUMBER DEFAULT 2) RETURN NUMBER;

  PROCEDURE COMPLETE_EXEC_ARTICOLO ( P_PRICING_EXEC_ID      IN PRICING_EXEC.PRICING_EXEC_ID%TYPE ,
                                     p_pricing_submodel_id  IN PRICING_SUBMODEL.PRICING_SUBMODEL_ID%TYPE  );

  PROCEDURE COMPLETE_PRICING_EXEC ( P_PRICING_EXEC_ID      IN PRICING_EXEC.PRICING_EXEC_ID%TYPE  );

  PROCEDURE DELETE_MODELLO (P_PRICING_MODEL_ID     IN PRICING_MODEL.PRICING_MODEL_ID%TYPE);

  PROCEDURE COPY_MODELLO (P_PRICING_MODEL_IN_ID     IN PRICING_MODEL.PRICING_MODEL_ID%TYPE,
                          P_PRICING_MODEL_OUT_ID     OUT PRICING_MODEL.PRICING_MODEL_ID%TYPE );

  PROCEDURE DUPLICA_SUBMODEL   ( I_PRICING_SUBMODEL_ID     IN PRICING_SUBMODEL.PRICING_SUBMODEL_ID%TYPE ,
                                 O_PRICING_SUBMODEL_ID     OUT PRICING_SUBMODEL.PRICING_SUBMODEL_ID%TYPE );

  PROCEDURE FILL_TMP_PRICING_ID ( P_FILTRO_XML IN PRICING_SUBMODEL_X_FILTRO.FILTRO_XML%TYPE,
                                  P_PRICING_SUBMODEL_ID IN PRICING_SUBMODEL.PRICING_SUBMODEL_ID%TYPE)  ;

  PROCEDURE DELETE_ELABORAZIONE (P_LISTA_ELABORAZIONI     IN VARCHAR2);

  PROCEDURE COPY_ASSOCIA_LISTINI ( P_ASSOCIA_LISTINI_IN_ID     IN ASSOCIA_LISTINI.ASSOCIA_LISTINI_ID%TYPE,
                                   P_ASSOCIA_LISTINI_OUT_ID   OUT ASSOCIA_LISTINI.ASSOCIA_LISTINI_ID%TYPE );

  PROCEDURE DELETE_PREZZO ( P_LISTA_PREZZI  IN VARCHAR2 ,
                            P_MESSAGE     OUT VARCHAR2);

  function flat_xml_filter (p_filtro_xml in xmltype)   return varchar2;


  FUNCTION GET_PREZZO (  P_LISTINO_CODICE IN VARCHAR2,
                         P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                         P_ARTICOLO_CODICE  IN VARCHAR2 ,
                         P_LISTINO_TIPO  IN VARCHAR2 ,
                         P_DATE IN DATE DEFAULT SYSDATE
                       ) RETURN NUMBER ;

  FUNCTION GET_ULTIMO_PERIODO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER;
                      
  FUNCTION GET_PROSSIMO_PERIODO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER ;
  FUNCTION GET_PROSSIMO_PERIODO_APERTO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER;   
  FUNCTION GET_PROSSIMO_PERIODO_CHIUSO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER;                                                                 
  FUNCTION GET_ULTIMO_PERIODO_CHIUSO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER ;
  FUNCTION GET_PERIODO_CHIUSO (  P_LISTINO_CODICE IN VARCHAR2,
                                 P_LINEA_CODICE IN VARCHAR2 DEFAULT NULL,
                                 P_ARTICOLO_CODICE  IN VARCHAR2 ,
                                 P_LISTINO_TIPO  IN VARCHAR2 ,
                                 P_DATE IN DATE DEFAULT SYSDATE
                      ) RETURN NUMBER;                      
END PRICING;
