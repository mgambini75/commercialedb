--liquibase formatted sql
--changeset fburghila:Invio flusso Supermercato24 logicalFilePath:v-1.0/XXXX-Invio_flusso_supermercato24.sql runInTransaction:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql

-- Connessione a DCC --------------------------------------------------------------------------------------------------------------
CONNECT &DB_DCC

-- 
-- Tutte le tabelle di SUPER24
--
@\DCC\tbl\SUPER24_TABELLE.sql

-- 
-- PROCEDURES(s)
--
@\DCC\prc\SP_EXPORT_CEDI_SUPER24.sql

-- 
-- PACKAGE(s)
--
@\DCC\pkg\UTL_EXPORT_SUPER24.sql
@\DCC\pkg\UTL_EXPORT_SUPER24_BODY.sql


INSERT INTO DB_FIELDDEFVAL (DB_OWNER, TABLE_NAME, FIELD_NAME, FIELD_DEFVAL) VALUES ('BRC_DCC', 'SUPER24', 'DIRECTORY_NAME', 'EXTERNAL_DIR_SUPER24');

-- publisher, correggere eventualmente il LOADID
INSERT INTO DB_MODXLOAXTAB (MODULO_ID, LOADID, TABLE_NAME, MODULE_TYPE, WORK_IN_PROGRESS) VALUES ('MORE2SUPER24','0', 'ANAG', 'P', 'N');

COMMIT;
