--liquibase formatted sql
--changeset fburghila:Carichi da MOVER verso MORE (DCC) logicalFilePath:v-1.0/0008-Carichi_dcc_mover2more.sql runInTransaction:true failOnError:true

-- Impostazioni variabili di script ------------------------------------------------------------------------------------------------
@\defines.sql

-- Connessione a DCC --------------------------------------------------------------------------------------------------------------
CONNECT &DB_DCC

create synonym MVR_DCC_CONFERMA_CARICO_T for brl_dcc.conferma_carico_t@MOVERDB;
create synonym MVR_DCC_CONFERMA_CARICO_D for brl_dcc.conferma_carico_d@MOVERDB;

create synonym comm_d2m_carichi_t for brc_comm.d2m_carichi_t;
create synonym comm_d2m_carichi_r for brc_comm.d2m_carichi_r;

CREATE SEQUENCE SQ_LOAD_ID_CARICHI START WITH 1;
CREATE SEQUENCE SQ_CARICO_LOG_IN_T START WITH 1;
CREATE SEQUENCE SQ_CARICO_LOG_IN_D START WITH 1;


INSERT INTO db_modxloaxtab (modulo_id, loadid, table_name, modulo_type) values ('MOVER2MORE_CARICHI', 0, 'CARICHI', 'S');

-- mi connetto a ditech_common
--CONNECT &DB_DITECH_COMMON???
--INSERT INTO MAIL_CONFIG (MODULO, DESTINATARI, MAIL_ABILITATA, MAIL_MULTIPLE) VALUES ('MOVER2MORE_DCC_CARICHI', 'flavius.burghila@ditechspa.it; diego.cenacchi@ditechspa.it', 'S', 'S')

--CONNECT &DB_COMM

CREATE SEQUENCE SQ_D2M_CARICHI_T START WITH 1;
CREATE SEQUENCE SQ_D2M_CARICHI_R START WITH 1;

/*ALTER TABLE D2M_CARICHI_T ADD (
	D_NUMERO_CARICO VARCHAR2(15),
	D_DATA_INIZIO_RICEVIMENTO DATE,
	D_FORNITORE_CODICE VARCHAR(50),
	D_PDD_CODICE VARCHAR(50),
	D_DATA_PREVISTA_CONSEGNA DATE,
	D_DATA_FINE_RICEVIMENTO DATE,
	D_CAUSALE_ORDINE VARCHAR2(5),
	d_sorgente_flusso varchar2(50),
	d_stato_carico varchar2(50)
);


COMMENT ON COLUMN D2M_CARICHI_T.D_NUMERO_CARICO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_DATA_INIZIO_RICEVIMENTO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_FORNITORE_CODICE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_PDD_CODICE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_DATA_PREVISTA_CONSEGNA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_DATA_FINE_RICEVIMENTO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_T.D_CAUSALE_ORDINE IS 'NEW';

ALTER TABLE D2M_CARICHI_R ADD (
  D_NUMERO_CARICO VARCHAR2(15),
  D_DATA_SCADENZA DATE,
  D_NUMERO_ORDINE NUMBER(19),
  D_NUMERO_RIGA_ORDINE NUMBER(5),
  D_NUMERO_BOLLA VARCHAR2(19),
  D_DATA_BOLLA DATE,
  D_NUMERO_RIGA_BOLLA NUMBER(5),
  D_PDD_CODICE VARCHAR(50),
  D_IMBALLO_CESSIONE NUMBER(10),
  D_IMBALLO_FORNITORE NUMBER(10),
  D_PALLET_PIANI NUMBER(4),
  D_PALLET_IMBALLI_PIANO NUMBER(4),
  D_PALLET_IMBALLI NUMBER(8),
  D_QUANTITA_CAR_PZ NUMBER(18,5),
  D_QUANTITA_CAR_KG NUMBER(18,5),
  D_QUANTITA_CAR_COLLI NUMBER(18,5),
  D_QUANTITA_DA_CAR_PZ NUMBER(18,5),
  D_QUANTITA_DA_CAR_KG NUMBER(18,5),
  D_QUANTITA_DA_CAR_COLLI NUMBER(18,5),
  D_CAUZIONE_CODICE VARCHAR2(50),
  D_CAUZIONE_QTA_RESA NUMBER(18,5),
  D_CODICE_EAN VARCHAR2(128),
  D_CAUSALE_TESTATA VARCHAR2(5),
  D_CAUSALE_RIGA VARCHAR2(5),
  D_FLAG_SOSTANZA_ZUCCHERINA CHAR(1),
  D_QTA_ORDINATA_OMAGGIO_COLLI NUMBER(18,5),
  D_QTA_RICEVUTA_OMAGGIO_COLLI NUMBER(18,5),
  D_QTA_RICEVUTA_OMAGGIO_PZ_KG NUMBER(18,5),
  D_COSTO_NETTO_UNITARIO NUMBER(18,5),
  D_USERNAME_OPERATORE VARCHAR2(50),
  D_STATO_RIGA_CARICO VARCHAR2(5),
  D_FORNITORE_CODICE VARCHAR2(50)
);


COMMENT ON COLUMN D2M_CARICHI_R.D_NUMERO_CARICO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_DATA_SCADENZA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_NUMERO_ORDINE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_NUMERO_RIGA_ORDINE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_NUMERO_BOLLA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_DATA_BOLLA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_NUMERO_RIGA_BOLLA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_PDD_CODICE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_IMBALLO_CESSIONE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_IMBALLO_FORNITORE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_PALLET_PIANI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_PALLET_IMBALLI_PIANO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_PALLET_IMBALLI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_CAR_PZ IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_CAR_KG IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_CAR_COLLI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_DA_CAR_PZ IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_DA_CAR_KG IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QUANTITA_DA_CAR_COLLI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_CAUZIONE_CODICE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_CAUZIONE_QTA_RESA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_CODICE_EAN IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_CAUSALE_TESTATA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_CAUSALE_RIGA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_FLAG_SOSTANZA_ZUCCHERINA IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QTA_ORDINATA_OMAGGIO_COLLI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QTA_RICEVUTA_OMAGGIO_COLLI IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_QTA_RICEVUTA_OMAGGIO_PZ_KG IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_COSTO_NETTO_UNITARIO IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_USERNAME_OPERATORE IS 'NEW';
COMMENT ON COLUMN D2M_CARICHI_R.D_STATO_RIGA_CARICO IS 'NEW';
*/


-- mi connetto nuovamente al DCC
create synonym comm_sq_d2m_carichi_t for brc_comm.sq_d2m_carichi_t;
create synonym comm_sq_d2m_carichi_r for brc_comm.sq_d2m_carichi_r;

@\DCC\tbl\TRANSCODIFICA_CAUSALI.sql

INSERT INTO BRC_DCC.TRANSCODIFICA_CAUSALI (D_TRANSCODIFICA_CAUSALI_ID, D_FLUSSO, D_CAUSALE_EXT_CODICE, D_CAUSALE_MORE_CODICE, D_ANNULLATO, INS_DATA, INS_UTENTE, INS_MODULO, UPD_DATA, UPD_UTENTE, UPD_MODULO, VERSION) VALUES ((select nvl(max(D_TRANSCODIFICA_CAUSALI_ID)+1,1) from TRANSCODIFICA_CAUSALI), 'MOVER_CARICHI', '0006', '60', 'N', SYSDATE, 'DC', 'A MANO',SYSDATE, 'DC', 'A MANO', TO_TIMESTAMP(SYSDATE, 'YYYY-MM-DD HH24:MI:SS.FF'));

INSERT INTO BRC_DCC.TRANSCODIFICA_CAUSALI (D_TRANSCODIFICA_CAUSALI_ID, D_FLUSSO, D_CAUSALE_EXT_CODICE, D_CAUSALE_MORE_CODICE, D_ANNULLATO, INS_DATA, INS_UTENTE, INS_MODULO, UPD_DATA, UPD_UTENTE, UPD_MODULO, VERSION) VALUES ((select nvl(max(D_TRANSCODIFICA_CAUSALI_ID)+1,1) from TRANSCODIFICA_CAUSALI), 'MOVER_CARICHI', '0811', '61', 'N', SYSDATE, 'DC', 'A MANO',SYSDATE, 'DC', 'A MANO', TO_TIMESTAMP(SYSDATE, 'YYYY-MM-DD HH24:MI:SS.FF'));

INSERT INTO BRC_DCC.TRANSCODIFICA_CAUSALI (D_TRANSCODIFICA_CAUSALI_ID, D_FLUSSO, D_CAUSALE_EXT_CODICE, D_CAUSALE_MORE_CODICE, D_ANNULLATO, INS_DATA, INS_UTENTE, INS_MODULO, UPD_DATA, UPD_UTENTE, UPD_MODULO, VERSION) VALUES ((select nvl(max(D_TRANSCODIFICA_CAUSALI_ID)+1,1) from TRANSCODIFICA_CAUSALI), 'MOVER_CARICHI', '0072', '40', 'N', SYSDATE, 'DC', 'A MANO',SYSDATE, 'DC', 'A MANO', TO_TIMESTAMP(SYSDATE, 'YYYY-MM-DD HH24:MI:SS.FF'));

COMMIT;


-- 
-- tabelle CARICHI - T e D
--
@\DCC\tbl\CARICO_LOG_IN_T.sql
@\DCC\tbl\CARICO_LOG_IN_D.sql

-- 
-- PACKAGE(s)
--
@\DCC\pkg\PKG_LOAD_CONFERMA_CARICHI_MVR.sql
@\DCC\pkg\PKG_LOAD_CONFERMA_CARICHI_MVR_BODY.sql
