#!/bin/bash

# copia in tmp
function copy_to_tmp () {
if [ -n "$1" ];
then
    files="$1"
	length=${#files[*]}
	for ((i=0;i<=$(($length - 1)); i++))
	do
		cp ${files[$i]} 'tmp/'
	done
else
    exit 0;
fi    
}

# crea zip in tmp con compressione livello 4
function create_zip () {
if [ -n "$1" ];
then
    files="$1"
	length=${#files[*]}
	for ((i=0;i<=$(($length - 1)); i++))
	do 
		cd tmp; zip -ur -4 $2 ${files[$i]}
	done
	cd ..;
fi
}

# verifica integrit� zip
function check_zip_integrity () {
if [ -a "tmp/$1" ];
then
	result=`unzip -tq tmp/$1`
	if [[ $result == *"No errors"* ]]
	then
		res_integr='0'
	elif [[ $result == *"At least one"* ]]
	then
		res_integr='1'
	else
		res_integr='2' # uso futuro?
	fi
fi
}

# calcola hash MD5
# $1 -> zip
function md5_zip () {
if [ -a "tmp/$1" ];
then
	cd tmp; md5sum $1 >> md5sum.md5; cd ..;
else
	cd ..; exit 1; # hash md5 di questo zip verr� scartato
fi
}

# sposta zip e i txts in elab
# $1 -> lista txts
# $2 -> zip
function move_files() {
if [ -n "$1" ] && [ -a "tmp/$2" ] && [ "$3" != "" ];
then
    files="$1"
	length=${#files[*]}
	for ((i=0;i<=$(($length - 1)); i++))
	do 
		mv ${files[$i]} $3
		rm tmp/${files[$i]}
	done
	mv tmp/$2 $3 # zip
fi
}

# upload zip via https usando curl
# $1 -> zip
function upload_via_https() {
if [ -a "tmp/$1" ];
then
	cookie="cookie_$(date +%s)"
    curl --user maxidi:maxidi06 --cookie-jar var/$cookie https://eudropbox.demandtec.com/ -k
	curl --cookie var/$cookie https://eudropbox.demandtec.com/upload -T tmp/$1 -k 
fi
}

# upload zip via scp
# premessa: autenticazione via chiave
function upload_via_scp() {
if [ -a "tmp/$1" ];
then
	scp -c blowfish -P 160 tmp/$1 maxidi@eudropbox.demandtec.com:/upload; scp_retcode=$?
fi
}

# log
function log() {
if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ];
then
	log_folder="$1"
	log_zipfilename="$2"
	log_return_code="$3"
	
	log_md5=($(md5sum ${1}${log_zipfilename}))

	echo $log_zipfilename  >> upload.log
	echo $log_md5 >> upload.log
	
	printf '%s\n' "${files[@]}" >> upload.log
	echo "SCP EXIT CODE: " $log_return_code >> upload.log
	echo "-------------------------------------------------" >> upload.log
fi
}

# esegue il PKG esportazione tabelle IBM
function run_pkg_ibm_estrai_txt() {
    if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ]; 
	then
		sqlplus -l -s $1/$2@moreprod<<EOF
            WHENEVER SQLERROR EXIT 99;
			WHENEVER OSERROR EXIT 88;
			BEGIN
				PKG_IBM_ESTRAI_TXT.SP_MAIN(P_FREQUENZA => '$3');
			 END;
			/  
EOF
		pkg_ibm_retcode=$?
		echo $DATE_LOG >> upload.log
	    echo "PKG IBM EXIT CODE: " $pkg_ibm_retcode >> upload.log
	fi
}


# sqlplus -> invia mail
function send_mail() {
if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ] && [ "$4" != "" ];
    then
		if [ "$3" == 0 ] && [ "$4" == 0 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'luca.pancaldi@ditechspa.it,diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[OK] FLUSSO ESTRAZIONE IBM PRICING ',
				v_message => 'ZIP ${zip_filename} caricato correttamente' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/upload.log');
			END; 
			/  
EOF
		elif [ "$3" == 1 ] && [ "$4" == 1 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'luca.pancaldi@ditechspa.it,diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[OK] FLUSSO ESTRAZIONE IBM PRICING ',
				v_message => 'Nessun txt trovato' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/upload.log');
			END; 
			/  
EOF
	    echo "Nessun txt trovato" >> upload.log
		echo "-------------------------------------------------" >> upload.log
		elif [ "$3" == 0 ] && [ "$4" != 0 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'luca.pancaldi@ditechspa.it,diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO ', v_message => 'ZIP ${zip_filename} non caricato' || CHR(10) || CHR(13) || 'SCP EXIT CODE:= $4' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/upload.log');
			END;
			/
EOF
		else
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'luca.pancaldi@ditechspa.it,diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[ERR] FLUSSO ESTRAZIONE IBM PRICING BLOCCATO ', v_message => 'Errore generico' || CHR(10) || CHR(13) ||  'PKG IBM EXIT CODE:= $3' || CHR(10) || CHR(13) || 'SCP EXIT CODE:= $4' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/upload.log');
			END;
			/
EOF
		fi
fi
}

# get data corrente
DATE_FILE=`date +%Y%m%d`
DATE_LOG=`date +%d/%m/%y-%H:%M:%S`

export HOME=/home/oracle
export ORACLE_HOME=/oracle/app/db
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch:$PATH
export HOME_ACFS=/acfs
export LOG=$HOME_ACFS/Log

DIR='/acfs/external/PricingIbm/'
FREQUENZA=$1

USER='BRC_DCC'
PASW='BRC_DCC'

cd $DIR;

run_pkg_ibm_estrai_txt $USER $PASW $FREQUENZA

if [ "$pkg_ibm_retcode" == 0 ]; 
then
	# grep file per regex XYZ_MAXIDI_YYYYMMDD.txt
	files=($(ls | grep -E '[A-Z]{3}_MAXIDI_[0-9]{8}.txt$'))
	count=${#files[*]}
	
	if (( "$count" > 0 ));
	then
		# set nome file zip
		zip_filename="ANAGRAFICHE_${DATE_FILE}.zip"

		# stampa lista dei file
		#printf '%s\n' "${files[@]}"
		
		copy_to_tmp $files

		create_zip $files $zip_filename
		
		md5_zip $zip_filename

		upload_via_scp $zip_filename
		#upload_via_https $zip_filename

		send_mail $USER $PASW $pkg_ibm_retcode $scp_retcode 
		
		if [ "$scp_retcode" == 0 ]; 
		then
			move_files $files $zip_filename 'elab/'
			log 'elab/' $zip_filename $scp_retcode 
		else
			move_files $files $zip_filename 'err/'
			log 'err/' $zip_filename $scp_retcode 
		fi
	else
		send_mail $USER $PASW 1 1
	fi
fi
