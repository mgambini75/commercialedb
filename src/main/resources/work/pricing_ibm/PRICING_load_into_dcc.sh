#! bin/bash

# esegue ls sul server remoto, conta quanti listini ci sono (regex)
function get_files() {
	 sftp -b - > ls_dropbox.lst -oPort=160 maxidi@eudropbox.demandtec.com:/download/<<EOF
ls
EOF
	sed -e '1,2d' -i ls_dropbox.lst
	count_listini=($(grep -i -E 'dtp1001_maxidi_[0-9]{8}.txt' ls_dropbox.lst | wc -l))
}

# unisce i file
function merge_files() {
if [ -n "$1" ];
then
    files="$1"
	length=${#files[*]}
	for ((i=0;i<=$(($length - 1)); i++))
	do
		(cat ${files[$i]}; echo) >> dtp1001_maxidi.txt
	done
else
    exit 1;
fi
}

# download via scp
function download_via_scp() {
	 scp -P 160 maxidi@eudropbox.demandtec.com:/download/dtp1001_*.txt $DIR; scp_retcode=$?
}

# rimuove solo i file scaricati, nel dropbox
function remove_from_dropbox(){
if [ "$1" != "" ] 
then
	for ((i=0;i<=$(($length - 1)); i++))
	do
		echo -e "rm ${files[$i]}" >> sftp_batch_pricing; 
	done
	sftp -oPort=160 -b sftp_batch_pricing maxidi@eudropbox.demandtec.com:/download/; 
	rm sftp_batch_pricing
else
	exit 1;
fi
}

function move_files() {
if [ -n "$1" ] && [ "$2" != "" ];
then
    files="$1"
	length=${#files[*]}
	for ((i=0;i<=$(($length - 1)); i++))
	do 
		mv ${files[$i]} $2/${files[$i]}-$DATE_LOG
	done
fi
}

# log
function log() {
if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ];
then
	log_folder="$1"
	log_return_code="$3"

	files="$2"
	
	printf '%s\n' "${files[@]}" >> load_into.log
	echo "SCP EXIT CODE: " $log_return_code >> load_into.log
	echo "-------------------------------------------------" >> load_into.log
fi
}

# sqlplus -> invia mail
function send_mail() {
if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ] && [ "$4" != "" ];
    then
		if [ "$3" == 0 ] && [ "$4" == 0 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[OK] FLUSSO CARICAMENTO [FILE] LISTINI IBM ',
				v_message => '${files} - flusso caricato nel DCC correttamente' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/load_into.log');
			END; 
			/  
EOF
		elif [ "$3" == 1 ] && [ "$4" == 1 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject =>'[OK] FLUSSO CARICAMENTO [FILE] LISTINI IBM ',
				v_message => 'Nessun txt trovato' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/load_into.log');
			END; 
			/  
EOF
	    echo "Nessun txt trovato" >> load_into.log
		echo "-------------------------------------------------" >> load_into.log
		elif [ "$3" != 0 ] && [ "$4" == 0 ];
		then
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[ERR] FLUSSO CARICAMENTO [FILE] LISTINI IBM BLOCCATO', 
				v_message => '${files} - flusso non caricato nel DCC correttamente' || CHR(10) || CHR(13) || 'SCP EXIT CODE:= $4' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/load_into.log');
			END;
			/
EOF
		else
			sqlplus -l -s $1/$2@moreprod <<EOF
			BEGIN
				DITECH_MONITOR.SEND_MAIL(v_sender => 'MAXIDI_MORE <finprod@maxidi.it>', v_receiver => 'diego.cenacchi@ditechspa.it,flavius.burghila@ditechspa.it' , v_subject => '[ERR] FLUSSO CARICAMENTO [FILE] LISTINI IBM BLOCCATO',
				v_message => 'Errore generico' || CHR(10) || CHR(13) ||  'PKG IBM EXIT CODE:= $3' || CHR(10) || CHR(13) || 'SCP EXIT CODE:= $4' || CHR(10) || CHR(13) || 'LOG in /acfs/external/PricingIbm/load_into.log');
			END;
			/
EOF
	    echo "Errore generico - "  $DATE_LOG >> load_into.log
		echo "-------------------------------------------------" >> load_into.log
		fi
fi
}

# esegue il PKG per l'importazione nel DCC
function run_pkg_ibm_ext2dcc() {
    if [ "$1" != "" ] && [ "$2" != "" ]; 
	then
		sqlplus -l -s $1/$2@moreprod<<EOF
            WHENEVER SQLERROR EXIT 99;
			WHENEVER OSERROR EXIT 88;
			BEGIN
				PKG_IBM_EXT2DCC.SP_MAIN();
			 END;
			/  
EOF
		pkg_ibm_retcode=$?
		echo $DATE_LOG >> load_into.log
	    echo "PKG IBM EXIT CODE: " $pkg_ibm_retcode >> load_into.log
	fi
}

# get data corrente
DATE_LOG=`date +%d-%m-%y-%H:%M:%S`

export HOME=/home/oracle
export ORACLE_HOME=/oracle/app/db
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch:$PATH
export HOME_ACFS=/acfs
export LOG=$HOME_ACFS/Log

DIR='/acfs/external/PricingIbm/'
FILE_EXT='dtp1001_maxidi.txt'

USER='BRC_DCC'
PASW='BRC_DCC'

cd $DIR;

get_files

if (( "$count_listini" > 0 ));
then

	download_via_scp

	if [ "$scp_retcode" == 0 ]; 
	then
		files=($(ls | grep -i -E 'dtp1001_maxidi_[0-9]{8}.txt'))
		count=${#files[*]}
		if (( "$count" > 0 ));
		then
				merge_files $files
				run_pkg_ibm_ext2dcc $USER $PASW
				
				send_mail $USER $PASW $pkg_ibm_retcode $scp_retcode
				
				if [ "$pkg_ibm_retcode" == 0 ]; 
				then
						move_files $files 'elab/listini/'
						mv $FILE_EXT elab/listini/$FILE_EXT-$DATE_LOG
						log 'elab/listini/' $files $scp_retcode
						remove_from_dropbox $files
				else
						move_files $files 'err/listini/'
						mv $FILE_EXT err/listini/$FILE_EXT-$DATE_LOG
						log 'err/listini/' $files $scp_retcode
				fi
		else
			 send_mail $USER $PASW 1 1
		fi
	else 
		send_mail $USER $PASW 101 101
	fi
else
	    echo "Nessun txt scaricato - DATA:" $DATE_LOG >> load_into.log
		echo "-------------------------------------------------" >> load_into.log
fi

